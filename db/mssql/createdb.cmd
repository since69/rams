@ECHO OFF
ECHO 0: Commandline	: %0
ECHO 1: Instance	: %1
ECHO 2: Source Path	: %2
ECHO 3: Data Path	: %3
ECHO.
ECHO Creating RAMS database.

ECHO TODO Use the environment variables, if no commandline arguments specified.

:: SQL Server 2016 instance name.
:: Remove quotes
SET _instancename=%1
SET _instancename=###%_instancename%###
SET _instancename=%_instancename:"###=%
SET _instancename=%_instancename:###"=%
SET _instancename=%_instancename:###=%
IF "%_instancename%"=="" GOTO Usage

:: Rams common source path, slash-terminated.
:: Remove quotes
SET _sourcepath=%2
SET _sourcepath=###%_sourcepath%###
SET _sourcepath=%_sourcepath:"###=%
SET _sourcepath=%_sourcepath:###"=%
SET _sourcepath=%_sourcepath:###=%
IF "%_sourcepath%"=="" GOTO Usage

:: SQL Server 2016 common root path, slash-terminated.
:: Remove quotes
SET _datapath=%3
SET _datapath=###%_datapath%###
SET _datapath=%_datapath:"###=%
SET _datapath=%_datapath:###"=%
SET _datapath=%_datapath:###=%
IF "%_datapath%"=="" GOTO Usage

:: RAMS ::
sqlcmd -E -S %_instancename% -d master -i "%_sourcepath%instramsdb.sql" -b -v RamsSourceDataPath = "%_sourcepath%" -v RamsDatabasePath = "%_datapath%"

GOTO Exit

:Usage
ECHO.
ECHO.
ECHO    USAGE
ECHO.
ECHO    Three (3) parameters are required:
ECHO.
ECHO    1. SQL Server 2014 instance name must be specified.
ECHO    2. Rams root source data path must be specified.
ECHO    3. SQL Server 2014 common data path for that instance.
ECHO.
ECHO    For example:
ECHO.
ECHO       createdb.cmd ".\SQLSERVER2014" "%PROGRAMFILES%\Microsoft SQL Server\120\" "%PROGRAMFILES%\Microsoft SQL Server\MSSQL12.MSSQLSERVER\"
ECHO.

:Exit
ECHO.
ECHO Script execution complete.
:EXIT 0
