@ECHO OFF
ECHO 0: Commandline : %0
ECHO 1: Instance    : %1
ECHO.
ECHO Dropping RAMS databases.

ECHO TODO Use the environment variables, if no commandline arguments specified.

:: SQL Server 2014 instance name.
:: Remove quotes
SET _instancename=%1
SET _instancename=###%_instancename%###
SET _instancename=%_instancename:"###=%
SET _instancename=%_instancename:###"=%
SET _instancename=%_instancename:###=%
IF "%_instancename%"=="" GOTO Usage

ECHO DROP DATABASE RAMS
sqlcmd -S %_instancename% -E -Q "IF EXISTS(SELECT * FROM sys.databases WHERE [name] = 'rams') BEGIN EXECUTE (N'ALTER DATABASE [rams] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;'); EXECUTE (N'DROP DATABASE [rams];'); END"

GOTO Exit

:Usage
ECHO.
ECHO.
ECHO    USAGE
ECHO.
ECHO    One (1) parameters are required:
ECHO.
ECHO    1. SQL Server 2014 instance name must be specified.
ECHO.
ECHO    For example:
ECHO.
ECHO       dropdb.cmd ".\SQLSERVER2014"
ECHO.

:Exit
ECHO.
ECHO Script execution complete.
:EXIT 0
