/* =============================================================================
   File Name	: instramsdb.sql

   Summary		: Creates the RAMS case-insensitive OLTP database.

   Created At	: 2017.03.14 07:36
   Created By	: Joohyoung Kim

   Updated At 	: 2017.05.01 13:36
   Updated By 	: Joohyong Kim

   SQL Version : 13.0.4202.0
--------------------------------------------------------------------------------
   This file is one of Thinkbox productions.

   Copyright (C) Thinkbox Corporation. All rights reserved.

   THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
   KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
   PARTICULAR PURPOSE.
============================================================================= */

-- Be sure to enable FULL TEXT SEARCH before running this script!

-->> NOTE: THIS SCRIPT MUST BE RUN IN SQLCMD MODE INSIDE SQL SERVER MANAGEMENT STUDIO. <<--
:on error exit


/*
 * In order to run this script manually, either set the environment variables,
 * or uncomment the setvar statements and provide the necessary values if
 * the defaults are not correct for your installation.
 */

--:setvar RamsDatabasePath "D:\DevTools\Microsoft\Sql\2K14\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
--NOTE: Change this path if you copied the script source to another path
--:setvar RamsSourceDataPath ".\"

IF '$(RamsSourceDataPath)' IS NULL OR '$(RamsSourceDataPath)' = ''
BEGIN
	RAISERROR(N'The variable RamsSourceDataPath must be defined.', 16, 127) WITH NOWAIT;
	RETURN;
END;

IF '$(RamsDatabasePath)' IS NULL OR '$(RamsDatabasePath)' = ''
BEGIN
	RAISERROR(N'The variable RamsDatabasePath must be defined.', 16, 127) WITH NOWAIT;
	RETURN;
END;

SET NOCOUNT OFF;
GO

PRINT CONVERT(varchar(1000), @@VERSION);
GO

PRINT '';
PRINT 'Started - ' + CONVERT(varchar, GetDate(), 121);
GO

USE [master];
GO


/* ---------------------------- Drop Database  ------------------------------ */

PRINT '';
PRINT '*** Dropping Database';
GO

IF EXISTS (SELECT [name] FROM [master].[sys].[databases] WHERE [name] = N'Rams')
	DROP DATABASE [Rams];

-- If the database has any other open connections close the network connection.
IF @@ERROR = 3702
	RAISERROR('[Rams] database cannot be dropped because there are still other open connections',
		127, 127)
GO


/* --------------------------- Create Database ------------------------------ */

PRINT '';
PRINT '*** Creating Database';
GO

CREATE DATABASE [Rams]
	ON (NAME = 'Rams_Data', FILENAME = N'$(RamsDatabasePath)Rams_Data.mdf', SIZE = 170, FILEGROWTH = 8)
	LOG ON (NAME = 'Rams_Log', FILENAME = N'$(RamsDatabasePath)Rams_Log.ldf', SIZE = 2, FILEGROWTH = 96);
GO

ALTER DATABASE [Rams]
SET
	RECOVERY SIMPLE
	, ANSI_NULLS ON
	, ANSI_PADDING ON
	, ANSI_WARNINGS ON
	, ARITHABORT ON
	, CONCAT_NULL_YIELDS_NULL ON
	, QUOTED_IDENTIFIER ON
	, NUMERIC_ROUNDABORT OFF
	, PAGE_VERIFY CHECKSUM
	, ALLOW_SNAPSHOT_ISOLATION OFF;
GO

ALTER DATABASE [Rams]
    -- NOTE: Galacian(English - U.S.A.)
	--COLLATE Latin1_General_CS_AS;
    -- NOTE: Korean(Extended Wansung)
    COLLATE Korean_Wansung_CI_AS;
GO

USE [Rams];
GO


/* ------------------- Create DDL Trigger for Database ---------------------- */

PRINT '';
PRINT '*** Creating DDL Trigger for Database';
GO

SET QUOTED_IDENTIFIER ON;
GO

-- Create table to store database object creating message
-- *** WARNING: THIS TABLE IS INTENTIONALLY A HEAP - DO NOT ADD A PRIMARY KEY ***
CREATE TABLE [dbo].[DatabaseLog]
(
	[DatabaseLogID] [int] IDENTITY (1, 1) NOT NULL
	, [PostTime] [datetime] NOT NULL
	, [DatabaseUser] [sysname] NOT NULL
	, [Event] [sysname] NOT NULL
	, [Schema] [sysname] NULL
	, [Object] [sysname] NULL
	, [TSQL] [nvarchar](max) NOT NULL
	, [XmlEvent] [xml] NOT NULL
) ON [PRIMARY];
GO


CREATE TRIGGER [ddlDatabaseTriggerLog] ON DATABASE
FOR DDL_DATABASE_LEVEL_EVENTS
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @data XML;
    DECLARE @schema sysname;
    DECLARE @object sysname;
    DECLARE @eventType sysname;

    SET @data = EVENTDATA();
    SET @eventType = @data.value('(/EVENT_INSTANCE/EventType)[1]', 'sysname');
    SET @schema = @data.value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname');
    SET @object = @data.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname');

    IF @object IS NOT NULL
        PRINT '  ' + @eventType + ' - ' + @schema + '.' + @object;
    ELSE
        PRINT '  ' + @eventType + ' - ' + @schema;

    IF @eventType IS NULL
        PRINT CONVERT(nvarchar(max), @data);

    INSERT [dbo].[DatabaseLog]
    (
        [PostTime]
        , [DatabaseUser]
        , [Event]
        , [Schema]
        , [Object]
        , [TSQL]
        , [XmlEvent]
    )
    VALUES
    (
        GETDATE(),
        CONVERT(sysname, CURRENT_USER),
        @eventType,
        CONVERT(sysname, @schema),
        CONVERT(sysname, @object),
        @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)'),
        @data
    );
END;
GO


/* ---------------------- Create Error Log objects ------------------------- */

PRINT '';
PRINT '*** Creating Error Log objects';
GO

-- Create table to store error information
CREATE TABLE [dbo].[ErrorLog]
(
    [ErrorLogID] [int] IDENTITY (1, 1) NOT NULL
    , [ErrorTime] [datetime] NOT NULL CONSTRAINT [DF_ErrorLog_ErrorTime] DEFAULT (GETDATE())
    , [UserName] [sysname] NOT NULL
    , [ErrorNumber] [int] NOT NULL
    , [ErrorSeverity] [int] NULL
    , [ErrorState] [int] NULL
    , [ErrorProcedure] [nvarchar](126) NULL
    , [ErrorLine] [int] NULL
    , [ErrorMessage] [nvarchar](4000) NOT NULL
) ON [PRIMARY];
GO

ALTER TABLE [dbo].[ErrorLog] WITH CHECK ADD
    CONSTRAINT [PK_ErrorLog_ErrorLogID] PRIMARY KEY CLUSTERED
    (
        [ErrorLogID]
    )  ON [PRIMARY];
GO

-- uspPrintError prints error information about the error that caused
-- execution to jump to the CATCH block of a TRY...CATCH construct.
-- Should be executed from within the scope of a CATCH block otherwise
-- it will return without printing any error information.
CREATE PROCEDURE [dbo].[uspPrintError]
AS
BEGIN
    SET NOCOUNT ON;

    -- Print error information.
    PRINT 'Error ' + CONVERT(varchar(50), ERROR_NUMBER()) +
          ', Severity ' + CONVERT(varchar(5), ERROR_SEVERITY()) +
          ', State ' + CONVERT(varchar(5), ERROR_STATE()) +
          ', Procedure ' + ISNULL(ERROR_PROCEDURE(), '-') +
          ', Line ' + CONVERT(varchar(5), ERROR_LINE());
    PRINT ERROR_MESSAGE();
END;
GO

-- uspLogError logs error information in the ErrorLog table about the
-- error that caused execution to jump to the CATCH block of a
-- TRY...CATCH construct. This should be executed from within the scope
-- of a CATCH block otherwise it will return without inserting error
-- information.
CREATE PROCEDURE [dbo].[uspLogError]
    @ErrorLogID [int] = 0 OUTPUT -- contains the ErrorLogID of the row inserted
AS                               -- by uspLogError in the ErrorLog table
BEGIN
    SET NOCOUNT ON;

    -- Output parameter value of 0 indicates that error
    -- information was not logged
    SET @ErrorLogID = 0;

    BEGIN TRY
        -- Return if there is no error information to log
        IF ERROR_NUMBER() IS NULL
            RETURN;

        -- Return if inside an uncommittable transaction.
        -- Data insertion/modification is not allowed when
        -- a transaction is in an uncommittable state.
        IF XACT_STATE() = -1
        BEGIN
            PRINT 'Cannot log error since the current transaction is in an uncommittable state. '
                + 'Rollback the transaction before executing uspLogError in order to successfully log error information.';
            RETURN;
        END;

        INSERT [dbo].[ErrorLog]
        (
            [UserName]
            , [ErrorNumber]
            , [ErrorSeverity]
            , [ErrorState]
            , [ErrorProcedure]
            , [ErrorLine]
            , [ErrorMessage]
        )
        VALUES
        (
            CONVERT(sysname, CURRENT_USER)
            , ERROR_NUMBER()
            , ERROR_SEVERITY()
            , ERROR_STATE()
            , ERROR_PROCEDURE()
            , ERROR_LINE()
            , ERROR_MESSAGE()
        );

        -- Pass back the ErrorLogID of the row inserted
        SET @ErrorLogID = @@IDENTITY;
    END TRY
    BEGIN CATCH
        PRINT 'An error occurred in stored procedure uspLogError: ';
        EXECUTE [dbo].[uspPrintError];
        RETURN -1;
    END CATCH;
END;
GO


/* -------------------------- Create Data Types ---------------------------- */

PRINT '';
PRINT '**** Creating Data Types';
GO

--CREATE TYPE [Description] FROM nvarchar(256) NULL;
CREATE TYPE [BirthDate] FROM smalldatetime NULL;
CREATE TYPE [EmailAddress] FROM nvarchar(50) NOT NULL;
CREATE TYPE [Flag] FROM bit NOT NULL;
CREATE TYPE [Gender] FROM nchar(1) NOT NULL;
CREATE TYPE [NameStyle] FROM bit NOT NULL;
CREATE TYPE [Name] FROM nvarchar(50) NULL;
CREATE TYPE [PhoneNumber] FROM nvarchar(25) NULL;
GO


/* ------------------- Add pre-table database functions -------------------- */

PRINT '';
PRINT '**** Creating Pre-Table Database Functions';
GO

CREATE FUNCTION [dbo].[ufnLeadingZeros]
(
    @Value int
)
RETURNS varchar(8)
WITH SCHEMABINDING
AS
BEGIN
    DECLARE @ReturnValue varchar(8);

    SET @ReturnValue = CONVERT(varchar(8), @Value);
    SET @ReturnValue = REPLICATE('0', 8 - DATALENGTH(@ReturnValue)) + @ReturnValue;

    RETURN (@ReturnValue);
END;
GO


/* ----------------------- Create database schemas ------------------------- */

PRINT '';
PRINT '**** Creating Database Schemas';
GO

CREATE SCHEMA [Person] AUTHORIZATION [dbo];
GO

CREATE SCHEMA [Security] AUTHORIZATION [dbo];
GO

CREATE SCHEMA [Training] AUTHORIZATION [dbo];
GO


/* ------------------------- Create XML schemas ---------------------------- */

PRINT '';
PRINT '*** Creating XML Schemas';
GO


/* --------------------------- Create tables ------------------------------- */

PRINT '';
PRINT '*** Creating Tables';
GO

CREATE TABLE [dbo].[RamsBuildVersion]
(
    [SystemInformationID] [tinyint] IDENTITY (1, 1) NOT NULL
    , [DatabaseVersion] [nvarchar](25) NOT NULL
    , [VersionDate] [datetime] NOT NULL
    , [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_RamsBuildVersion_ModifiedDate] DEFAULT (GETDATE())
) ON [PRIMARY];
GO

CREATE TABLE [Person].[AddressType]
(
    [AddressTypeID] [int] IDENTITY (1, 1) NOT NULL
    , [Name] [Name] NOT NULL
    , [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_AddressType_ModifiedDate] DEFAULT (GETDATE())
) ON [PRIMARY];
GO

CREATE TABLE [Person].[BusinessEntity]
(
    [BusinessEntityID] [int] IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL
    , [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_BusinessEntity_ModifiedDate] DEFAULT (GETDATE())
) ON [PRIMARY];
GO

CREATE TABLE [Person].[EmailAddress]
(
	[EmailAddressID] [int] IDENTITY(1, 1) NOT NULL
	, [BusinessEntityID] [int] NOT NULL
	, [EmailAddress] [EmailAddress] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_EmailAddress_ModifiedDate] DEFAULT(GETDATE())
) ON [PRIMARY];
GO

CREATE TABLE [Person].[Person]
(
	[BusinessEntityID] [int] NOT NULL
	, [PersonType] [nchar](2) NOT NULL
	, [NameStyle] [NameStyle] NOT NULL CONSTRAINT [DF_Person_NameStyle] DEFAULT(0)
	, [Title] [nvarchar](8) NULL
	, [FirstName] [Name] NOT NULL
	, [MiddleName] [Name] NULL
	, [LastName] [Name] NOT NULL
	, [Suffix] [nvarchar](10) NULL
	, [EmailPromotion] [int] NOT NULL CONSTRAINT [DF_Person_EmailPromotion] DEFAULT(0)
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Person_ModifiedDate] DEFAULT(GETDATE())
	, CONSTRAINT [CK_Person_EmailPromotion] CHECK((EmailPromotion) BETWEEN 0 AND 2)
	, CONSTRAINT [CK_Person_PersonType] CHECK([PersonType] IS NULL OR UPPER([PersonType]) IN ('TN', 'TE'))
) ON [PRIMARY];
GO

CREATE TABLE [Person].[PersonPhone]
(
	[BusinessEntityID] [int] NOT NULL
	, [PhoneNumberTypeID] [int] NOT NULL
	, [PhoneNumber] [PhoneNumber] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_PersonPhone_ModifiedDate] DEFAULT(GETDATE())
) ON [PRIMARY];
GO

CREATE TABLE [Person].[PhoneNumberType]
(
	[PhoneNumberTypeID] [int] IDENTITY(1, 1) NOT NULL
	, [Name] [Name] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_PhoneNumberType_ModifiedDate] DEFAULT(GETDATE())
) ON [PRIMARY];
GO

CREATE TABLE [Training].[Trainee]
(
	[BusinessEntityID] [int] NOT NULL
	, [Gender] [Gender] NOT NULL
	, [BirthDate] [BirthDate] NOT NULL
    , [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Trainee_ModifiedDate] DEFAULT(GETDATE())
	, CONSTRAINT [CK_Trainee_BirthDate] CHECK ([BirthDate] >= '1960-01-01')
	, CONSTRAINT [CK_Trainee_Gender] CHECK (UPPER([Gender]) IN ('M', 'F')) -- Male or Female
) ON [PRIMARY];
GO

CREATE TABLE [Training].[Trainer]
(
	[BusinessEntityID] [int] NOT NULL
	, [Gender] [Gender] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Trainer_ModifiedDate] DEFAULT(GETDATE())
	, CONSTRAINT [CK_Trainer_Gender] CHECK (UPPER([Gender]) IN ('M', 'F')), -- Male or Female
) ON [PRIMARY];
GO

CREATE TABLE [Training].[Culture]
(
	[CultureID] [nchar](6) NOT NULL
	, [Name] [Name] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Culture_ModifiedDate] DEFAULT (GETDATE()) 
)
GO

CREATE TABLE [Training].[TestText]
(
	[TestTextID] [int] IDENTITY(1, 1) NOT NULL
	, [CultureID] [nchar](6) NOT NULL
	, [Level] [tinyint] NOT NULL
	, [Title] [Name] NOT NULL
	, [Text] [nvarchar](max) NOT NULL
	, [FontName] [Name] NOT NULL
	, [FontSize] [smallint] NOT NULL
    , [CharacterWidth] [decimal](3,2) NOT NULL
    , [CharacterHeight] [decimal](3,2) NOT NULL
    , [LineSpacing] [decimal](3,2) NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_TestText_ModifiedDate] DEFAULT (GETDATE()) 
	, CONSTRAINT [CK_TestText_Level] CHECK (([Level] > 0) AND ([Level] < 11))
)
GO

CREATE TABLE [Training].[TestQuestion]
(
	[TestQuestionID] [int] IDENTITY(1, 1) NOT NULL
	, [TestTextID] [int] NOT NULL
	, [Question] [nvarchar](100) NOT NULL
	, [Answer] [bit] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_TestQuestion_ModifiedDate] DEFAULT(GETDATE())
)
GO

CREATE TABLE [Training].[Test]
(
	[TestID] [int] IDENTITY(1, 1) NOT NULL
	, [Grade] [tinyint] NOT NULL
	, [TraineeID] [int] NOT NULL
	, [TrainerID] [int] NOT NULL
	, [TestTextID] [int] NOT NULL
	, [RecordFile] [nvarchar](256) NULL
	, [StartTime] [datetime] NOT NULL
	, [EndTime] [datetime] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Test_ModifiedDate] DEFAULT(GETDATE())
	, CONSTRAINT [CK_Test_Grade] CHECK (([Grade] > 0) AND ([Grade] < 11))
)
GO

CREATE TABLE [Training].[TestAnswer]
(
	[TestAnswerID] [int] IDENTITY(1, 1) NOT NULL
	, [TestID] [int] NOT NULL
	, [TestQuestionID] [int] NOT NULL
	, [Answer] [bit] NOT NULL
	, [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_TestAnswer_ModifiedDate] DEFAULT(GETDATE())
	, CONSTRAINT [CK_TestAnswer_Answer] CHECK (([Answer] = 0) OR ([Answer] = 1))
)
GO


/* ------------------------------ Load data -------------------------------- */

PRINT '';
PRINT '*** Loading Data';
GO

PRINT 'Loading [dbo].[RamsBuildVersion]';

BULK INSERT [dbo].[RamsBuildVersion] FROM '$(RamsSourceDataPath)data\RamsBuildVersion.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE = 'char'
	, FIELDTERMINATOR= '\t'
	, ROWTERMINATOR = '\n'
	, KEEPIDENTITY
	, TABLOCK
);
/*
PRINT 'Loading [Person].[Address]';

BULK INSERT [Person].[Address] FROM '$(RamsSourceDataPath)Address.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE = 'widechar'
	, FIELDTERMINATOR= '\t'
	, ROWTERMINATOR = '\n'
	, KEEPIDENTITY
	, TABLOCK
);
*/
PRINT 'Loading [Person].[AddressType]';

BULK INSERT [Person].[AddressType] FROM '$(RamsSourceDataPath)data\AddressType.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE = 'char'
	, FIELDTERMINATOR= '\t'
	, ROWTERMINATOR = '\n'
	, KEEPIDENTITY
	, TABLOCK
);

PRINT 'Loading [Person].[BusinessEntity]';

BULK INSERT [Person].[BusinessEntity] FROM '$(RamsSourceDataPath)data\BusinessEntity.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE='widechar'
	, FIELDTERMINATOR='+|'
	, ROWTERMINATOR='&|\n'
	, KEEPIDENTITY
	, TABLOCK
);

PRINT 'Loading [Person].[Person]';

BULK INSERT [Person].[Person] FROM '$(RamsSourceDataPath)data\Person.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE='widechar'
	, FIELDTERMINATOR='+|'
	, ROWTERMINATOR='&|\n'
	, KEEPIDENTITY
	, TABLOCK
);

PRINT 'Loading [Training].[Trainee]';

BULK INSERT [Training].[Trainee] FROM '$(RamsSourceDataPath)data\Trainee.csv'
WITH (
    CHECK_CONSTRAINTS
    , CODEPAGE='ACP'
    , DATAFILETYPE='widechar'
    , FIELDTERMINATOR='\t'
    , ROWTERMINATOR='\n'
    , KEEPIDENTITY
    , TABLOCK
);


PRINT 'Loading [Training].[Trainer]';

BULK INSERT [Training].[Trainer] FROM '$(RamsSourceDataPath)data\Trainer.csv'
WITH (
    CHECK_CONSTRAINTS
    , CODEPAGE='ACP'
    , DATAFILETYPE='widechar'
    , FIELDTERMINATOR='\t'
    , ROWTERMINATOR='\n'
    , KEEPIDENTITY
    , TABLOCK
);

PRINT 'Loading [Training].[Culture]';

BULK INSERT [Training].[Culture] FROM '$(RamsSourceDataPath)data\Culture.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE='char'
	, FIELDTERMINATOR='\t'
	, ROWTERMINATOR='\n'
	, KEEPIDENTITY
	, TABLOCK
);

PRINT 'Loading [Training].[TestText]';

BULK INSERT [Training].[TestText] FROM '$(RamsSourceDataPath)data\TestText.csv'
WITH (
    CHECK_CONSTRAINTS
    , CODEPAGE='ACP'
    , DATAFILETYPE='widechar'
    , FIELDTERMINATOR='+|'
    , ROWTERMINATOR='&|\n'
    , KEEPIDENTITY
    , TABLOCK   
);

PRINT 'Loading [Training].[TestQuestion]';

BULK INSERT [Training].[TestQuestion] FROM '$(RamsSourceDataPath)data\TestQuestion.csv'
WITH (
    CHECK_CONSTRAINTS
    , CODEPAGE='ACP'
    , DATAFILETYPE='widechar'
    , FIELDTERMINATOR='\t'
    , ROWTERMINATOR='\n'
    , KEEPIDENTITY
    , TABLOCK   
);

/*
PRINT 'Loading [Training].[Test]';

BULK INSERT [Training].[Test] FROM '$(RamsSourceDataPath)data\Test.csv'
WITH (
    CHECK_CONSTRAINTS
    , CODEPAGE='ACP'
    , DATAFILETYPE='widechar'
    , FIELDTERMINATOR='+|'
    , ROWTERMINATOR='&|\n'
    , KEEPIDENTITY
    , TABLOCK
);

PRINT 'Loading [Training].[TestAnswer]';

BULK INSERT [Training].[TestAnswer] FROM '$(RamsSourceDataPath)data\TestAnswer.csv'
WITH (
    CHECK_CONSTRAINTS
	, CODEPAGE='ACP'
	, DATAFILETYPE='char'
	, FIELDTERMINATOR='\t'
	, ROWTERMINATOR='\n'
	, KEEPIDENTITY
	, TABLOCK
);
*/


/* -------------------------- Add Primary Keys ----------------------------- */

PRINT '';
PRINT '*** Adding Primary Keys';
GO

SET QUOTED_IDENTIFIER ON;

ALTER TABLE [dbo].[RamsBuildVersion] WITH CHECK ADD
    CONSTRAINT [PK_RamsBuildVersion_SystemInformationID] PRIMARY KEY CLUSTERED
    (
        [SystemInformationID]
    )  ON [PRIMARY];
GO

ALTER TABLE [dbo].[DatabaseLog] WITH CHECK ADD
    CONSTRAINT [PK_DatabaseLog_DatabaseLogID] PRIMARY KEY NONCLUSTERED
    (
        [DatabaseLogID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Person].[AddressType] WITH CHECK ADD 
    CONSTRAINT [PK_AddressType_AddressTypeID] PRIMARY KEY CLUSTERED 
    (
        [AddressTypeID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Person].[BusinessEntity] WITH CHECK ADD
    CONSTRAINT [PK_BusinessEntity_BusinessEntityID] PRIMARY KEY CLUSTERED
    (
        [BusinessEntityID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Person].[EmailAddress] WITH CHECK ADD
    CONSTRAINT [PK_EmailAddress_BusinessEntityID_EmailAddressID] PRIMARY KEY CLUSTERED
    (
        [BusinessEntityID]
		, [EmailAddressID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Person].[Person] WITH CHECK ADD
    CONSTRAINT [PK_Person_BusinessEntityID] PRIMARY KEY CLUSTERED
    (
        [BusinessEntityID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Person].[PersonPhone] WITH CHECK ADD
    CONSTRAINT [PK_PersonPhone_BusinessEntityID_PhoneNumber_PhoneNumberTypeID] PRIMARY KEY CLUSTERED
    (
        [BusinessEntityID]
		, [PhoneNumber]
		, [PhoneNumberTypeID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Person].[PhoneNumberType] WITH CHECK ADD
    CONSTRAINT [PK_PhoneNumberType_PhoneNumberTypeID] PRIMARY KEY CLUSTERED
    (
        [PhoneNumberTypeID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[Trainee] WITH CHECK ADD
    CONSTRAINT [PK_Trainee_BusinessEntityID] PRIMARY KEY CLUSTERED
    (
        [BusinessEntityID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[Trainer] WITH CHECK ADD
    CONSTRAINT [PK_Trainer_BusinessEntityID] PRIMARY KEY CLUSTERED
    (
        [BusinessEntityID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[Culture] WITH CHECK ADD 
    CONSTRAINT [PK_Culture_CultureID] PRIMARY KEY CLUSTERED 
    (
        [CultureID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[TestText] WITH CHECK ADD
    CONSTRAINT [PK_TestText_TestTextID] PRIMARY KEY CLUSTERED
    (
        [TestTextID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[TestQuestion] WITH CHECK ADD
    CONSTRAINT [PK_TestQuestion_TestQuestionID] PRIMARY KEY CLUSTERED
    (
        [TestQuestionID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[Test] WITH CHECK ADD
    CONSTRAINT [PK_Test_TestID] PRIMARY KEY CLUSTERED
    (
        [TestID]
    )  ON [PRIMARY];
GO

ALTER TABLE [Training].[TestAnswer] WITH CHECK ADD
    CONSTRAINT [PK_TestAnswer_TestAnswerID] PRIMARY KEY CLUSTERED
    (
    	[TestAnswerID]
    )  ON [PRIMARY];
GO


/* ---------------------------- Add Indexes -------------------------------- */

PRINT '';
PRINT '*** Adding Indexes';
GO

CREATE UNIQUE INDEX [AK_AddressType_Name] ON [Person].[AddressType]([Name]) ON [PRIMARY];
GO

CREATE INDEX [IX_EmailAddress_EmailAddress] ON [Person].[EmailAddress] ([EmailAddress]) ON [PRIMARY];
GO

CREATE INDEX [IX_Person_LastName_FirstName_MiddleName] ON [Person].[Person] ([LastName], [FirstName], [MiddleName]) ON [PRIMARY];
CREATE INDEX [IX_PersonPhone_PhoneNumber] ON [Person].[PersonPhone] ([PhoneNumber]) ON [PRIMARY];
GO

CREATE UNIQUE INDEX [AK_Culture_Name] ON [Training].[Culture]([Name]) ON [PRIMARY];
GO


/* ---------------- Create XML index for each XML column ------------------- */

PRINT '';
PRINT '*** Creating XML index for each XML column';
GO

SET ARITHABORT ON;
SET QUOTED_IDENTIFIER ON;
SET ANSI_NULLS ON;
SET ANSI_WARNINGS ON;
SET CONCAT_NULL_YIELDS_NULL ON;
SET NUMERIC_ROUNDABORT OFF;

--CREATE PRIMARY XML INDEX [PXML_] ON [].[] ([]);
--GO


/* ---------------- Create Full Text catalog and indexes ------------------- */

PRINT '';
PRINT '*** Creating Full Text catalog and indexes';
GO

-- This creates a default FULLTEXT CATALOG where to logically store
-- all the FTIndexes going to be created.
CREATE FULLTEXT CATALOG RamsFullTextCatalog AS DEFAULT;
GO


/* ------------------- Create Foreign key constraints ---------------------- */

PRINT '';
PRINT '*** Creating Foreign Key constraints';
GO

ALTER TABLE [Person].[EmailAddress] ADD
	CONSTRAINT [FK_EmailAddress_Person_BusinessEntityID] FOREIGN KEY
	(
		[BusinessEntityID]
	)
	REFERENCES [Person].[Person]
	(
		[BusinessEntityID]
	);
GO

ALTER TABLE [Person].[Person] ADD
	CONSTRAINT [FK_Person_BusinessEntity_BusinessEntityID] FOREIGN KEY
	(
		[BusinessEntityID]
	)
	REFERENCES [Person].[BusinessEntity]
	(
		[BusinessEntityID]
	);
GO

ALTER TABLE [Person].[PersonPhone] ADD
	CONSTRAINT [FK_PersonPhone_Person_BusinessEntityID] FOREIGN KEY
	(
		[BusinessEntityID]
	)
	REFERENCES [Person].[Person]
	(
		[BusinessEntityID]
	)
	, CONSTRAINT [FK_PersonPhone_PhoneNumberType_PhoneNumberTypeID] FOREIGN KEY
	(
		[PhoneNumberTypeID]
	)
	REFERENCES [Person].[PhoneNumberType]
	(
		[PhoneNumberTypeID]
	);
GO

ALTER TABLE [Training].[Trainee] ADD
	CONSTRAINT [FK_Trainee_Person_BusinessEntityID] FOREIGN KEY
	(
		[BusinessEntityID]
	)
	REFERENCES [Person].[Person]
	(
		[BusinessEntityID]
	);
GO

ALTER TABLE [Training].[Trainer] ADD
	CONSTRAINT [FK_Trainer_Person_BusinessEntityID] FOREIGN KEY
	(
		[BusinessEntityID]
	)
	REFERENCES [Person].[Person]
	(
		[BusinessEntityID]
	);
GO

ALTER TABLE [Training].[TestText] ADD
	CONSTRAINT [FK_TestText_Culture_CultureID] FOREIGN KEY
	(
		[CultureID]
	)
	REFERENCES [Training].[Culture]
	(
		[CultureID]
	);
GO

ALTER TABLE [Training].[TestQuestion] ADD
	CONSTRAINT [FK_TestQuestion_TestText_TestTextID] FOREIGN KEY
	(
		[TestTextID]
	)
	REFERENCES [Training].[TestText]
	(
		[TestTextID]
	);
GO

ALTER TABLE [Training].[Test] ADD
	CONSTRAINT [FK_Test_Trainee_TraineeID] FOREIGN KEY
	(
		[TraineeID]
	)
	REFERENCES [Training].[Trainee]
	(
		[BusinessEntityID]
	)
	, CONSTRAINT [FK_Test_Trainer_TrainerID] FOREIGN KEY
	(
		[TrainerID]
	)
	REFERENCES [Training].[Trainer]
	(
		[BusinessEntityID]
	)
	, CONSTRAINT [FK_Test_TestText_TestTextID] FOREIGN KEY
    (
        [TestTextID]
    )
    REFERENCES [Training].[TestText]
    (
        [TestTextID]
    );
GO

ALTER TABLE [Training].[TestAnswer] ADD
	CONSTRAINT [FK_TestAnswer_Test_TestID] FOREIGN KEY
	(
		[TestID]
	)
	REFERENCES [Training].[Test]
	(
		[TestID]
	)
	, CONSTRAINT [FK_TestAnswer_TestQuestion_TestQuestionID] FOREIGN KEY
	(
		[TestQuestionID]
	)
	REFERENCES [Training].[TestQuestion]
	(
		[TestQuestionID]
	);
GO


/* ------------------------- Add table triggers ---------------------------- */

PRINT '';
PRINT '*** Creating Table Triggers';
GO


/* ------------------------- Add database views ---------------------------- */

PRINT '';
PRINT '*** Creating Table Views';
GO


/* ----------------------- Add database functions -------------------------- */

PRINT '';
PRINT '*** Creating Database Functions';
GO


/* ---------------------- Create stored procedures ------------------------- */

PRINT '';
PRINT '*** Creating Stored Procedures';
GO


/* ----------------------- Add Extended Properties ------------------------- */

PRINT '';
PRINT '*** Creating Extended Properties';
GO

SET NOCOUNT ON;
GO

PRINT '    Database'
GO

-- Database
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Rams OLTP Database', NULL, NULL, NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Database trigger to audit all of the DDL changes made to the Rams database.', N'TRIGGER', [ddlDatabaseTriggerLog], NULL, NULL, NULL, NULL;
GO

PRINT '    Files and Filegroups';
GO

-- Files and Filegroups
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary filegroup for the Rams database.', N'FILEGROUP', [PRIMARY];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary data file for the Rams database.', N'FILEGROUP', [PRIMARY], N'Logical File Name', [Rams_Data];
GO

PRINT '    Schemas';
GO

-- Schemas
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Contains objects related to names and addresses of customers, vendors, and employees', N'SCHEMA', [Person], NULL, NULL, NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Contains objects related to applications, membership, users, roles and userroles.', N'SCHEMA', [Security], NULL, NULL, NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Contains objects related to trainer, trainee, measurement records and sentence.', N'SCHEMA', [Training], NULL, NULL, NULL, NULL;
GO

PRINT '    Tables and Columns';
GO

-- Tables and Columns
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Current version number of the Rams database. ', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for RamsBuildVersion records.', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'COLUMN', [SystemInformationID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Version number of the database in 9.yy.mm.dd.00 format.', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'COLUMN', [DatabaseVersion];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Version was last changed.', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'COLUMN', [VersionDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Audit table tracking errors in the the AdventureWorks database that are caught by the CATCH block of a TRY...CATCH construct. Data is inserted by stored procedure dbo.uspLogError when it is executed from inside the CATCH block of a TRY...CATCH construct.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for ErrorLog records.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorLogID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The date and time at which the error occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorTime];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The user who executed the batch in which the error occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [UserName];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The error number of the error that occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorNumber];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The severity of the error that occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorSeverity];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The state number of the error that occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorState];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The name of the stored procedure or trigger where the error occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorProcedure];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The line number at which the error occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorLine];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The message text of the error that occurred.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'COLUMN', [ErrorMessage];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Types of addresses stored in the Address table. ', N'SCHEMA', [Person], N'TABLE', [AddressType], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for AddressType records.', N'SCHEMA', [Person], N'TABLE', [AddressType], N'COLUMN', [AddressTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Address type description. For example, Billing, Home, or Shipping.', N'SCHEMA', [Person], N'TABLE', [AddressType], N'COLUMN', [Name];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Person], N'TABLE', [AddressType], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Source of the ID that connects vendors, customers, and employees with address and contact information.', N'SCHEMA', [Person], N'TABLE', [BusinessEntity], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for all customers, vendors, and employees.', N'SCHEMA', [Person], N'TABLE', [BusinessEntity], N'COLUMN', [BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Person], N'TABLE', [BusinessEntity], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Where to send a person email.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key. Person associated with this email address.  Foreign key to Person.BusinessEntityID', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'COLUMN', [BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key. ID of this email address.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'COLUMN', [EmailAddressID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'E-mail address for the person.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'COLUMN', [EmailAddress];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Human beings involved with Thinkbox: employees, customer contacts, and vendor contacts.', N'SCHEMA', [Person], N'TABLE', [Person], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for Person records.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary type of person: PM = Project Manager, PL = Project Leader, DV = Developer, QC = Quality Control, GC = General contact', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [PersonType];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'0 = The data in FirstName and LastName are stored in western style (first name, last name) order.  1 = Eastern style (last name, first name) order.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [NameStyle];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'A courtesy title. For example, Mr. or Ms.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [Title];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'First name of the person.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [FirstName];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Middle name or middle initial of the person.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [MiddleName];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Last name of the person.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [LastName];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Surname suffix. For example, Sr. or Jr.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [Suffix];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'0 = Contact does not wish to receive e-mail promotions, 1 = Contact does wish to receive e-mail promotions from AdventureWorks, 2 = Contact does wish to receive e-mail promotions from AdventureWorks and selected partners. ', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [EmailPromotion];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Person], N'TABLE', [Person], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Telephone number and type of a person.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Business entity identification number. Foreign key to Person.BusinessEntityID.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'COLUMN', [BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Telephone number identification number.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'COLUMN', [PhoneNumber];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Kind of phone number. Foreign key to PhoneNumberType.PhoneNumberTypeID.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'COLUMN', [PhoneNumberTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Type of phone number of a person.', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for telephone number type records.', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], N'COLUMN', [PhoneNumberTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Name of the telephone number type', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], N'COLUMN', [Name];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Trainee information such as birthdate, gender and title.', N'SCHEMA', [Training], N'TABLE', [Trainee], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Business entity identification number. Foreign key to Person.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Trainee], N'COLUMN', [BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'M = Male, F = Female', N'SCHEMA', [Training], N'TABLE', [Trainee], N'COLUMN', [Gender];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date of birth.', N'SCHEMA', [Training], N'TABLE', [Trainee], N'COLUMN', [BirthDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [Trainee], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Trainer information such as department, gender and title.', N'SCHEMA', [Training], N'TABLE', [Trainer], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Business entity identification number. Foreign key to Person.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Trainer], N'COLUMN', [BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'M = Male, F = Female', N'SCHEMA', [Training], N'TABLE', [Trainer], N'COLUMN', [Gender];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [Trainer], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Lookup table containing the languages in which text is stored.', N'SCHEMA', [Training], N'TABLE', [Culture], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for Culture records.', N'SCHEMA', [Training], N'TABLE', [Culture], N'COLUMN', [CultureID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Culture description.', N'SCHEMA', [Training], N'TABLE', [Culture], N'COLUMN', [Name];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [Culture], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Text information dyslexia test.', N'SCHEMA', [Training], N'TABLE', [TestText], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for text records.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [TestTextID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Culture identification code. Foreign key to Culture.CultureID.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [CultureID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Level of reading difficulty.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [Level];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Text for reading test.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [Text];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Name of the font used.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [FontName];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Size of the font used.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [FontSize];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Width of the character.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [CharacterWidth];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Height of the character.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [CharacterHeight];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Spacing between lines.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [LineSpacing];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [TestText], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Confirm question for dyslexia test.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for text records.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'COLUMN', [TestQuestionID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Related text id. Foreign key to Text.TextID.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'COLUMN', [TestTextID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Question for confirm text.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'COLUMN', [Question];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Answer to question.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'COLUMN', [Answer];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Dyslexia test information such as trainer, trainee and test text.', N'SCHEMA', [Training], N'TABLE', [Test], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for test records.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [TestID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Grade of test.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [Grade];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Related trainee id. Foreign key to Trainee.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [TraineeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Related trainer id. Foreign key to Trainer.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [TrainerID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Related testtext id. Foreign key to TestText.TestTextID.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [TestTextID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'The name of the file in which the saccade data recorded.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [RecordFile];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was test started.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [StartTime];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was test finished.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [EndTime];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [Test], N'COLUMN', [ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Confirm answer to confirm question.', N'SCHEMA', [Training], N'TABLE', [TestAnswer], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key for test answer records.', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'COLUMN', [TestAnswerID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Related test id. Foreign key to Test.TestID', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'COLUMN', [TestID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Related test question id. Foreign key to TestQuestion.TestQuestionID', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'COLUMN', [TestQuestionID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Answer to question.', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'COLUMN', [Answer];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Date and time the record was last updated.', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'COLUMN', [ModifiedDate];
GO


PRINT '    Triggers';
GO

-- Triggers


PRINT '    Views';
GO

-- Views


PRINT '    Indexes';
GO

-- Indexes
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'INDEX', [PK_RamsBuildVersion_SystemInformationID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Nonclustered index created by a primary key constraint.', N'SCHEMA', [dbo], N'TABLE', [DatabaseLog], N'INDEX', [PK_DatabaseLog_DatabaseLogID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'INDEX', [PK_ErrorLog_ErrorLogID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'MS_Description', N'Unique nonclustered index.', N'SCHEMA', [Person], N'TABLE', [AddressType], N'INDEX', [AK_AddressType_Name];
EXECUTE [sys].[sp_addextendedproperty] N'MS_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [Person], N'TABLE', [AddressType], N'INDEX', [PK_AddressType_AddressTypeID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [Person], N'TABLE', [BusinessEntity], N'INDEX', [PK_BusinessEntity_BusinessEntityID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Nonclustered index.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'INDEX', [IX_EmailAddress_EmailAddress];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'INDEX', [PK_EmailAddress_BusinessEntityID_EmailAddressID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [Person], N'TABLE', [Person], N'INDEX', [PK_Person_BusinessEntityID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'INDEX', [PK_PersonPhone_BusinessEntityID_PhoneNumber_PhoneNumberTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Nonclustered index.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'INDEX', [IX_PersonPhone_PhoneNumber];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Clustered index created by a primary key constraint.', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], N'INDEX', [PK_PhoneNumberType_PhoneNumberTypeID];
GO


PRINT '    Constraints - PK, FK, DF, CK';
GO

-- Constraints - PK, FK, DF, CK
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'CONSTRAINT', [PK_RamsBuildVersion_SystemInformationID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [dbo], N'TABLE', [RamsBuildVersion], N'CONSTRAINT', [DF_RamsBuildVersion_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (nonclustered) constraint', N'SCHEMA', [dbo], N'TABLE', [DatabaseLog], N'CONSTRAINT', [PK_DatabaseLog_DatabaseLogID];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'CONSTRAINT', [PK_ErrorLog_ErrorLogID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [dbo], N'TABLE', [ErrorLog], N'CONSTRAINT', [DF_ErrorLog_ErrorTime];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Person], N'TABLE', [AddressType], N'CONSTRAINT', [PK_AddressType_AddressTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Person], N'TABLE', [AddressType], N'CONSTRAINT', [DF_AddressType_ModifiedDate];
GO


EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Person], N'TABLE', [BusinessEntity], N'CONSTRAINT', [PK_BusinessEntity_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Person], N'TABLE', [BusinessEntity], N'CONSTRAINT', [DF_BusinessEntity_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'CONSTRAINT', [PK_EmailAddress_BusinessEntityID_EmailAddressID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Person.BusinessEntityID.', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'CONSTRAINT', [FK_EmailAddress_Person_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Person], N'TABLE', [EmailAddress], N'CONSTRAINT', [DF_EmailAddress_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [PK_Person_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing BusinessEntity.BusinessEntityID.', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [FK_Person_BusinessEntity_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of 0', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [DF_Person_EmailPromotion];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of 0', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [DF_Person_NameStyle];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [DF_Person_ModifiedDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [EmailPromotion] >= (0) AND [EmailPromotion] <= (2)', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [CK_Person_EmailPromotion];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [PersonType] is one of PM, PL, DV, QC or GC.', N'SCHEMA', [Person], N'TABLE', [Person], N'CONSTRAINT', [CK_Person_PersonType];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'CONSTRAINT', [PK_PersonPhone_BusinessEntityID_PhoneNumber_PhoneNumberTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Person.BusinessEntityID.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'CONSTRAINT', [FK_PersonPhone_Person_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing PhoneNumberType.PhoneNumberTypeID.', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'CONSTRAINT', [FK_PersonPhone_PhoneNumberType_PhoneNumberTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Person], N'TABLE', [PersonPhone], N'CONSTRAINT', [DF_PersonPhone_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], N'CONSTRAINT', [PK_PhoneNumberType_PhoneNumberTypeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Person], N'TABLE', [PhoneNumberType], N'CONSTRAINT', [DF_PhoneNumberType_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [Trainee], N'CONSTRAINT', [PK_Trainee_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Person.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Trainee], N'CONSTRAINT', [FK_Trainee_Person_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [Trainee], N'CONSTRAINT', [DF_Trainee_ModifiedDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [Gender]=''f'' OR [Gender]=''m''', N'SCHEMA', [Training], N'TABLE', [Trainee], N'CONSTRAINT', [CK_Trainee_Gender];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [Trainer], N'CONSTRAINT', [PK_Trainer_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Person.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Trainer], N'CONSTRAINT', [FK_Trainer_Person_BusinessEntityID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [Trainer], N'CONSTRAINT', [DF_Trainer_ModifiedDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [Gender]=''f'' OR [Gender]=''m''', N'SCHEMA', [Training], N'TABLE', [Trainer], N'CONSTRAINT', [CK_Trainer_Gender];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [Culture], N'CONSTRAINT', [PK_Culture_CultureID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [Culture], N'CONSTRAINT', [DF_Culture_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [TestText], N'CONSTRAINT', [PK_TestText_TestTextID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Culture.CultureID.', N'SCHEMA', [Training], N'TABLE', [TestText], N'CONSTRAINT', [FK_TestText_Culture_CultureID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [TestText], N'CONSTRAINT', [DF_TestText_ModifiedDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [Level] > 0 AND [Level] < 11', N'SCHEMA', [Training], N'TABLE', [TestText], N'CONSTRAINT', [CK_TestText_Level];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'CONSTRAINT', [PK_TestQuestion_TestQuestionID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing TestText.TextID.', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'CONSTRAINT', [FK_TestQuestion_TestText_TestTextID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [TestQuestion], N'CONSTRAINT', [DF_TestQuestion_ModifiedDate];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [Test], N'CONSTRAINT', [PK_Test_TestID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Trainee.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Test], N'CONSTRAINT', [FK_Test_Trainee_TraineeID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Trainer.BusinessEntityID.', N'SCHEMA', [Training], N'TABLE', [Test], N'CONSTRAINT', [FK_Test_Trainer_TrainerID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing TestText.TestTextID.', N'SCHEMA', [Training], N'TABLE', [Test], N'CONSTRAINT', [FK_Test_TestText_TestTextID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [Test], N'CONSTRAINT', [DF_Test_ModifiedDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [Grade] > 0 AND [Grade] < 11', N'SCHEMA', [Training], N'TABLE', [Test], N'CONSTRAINT', [CK_Test_Grade];
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Primary key (clustered) constraint', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'CONSTRAINT', [PK_TestAnswer_TestAnswerID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing Test.TestID.', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'CONSTRAINT', [FK_TestAnswer_Test_TestID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Foreign key constraint referencing TestQuestion.TestQuestionID.', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'CONSTRAINT', [FK_TestAnswer_TestQuestion_TestQuestionID];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Default constraint value of GETDATE()', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'CONSTRAINT', [DF_TestAnswer_ModifiedDate];
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Check constraint [Answer] == 0 OR [Answer] == 1', N'SCHEMA', [Training], N'TABLE', [TestAnswer], N'CONSTRAINT', [CK_TestAnswer_Answer];
GO


PRINT '    Functions';
GO

-- Functions
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Scalar function used by the Sales.Customer table to help set the account number.', N'SCHEMA', [dbo], N'FUNCTION', [ufnLeadingZeros], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Input parameter for the scalar function ufnLeadingZeros. Enter a valid integer.', N'SCHEMA', [dbo], N'FUNCTION', [ufnLeadingZeros], N'PARAMETER', '@Value';
GO


PRINT '    Stored Procedures';
GO

-- Stored Procedures
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Logs error information in the ErrorLog table about the error that caused execution to jump to the CATCH block of a TRY...CATCH construct. Should be executed from within the scope of a CATCH block otherwise it will return without inserting error information.', N'SCHEMA', [dbo], N'PROCEDURE', [uspLogError], NULL, NULL;
EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Output parameter for the stored procedure uspLogError. Contains the ErrorLogID value corresponding to the row inserted by uspLogError in the ErrorLog table.', N'SCHEMA', [dbo], N'PROCEDURE', [uspLogError], N'PARAMETER', '@ErrorLogID';
GO

EXECUTE [sys].[sp_addextendedproperty] N'Rams_Description', N'Prints error information about the error that caused execution to jump to the CATCH block of a TRY...CATCH construct. Should be executed from within the scope of a CATCH block otherwise it will return without printing any error information.', N'SCHEMA', [dbo], N'PROCEDURE', [uspPrintError], NULL, NULL;
GO

PRINT '    XML Schemas';
GO

-- XML Schemas

SET NOCOUNT OFF;
GO


/* -------------------- Drop DDL Trigger for Database ---------------------- */

PRINT '';
PRINT '*** Disabling DDL Trigger for Database';
GO

DISABLE TRIGGER [ddlDatabaseTriggerLog]
ON DATABASE;
GO

/*
-- Output database object creation messages
SELECT [PostTime], [DatabaseUser], [Event], [Schema], [Object], [TSQL], [XmlEvent]
FROM [dbo].[DatabaseLog];
*/
GO


/* --------------- Change File Growth Values for Database ------------------ */

PRINT '';
PRINT '*** Changing File Growth Values for Database';
GO

ALTER DATABASE [Rams]
MODIFY FILE (NAME = 'Rams_Data', FILEGROWTH = 16);
GO

ALTER DATABASE [Rams]
MODIFY FILE (NAME = 'Rams_Log', FILEGROWTH = 16);
GO



/* --------------------------- Shrink Database ----------------------------- */

PRINT '';
PRINT '*** Shrinking Database';
GO

DBCC SHRINKDATABASE ([Rams]);
GO


USE [master];
GO

PRINT 'Finished - ' + CONVERT(varchar, GETDATE(), 121);
GO
