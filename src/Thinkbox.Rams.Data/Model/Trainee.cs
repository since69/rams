﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Model
{
    public class Trainee : BaseModel
    {
        public virtual string Gender { get; set; }
        public virtual System.DateTime BirthDate { get; set; }

        public virtual Person Person { get; set; }
        public virtual IList<Test> Tests { get; set; }
    }
}
