﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Model
{
    public class TestText : BaseModel
    {
        public virtual int Level { get; set; }
        public virtual string Title { get; set; }
        public virtual string Text { get; set; }
        public virtual string FontName { get; set; }
        public virtual int FontSize { get; set; }
        public virtual decimal CharacterWidth { get; set; }
        public virtual decimal CharacterHeight { get; set; }
        public virtual decimal LineSpacing { get; set; }

        public virtual Test Test { get; set; }
        public virtual Culture Culture { get; set; }
        public virtual IList<TestQuestion> TestQuestions { get; set; }
    }
}
