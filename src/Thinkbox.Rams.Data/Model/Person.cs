﻿namespace Thinkbox.Rams.Data.Model
{
    public class Person : BaseModel
    {
        public virtual string PersonType { get; set; }
        public virtual bool NameStyle { get; set; }
        public virtual string Title { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Suffix { get; set; }
        public virtual int EmailPromotion { get; set; }

        public virtual BusinessEntity BusinessEntity { get; set; }
        public virtual Trainee Trainee { get; set; }
        public virtual Trainer Trainer { get; set; }
    }
}
