﻿namespace Thinkbox.Rams.Data.Model
{
    public class EmailAddress : BaseModel
    {
        /// <summary>
        /// Person id.
        /// </summary>
        public virtual int BusinessEntityId { get; set; }
        /// <summary>
        /// Email address.
        /// </summary>
        public virtual string Address { get; set; }
    }
}
