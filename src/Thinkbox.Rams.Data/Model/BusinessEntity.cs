﻿namespace Thinkbox.Rams.Data.Model
{
    public class BusinessEntity : BaseModel
    {
        public virtual Person Person { get; set; }
    }
}
