﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Model
{
    public class TestAnswer : BaseModel
    {
        public virtual bool Answer { get; set; }

        public virtual Test Test { get; set; }
        public virtual TestQuestion TestQuestion { get; set; }
    }
}
