﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Model
{
    public class Culture
    {
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual System.DateTime ModifiedDate { get; set; }

        public virtual IList<TestText> TestTexts { get; set; }
    }
}
