﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Model
{
    public class TestQuestion : BaseModel
    {
        public virtual string Question { get; set; }
        public virtual bool Answer { get; set; }

        public virtual TestText TestText { get; set; }
        public virtual IList<TestAnswer> TestAnswers { get; set; }
    }
}
