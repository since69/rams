﻿namespace Thinkbox.Rams.Data.Model
{
    public class BaseModel
    {
        public virtual int Id { get; set; }
        public virtual System.DateTime ModifiedDate { get; set; }
    }
}
