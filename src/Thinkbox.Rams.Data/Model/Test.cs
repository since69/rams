﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Model
{
    public class Test : BaseModel
    {
        public virtual byte Grade { get; set; }
        public virtual string RecordFile { get; set; }
        public virtual System.DateTime StartTime { get; set; }
        public virtual System.DateTime EndTime { get; set; }

        public virtual Trainee Trainee { get; set; }
        public virtual Trainer Trainer { get; set; }
        public virtual TestText TestText { get; set; }

        public virtual IList<TestAnswer> TestAnswers { get; set; }
    }
}
