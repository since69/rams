﻿using NHibernate;
using NHibernate.Cfg;

namespace Thinkbox.Rams.Data
{
    public class SessionProvider
    {
        #region PROPERTIES
        private static Configuration configuration;
        public static Configuration Configuration
        {
            get
            {
                if (configuration == null)
                {
                    configuration = new Configuration();
                    configuration.Configure();
                    configuration.AddAssembly(typeof(Model.BusinessEntity).Assembly);
                }
                return configuration;
            }
        }

        private static ISessionFactory sessionFactory;
        public static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                    sessionFactory = Configuration.BuildSessionFactory();
                return sessionFactory;
            }
        }
        #endregion

        #region CONSTRUCTOR
        private SessionProvider() { }
        #endregion

        #region METHODS
        public static ISession GetSession()
        {
            return sessionFactory.OpenSession();
        }
        #endregion
    }
}
