﻿using System.Collections.Generic;
using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public interface ITestAnswerRepository : IRepository<TestAnswer>
    {
        ICollection<TestAnswer> FindTestAnswers(int testId);
    }
}
