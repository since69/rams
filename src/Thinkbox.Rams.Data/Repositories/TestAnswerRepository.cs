﻿using System.Collections.Generic;
using NHibernate.Criterion;
using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class TestAnswerRepository : 
        Repository<TestAnswer>, ITestAnswerRepository
    {
        public ICollection<TestAnswer> FindTestAnswers(int testId)
        {
            return Session.CreateCriteria(typeof(TestAnswer))
                .Add(Restrictions.Eq("Test.Id", testId))
                .List<TestAnswer>();
        }
    }
}
