﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class BusinessEntityRepository : 
        Repository<BusinessEntity>, IBusinessEntityRepository
    {
    }
}
