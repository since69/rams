﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
    }
}
