﻿using System.Collections.Generic;

namespace Thinkbox.Rams.Data.Repositories
{
    public interface IRepository<T>
    {
        T GetById(int id);
        ICollection<T> FindAll();
        void Add(T entity);
        void Remove(T entity);
    }
}
