﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public interface ITestRepository : IRepository<Test>
    {
    }
}
