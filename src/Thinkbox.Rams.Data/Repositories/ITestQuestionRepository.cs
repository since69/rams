﻿using System.Collections.Generic;
using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public interface ITestQuestionRepository : IRepository<Model.TestQuestion>
    {
        ICollection<TestQuestion> FindTestQuestion(int testTextId);
    }
}
