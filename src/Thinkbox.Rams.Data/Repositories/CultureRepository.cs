﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class CultureRepository : 
        Repository<Culture>, ICultureRepository
    {
    }
}
