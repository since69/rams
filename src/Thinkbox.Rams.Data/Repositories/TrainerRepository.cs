﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class TrainerRepository : 
        Repository<Trainer>, ITrainerRepository
    {
    }
}
