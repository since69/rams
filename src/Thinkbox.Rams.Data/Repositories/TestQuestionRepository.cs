﻿using System.Collections.Generic;
using NHibernate.Criterion;
using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class TestQuestionRepository :
        Repository<TestQuestion>, ITestQuestionRepository
    {
        public ICollection<TestQuestion> FindTestQuestion(int testTextId)
        {
            return Session.CreateCriteria(typeof(TestQuestion))
                .Add(Restrictions.Eq("TestText.Id", testTextId))
                .List<TestQuestion>();
        }
    }
}
