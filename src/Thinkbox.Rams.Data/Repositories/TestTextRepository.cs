﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class TestTextRepository :
        Repository<TestText>, ITestTextRepository
    {
    }
}
