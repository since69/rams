﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public class TraineeRepository : 
        Repository<Trainee>, ITraineeRepository
    {
    }
}
