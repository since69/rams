﻿using Thinkbox.Rams.Data.Model;

namespace Thinkbox.Rams.Data.Repositories
{
    public interface IBusinessEntityRepository : IRepository<BusinessEntity>
    {
    }
}
