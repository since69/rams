﻿using System.Collections.Generic;
using NHibernate;
using Thinkbox.Data.NHibernate;

namespace Thinkbox.Rams.Data.Repositories
{
    public class Repository<T> : IRepository<T>
    {
        public ISession Session
        {
            get { return UnitOfWork.CurrentSession; }
        }

        public T GetById(int id)
        {
            return Session.Get<T>(id);
        }

        public ICollection<T> FindAll()
        {
            return Session.CreateCriteria(typeof(T)).List<T>();
        }

        public void Add(T entity)
        {
            Session.Save(entity);
        }

        public void Remove(T entity)
        {
            Session.Delete(entity);
        }
    }
}
