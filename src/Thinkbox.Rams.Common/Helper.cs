﻿//
// Thinkbox.Rams.Common.Helper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using log4net;

namespace Thinkbox.Rams.Common
{
    public class Helper
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Helper));
        #endregion

        #region METHODS
        //
        // AssemblyInfo methods.
        //

        public static string GetVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection
                .Assembly.GetExecutingAssembly();
            Version version = assembly.GetName().Version;
            return version.ToString();
        }


        //
        // File methods.
        //

        /// <summary>
        /// 주어진 디렉토리의 경로를 확인하여 경로가 없으면 경로를 생성한다.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>
        /// True - 경로가 존재하거나 생성. 
        /// False - 경로가 존재하지 않고, 생성에 실패.
        /// </returns>
        public static bool CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    log.Error(string.Format(
                        "Could not create directory. original message is {0}", 
                        ex.Message));
                    return false;
                }
            }
            
            return true;
        }


        //
        // Console methods.
        // 

        public static int? GetNumericReadLine()
        {
            int rc;
            if (int.TryParse(Console.ReadLine(), out rc))
                return rc;
            return null;
        }
        #endregion
    }
}
