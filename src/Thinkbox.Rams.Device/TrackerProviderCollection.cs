﻿//
// Thinkbox.Rams.Configuration
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//


namespace Thinkbox.Rams.Device
{
    using System;
    using System.Configuration.Provider;

    public sealed class TrackerProviderCollection : ProviderCollection
    {
        new public TrackerProvider this[string name]
        {
            get { return (TrackerProvider)base[name]; }
        }

        public void CopyTo(TrackerProvider[] array, int index)
        {
            base.CopyTo(array, index);
        }

        public override void Add(ProviderBase provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            if (!(provider is TrackerProvider))
                throw new ArgumentException("Only the Tracker class can be added.");

            base.Add(provider);
        }
    }
}
