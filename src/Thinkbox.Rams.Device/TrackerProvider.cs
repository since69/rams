﻿//
// Thinkbox.Rams.Device.TrackerProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Thinkbox Co. Ltd
//

using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration.Provider;
using log4net;

namespace Thinkbox.Rams.Device
{
    public abstract class TrackerProvider : ProviderBase
    {
        #region VARIABLES
        const string HELPER_TYPE_NAME = "Thinkbox.Rams.Device.TrackerHelper, Thinkbox.Rams.Device";
        private static readonly ILog log = LogManager.GetLogger(typeof(TrackerProvider));

        static readonly object gazeUpdateEvent = new object();
        EventHandlerList events = new EventHandlerList();
        public event GazeUpdateEventHandler GazeUpdate
        {
            add { events.AddHandler(gazeUpdateEvent, value); }
            remove { events.RemoveHandler(gazeUpdateEvent, value); }
        }
        #endregion

        #region PROPERTIES
        internal static ITrackerHelper helper;
        internal static ITrackerHelper Helper
        {
            get { return helper; }
        }

        public abstract int ScreenWidth { get; set; }
        public abstract int ScreenHeight { get; set; }

        public abstract TrackerStatus Status { get; }
        #endregion

        #region CONSTRUCTORS
        static TrackerProvider()
        {
            Type type = Type.GetType(HELPER_TYPE_NAME, false);
            if (type == null) return;
            try
            {
                helper = Activator.CreateInstance(type) as ITrackerHelper;
            }
            catch
            {
                // ignore
            }
        }

        protected TrackerProvider() { }
        #endregion

        #region ABSTRACT METHODS
        /// <summary>
        /// Start measurement.
        /// </summary>
        public abstract void Start();
        /// <summary>
        /// Stop measurement.
        /// </summary>
        public abstract void Stop();
        /// <summary>
        /// Calibrate the tracker according to the user before the measurement.
        /// </summary>
        public abstract void Calibrate();
        /// <summary>
        /// Show calibration result window.
        /// </summary>
        public abstract void ShowCalibrateResult();

        public abstract void OpenEyeTracker();
        public abstract void CloseEyeTracker();
        #endregion

        #region VIRTUAL METHODS
        protected virtual string GetStringConfigValue(NameValueCollection config,
            string name, string def)
        {
            string rv = def;
            string val = config[name];
            if (val != null) rv = val;
            return rv;
        }

        protected virtual int GetIntConfigValue(NameValueCollection config, 
            string name, int def)
        {
            int rv = def;
            string val = config[name];
            if (val != null)
            {
                try { rv = Int32.Parse(val); }
                catch (Exception e)
                {
                    throw new ProviderException(
                        String.Format("{0} must be an integer", name), e);
                }
            }
            return rv;
        }

        protected virtual int GetEnumConfigValue(NameValueCollection config,
            string name, Type enumType, int def)
        {
            int rv = def;
            string val = config[name];
            if (val != null)
            {
                try { rv = (int)Enum.Parse(enumType, val); }
                catch (Exception e)
                {
                    throw new ProviderException(
                        String.Format("{0} must be one of the following values: {1}",
                            name, String.Join(",", Enum.GetNames(enumType))), e);
                }
            }
            return rv;
        }

        protected virtual bool GetBoolConfigValue(NameValueCollection config,
            string name, bool def)
        {
            bool rv = def;
            string val = config[name];
            if (val != null)
            {
                try
                {
                    rv = Boolean.Parse(val);
                }
                catch (Exception e)
                {
                    throw new ProviderException(
                        String.Format("{0} must be true or false", name), e);
                }
            }
            return rv;
        }
        #endregion

        #region EVENTS
        protected virtual void OnGazeUpdate(GazeUpdateEventArgs e)
        {
            if (events[gazeUpdateEvent] is GazeUpdateEventHandler eh)
                eh(this, e);
        }
        #endregion
    }
}
