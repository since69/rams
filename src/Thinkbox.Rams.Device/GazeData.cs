﻿//
// Thinkbox.Rams.Device.GazeData
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    /// <summary>
    /// Saccade data wrapping class.
    /// </summary>
    public class GazeData
    {
        private int elapsed;
        public int Elapsed
        {
            get { return elapsed; }
            set { elapsed = value; }
        }
        private decimal averageX;
        public decimal AverageX
        {
            get { return averageX; }
            set { averageX = value; }
        }
        private decimal averageY;
        public decimal AverageY
        {
            get { return averageY; }
            set { averageY = value; }
        }
        private decimal leftX;
        public decimal LeftX
        {
            get { return leftX; }
            set { leftX = value; }
        }
        private decimal leftY;
        public decimal LeftY
        {
            get { return leftY; }
            set { leftY = value; }
        }
        private decimal rightX;
        public decimal RightX
        {
            get { return rightX; }
            set { rightX = value; }
        }
        private decimal rightY;
        public decimal RightY
        {
            get { return rightY; }
            set { rightY = value; }
        }
    }
}
