﻿//
// Thinkbox.Rams.Device.GazeTrackerProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading;
using log4net;
using GTWrapper;
using Thinkbox.Rams.Configuration;

namespace Thinkbox.Rams.Device
{
    public class GazeTrackerProvider : TrackerProvider
    {
        #region ENUMS
        public enum TrackingDevice
        {
            /// <summary>Device is none.</summary>
            None = 0x00,
            /// <summary>General camera.</summary>
            GenericCamera = 0x01,
            /// <summary>Logitch C600.</summary>
            C600 = 0x01,
            /// <summary>Integrated camera.</summary>
            IntegratedCamera,
            Unknown1,
            Unknown2,
            Unknown3,
            Unknown4,
            Unknown5,
            Unknown6,
            Unknown7
        }

        public enum TrackerSettings
        {
            RemoveTremor = 0x00,
            UseZSesor,
            EyeDetectionMode,
            DetectBlink
        }

        /// <summary>
        /// The possible states of the trace device.
        /// </summary>
        public enum TrackerState
        {
            /// <summary>After initialized.</summary>
            Initialized = 0x01,
            /// <summary>Currently tracking.</summary>
            Tracking,
            /// <summary>Tracking is paused.</summary>
            Paused,
            /// <summary>Before initialized.</summary>
            NotInitialized
        }

        public enum TrackerError
        {
            /// <summary>Unknown error.</summary>
            Unknown = -1,
            /// <summary>No error.</summary>
            None = 1,

            /// <summary>Initialize failed.</summary>
            Flea3 = 100,
            Camera,
            VideoFile,
            ImageFile,
            Initialize,

            /// <summary>Initialization(Configuration) file error.</summary>
            IniFile = 200,

            /// <summary>Image error.</summary>
            Image = 400,

            /// <summary>Callback function error.</summary>
            Callback = 500
        }
        #endregion

        #region VARIABLES
        //private readonly string logFormat = "{0},{1:00.00000},{2:00.00000}," +
        //    "{3:00.00000},{4:00.00000},{5:00.00000},{6:00.00000}";
        private static readonly ILog log = LogManager.GetLogger(typeof(GazeTrackerProvider));
        private GTClr gazeTracker = new GTClr();
        private string recordingFolder;
        private bool eyeTrackerOpend = false;
        #endregion

        #region PROPERTIES
        private int screenWidth;
        public override int ScreenWidth
        {
            get { return screenWidth; }
            set { screenWidth = value; }
        }

        private int screenHeight;
        public override int ScreenHeight
        {
            get { return screenHeight; }
            set { screenHeight = value; }
        }

        internal TrackerStatus status;
        public override TrackerStatus Status
        {
            get { return status; }
        }
        #endregion

        #region CONSTRUCTORS
        public GazeTrackerProvider() { }
        #endregion

        #region OVERRIDE METHODS
        public override void OpenEyeTracker()
        {
            gazeTracker.StartDisplayingOnNewWindow();
            eyeTrackerOpend = true;
        }

        public override void CloseEyeTracker()
        {
            if (eyeTrackerOpend)
            {
                gazeTracker.StopDisplayingOnNewWindow();
                eyeTrackerOpend = false;
            }
        }

        public override void Calibrate()
        {
            if (status == TrackerStatus.Started)
            {
                gazeTracker.DoCalibration();
            }
        }
        
        public override void ShowCalibrateResult()
        {
            gazeTracker.ShowCalibrationResult();
        }

        //public override void Pause()
        //{
        //    if (status == TrackerStatus.Started)
        //    {
        //        gazeTracker.Pause();
        //        status = TrackerStatus.Paused;
        //        log.Info("Gaze tracker is paused.");
        //    }
        //}

        //public override void Resume()
        //{
        //    if (status == TrackerStatus.Paused)
        //    {
        //        gazeTracker.Resume();
        //        status = TrackerStatus.Started;
        //        log.Info("Gaze tracker is resumed.");
        //    }
        //}

        public override void Start()
        {
            if (status == TrackerStatus.Ready)
            {
                gazeTracker.RegCallback2();

                if ((TrackerState) gazeTracker.GetGTStatus() == TrackerState.Initialized)
                {
                    gazeTracker.Start();
                    status = TrackerStatus.Started;
                    log.Info("Gaze tracker is started.");
                }
                else
                {
                    log.Fatal("Gaze tracker is not initialized.");
                }
            }
        }

        public override void Stop()
        {
            if (status == TrackerStatus.Started)
            {

                gazeTracker.Stop();
                status = TrackerStatus.Ready;
                log.Info("Gaze tracker is stopped.");
            }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            base.Initialize(name, config);

            screenWidth = GetIntConfigValue(config, "screenWidth", 1920);
            screenHeight = GetIntConfigValue(config, "screenHeight", 1080);

            TrackerSection section = (TrackerSection)ConfigurationManager
                .GetSection("tracker");
            recordingFolder = section.RecordingFolder;

            if (gazeTracker == null)
            {
                log.Fatal("Gaze tracker is not created.");
                return;
            }

            InitaizeGazeTracker();
        }

        private void InitaizeGazeTracker()
        {
            TrackerError rc = (TrackerError) gazeTracker.Init(
                (GTInputSource_wrap)TrackingDevice.IntegratedCamera);
            if (rc == TrackerError.None)
            {
                log.Info("Tracker is initialized.");
            }
            else
            {
                log.Fatal("Gaze tracker is not initialized.");
                return;
            }

            // Regist callback event.
            GTClr.eventSender2 += new CbEventHandler2(OnGazeData);

            // Set properties.
            gazeTracker.SetGazeTrackerProperty((GTProperty_wrap)TrackerSettings.RemoveTremor, 1);
            gazeTracker.SetGazeTrackerProperty((GTProperty_wrap)TrackerSettings.EyeDetectionMode, 1);

            status = GetGazeTrackerStatus();
        }

        private TrackerStatus GetGazeTrackerStatus()
        {
            if ((TrackerState)gazeTracker.GetGTStatus() == TrackerState.Initialized)
                return TrackerStatus.Ready;
            else
                return TrackerStatus.NotReady;
        }

        /// <summary>
        /// 측정된 Y 좌표를 스크린 좌표로 변환.
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        private float CorrectY(float y)
        {
            double measurementAreaHeight = 126;
            return (float)((y - 30) / screenHeight * measurementAreaHeight);
        }

        /// <summary>
        /// 측정된 X 좌표를 스크린 좌표로 변환.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private float CorrectX(float x)
        {
            double measurementAreaWidth = 190;
            return (float)((x - 30) / screenWidth * measurementAreaWidth);
        }
        #endregion

        #region EVENTS
        private void OnGazeData(int elapsed, float avgX, float avgY,
            float leftX, float leftY, float rightX, float rightY)
        {
            GazeData data = new GazeData()
            {
                Elapsed = elapsed,
                AverageX = (decimal)CorrectX(avgX),
                AverageY = (decimal)CorrectY(avgY),
                LeftX = (decimal)CorrectX(leftX),
                LeftY = (decimal)CorrectY(leftY),
                RightX = (decimal)CorrectX(rightX),
                RightY = (decimal)CorrectY(rightY)
            };

            GazeUpdateEventArgs args = new GazeUpdateEventArgs(data);
            OnGazeUpdate(args);
            //log.DebugFormat(logFormat, data.Elapsed, 
            //    data.AverageX, data.AverageY, data.LeftX, data.LeftY, 
            //    data.RightX, data.RightY);
        }
        #endregion
    }
}
