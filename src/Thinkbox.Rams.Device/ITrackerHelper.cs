﻿//
// Thinkbox.Rams.Device.ITrackerHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    interface ITrackerHelper
    {
        TrackerProviderCollection Providers { get; }
    }
}
