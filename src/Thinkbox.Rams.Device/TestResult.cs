﻿//
// Thinkbox.Rams.Device.TestResult
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    /// <summary>
    /// The analysis result is saved and passed to the class that 
    /// uses the analysis result.
    /// </summary>
    public class TestResult
    {
        #region PROPERTIES
        private System.Collections.Generic.List<AnalyzedGazeData> records;
        public System.Collections.Generic.List<AnalyzedGazeData> Records
        {
            get { return records; }
        }

        private string traineeName = string.Empty;
        public string TraineeName
        {
            get { return traineeName; }
            set { traineeName = value; }
        }

        private int grade = 0;
        public int Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        private decimal crossCorrelation = 0;
        /// <summary>
        /// Coordination rate of both eyes.
        /// </summary>
        public decimal CrossCorrelation
        {
            get { return crossCorrelation; }
            set { crossCorrelation = value; }
        }

        private int numOfLines = 0;
        /// <summary>
        /// Number of all lines in the text.
        /// </summary>
        public int NumOfLines
        {
            get { return numOfLines; }
            set { numOfLines = value; }
        }

        private int numOfUsedLines = 0;
        /// <summary>
        /// Number of lines used for analysis.
        /// </summary>
        public int NumOfUsedLines
        {
            get { return numOfUsedLines; }
            set { numOfUsedLines = value; }
        }

        private int numOfWords = 0;
        /// <summary>
        /// Number of all words in the text.
        /// </summary>
        public int NumOfWords
        {
            get { return numOfWords; }
            set { numOfWords = value; }
        }

        private int[] numOfFixations = { 0, 0 };
        /// <summary>
        /// Fixed number of both eyes.
        /// </summary>
        public int[] NumOfFixations
        {
            get { return numOfFixations; }
            set { numOfFixations = value; }
        }

        private Duration fixationDuration;
        public Duration FixationDuration
        {
            get { return fixationDuration; }
            set { fixationDuration = value; }
        }

        private decimal fixationWidth = 0;
        /// <summary>
        /// Number of words read per minute.
        /// </summary>
        public decimal FixationWidth
        {
            get { return fixationWidth; }
            set { fixationWidth = value; }
        }

        private Regression numOfRegression;
        /// <summary>
        /// Retrograde number of both eyes. 
        /// </summary>
        public Regression NumOfRegression
        {
            get { return numOfRegression; }
        }

        private decimal readingRate = 0;
        public decimal ReadingRate
        {
            get { return readingRate; }
            set { readingRate = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public TestResult()
        {
            records = new System.Collections.Generic.List<AnalyzedGazeData>();
            numOfRegression = new Regression();
            fixationDuration = new Duration();
        }
        #endregion

        #region INTERNAL CLASSES
        public class Regression
        {
            #region PROPERTIES
            private int left = 0;
            public int Left
            {
                get { return left; }
                set { left = value;  }
            }
            private int right = 0;
            public int Right
            {
                get { return right; }
                set { right = value; }
            }
            #endregion

            #region CONSTRUTORS
            public Regression()
            {
            }
            #endregion
        }

        public class Duration
        {
            #region PROPERTIES
            private decimal left = 0;
            public decimal Left
            {
                get { return left; }
                set { left = value; }
            }
            private decimal right = 0;
            public decimal Right
            {
                get { return right; }
                set { right = value; }
            }
            #endregion

            #region CONSTRUTORS
            public Duration()
            {
            }
            #endregion
        }
        #endregion
    }
}
