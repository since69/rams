﻿//
// Thinkbox.Rams.Device.AnalyzedGazeData
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    /// <summary>
    /// Saccade data wrapping class.
    /// </summary>
    public class AnalyzedGazeData
    {
        
        private string calculatedLine;
        public string CalculatedLine
        {
            get { return calculatedLine; }
            set { calculatedLine = value; }
        }
        private int lineNumber;
        public int LineNumber
        {
            get { return lineNumber; }
            set { lineNumber = value; }
        }
        private GazeData rawData;
        public GazeData RawData
        {
            get { return rawData; }
            set { rawData = value; }
        }
    }
}
