﻿//
// Thinkbox.Rams.Device.DemoTrackerProvider
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading;
using System.Windows.Forms;
using log4net;
using Thinkbox.Rams.Configuration;

namespace Thinkbox.Rams.Device
{
    public class DemoTrackerProvider : TrackerProvider    
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(DemoTrackerProvider));
        private System.Threading.Timer dataGenerationTimer;
        private readonly string unsupportedFeatureMsg = "This feature is not supported.";
        private string recordingFolder;
        #endregion

        #region PROPERTIES
        private int screenWidth;
        public override int ScreenWidth
        {
            get { return screenWidth; }
            set { screenWidth = value; }
        }

        private int screenHeight;
        public override int ScreenHeight
        {
            get { return screenHeight; }
            set { screenHeight = value; }
        }

        internal TrackerStatus status;
        public override TrackerStatus Status
        {
            get { return status; }
        }
        #endregion

        #region GENERIC METHODS
        private void StartTimer()
        {
            dataGenerationTimer.Change(0, 1000);
        }

        private void StopTimer()
        {
            dataGenerationTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        /// <summary>
        /// Timer callback function.
        /// </summary>
        /// <param name="state"></param>
        private void GenerateData(object state)
        {
            GazeData data = new GazeData()
            {
                Elapsed = 1000,
                AverageX = 0,
                AverageY = 0,
                LeftX = 0,
                LeftY = 0,
                RightX = 0,
                RightY = 0
            };

            GazeUpdateEventArgs args = new GazeUpdateEventArgs(data);
            OnGazeUpdate(args);

            //log.DebugFormat("E:{0,5}, A:{1},{2}, L:{3},{4}, R:{5},{6}",
            //    data.Elapsed, data.AverageX, data.AverageY,
            //    data.LeftX, data.LeftY, data.RightX, data.RightY);
        }
        #endregion

        #region OVERRIDE METHODS
        public override void OpenEyeTracker()
        {
            MessageBox.Show(unsupportedFeatureMsg, "ReadAlyzer", 
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public override void CloseEyeTracker()
        {
            MessageBox.Show(unsupportedFeatureMsg, "ReadAlyzer",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public override void Calibrate()
        {
            MessageBox.Show(unsupportedFeatureMsg, "ReadAlyzer",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public override void ShowCalibrateResult()
        {
            MessageBox.Show(unsupportedFeatureMsg, "ReadAlyzer",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public override void Start()
        {
            if (status == TrackerStatus.Ready)
            {
                StartTimer();
                status = TrackerStatus.Started;
                log.Info("Demo tracker is started.");
            }
        }

        public override void Stop()
        {
            if (status == TrackerStatus.Started)
            {
                StopTimer();
                status = TrackerStatus.Ready;
                log.Info("Demo tracker is stopped.");
            }
        }

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            base.Initialize(name, config);

            screenWidth = GetIntConfigValue(config, "screenWidth", 1920);
            screenHeight = GetIntConfigValue(config, "screenHeight", 1080);
            status = TrackerStatus.Ready;

            TrackerSection section = (TrackerSection)ConfigurationManager
                .GetSection("tracker");
            recordingFolder = section.RecordingFolder;

            dataGenerationTimer = new System.Threading.Timer(
                new TimerCallback(GenerateData), null, Timeout.Infinite, 
                Timeout.Infinite);
        }
        #endregion
    }
}
