﻿//
// Thinkbox.Rams.Device.SaccadeType
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    public enum SaccadeType
    {
        Fixed,
        Retrograde
    }
}
