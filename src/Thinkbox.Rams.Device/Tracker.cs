﻿//
// Thinkbox.Rams.Device.Tracker
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Configuration;
using Thinkbox.Rams.Configuration;

namespace Thinkbox.Rams.Device
{
    public static class Tracker
    {
        #region VARIABLES
        #endregion

        #region PROPERTIES
        static TrackerProviderCollection providers;
        public static TrackerProviderCollection Providers
        {
            get { return providers; }
        }

        static TrackerProvider provider;
        public static TrackerProvider Provider
        {
            get { return provider; }
        }

        public static TrackerStatus Status
        {
            get { return Provider.Status; }
        }

        private static string recordingFolder;
        public static string RecordingFolder
        {
            get { return recordingFolder; }
        }
        #endregion

        #region CONSTRUCTORS
        static Tracker()
        {
            TrackerSection section = (TrackerSection) ConfigurationManager
                .GetSection("tracker");

            providers = new TrackerProviderCollection();
            System.Web.Configuration.ProvidersHelper.InstantiateProviders(
                section.Providers, providers, typeof(TrackerProvider));

            provider = providers[section.DefaultProvider];

            recordingFolder = section.RecordingFolder.ToString();
        }
        #endregion

        #region ABSTRACT METHODS
        #endregion

        #region VIRTUAL METHODS
        #endregion

        #region EVENTS
        public static event GazeUpdateEventHandler GazeUpdate
        {
            add { Provider.GazeUpdate += value; }
            remove { Provider.GazeUpdate -= value; }
        }
        #endregion
    }
}
