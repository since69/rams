﻿//
// Thinkbox.Rams.Device.TrackerHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    sealed class TrackerHelper : ITrackerHelper
    {
        #region PROPERTIES
        public TrackerProviderCollection Providers
        {
            get { return Tracker.Providers; }
        }
        #endregion
    }
}
