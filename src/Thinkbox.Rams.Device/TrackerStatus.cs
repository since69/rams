﻿//
// Thinkbox.Rams.Device.TrackerStatus
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    public enum TrackerStatus
    {
        Ready,
        NotReady,
        Paused,
        Started,
    }
}
