﻿//
// Thinkbox.Rams.Device.MeasureEventArgs
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    public sealed class GazeUpdateEventArgs : System.EventArgs
    {
        #region PROPERTIES
        private GazeData gazeData;
        public GazeData GazeData
        {
            get { return gazeData; }
        }
        #endregion

        #region CONSTRUCTORS
        public GazeUpdateEventArgs(GazeData gazeData)
        {
            this.gazeData = gazeData;
        }
        #endregion
    }
}
