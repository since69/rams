﻿//
// Thinkbox.Rams.Device.DataAnalyzer
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Generic;
using log4net;

namespace Thinkbox.Rams.Device
{
    public class DataAnalyzer
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(DataAnalyzer));

        // Dcoument size (A5 (210x148)  base).
        private static readonly decimal docWidth = 190.0m;
        //private static readonly decimal docHeight = 126.0m;
        // Margin (T:45, L:26, R:18)
        private static readonly decimal leftMargin = 7.0m;
        private static readonly decimal rightMargin = 7.0m;
        private static readonly decimal topMargin = 3.0m;
        // 나눔고딕 10pt.
        private static decimal charWidth = 3.5m;
        private static decimal charHeight = 3.1m;
        private static decimal lineSpacing = 2.0m;
        private static decimal lineWidth = docWidth - (leftMargin + rightMargin);
        
        // 지문의 넓이와 높이.
        private static decimal textWidth = 0;
        private static decimal textHeight = 0;

        private static List<GazeData> records = new List<GazeData>();
        private static List<LineCoordinate> lineCoordinates = new List<LineCoordinate>();

        private static int corrationLimit = 10;
        private static int fixationLimit = 190;
        private static decimal returnSweepLimit = 150.0m;
        #endregion
        
        #region METHODS
        public static int GetNumOfLines(string text)
        {
            int numOfLines = 0;

            if (text != null)
                numOfLines = text.Split('\n').Length;

            return numOfLines;
        }

        public static int GetNumOfWords(string text)
        {
            //var text = richTextBox1.Text.Trim();
            //int wordCount = 0, index = 0;

            //while (index < text.Length)
            //{
            //    // check if current char is part of a word
            //    while (index < text.Length && !char.IsWhiteSpace(text[index]))
            //        index++;

            //    wordCount++;

            //    // skip whitespace until next word
            //    while (index < text.Length && char.IsWhiteSpace(text[index]))
            //        index++;
            //}

            int index = 0;
            int nums = 0;

            while (index < text.Length)
            {
                while (index < text.Length && !char.IsWhiteSpace(text[index]))
                    index++;

                nums++;

                while (index < text.Length && char.IsWhiteSpace(text[index]))
                    index++;
            }

            return nums;
        }

        /// <summary>
        /// 안구의 움직임을 기록한 파일을 분석한 결과를 반환.
        /// </summary>
        public static TestResult Analyze(decimal width, decimal height, 
            decimal spacing, string text, List<GazeData> rawRecords)
        {
            TestResult result = new TestResult();
            charWidth = width;
            charHeight = height;
            lineSpacing = spacing;

            ParseTestText(text, result);
            
            records.Clear();
            records = LeachRecords(rawRecords);
            GazeData record = records[0];
            GazeData prevRecord = records[0];
            AddRecord(result.Records, "STR", 
                GetLineNumber(record), record);
            for (int i = 1; i < records.Count - 2; i++)
            {
                record = records[i];
                if ((record.LeftX > 0) && (record.RightX > 0))
                {
                    AddRecord(result.Records, 
                        IsReturnSweep(record, prevRecord) ? "RSW" : string.Empty, 
                        GetLineNumber(record), record);
                    prevRecord = record;
                }
            }
            record = records[records.Count - 1];
            AddRecord(result.Records, "ETR", 
                GetLineNumber(record), record);
            
            GetSaccades(result);
            GetFixationWidth(result);
            GetReadingRate(result);

            return result;
        }

        private static void ParseTestText(string text, TestResult result)
        {
            if (text == null) return;

            string[] lines = text.Split('\n');
            result.NumOfLines = lines.Length;
            result.NumOfWords = GetNumOfWords(text);

            // get the line coordinate of each line.
            for (int i = 0; i < result.NumOfLines; i++)
            {
                lineCoordinates.Add(new LineCoordinate()
                {
                    LineNo = i + 1,
                    XCoordinate = leftMargin,
                    YCoordinate = topMargin + i * (charHeight + lineSpacing)
                });
            }

            // get the width of the last line.
            decimal lastLineLength = lines[result.NumOfLines - 1].Length * charWidth;
            Console.WriteLine("Last line length is {0}\n\n", lastLineLength);
            textWidth = lineWidth * (result.NumOfLines - 1) + lastLineLength;
            textHeight = result.NumOfLines * (charHeight + lineSpacing);
        }

        private static void GetFixationWidth(TestResult result)
        {
            int firstElapsed = int.MinValue;
            int lastElapsed = int.MinValue;

            foreach (AnalyzedGazeData record in result.Records)
            {
                if (record.CalculatedLine.CompareTo("STR") == 0)
                    firstElapsed = record.RawData.Elapsed;
                if (record.CalculatedLine.CompareTo("ETR") == 0)
                    lastElapsed = record.RawData.Elapsed;
            }

            if (firstElapsed != int.MinValue && lastElapsed != int.MinValue)
            {
                int elapsed = lastElapsed - firstElapsed;
                result.FixationWidth = textWidth / elapsed * 1000;
            }
            else
                result.FixationWidth = 0;
        }

        private static int GetLineNumber(GazeData record)
        {
            if (record.LeftY < topMargin || 
                record.LeftY > topMargin + textHeight)
                return 0;

            int lineNo = 1;
            decimal prevOffset = topMargin;
            foreach (LineCoordinate c in lineCoordinates)
            {
                decimal offset = Math.Abs(record.LeftY - c.YCoordinate);
                if (offset < prevOffset)
                {
                    prevOffset = offset;
                    lineNo = c.LineNo;
                }
            }
            return lineNo;
        }

        private static void GetReadingRate(TestResult result)
        {
            if (result.NumOfWords == 0)
            {
                log.Debug("Reading rate was not calculated because the number of words was zero.");
                return;
            }

            int firstElapsed = 0;
            int lastElapsed = 0;

            foreach (AnalyzedGazeData record in result.Records)
            {
                if (record.CalculatedLine.CompareTo("STR") == 0)
                    firstElapsed = record.RawData.Elapsed;
                if (record.CalculatedLine.CompareTo("ETR") == 0)
                    lastElapsed = record.RawData.Elapsed;
            }

            if (firstElapsed != 0 && lastElapsed != 0)
            {
                int elapsed = lastElapsed - firstElapsed;
                result.ReadingRate = (decimal)result.NumOfWords * 60000 / elapsed;
            }
            else
            {
                log.Debug("Incorrect data at the time of the first record or last record.");
                result.ReadingRate = 0;
            }
        }

        private static bool IsReturnSweep(GazeData curRecord, GazeData prevRecord)
        {
            if (Math.Abs(curRecord.LeftX - prevRecord.LeftX) >= returnSweepLimit
                || Math.Abs(curRecord.RightX - prevRecord.RightX) >= returnSweepLimit)
                return true;
            return false;
        }

        private static void GetSaccades(TestResult result)
        {
            AnalyzedGazeData prevData = null;

            foreach (AnalyzedGazeData record in result.Records)
            {
                if (prevData == null)
                {
                    prevData = record;
                    continue;
                }

                // get return sweeps.
                if (IsReturnSweep(record.RawData, prevData.RawData))
                    ++result.NumOfUsedLines;

                // get num of correlation.
                if (Math.Abs(record.RawData.RightX - record.RawData.LeftX) <= corrationLimit)
                    ++result.CrossCorrelation;

                // get num of fixations & fixations duration.
                if (prevData.RawData.LeftX == record.RawData.LeftX)
                {
                    if (record.RawData.Elapsed - prevData.RawData.Elapsed > fixationLimit)
                    {
                        ++result.NumOfFixations[0];
                        result.FixationDuration.Left += (record.RawData.Elapsed -
                            prevData.RawData.Elapsed);
                    }
                }
                if (prevData.RawData.RightX == record.RawData.RightX)
                {
                    if (record.RawData.Elapsed - prevData.RawData.Elapsed > fixationLimit)
                    {
                        ++result.NumOfFixations[1];
                        result.FixationDuration.Right += (record.RawData.Elapsed -
                            prevData.RawData.Elapsed);
                    }
                }

                // get num of regression.
                if (prevData.RawData.LeftX > record.RawData.LeftX)
                    ++result.NumOfRegression.Left;
                if (prevData.RawData.RightX > record.RawData.RightX)
                    ++result.NumOfRegression.Right;

                prevData = record;
            }

            if (result.NumOfWords != 0)
            {
                result.NumOfFixations[0] = result.NumOfFixations[0] * 100 /
                    result.NumOfWords;
                result.NumOfFixations[1] = result.NumOfFixations[1] * 100 /
                    result.NumOfWords;
                result.FixationDuration.Left = result.NumOfFixations[0] == 0 ? 0 : 
                    result.FixationDuration.Left / result.NumOfFixations[0];
                result.FixationDuration.Right = result.NumOfFixations[1] == 0 ? 0 : 
                    result.FixationDuration.Right / result.NumOfFixations[1];
            }
            else
            {
                log.Debug("Fixations was not calculated because the number of words was zero.");
                result.NumOfFixations[0] = 0;
                result.NumOfFixations[1] = 0;
                result.FixationDuration.Left = 0;
                result.FixationDuration.Right = 0;
            }

            if (result.Records.Count != 0)
            {
                result.CrossCorrelation = result.CrossCorrelation / 
                    result.Records.Count;
            }
            else
            {
                log.Debug("Cross Correlation was not calculated because the number of records was zero.");
                result.CrossCorrelation = 0;
            }
        }
        
        private static void AddRecord(List<AnalyzedGazeData> records, 
            string calculatedLineResult, int lineNumber, GazeData record)
        {
            records.Add(new AnalyzedGazeData()
            {
                CalculatedLine = calculatedLineResult,
                LineNumber = lineNumber,
                RawData = new GazeData
                {
                    Elapsed = record.Elapsed,
                    AverageX = record.AverageX,
                    AverageY = record.AverageY,
                    LeftX = record.LeftX,
                    LeftY = record.LeftY,
                    RightX = record.RightX,
                    RightY = record.RightY
                }
            });
        }

        /// <summary>
        /// 진행 시간만 (Elapsed) 다르고 나머지 값들이 일치하는 자료를
        /// 첫 번째 자료만을 취해서 목록으로 전달.
        /// </summary>
        /// <param name="rawRecords">원본 자료 목록</param>
        /// <returns>필터링 된 자료 목록</returns>
        private static List<GazeData> LeachRecords(List<GazeData> rawRecords)
        {
            List<GazeData> records = new List<GazeData>();
            GazeData prevRecord = null;

            foreach (GazeData record in rawRecords)
            {
                if (prevRecord == null)
                {
                    prevRecord = record;
                    records.Add(record);
                    continue;
                }

                if (prevRecord.LeftX != record.LeftX || 
                    prevRecord.LeftY != record.LeftY ||
                    prevRecord.RightX != record.RightX ||
                    prevRecord.RightY != record.RightY)
                {
                    records.Add(record);
                }
                
                prevRecord = record;
            }
            return records;
        }
        #endregion

        #region INTERNAL CLASS
        public class LineCoordinate
        {
            public int LineNo { get; set; }
            public decimal XCoordinate { get; set; }
            public decimal YCoordinate { get; set; }

            public LineCoordinate() {}
        }
        #endregion
    }
}
