﻿//
// Thinkbox.Rams.Device.GazeUpdateEventHandler
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.Device
{
    public delegate void GazeUpdateEventHandler(object sender, GazeUpdateEventArgs e);
}
