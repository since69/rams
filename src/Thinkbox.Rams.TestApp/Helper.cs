﻿//
// Thinkbox.Rams.TestApp
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Thinkbox.Rams.Device;

namespace Thinkbox.Rams.TestApp
{
    public class Helper
    {
        //
        // AssemblyhInfo methods.
        //

        public static string GetVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection
                .Assembly.GetExecutingAssembly();
            Version version = assembly.GetName().Version;
            return version.ToString();
        }

        public static string GetAppName()
        {
            return string.Format("PICKING SIMULATOR {0}",
                GetVersion());
        }


        //
        // Console methods.
        // 

        public static void WriteMessageWithSleep(string msg, int interval)
        {
            Console.WriteLine(msg);
            Thread.Sleep(interval);
        }

        public static void WriteError(string msg)
        {
            Console.WriteLine("*** ERROR: {0} ***", msg);
        }

        public static void WriteAppName()
        {
            Console.WriteLine(GetAppName());
        }

        public static void WriteAppTerminated()
        {
            Console.WriteLine("\n\n");
            WritePressAnyKey();
            Console.WriteLine("{0} IS TERMINATED     \n", GetAppName());
        }

        public static void WriteSingleLine()
        {
            Console.WriteLine("--------------------------------------------------------------------------------");
        }

        public static void WritePressAnyKey()
        {
            Console.Write("PRESS ANY KEY TO CONTINUE");
            //Console.CursorVisible = false;
            Console.ReadKey();
            //Console.CursorVisible = true;
        }
    }
}
