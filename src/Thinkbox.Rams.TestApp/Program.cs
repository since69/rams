﻿//
// Thinkbox.Rams.TestApp
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using GTWrapper;
using Thinkbox.Rams.Common;
using Thinkbox.Rams.Configuration;
using Thinkbox.Rams.Device;

namespace Thinkbox.Rams.TestApp
{
    class Program
    {
        #region VARIABLES
        #endregion

        #region METHODS
        static void DoCalibration()
        {
            Console.Clear();
            Tracker.Provider.Calibrate();
            Helper.WritePressAnyKey();
        }

        static void DoTracking()
        {
            Console.Clear();
            Console.WriteLine("PRESS 'X' KEY TO STOP MEASURING.\n");

            Tracker.Provider.Start();
            char key = Console.ReadKey().KeyChar;
            if (key == 'x' || key == 'X')
            {
                Tracker.Provider.Stop();
                Helper.WritePressAnyKey();
            }
        }
        #endregion

        #region CONSTRUCTORS
        static void Main(string[] args)
        {
            Tracker.GazeUpdate +=
                new GazeUpdateEventHandler(OnGazeUpdate);

            Console.Clear();

            bool esc = false;
            do
            {
                switch (Menu.Draw())
                {
                    case 0: DoCalibration(); break;
                    case 1: DoTracking(); break;
                    default: esc = true; break;
                }
            } while (!esc);
           
            Helper.WriteAppTerminated();
        }
        #endregion

       static void OnGazeUpdate(object sender, GazeUpdateEventArgs args)
       {
            GazeData data = args.GazeData;
            Console.WriteLine(
                "E:{0,5}, A:{1},{2}, L:{3},{4}, R:{5},{6}",
                data.Elapsed, data.AverageX, data.AverageY,
                data.LeftX, data.LeftY, data.RightX, data.RightY);
       }
    }
}
