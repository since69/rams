﻿//
// Thinkbox.Rams.TestApp.Menu
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;

namespace Thinkbox.Rams.TestApp
{
    public class Menu
    {
        #region VARIABLES
        static readonly string[] mainMenuItems = new string[]
        {
            "CALIBRATION", "TRACKING", "EXIT"
        };
        static int selectedItem = 0;
        #endregion

        private static int DrawMenu(string[] items)
        {
            bool esc = false;
            do
            {
                Helper.WriteAppName();
                Console.WriteLine();

                for (int i = 0; i < items.Length; i++)
                {
                    if (selectedItem == i)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(items[i]);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine(items[i]);
                    }
                }

                ConsoleKeyInfo cki = Console.ReadKey();
                switch (cki.Key)
                {
                    case ConsoleKey.UpArrow:
                        if ((selectedItem - 1) < 0)
                            selectedItem = items.Length - 1;
                        else
                            selectedItem--;
                        break;
                    case ConsoleKey.DownArrow:
                        if ((selectedItem + 1) > items.Length - 1)
                            selectedItem = 0;
                        else
                            selectedItem++;
                        break;
                    case ConsoleKey.Enter:
                        esc = true;
                        break;
                }
                Console.Clear();
            } while (!esc);

            return selectedItem;
        }

        public static int Draw()
        {
            Console.Clear();
            Console.CursorVisible = false;
            int rc = DrawMenu(mainMenuItems);
            Console.CursorVisible = true;
            return rc;
        }
    }
}
