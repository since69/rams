﻿//
// Thinkbox.Rams.Configuration.GeneralElement
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Configuration;

namespace Thinkbox.Rams.Configuration
{
    public class GeneralElement : ConfigurationElement
    {
        #region PROPERTIES
        [ConfigurationProperty("culture", DefaultValue = "en-US")]
        public string Culture
        {
            get { return (string)base["culture"]; }
        }
        #endregion
    }
}
