﻿//
// Thinkbox.Rams.Configuration.ReadAlyzerSection
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Configuration;


namespace Thinkbox.Rams.Configuration
{
    /// <summary>
    /// Configuration managemnet class.
    /// </summary>
    public class ReadAlyzerSection : ConfigurationSection
    {
        #region PROPOERTIES
        [ConfigurationProperty("general", IsRequired = true)]
        public GeneralElement General
        {
            get { return (GeneralElement)base["general"]; }
        }
        #endregion
    }

}
