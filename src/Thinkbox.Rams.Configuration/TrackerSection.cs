﻿//
// Thinkbox.Rams.Configuration.TrackerSetion
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Configuration;

namespace Thinkbox.Rams.Configuration
{
    /// <summary>
    /// Configuration managemnet class.
    /// </summary>
    public sealed class TrackerSection : ConfigurationSection
    {
        #region PROPOERTIES
        private static ConfigurationPropertyCollection properties;
        protected override ConfigurationPropertyCollection Properties
        {
            get { return properties; }
        }
        
        private static readonly ConfigurationProperty propProviders;
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get { return (ProviderSettingsCollection)base[propProviders]; }
        }

        private static readonly ConfigurationProperty propDefaultProvider;
        [ConfigurationProperty("defaultProvider", DefaultValue ="DemoTrackerProvider")]
        [StringValidator(MinLength = 1)]
        public string DefaultProvider
        {
            get { return (string)base[propDefaultProvider]; }
            set { base[propDefaultProvider] = value; }
        }

        private static readonly ConfigurationProperty propRecordingFolder;
        [ConfigurationProperty("recordingFolder", DefaultValue = "Recordings")]
        [StringValidator(MinLength = 1)]
        public string RecordingFolder
        {
            get { return (string)base[propRecordingFolder]; }
            set { base[propRecordingFolder] = value; }
        }
        #endregion

        #region CONSTRUCTORS
        static TrackerSection()
        {
            // Property initialization
            propProviders = new ConfigurationProperty("providers",
                typeof(ProviderSettingsCollection), null, 
                ConfigurationPropertyOptions.None);
            propDefaultProvider = new ConfigurationProperty("defaultProvider",
                typeof(string), "DemoTrackerProvider", null,
                StdValidatorsAndConverters.NonEmptyStringValidator,
                ConfigurationPropertyOptions.None);
            propRecordingFolder = new ConfigurationProperty("recordingFolder",
                typeof(string), "Recordings", null,
                StdValidatorsAndConverters.NonEmptyStringValidator,
                ConfigurationPropertyOptions.None);

            properties = new ConfigurationPropertyCollection();
            properties.Add(propProviders);
            properties.Add(propDefaultProvider);
            properties.Add(propRecordingFolder);
        }

        public TrackerSection() { }
        #endregion
    }
}
