﻿//
// Thinkbox.Rams.Configuration
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Configuration;
using log4net;

namespace Thinkbox.Rams.Configuration
{
    public static class Configurator
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Configurator));
        #endregion

        #region PROPERTIES
        private static ReadAlyzerSection readAlyzerSettings = null;
        public static ReadAlyzerSection ReadAlyzerSettings
        {
            get { return readAlyzerSettings; }
        }
        #endregion

        #region CONSTRUCTORS
        static Configurator()
        {
            Configure();
        }
        #endregion

        #region METHODS
        private static bool Configure()
        {
            try
            {
                object section = ConfigurationManager.GetSection("readAlyzer");
                readAlyzerSettings = (ReadAlyzerSection)section;
            }
            catch (ConfigurationException ex)
            {
                string msg = string.Format("Configuration error. Original message is {0}",
                    ex.Message);
                log.Error(msg);
                return false;
            }
            return true;
        }
        #endregion
    }
}
