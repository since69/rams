﻿namespace Thinkbox.Rams.ReadAlyzer
{
    public class Enums
    {
        public enum Gender { Male, Female }

        public enum NameStyle { Western, Eastern }
    }
}
