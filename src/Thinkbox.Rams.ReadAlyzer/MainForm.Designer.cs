﻿namespace Thinkbox.Rams.ReadAlyzer
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            this.rcMain = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.bvcMain = new DevExpress.XtraBars.Ribbon.BackstageViewControl();
            this.bvccInfo = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.pageHelp = new Thinkbox.Rams.ReadAlyzer.Controls.HelpPage();
            this.bvccPrint = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.pagePrint = new Thinkbox.Rams.ReadAlyzer.Controls.PrintPage();
            this.bvccExport = new DevExpress.XtraBars.Ribbon.BackstageViewClientControl();
            this.pageExport = new Thinkbox.Rams.ReadAlyzer.Controls.ExportPage();
            this.bvtiInfo = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.bvbiSaveAs = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.bvtiPrint = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.bvtiExport = new DevExpress.XtraBars.Ribbon.BackstageViewTabItem();
            this.bvbiExit = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.backstageViewItemSeparator1 = new DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator();
            this.bvbiOption = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.bsiInfo = new DevExpress.XtraBars.BarStaticItem();
            this.bbiNormal = new DevExpress.XtraBars.BarButtonItem();
            this.bbiReading = new DevExpress.XtraBars.BarButtonItem();
            this.beiZoom = new DevExpress.XtraBars.BarEditItem();
            this.riztbMain = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.bbiReminder = new DevExpress.XtraBars.BarButtonItem();
            this.bsiTemp = new DevExpress.XtraBars.BarStaticItem();
            this.bbiPrintPreview = new DevExpress.XtraBars.BarButtonItem();
            this.bsiNavigation = new DevExpress.XtraBars.BarSubItem();
            this.bbiNewTrainee = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNewTrainer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditTrainer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditTrainee = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEditRecord = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteRecord = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteTrainee = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteTrainer = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiRecordsCurrentView = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.bbiNewTest = new DevExpress.XtraBars.BarButtonItem();
            this.bbiStartTest = new DevExpress.XtraBars.BarButtonItem();
            this.bbiStopTest = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCancelTest = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalysisTest = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalysisRecord = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrintRecord = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSimulateRecord = new DevExpress.XtraBars.BarButtonItem();
            this.rpTest = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgTest = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpRecords = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgRecords = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCvRecords = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpTrainees = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgTrainees = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpTrainers = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgTrainers = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpText = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgCvTexts = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpView = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgNavigation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgPrint = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rsbMain = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.nbcMain = new DevExpress.XtraNavBar.NavBarControl();
            this.nbgModules = new DevExpress.XtraNavBar.NavBarGroup();
            this.nbiTest = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiRecord = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiTrainee = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiTrainer = new DevExpress.XtraNavBar.NavBarItem();
            this.nbiText = new DevExpress.XtraNavBar.NavBarItem();
            this.pcMain = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.rcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bvcMain)).BeginInit();
            this.bvcMain.SuspendLayout();
            this.bvccInfo.SuspendLayout();
            this.bvccPrint.SuspendLayout();
            this.bvccExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.riztbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcMain)).BeginInit();
            this.pcMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // rcMain
            // 
            this.rcMain.ApplicationButtonDropDownControl = this.bvcMain;
            this.rcMain.ExpandCollapseItem.Id = 0;
            this.rcMain.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rcMain.ExpandCollapseItem,
            this.bsiInfo,
            this.bbiNormal,
            this.bbiReading,
            this.beiZoom,
            this.bbiReminder,
            this.bsiTemp,
            this.bbiPrintPreview,
            this.bsiNavigation,
            this.bbiNewTrainee,
            this.bbiNewTrainer,
            this.bbiEditTrainer,
            this.bbiEditTrainee,
            this.bbiEditRecord,
            this.bbiDeleteRecord,
            this.bbiDeleteTrainee,
            this.bbiDeleteTrainer,
            this.rgbiRecordsCurrentView,
            this.bbiNewTest,
            this.bbiStartTest,
            this.bbiStopTest,
            this.bbiCancelTest,
            this.bbiAnalysisTest,
            this.bbiAnalysisRecord,
            this.bbiPrintRecord,
            this.bbiSimulateRecord});
            resources.ApplyResources(this.rcMain, "rcMain");
            this.rcMain.MaxItemId = 7;
            this.rcMain.Name = "rcMain";
            this.rcMain.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpTest,
            this.rpRecords,
            this.rpTrainees,
            this.rpTrainers,
            this.rpText,
            this.rpView});
            this.rcMain.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riztbMain});
            this.rcMain.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.rcMain.StatusBar = this.rsbMain;
            this.rcMain.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // bvcMain
            // 
            this.bvcMain.Controls.Add(this.bvccInfo);
            this.bvcMain.Controls.Add(this.bvccPrint);
            this.bvcMain.Controls.Add(this.bvccExport);
            this.bvcMain.Items.Add(this.bvtiInfo);
            this.bvcMain.Items.Add(this.bvbiSaveAs);
            this.bvcMain.Items.Add(this.bvtiPrint);
            this.bvcMain.Items.Add(this.bvtiExport);
            this.bvcMain.Items.Add(this.bvbiExit);
            this.bvcMain.Items.Add(this.backstageViewItemSeparator1);
            this.bvcMain.Items.Add(this.bvbiOption);
            resources.ApplyResources(this.bvcMain, "bvcMain");
            this.bvcMain.Name = "bvcMain";
            this.bvcMain.OwnerControl = this.rcMain;
            this.bvcMain.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvcMain_ItemClick);
            // 
            // bvccInfo
            // 
            this.bvccInfo.Controls.Add(this.pageHelp);
            resources.ApplyResources(this.bvccInfo, "bvccInfo");
            this.bvccInfo.Name = "bvccInfo";
            // 
            // pageHelp
            // 
            this.pageHelp.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.pageHelp, "pageHelp");
            this.pageHelp.ForeColor = System.Drawing.Color.Transparent;
            this.pageHelp.Name = "pageHelp";
            // 
            // bvccPrint
            // 
            this.bvccPrint.Controls.Add(this.pagePrint);
            resources.ApplyResources(this.bvccPrint, "bvccPrint");
            this.bvccPrint.Name = "bvccPrint";
            // 
            // pagePrint
            // 
            this.pagePrint.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pagePrint, "pagePrint");
            this.pagePrint.ForeColor = System.Drawing.Color.Transparent;
            this.pagePrint.Name = "pagePrint";
            // 
            // bvccExport
            // 
            this.bvccExport.Controls.Add(this.pageExport);
            resources.ApplyResources(this.bvccExport, "bvccExport");
            this.bvccExport.Name = "bvccExport";
            // 
            // pageExport
            // 
            this.pageExport.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pageExport, "pageExport");
            this.pageExport.ForeColor = System.Drawing.Color.Transparent;
            this.pageExport.Name = "pageExport";
            // 
            // bvtiInfo
            // 
            resources.ApplyResources(this.bvtiInfo, "bvtiInfo");
            this.bvtiInfo.ContentControl = this.bvccInfo;
            this.bvtiInfo.Name = "bvtiInfo";
            // 
            // bvbiSaveAs
            // 
            resources.ApplyResources(this.bvbiSaveAs, "bvbiSaveAs");
            this.bvbiSaveAs.Name = "bvbiSaveAs";
            // 
            // bvtiPrint
            // 
            resources.ApplyResources(this.bvtiPrint, "bvtiPrint");
            this.bvtiPrint.ContentControl = this.bvccPrint;
            this.bvtiPrint.Name = "bvtiPrint";
            this.bvtiPrint.SelectedChanged += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvtiPrint_SelectedChanged);
            // 
            // bvtiExport
            // 
            resources.ApplyResources(this.bvtiExport, "bvtiExport");
            this.bvtiExport.ContentControl = this.bvccExport;
            this.bvtiExport.Name = "bvtiExport";
            // 
            // bvbiExit
            // 
            resources.ApplyResources(this.bvbiExit, "bvbiExit");
            this.bvbiExit.Glyph = ((System.Drawing.Image)(resources.GetObject("bvbiExit.Glyph")));
            this.bvbiExit.Name = "bvbiExit";
            this.bvbiExit.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvbiExit_ItemClick);
            // 
            // backstageViewItemSeparator1
            // 
            this.backstageViewItemSeparator1.Name = "backstageViewItemSeparator1";
            // 
            // bvbiOption
            // 
            resources.ApplyResources(this.bvbiOption, "bvbiOption");
            this.bvbiOption.Name = "bvbiOption";
            this.bvbiOption.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.bvbiOption_ItemClick);
            // 
            // bsiInfo
            // 
            this.bsiInfo.Id = 2;
            this.bsiInfo.Name = "bsiInfo";
            // 
            // bbiNormal
            // 
            this.bbiNormal.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiNormal.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            resources.ApplyResources(this.bbiNormal, "bbiNormal");
            this.bbiNormal.Id = 3;
            this.bbiNormal.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NormalLayout_16x16;
            this.bbiNormal.Name = "bbiNormal";
            this.bbiNormal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNormal_ItemClick);
            // 
            // bbiReading
            // 
            this.bbiReading.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiReading.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiReading.Id = 4;
            this.bbiReading.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ReadingLayout_16x16;
            this.bbiReading.Name = "bbiReading";
            this.bbiReading.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReading_ItemClick);
            // 
            // beiZoom
            // 
            this.beiZoom.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.beiZoom.Edit = this.riztbMain;
            this.beiZoom.EditValue = 10;
            resources.ApplyResources(this.beiZoom, "beiZoom");
            this.beiZoom.Id = 5;
            this.beiZoom.Name = "beiZoom";
            // 
            // riztbMain
            // 
            this.riztbMain.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.riztbMain.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.riztbMain.Maximum = 19;
            this.riztbMain.Minimum = 1;
            this.riztbMain.Name = "riztbMain";
            // 
            // bbiReminder
            // 
            this.bbiReminder.Id = 6;
            this.bbiReminder.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.bbiReminder.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Reminder_16x16;
            this.bbiReminder.Name = "bbiReminder";
            // 
            // bsiTemp
            // 
            this.bsiTemp.Id = 7;
            this.bsiTemp.Name = "bsiTemp";
            // 
            // bbiPrintPreview
            // 
            resources.ApplyResources(this.bbiPrintPreview, "bbiPrintPreview");
            this.bbiPrintPreview.Id = 8;
            this.bbiPrintPreview.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Preview_16x16;
            this.bbiPrintPreview.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Preview_32x32;
            this.bbiPrintPreview.Name = "bbiPrintPreview";
            this.bbiPrintPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintPreview_ItemClick);
            // 
            // bsiNavigation
            // 
            resources.ApplyResources(this.bsiNavigation, "bsiNavigation");
            this.bsiNavigation.Id = 9;
            this.bsiNavigation.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Compass_16x16;
            this.bsiNavigation.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Compass_32x32;
            this.bsiNavigation.Name = "bsiNavigation";
            // 
            // bbiNewTrainee
            // 
            resources.ApplyResources(this.bbiNewTrainee, "bbiNewTrainee");
            this.bbiNewTrainee.Id = 11;
            this.bbiNewTrainee.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NewTrainee_16x16;
            this.bbiNewTrainee.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NewTrainee_32x32;
            this.bbiNewTrainee.Name = "bbiNewTrainee";
            // 
            // bbiNewTrainer
            // 
            resources.ApplyResources(this.bbiNewTrainer, "bbiNewTrainer");
            this.bbiNewTrainer.Id = 12;
            this.bbiNewTrainer.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NewTrainer_16x16;
            this.bbiNewTrainer.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NewTrainer_32x32;
            this.bbiNewTrainer.Name = "bbiNewTrainer";
            // 
            // bbiEditTrainer
            // 
            resources.ApplyResources(this.bbiEditTrainer, "bbiEditTrainer");
            this.bbiEditTrainer.Id = 13;
            this.bbiEditTrainer.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.EditTrainer_16x16;
            this.bbiEditTrainer.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.EditTrainer_32x32;
            this.bbiEditTrainer.Name = "bbiEditTrainer";
            // 
            // bbiEditTrainee
            // 
            resources.ApplyResources(this.bbiEditTrainee, "bbiEditTrainee");
            this.bbiEditTrainee.Id = 14;
            this.bbiEditTrainee.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.EditTrainee_16x16;
            this.bbiEditTrainee.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.EditTrainee_32x32;
            this.bbiEditTrainee.Name = "bbiEditTrainee";
            // 
            // bbiEditRecord
            // 
            resources.ApplyResources(this.bbiEditRecord, "bbiEditRecord");
            this.bbiEditRecord.Id = 15;
            this.bbiEditRecord.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.EditRecord_16x16;
            this.bbiEditRecord.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.EditRecord_32x32;
            this.bbiEditRecord.Name = "bbiEditRecord";
            // 
            // bbiDeleteRecord
            // 
            resources.ApplyResources(this.bbiDeleteRecord, "bbiDeleteRecord");
            this.bbiDeleteRecord.Id = 16;
            this.bbiDeleteRecord.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.DeleteRecord_16x16;
            this.bbiDeleteRecord.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.DeleteRecord_32x32;
            this.bbiDeleteRecord.Name = "bbiDeleteRecord";
            // 
            // bbiDeleteTrainee
            // 
            resources.ApplyResources(this.bbiDeleteTrainee, "bbiDeleteTrainee");
            this.bbiDeleteTrainee.Id = 17;
            this.bbiDeleteTrainee.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.DeleteTrainee_16x16;
            this.bbiDeleteTrainee.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.DeleteTrainee_32x32;
            this.bbiDeleteTrainee.Name = "bbiDeleteTrainee";
            // 
            // bbiDeleteTrainer
            // 
            resources.ApplyResources(this.bbiDeleteTrainer, "bbiDeleteTrainer");
            this.bbiDeleteTrainer.Id = 18;
            this.bbiDeleteTrainer.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.DeleteTrainer_16x16;
            this.bbiDeleteTrainer.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.DeleteTrainer_32x32;
            this.bbiDeleteTrainer.Name = "bbiDeleteTrainer";
            // 
            // rgbiRecordsCurrentView
            // 
            resources.ApplyResources(this.rgbiRecordsCurrentView, "rgbiRecordsCurrentView");
            // 
            // 
            // 
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Disabled.Options.UseFont = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.rgbiRecordsCurrentView.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiRecordsCurrentView.Gallery.ColumnCount = 4;
            galleryItem1.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Calendar_32x32;
            galleryItem1.Tag = "RecordsDate";
            galleryItem2.Checked = true;
            galleryItem2.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainee_32x32;
            galleryItem2.Tag = "RecordsTrainee";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2});
            this.rgbiRecordsCurrentView.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.rgbiRecordsCurrentView.Gallery.ImageSize = new System.Drawing.Size(32, 32);
            this.rgbiRecordsCurrentView.Gallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            this.rgbiRecordsCurrentView.Gallery.RowCount = 1;
            this.rgbiRecordsCurrentView.Gallery.ShowItemText = true;
            this.rgbiRecordsCurrentView.Id = 19;
            this.rgbiRecordsCurrentView.Name = "rgbiRecordsCurrentView";
            // 
            // bbiNewTest
            // 
            resources.ApplyResources(this.bbiNewTest, "bbiNewTest");
            this.bbiNewTest.Id = 20;
            this.bbiNewTest.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NewTest_16x16;
            this.bbiNewTest.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.NewTest_32x32;
            this.bbiNewTest.Name = "bbiNewTest";
            // 
            // bbiStartTest
            // 
            resources.ApplyResources(this.bbiStartTest, "bbiStartTest");
            this.bbiStartTest.Id = 21;
            this.bbiStartTest.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.StartTest_16x16;
            this.bbiStartTest.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.StartTest_32x32;
            this.bbiStartTest.Name = "bbiStartTest";
            // 
            // bbiStopTest
            // 
            resources.ApplyResources(this.bbiStopTest, "bbiStopTest");
            this.bbiStopTest.Id = 22;
            this.bbiStopTest.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.StopTest_16x16;
            this.bbiStopTest.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.StopTest_32x32;
            this.bbiStopTest.Name = "bbiStopTest";
            // 
            // bbiCancelTest
            // 
            resources.ApplyResources(this.bbiCancelTest, "bbiCancelTest");
            this.bbiCancelTest.Id = 23;
            this.bbiCancelTest.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.CancelTest_16x16;
            this.bbiCancelTest.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.CancelTest_32x32;
            this.bbiCancelTest.Name = "bbiCancelTest";
            // 
            // bbiAnalysisTest
            // 
            resources.ApplyResources(this.bbiAnalysisTest, "bbiAnalysisTest");
            this.bbiAnalysisTest.Id = 24;
            this.bbiAnalysisTest.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Impress_16x16;
            this.bbiAnalysisTest.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Impress_32x32;
            this.bbiAnalysisTest.Name = "bbiAnalysisTest";
            // 
            // bbiAnalysisRecord
            // 
            resources.ApplyResources(this.bbiAnalysisRecord, "bbiAnalysisRecord");
            this.bbiAnalysisRecord.Id = 25;
            this.bbiAnalysisRecord.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Impress_16x16;
            this.bbiAnalysisRecord.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Impress_32x32;
            this.bbiAnalysisRecord.Name = "bbiAnalysisRecord";
            // 
            // bbiPrintRecord
            // 
            resources.ApplyResources(this.bbiPrintRecord, "bbiPrintRecord");
            this.bbiPrintRecord.Id = 5;
            this.bbiPrintRecord.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Printer_16x16;
            this.bbiPrintRecord.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Printer_32x32;
            this.bbiPrintRecord.Name = "bbiPrintRecord";
            // 
            // bbiSimulateRecord
            // 
            resources.ApplyResources(this.bbiSimulateRecord, "bbiSimulateRecord");
            this.bbiSimulateRecord.Id = 6;
            this.bbiSimulateRecord.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExecutableScript_16x16;
            this.bbiSimulateRecord.ImageOptions.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExecutableScript_32x32;
            this.bbiSimulateRecord.Name = "bbiSimulateRecord";
            // 
            // rpTest
            // 
            this.rpTest.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgTest});
            this.rpTest.Name = "rpTest";
            this.rpTest.Tag = "Test";
            resources.ApplyResources(this.rpTest, "rpTest");
            // 
            // rpgTest
            // 
            this.rpgTest.ItemLinks.Add(this.bbiNewTest);
            this.rpgTest.ItemLinks.Add(this.bbiStartTest, true);
            this.rpgTest.ItemLinks.Add(this.bbiStopTest);
            this.rpgTest.ItemLinks.Add(this.bbiCancelTest);
            this.rpgTest.ItemLinks.Add(this.bbiAnalysisTest, true);
            this.rpgTest.Name = "rpgTest";
            this.rpgTest.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgTest, "rpgTest");
            // 
            // rpRecords
            // 
            this.rpRecords.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgRecords,
            this.rpgCvRecords});
            this.rpRecords.Name = "rpRecords";
            this.rpRecords.Tag = "Records";
            resources.ApplyResources(this.rpRecords, "rpRecords");
            // 
            // rpgRecords
            // 
            this.rpgRecords.ItemLinks.Add(this.bbiEditRecord);
            this.rpgRecords.ItemLinks.Add(this.bbiDeleteRecord);
            this.rpgRecords.ItemLinks.Add(this.bbiAnalysisRecord);
            this.rpgRecords.ItemLinks.Add(this.bbiSimulateRecord);
            this.rpgRecords.ItemLinks.Add(this.bbiPrintRecord);
            this.rpgRecords.Name = "rpgRecords";
            this.rpgRecords.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgRecords, "rpgRecords");
            // 
            // rpgCvRecords
            // 
            this.rpgCvRecords.ItemLinks.Add(this.rgbiRecordsCurrentView);
            this.rpgCvRecords.Name = "rpgCvRecords";
            this.rpgCvRecords.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgCvRecords, "rpgCvRecords");
            // 
            // rpTrainees
            // 
            this.rpTrainees.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgTrainees});
            this.rpTrainees.Name = "rpTrainees";
            this.rpTrainees.Tag = "Trainees";
            resources.ApplyResources(this.rpTrainees, "rpTrainees");
            // 
            // rpgTrainees
            // 
            this.rpgTrainees.ItemLinks.Add(this.bbiNewTrainee);
            this.rpgTrainees.ItemLinks.Add(this.bbiEditTrainee);
            this.rpgTrainees.ItemLinks.Add(this.bbiDeleteTrainee);
            this.rpgTrainees.Name = "rpgTrainees";
            this.rpgTrainees.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgTrainees, "rpgTrainees");
            // 
            // rpTrainers
            // 
            this.rpTrainers.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgTrainers});
            this.rpTrainers.Name = "rpTrainers";
            this.rpTrainers.Tag = "Trainers";
            resources.ApplyResources(this.rpTrainers, "rpTrainers");
            // 
            // rpgTrainers
            // 
            this.rpgTrainers.ItemLinks.Add(this.bbiNewTrainer);
            this.rpgTrainers.ItemLinks.Add(this.bbiEditTrainer);
            this.rpgTrainers.ItemLinks.Add(this.bbiDeleteTrainer);
            this.rpgTrainers.Name = "rpgTrainers";
            this.rpgTrainers.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgTrainers, "rpgTrainers");
            // 
            // rpText
            // 
            this.rpText.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgCvTexts});
            this.rpText.Name = "rpText";
            this.rpText.Tag = "Texts";
            resources.ApplyResources(this.rpText, "rpText");
            // 
            // rpgCvTexts
            // 
            this.rpgCvTexts.Name = "rpgCvTexts";
            resources.ApplyResources(this.rpgCvTexts, "rpgCvTexts");
            // 
            // rpView
            // 
            this.rpView.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgNavigation,
            this.rpgPrint});
            this.rpView.Name = "rpView";
            resources.ApplyResources(this.rpView, "rpView");
            // 
            // rpgNavigation
            // 
            this.rpgNavigation.AllowTextClipping = false;
            this.rpgNavigation.ItemLinks.Add(this.bsiNavigation);
            this.rpgNavigation.Name = "rpgNavigation";
            this.rpgNavigation.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgNavigation, "rpgNavigation");
            // 
            // rpgPrint
            // 
            this.rpgPrint.AllowTextClipping = false;
            this.rpgPrint.ItemLinks.Add(this.bbiPrintPreview);
            this.rpgPrint.Name = "rpgPrint";
            this.rpgPrint.ShowCaptionButton = false;
            resources.ApplyResources(this.rpgPrint, "rpgPrint");
            // 
            // rsbMain
            // 
            this.rsbMain.ItemLinks.Add(this.bsiInfo);
            this.rsbMain.ItemLinks.Add(this.bbiNormal);
            this.rsbMain.ItemLinks.Add(this.bbiReading);
            this.rsbMain.ItemLinks.Add(this.beiZoom, true);
            this.rsbMain.ItemLinks.Add(this.bbiReminder, true);
            this.rsbMain.ItemLinks.Add(this.bsiTemp, true);
            resources.ApplyResources(this.rsbMain, "rsbMain");
            this.rsbMain.Name = "rsbMain";
            this.rsbMain.Ribbon = this.rcMain;
            // 
            // nbcMain
            // 
            this.nbcMain.ActiveGroup = this.nbgModules;
            resources.ApplyResources(this.nbcMain, "nbcMain");
            this.nbcMain.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nbgModules});
            this.nbcMain.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nbiRecord,
            this.nbiTrainee,
            this.nbiTrainer,
            this.nbiTest,
            this.nbiText});
            this.nbcMain.LinkSelectionMode = DevExpress.XtraNavBar.LinkSelectionModeType.OneInControl;
            this.nbcMain.MenuManager = this.rcMain;
            this.nbcMain.Name = "nbcMain";
            this.nbcMain.NavigationPaneGroupClientHeight = 320;
            this.nbcMain.OptionsNavPane.ExpandedWidth = ((int)(resources.GetObject("resource.ExpandedWidth")));
            this.nbcMain.OptionsNavPane.ShowOverflowButton = false;
            this.nbcMain.OptionsNavPane.ShowOverflowPanel = false;
            this.nbcMain.OptionsNavPane.ShowSplitter = false;
            this.nbcMain.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.nbcMain.SelectedLinkChanged += new DevExpress.XtraNavBar.ViewInfo.NavBarSelectedLinkChangedEventHandler(this.nbcMain_SelectedLinkChanged);
            this.nbcMain.NavPaneStateChanged += new System.EventHandler(this.nbcMain_NavPaneStateChanged);
            // 
            // nbgModules
            // 
            resources.ApplyResources(this.nbgModules, "nbgModules");
            this.nbgModules.Expanded = true;
            this.nbgModules.GroupCaptionUseImage = DevExpress.XtraNavBar.NavBarImage.Large;
            this.nbgModules.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.nbgModules.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiTest),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiRecord),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiTrainee),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiTrainer),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nbiText)});
            this.nbgModules.Name = "nbgModules";
            this.nbgModules.NavigationPaneVisible = false;
            this.nbgModules.SelectedLinkIndex = 4;
            // 
            // nbiTest
            // 
            resources.ApplyResources(this.nbiTest, "nbiTest");
            this.nbiTest.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Test_32x32;
            this.nbiTest.Name = "nbiTest";
            this.nbiTest.SmallImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Test_16x16;
            // 
            // nbiRecord
            // 
            resources.ApplyResources(this.nbiRecord, "nbiRecord");
            this.nbiRecord.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.MediaTape_32x32;
            this.nbiRecord.Name = "nbiRecord";
            this.nbiRecord.SmallImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.MediaTape_16x16;
            // 
            // nbiTrainee
            // 
            resources.ApplyResources(this.nbiTrainee, "nbiTrainee");
            this.nbiTrainee.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainee_32x32;
            this.nbiTrainee.Name = "nbiTrainee";
            this.nbiTrainee.SmallImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainee_16x16;
            // 
            // nbiTrainer
            // 
            resources.ApplyResources(this.nbiTrainer, "nbiTrainer");
            this.nbiTrainer.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainer_32x32;
            this.nbiTrainer.Name = "nbiTrainer";
            this.nbiTrainer.SmallImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainer_16x16;
            // 
            // nbiText
            // 
            resources.ApplyResources(this.nbiText, "nbiText");
            this.nbiText.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Text_32x32;
            this.nbiText.Name = "nbiText";
            this.nbiText.SmallImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Text_16x16;
            // 
            // pcMain
            // 
            this.pcMain.Controls.Add(this.bvcMain);
            resources.ApplyResources(this.pcMain, "pcMain");
            this.pcMain.Name = "pcMain";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pcMain);
            this.Controls.Add(this.nbcMain);
            this.Controls.Add(this.rsbMain);
            this.Controls.Add(this.rcMain);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Ribbon = this.rcMain;
            this.StatusBar = this.rsbMain;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.rcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bvcMain)).EndInit();
            this.bvcMain.ResumeLayout(false);
            this.bvccInfo.ResumeLayout(false);
            this.bvccPrint.ResumeLayout(false);
            this.bvccExport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.riztbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcMain)).EndInit();
            this.pcMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl rcMain;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpRecords;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgRecords;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar rsbMain;
        private DevExpress.XtraNavBar.NavBarControl nbcMain;
        private DevExpress.XtraNavBar.NavBarGroup nbgModules;
        private DevExpress.XtraEditors.PanelControl pcMain;
        private DevExpress.XtraBars.BarStaticItem bsiInfo;
        private DevExpress.XtraBars.BarButtonItem bbiNormal;
        private DevExpress.XtraBars.BarButtonItem bbiReading;
        private DevExpress.XtraBars.BarEditItem beiZoom;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar riztbMain;
        private DevExpress.XtraBars.BarButtonItem bbiReminder;
        private DevExpress.XtraBars.BarStaticItem bsiTemp;
        private DevExpress.XtraNavBar.NavBarItem nbiRecord;
        private DevExpress.XtraNavBar.NavBarItem nbiTrainee;
        private DevExpress.XtraNavBar.NavBarItem nbiTrainer;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpTrainees;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpTrainers;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgTrainees;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgTrainers;
        private DevExpress.XtraBars.BarButtonItem bbiPrintPreview;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpView;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgNavigation;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPrint;
        private DevExpress.XtraBars.BarSubItem bsiNavigation;
        private DevExpress.XtraBars.BarButtonItem bbiNewTrainee;
        private DevExpress.XtraBars.BarButtonItem bbiNewTrainer;
        private DevExpress.XtraBars.BarButtonItem bbiEditTrainer;
        private DevExpress.XtraBars.BarButtonItem bbiEditTrainee;
        private DevExpress.XtraBars.BarButtonItem bbiEditRecord;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteRecord;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteTrainee;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteTrainer;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiRecordsCurrentView;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCvRecords;
        private DevExpress.XtraBars.Ribbon.BackstageViewControl bvcMain;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl bvccInfo;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl bvccPrint;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvbiSaveAs;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem bvtiInfo;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem bvtiPrint;
        private DevExpress.XtraBars.Ribbon.BackstageViewClientControl bvccExport;
        private DevExpress.XtraBars.Ribbon.BackstageViewTabItem bvtiExport;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvbiExit;
        private DevExpress.XtraNavBar.NavBarItem nbiTest;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpTest;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgTest;
        private DevExpress.XtraBars.BarButtonItem bbiNewTest;
        private Controls.HelpPage pageHelp;
        private DevExpress.XtraBars.BarButtonItem bbiStartTest;
        private DevExpress.XtraBars.BarButtonItem bbiStopTest;
        private DevExpress.XtraBars.BarButtonItem bbiCancelTest;
        private DevExpress.XtraBars.BarButtonItem bbiAnalysisTest;
        private DevExpress.XtraBars.BarButtonItem bbiAnalysisRecord;
        private Controls.PrintPage pagePrint;
        private Controls.ExportPage pageExport;
        private DevExpress.XtraBars.Ribbon.BackstageViewItemSeparator backstageViewItemSeparator1;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvbiOption;
        private DevExpress.XtraNavBar.NavBarItem nbiText;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpText;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCvTexts;
        private DevExpress.XtraBars.BarButtonItem bbiPrintRecord;
        private DevExpress.XtraBars.BarButtonItem bbiSimulateRecord;
    }
}

