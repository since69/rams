﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ucPhotoPresenter
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.lblTag = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnValue = new System.Windows.Forms.TableLayoutPanel();
            this.pnValuePhoto = new System.Windows.Forms.TableLayoutPanel();
            this.pePhoto = new DevExpress.XtraEditors.PictureEdit();
            this.pnValueText = new System.Windows.Forms.TableLayoutPanel();
            this.lblMajorValue = new System.Windows.Forms.Label();
            this.lblMinorValue = new System.Windows.Forms.Label();
            this.pnWrapper.SuspendLayout();
            this.pnValue.SuspendLayout();
            this.pnValuePhoto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pePhoto.Properties)).BeginInit();
            this.pnValueText.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.ColumnCount = 3;
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Controls.Add(this.lblTag, 0, 0);
            this.pnWrapper.Controls.Add(this.lblTitle, 2, 0);
            this.pnWrapper.Controls.Add(this.pnValue, 2, 1);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.RowCount = 2;
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Size = new System.Drawing.Size(310, 110);
            this.pnWrapper.TabIndex = 0;
            // 
            // lblTag
            // 
            this.lblTag.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTag.Location = new System.Drawing.Point(0, 0);
            this.lblTag.Margin = new System.Windows.Forms.Padding(0);
            this.lblTag.Name = "lblTag";
            this.pnWrapper.SetRowSpan(this.lblTag, 2);
            this.lblTag.Size = new System.Drawing.Size(3, 110);
            this.lblTag.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.lblTitle.Location = new System.Drawing.Point(12, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(295, 30);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "title";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnValue
            // 
            this.pnValue.ColumnCount = 2;
            this.pnValue.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.pnValue.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnValue.Controls.Add(this.pnValuePhoto, 0, 0);
            this.pnValue.Controls.Add(this.pnValueText, 1, 0);
            this.pnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnValue.Location = new System.Drawing.Point(9, 30);
            this.pnValue.Margin = new System.Windows.Forms.Padding(0);
            this.pnValue.Name = "pnValue";
            this.pnValue.RowCount = 1;
            this.pnValue.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnValue.Size = new System.Drawing.Size(301, 80);
            this.pnValue.TabIndex = 2;
            // 
            // pnValuePhoto
            // 
            this.pnValuePhoto.ColumnCount = 1;
            this.pnValuePhoto.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnValuePhoto.Controls.Add(this.pePhoto, 0, 1);
            this.pnValuePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnValuePhoto.Location = new System.Drawing.Point(3, 3);
            this.pnValuePhoto.Name = "pnValuePhoto";
            this.pnValuePhoto.RowCount = 3;
            this.pnValuePhoto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValuePhoto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.pnValuePhoto.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValuePhoto.Size = new System.Drawing.Size(80, 74);
            this.pnValuePhoto.TabIndex = 2;
            // 
            // pePhoto
            // 
            this.pePhoto.Cursor = System.Windows.Forms.Cursors.Default;
            this.pePhoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pePhoto.Location = new System.Drawing.Point(3, 0);
            this.pePhoto.Name = "pePhoto";
            this.pePhoto.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pePhoto.Properties.Appearance.Options.UseBackColor = true;
            this.pePhoto.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pePhoto.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pePhoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pePhoto.Properties.ZoomAccelerationFactor = 1D;
            this.pePhoto.Size = new System.Drawing.Size(74, 74);
            this.pePhoto.TabIndex = 0;
            // 
            // pnValueText
            // 
            this.pnValueText.ColumnCount = 1;
            this.pnValueText.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnValueText.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnValueText.Controls.Add(this.lblMajorValue, 0, 0);
            this.pnValueText.Controls.Add(this.lblMinorValue, 0, 1);
            this.pnValueText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnValueText.Location = new System.Drawing.Point(89, 3);
            this.pnValueText.Name = "pnValueText";
            this.pnValueText.RowCount = 2;
            this.pnValueText.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValueText.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValueText.Size = new System.Drawing.Size(209, 74);
            this.pnValueText.TabIndex = 1;
            // 
            // lblMajorValue
            // 
            this.lblMajorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMajorValue.Font = new System.Drawing.Font("나눔스퀘어 Bold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMajorValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.lblMajorValue.Location = new System.Drawing.Point(3, 0);
            this.lblMajorValue.Name = "lblMajorValue";
            this.lblMajorValue.Size = new System.Drawing.Size(203, 37);
            this.lblMajorValue.TabIndex = 0;
            this.lblMajorValue.Text = "major value";
            this.lblMajorValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMinorValue
            // 
            this.lblMinorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMinorValue.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMinorValue.ForeColor = System.Drawing.Color.DarkGray;
            this.lblMinorValue.Location = new System.Drawing.Point(3, 37);
            this.lblMinorValue.Name = "lblMinorValue";
            this.lblMinorValue.Size = new System.Drawing.Size(203, 37);
            this.lblMinorValue.TabIndex = 1;
            this.lblMinorValue.Text = "minor value";
            this.lblMinorValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ucPhotoPresenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.Controls.Add(this.pnWrapper);
            this.Name = "ucPhotoPresenter";
            this.Size = new System.Drawing.Size(310, 110);
            this.pnWrapper.ResumeLayout(false);
            this.pnWrapper.PerformLayout();
            this.pnValue.ResumeLayout(false);
            this.pnValuePhoto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pePhoto.Properties)).EndInit();
            this.pnValueText.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private System.Windows.Forms.Label lblTag;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TableLayoutPanel pnValue;
        private System.Windows.Forms.TableLayoutPanel pnValueText;
        private System.Windows.Forms.Label lblMajorValue;
        private System.Windows.Forms.Label lblMinorValue;
        private System.Windows.Forms.TableLayoutPanel pnValuePhoto;
        private DevExpress.XtraEditors.PictureEdit pePhoto;
    }
}
