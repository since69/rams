﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ucTestInformation
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucTestInformation));
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.ucTrainee = new Thinkbox.Rams.ReadAlyzer.Controls.ucPhotoPresenter();
            this.ucTestText = new Thinkbox.Rams.ReadAlyzer.Controls.ucMultiValuePresenter();
            this.ucGazeData = new Thinkbox.Rams.ReadAlyzer.Controls.ucDataPresenter();
            this.pnWrapper.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.ColumnCount = 1;
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Controls.Add(this.ucTrainee, 0, 0);
            this.pnWrapper.Controls.Add(this.ucTestText, 0, 1);
            this.pnWrapper.Controls.Add(this.ucGazeData, 0, 2);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.RowCount = 3;
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.pnWrapper.Size = new System.Drawing.Size(314, 500);
            this.pnWrapper.TabIndex = 0;
            // 
            // ucTrainee
            // 
            this.ucTrainee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.ucTrainee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTrainee.Location = new System.Drawing.Point(3, 3);
            this.ucTrainee.MajorValue = "";
            this.ucTrainee.MajorValueColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.ucTrainee.MinorValue = "";
            this.ucTrainee.MinorValueColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.ucTrainee.Name = "ucTrainee";
            this.ucTrainee.Photo = ((System.Drawing.Image)(resources.GetObject("ucTrainee.Photo")));
            this.ucTrainee.Size = new System.Drawing.Size(308, 110);
            this.ucTrainee.TabIndex = 0;
            this.ucTrainee.TagColor = System.Drawing.Color.DodgerBlue;
            this.ucTrainee.Title = "Trainee";
            this.ucTrainee.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            // 
            // ucTestText
            // 
            this.ucTestText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.ucTestText.CaptionColor = System.Drawing.Color.DarkGray;
            this.ucTestText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTestText.FirstCaption = "Level";
            this.ucTestText.FirstValue = "";
            this.ucTestText.Location = new System.Drawing.Point(3, 119);
            this.ucTestText.Name = "ucTestText";
            this.ucTestText.SecondCaption = "Line";
            this.ucTestText.SecondValue = "";
            this.ucTestText.Size = new System.Drawing.Size(308, 110);
            this.ucTestText.TabIndex = 2;
            this.ucTestText.TagColor = System.Drawing.Color.DodgerBlue;
            this.ucTestText.ThirdCaption = "Word";
            this.ucTestText.ThirdValue = "";
            this.ucTestText.Title = "Text";
            this.ucTestText.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.ucTestText.ValueColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            // 
            // ucGazeData
            // 
            this.ucGazeData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.ucGazeData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucGazeData.Location = new System.Drawing.Point(3, 235);
            this.ucGazeData.Name = "ucGazeData";
            this.ucGazeData.Size = new System.Drawing.Size(308, 262);
            this.ucGazeData.TabIndex = 3;
            this.ucGazeData.TagColor = System.Drawing.Color.DodgerBlue;
            this.ucGazeData.Title = "title";
            this.ucGazeData.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            // 
            // ucTestInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnWrapper);
            this.MinimumSize = new System.Drawing.Size(314, 350);
            this.Name = "ucTestInformation";
            this.Size = new System.Drawing.Size(314, 500);
            this.pnWrapper.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private ucPhotoPresenter ucTrainee;
        private ucMultiValuePresenter ucTestText;
        private ucDataPresenter ucGazeData;
    }
}
