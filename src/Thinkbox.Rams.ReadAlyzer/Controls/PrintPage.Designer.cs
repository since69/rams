﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class PrintPage
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintPage));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            this.pnSplitContainer = new System.Windows.Forms.SplitContainer();
            this.lciOption = new DevExpress.XtraLayout.LayoutControl();
            this.ddbPrintStyle = new DevExpress.XtraEditors.DropDownButton();
            this.ddbPaperSize = new DevExpress.XtraEditors.DropDownButton();
            this.ddbMargins = new DevExpress.XtraEditors.DropDownButton();
            this.ddbOrientation = new DevExpress.XtraEditors.DropDownButton();
            this.ddbDuplex = new DevExpress.XtraEditors.DropDownButton();
            this.lblSettings = new DevExpress.XtraEditors.LabelControl();
            this.ddbPrinter = new DevExpress.XtraEditors.DropDownButton();
            this.lblPrinter = new DevExpress.XtraEditors.LabelControl();
            this.seCopy = new DevExpress.XtraEditors.SpinEdit();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.ddbCollate = new DevExpress.XtraEditors.DropDownButton();
            this.lcgOption = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciPrint = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCopy = new DevExpress.XtraLayout.LayoutControlItem();
            this.esiOne = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciPrinterLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciPrinter = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSettings = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciDuplex = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCollated = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciOrientation = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciMargins = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciPaperSize = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciPrintStyle = new DevExpress.XtraLayout.LayoutControlItem();
            this.pnPage = new System.Windows.Forms.Panel();
            this.pcPrint = new DevExpress.XtraPrinting.Control.PrintControl();
            this.pnFooter = new System.Windows.Forms.Panel();
            this.bePage = new DevExpress.XtraEditors.ButtonEdit();
            this.seZoom = new DevExpress.XtraEditors.SpinEdit();
            this.ztbPaper = new DevExpress.XtraEditors.ZoomTrackBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnSplitContainer)).BeginInit();
            this.pnSplitContainer.Panel1.SuspendLayout();
            this.pnSplitContainer.Panel2.SuspendLayout();
            this.pnSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lciOption)).BeginInit();
            this.lciOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seCopy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgOption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.esiOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrinterLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDuplex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCollated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOrientation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMargins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPaperSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrintStyle)).BeginInit();
            this.pnPage.SuspendLayout();
            this.pnFooter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bePage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seZoom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ztbPaper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ztbPaper.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pnSplitContainer
            // 
            this.pnSplitContainer.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pnSplitContainer, "pnSplitContainer");
            this.pnSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.pnSplitContainer.ForeColor = System.Drawing.Color.Transparent;
            this.pnSplitContainer.Name = "pnSplitContainer";
            // 
            // pnSplitContainer.Panel1
            // 
            this.pnSplitContainer.Panel1.Controls.Add(this.lciOption);
            // 
            // pnSplitContainer.Panel2
            // 
            this.pnSplitContainer.Panel2.Controls.Add(this.pnPage);
            this.pnSplitContainer.Panel2.Controls.Add(this.pnFooter);
            // 
            // lciOption
            // 
            this.lciOption.Controls.Add(this.ddbPrintStyle);
            this.lciOption.Controls.Add(this.ddbPaperSize);
            this.lciOption.Controls.Add(this.ddbMargins);
            this.lciOption.Controls.Add(this.ddbOrientation);
            this.lciOption.Controls.Add(this.ddbDuplex);
            this.lciOption.Controls.Add(this.lblSettings);
            this.lciOption.Controls.Add(this.ddbPrinter);
            this.lciOption.Controls.Add(this.lblPrinter);
            this.lciOption.Controls.Add(this.seCopy);
            this.lciOption.Controls.Add(this.btnPrint);
            this.lciOption.Controls.Add(this.lblTitle);
            this.lciOption.Controls.Add(this.ddbCollate);
            resources.ApplyResources(this.lciOption, "lciOption");
            this.lciOption.Name = "lciOption";
            this.lciOption.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(-1180, 447, 648, 350);
            this.lciOption.Root = this.lcgOption;
            // 
            // ddbPrintStyle
            // 
            this.ddbPrintStyle.Appearance.Options.UseTextOptions = true;
            this.ddbPrintStyle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbPrintStyle.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbPrintStyle, "ddbPrintStyle");
            this.ddbPrintStyle.Name = "ddbPrintStyle";
            this.ddbPrintStyle.StyleController = this.lciOption;
            // 
            // ddbPaperSize
            // 
            this.ddbPaperSize.Appearance.Options.UseTextOptions = true;
            this.ddbPaperSize.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbPaperSize.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbPaperSize, "ddbPaperSize");
            this.ddbPaperSize.Name = "ddbPaperSize";
            this.ddbPaperSize.StyleController = this.lciOption;
            // 
            // ddbMargins
            // 
            this.ddbMargins.Appearance.Options.UseTextOptions = true;
            this.ddbMargins.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbMargins.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbMargins, "ddbMargins");
            this.ddbMargins.Name = "ddbMargins";
            this.ddbMargins.StyleController = this.lciOption;
            // 
            // ddbOrientation
            // 
            this.ddbOrientation.Appearance.Options.UseTextOptions = true;
            this.ddbOrientation.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbOrientation.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbOrientation, "ddbOrientation");
            this.ddbOrientation.Name = "ddbOrientation";
            this.ddbOrientation.StyleController = this.lciOption;
            // 
            // ddbDuplex
            // 
            this.ddbDuplex.Appearance.Options.UseTextOptions = true;
            this.ddbDuplex.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbDuplex.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbDuplex, "ddbDuplex");
            this.ddbDuplex.Name = "ddbDuplex";
            this.ddbDuplex.StyleController = this.lciOption;
            // 
            // lblSettings
            // 
            this.lblSettings.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblSettings.Appearance.Font")));
            this.lblSettings.Appearance.Options.UseFont = true;
            this.lblSettings.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblSettings.LineVisible = true;
            resources.ApplyResources(this.lblSettings, "lblSettings");
            this.lblSettings.Name = "lblSettings";
            this.lblSettings.ShowLineShadow = false;
            this.lblSettings.StyleController = this.lciOption;
            // 
            // ddbPrinter
            // 
            this.ddbPrinter.Appearance.Options.UseTextOptions = true;
            this.ddbPrinter.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbPrinter.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbPrinter, "ddbPrinter");
            this.ddbPrinter.Name = "ddbPrinter";
            this.ddbPrinter.StyleController = this.lciOption;
            // 
            // lblPrinter
            // 
            this.lblPrinter.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblPrinter.Appearance.Font")));
            this.lblPrinter.Appearance.Options.UseFont = true;
            this.lblPrinter.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblPrinter.LineVisible = true;
            resources.ApplyResources(this.lblPrinter, "lblPrinter");
            this.lblPrinter.Name = "lblPrinter";
            this.lblPrinter.ShowLineShadow = false;
            this.lblPrinter.StyleController = this.lciOption;
            // 
            // seCopy
            // 
            resources.ApplyResources(this.seCopy, "seCopy");
            this.seCopy.Name = "seCopy";
            this.seCopy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seCopy.Properties.IsFloatValue = false;
            this.seCopy.Properties.Mask.EditMask = resources.GetString("seCopy.Properties.Mask.EditMask");
            this.seCopy.Properties.MaxValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.seCopy.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.seCopy.StyleController = this.lciOption;
            // 
            // btnPrint
            // 
            this.btnPrint.ImageOptions.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Printer_32x32;
            this.btnPrint.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            resources.ApplyResources(this.btnPrint, "btnPrint");
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.StyleController = this.lciOption;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTitle.Appearance.Font")));
            this.lblTitle.Appearance.Options.UseFont = true;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTitle.LineVisible = true;
            resources.ApplyResources(this.lblTitle, "lblTitle");
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.ShowLineShadow = false;
            this.lblTitle.StyleController = this.lciOption;
            // 
            // ddbCollate
            // 
            this.ddbCollate.Appearance.Options.UseTextOptions = true;
            this.ddbCollate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ddbCollate.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            resources.ApplyResources(this.ddbCollate, "ddbCollate");
            this.ddbCollate.Name = "ddbCollate";
            this.ddbCollate.StyleController = this.lciOption;
            // 
            // lcgOption
            // 
            this.lcgOption.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgOption.GroupBordersVisible = false;
            this.lcgOption.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTitle,
            this.lciPrint,
            this.lciCopy,
            this.esiOne,
            this.lciPrinterLabel,
            this.lciPrinter,
            this.lciSettings,
            this.lciDuplex,
            this.lciCollated,
            this.lciOrientation,
            this.lciMargins,
            this.lciPaperSize,
            this.lciPrintStyle});
            this.lcgOption.Location = new System.Drawing.Point(0, 0);
            this.lcgOption.Name = "Root";
            this.lcgOption.Size = new System.Drawing.Size(606, 600);
            this.lcgOption.TextVisible = false;
            // 
            // lciTitle
            // 
            this.lciTitle.Control = this.lblTitle;
            this.lciTitle.Location = new System.Drawing.Point(84, 0);
            this.lciTitle.MaxSize = new System.Drawing.Size(0, 36);
            this.lciTitle.MinSize = new System.Drawing.Size(14, 36);
            this.lciTitle.Name = "lciTitle";
            this.lciTitle.Size = new System.Drawing.Size(502, 36);
            this.lciTitle.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            resources.ApplyResources(this.lciTitle, "lciTitle");
            this.lciTitle.TextSize = new System.Drawing.Size(0, 0);
            this.lciTitle.TextVisible = false;
            // 
            // lciPrint
            // 
            this.lciPrint.Control = this.btnPrint;
            this.lciPrint.Location = new System.Drawing.Point(0, 0);
            this.lciPrint.MaxSize = new System.Drawing.Size(84, 80);
            this.lciPrint.MinSize = new System.Drawing.Size(84, 80);
            this.lciPrint.Name = "lciPrint";
            this.lciPrint.Size = new System.Drawing.Size(84, 80);
            this.lciPrint.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciPrint.TextSize = new System.Drawing.Size(0, 0);
            this.lciPrint.TextVisible = false;
            // 
            // lciCopy
            // 
            this.lciCopy.Control = this.seCopy;
            resources.ApplyResources(this.lciCopy, "lciCopy");
            this.lciCopy.Location = new System.Drawing.Point(84, 36);
            this.lciCopy.MaxSize = new System.Drawing.Size(180, 24);
            this.lciCopy.MinSize = new System.Drawing.Size(180, 24);
            this.lciCopy.Name = "lciCopy";
            this.lciCopy.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 20, 2, 2);
            this.lciCopy.Size = new System.Drawing.Size(180, 44);
            this.lciCopy.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCopy.TextSize = new System.Drawing.Size(39, 14);
            // 
            // esiOne
            // 
            this.esiOne.AllowHotTrack = false;
            this.esiOne.Location = new System.Drawing.Point(264, 36);
            this.esiOne.Name = "esiOne";
            this.esiOne.Size = new System.Drawing.Size(322, 44);
            this.esiOne.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciPrinterLabel
            // 
            this.lciPrinterLabel.Control = this.lblPrinter;
            resources.ApplyResources(this.lciPrinterLabel, "lciPrinterLabel");
            this.lciPrinterLabel.Location = new System.Drawing.Point(0, 80);
            this.lciPrinterLabel.MaxSize = new System.Drawing.Size(0, 36);
            this.lciPrinterLabel.MinSize = new System.Drawing.Size(14, 36);
            this.lciPrinterLabel.Name = "lciPrinterLabel";
            this.lciPrinterLabel.Size = new System.Drawing.Size(586, 36);
            this.lciPrinterLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciPrinterLabel.TextSize = new System.Drawing.Size(0, 0);
            this.lciPrinterLabel.TextVisible = false;
            // 
            // lciPrinter
            // 
            this.lciPrinter.Control = this.ddbPrinter;
            this.lciPrinter.Location = new System.Drawing.Point(0, 116);
            this.lciPrinter.MaxSize = new System.Drawing.Size(0, 56);
            this.lciPrinter.MinSize = new System.Drawing.Size(100, 56);
            this.lciPrinter.Name = "lciPrinter";
            this.lciPrinter.Size = new System.Drawing.Size(586, 56);
            this.lciPrinter.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciPrinter.TextSize = new System.Drawing.Size(0, 0);
            this.lciPrinter.TextVisible = false;
            // 
            // lciSettings
            // 
            this.lciSettings.Control = this.lblSettings;
            resources.ApplyResources(this.lciSettings, "lciSettings");
            this.lciSettings.Location = new System.Drawing.Point(0, 172);
            this.lciSettings.MaxSize = new System.Drawing.Size(0, 36);
            this.lciSettings.MinSize = new System.Drawing.Size(14, 36);
            this.lciSettings.Name = "lciSettings";
            this.lciSettings.Size = new System.Drawing.Size(586, 36);
            this.lciSettings.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciSettings.TextSize = new System.Drawing.Size(0, 0);
            this.lciSettings.TextVisible = false;
            // 
            // lciDuplex
            // 
            this.lciDuplex.Control = this.ddbDuplex;
            resources.ApplyResources(this.lciDuplex, "lciDuplex");
            this.lciDuplex.Location = new System.Drawing.Point(0, 208);
            this.lciDuplex.MaxSize = new System.Drawing.Size(0, 56);
            this.lciDuplex.MinSize = new System.Drawing.Size(68, 56);
            this.lciDuplex.Name = "lciDuplex";
            this.lciDuplex.Size = new System.Drawing.Size(586, 56);
            this.lciDuplex.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciDuplex.TextSize = new System.Drawing.Size(0, 0);
            this.lciDuplex.TextVisible = false;
            // 
            // lciCollated
            // 
            this.lciCollated.Control = this.ddbCollate;
            resources.ApplyResources(this.lciCollated, "lciCollated");
            this.lciCollated.Location = new System.Drawing.Point(0, 264);
            this.lciCollated.MaxSize = new System.Drawing.Size(0, 56);
            this.lciCollated.MinSize = new System.Drawing.Size(100, 56);
            this.lciCollated.Name = "lciCollated";
            this.lciCollated.Size = new System.Drawing.Size(586, 56);
            this.lciCollated.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCollated.TextSize = new System.Drawing.Size(0, 0);
            this.lciCollated.TextVisible = false;
            // 
            // lciOrientation
            // 
            this.lciOrientation.Control = this.ddbOrientation;
            resources.ApplyResources(this.lciOrientation, "lciOrientation");
            this.lciOrientation.Location = new System.Drawing.Point(0, 320);
            this.lciOrientation.MaxSize = new System.Drawing.Size(0, 56);
            this.lciOrientation.MinSize = new System.Drawing.Size(100, 56);
            this.lciOrientation.Name = "lciOrientation";
            this.lciOrientation.Size = new System.Drawing.Size(586, 56);
            this.lciOrientation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOrientation.TextSize = new System.Drawing.Size(0, 0);
            this.lciOrientation.TextVisible = false;
            // 
            // lciMargins
            // 
            this.lciMargins.Control = this.ddbMargins;
            resources.ApplyResources(this.lciMargins, "lciMargins");
            this.lciMargins.Location = new System.Drawing.Point(0, 376);
            this.lciMargins.MaxSize = new System.Drawing.Size(0, 56);
            this.lciMargins.MinSize = new System.Drawing.Size(100, 56);
            this.lciMargins.Name = "lciMargins";
            this.lciMargins.Size = new System.Drawing.Size(586, 56);
            this.lciMargins.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciMargins.TextSize = new System.Drawing.Size(0, 0);
            this.lciMargins.TextVisible = false;
            // 
            // lciPaperSize
            // 
            this.lciPaperSize.Control = this.ddbPaperSize;
            this.lciPaperSize.Location = new System.Drawing.Point(0, 432);
            this.lciPaperSize.MaxSize = new System.Drawing.Size(0, 56);
            this.lciPaperSize.MinSize = new System.Drawing.Size(100, 56);
            this.lciPaperSize.Name = "lciPaperSize";
            this.lciPaperSize.Size = new System.Drawing.Size(586, 56);
            this.lciPaperSize.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            resources.ApplyResources(this.lciPaperSize, "lciPaperSize");
            this.lciPaperSize.TextSize = new System.Drawing.Size(0, 0);
            this.lciPaperSize.TextVisible = false;
            // 
            // lciPrintStyle
            // 
            this.lciPrintStyle.Control = this.ddbPrintStyle;
            this.lciPrintStyle.Location = new System.Drawing.Point(0, 488);
            this.lciPrintStyle.MaxSize = new System.Drawing.Size(0, 56);
            this.lciPrintStyle.MinSize = new System.Drawing.Size(79, 26);
            this.lciPrintStyle.Name = "lciPrintStyle";
            this.lciPrintStyle.Size = new System.Drawing.Size(586, 92);
            this.lciPrintStyle.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            resources.ApplyResources(this.lciPrintStyle, "lciPrintStyle");
            this.lciPrintStyle.TextSize = new System.Drawing.Size(0, 0);
            this.lciPrintStyle.TextVisible = false;
            // 
            // pnPage
            // 
            this.pnPage.Controls.Add(this.pcPrint);
            resources.ApplyResources(this.pnPage, "pnPage");
            this.pnPage.Name = "pnPage";
            // 
            // pcPrint
            // 
            resources.ApplyResources(this.pcPrint, "pcPrint");
            this.pcPrint.Name = "pcPrint";
            this.pcPrint.SelectedPageChanged += new System.EventHandler(this.pcPrint_SelectedPageChanged);
            // 
            // pnFooter
            // 
            this.pnFooter.Controls.Add(this.bePage);
            this.pnFooter.Controls.Add(this.seZoom);
            this.pnFooter.Controls.Add(this.ztbPaper);
            resources.ApplyResources(this.pnFooter, "pnFooter");
            this.pnFooter.Name = "pnFooter";
            // 
            // bePage
            // 
            resources.ApplyResources(this.bePage, "bePage");
            this.bePage.Name = "bePage";
            this.bePage.Properties.Appearance.Options.UseTextOptions = true;
            this.bePage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.bePage.Properties.AutoHeight = ((bool)(resources.GetObject("bePage.Properties.AutoHeight")));
            this.bePage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("bePage.Properties.Buttons"))), resources.GetString("bePage.Properties.Buttons1"), ((int)(resources.GetObject("bePage.Properties.Buttons2"))), ((bool)(resources.GetObject("bePage.Properties.Buttons3"))), ((bool)(resources.GetObject("bePage.Properties.Buttons4"))), ((bool)(resources.GetObject("bePage.Properties.Buttons5"))), editorButtonImageOptions1),
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("bePage.Properties.Buttons6"))))});
            this.bePage.Properties.DisplayFormat.FormatString = "Page {0}";
            this.bePage.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.bePage.EditValueChanged += new System.EventHandler(this.bePage_EditValueChanged);
            this.bePage.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.bePage_EditValueChanging);
            // 
            // seZoom
            // 
            resources.ApplyResources(this.seZoom, "seZoom");
            this.seZoom.Name = "seZoom";
            this.seZoom.Properties.DisplayFormat.FormatString = "{0}%";
            this.seZoom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.seZoom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.seZoom.Properties.Mask.EditMask = resources.GetString("seZoom.Properties.Mask.EditMask");
            this.seZoom.Properties.MaxValue = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.seZoom.Properties.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.seZoom.EditValueChanged += new System.EventHandler(this.seZoom_EditValueChanged);
            // 
            // ztbPaper
            // 
            resources.ApplyResources(this.ztbPaper, "ztbPaper");
            this.ztbPaper.Name = "ztbPaper";
            this.ztbPaper.Properties.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.ztbPaper.Properties.Maximum = 80;
            this.ztbPaper.Properties.SmallChange = 2;
            this.ztbPaper.Value = 40;
            this.ztbPaper.EditValueChanged += new System.EventHandler(this.ztbPaper_EditValueChanged);
            // 
            // PrintPage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnSplitContainer);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.Name = "PrintPage";
            this.pnSplitContainer.Panel1.ResumeLayout(false);
            this.pnSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnSplitContainer)).EndInit();
            this.pnSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lciOption)).EndInit();
            this.lciOption.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seCopy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgOption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.esiOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrinterLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDuplex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCollated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOrientation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMargins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPaperSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrintStyle)).EndInit();
            this.pnPage.ResumeLayout(false);
            this.pnFooter.ResumeLayout(false);
            this.pnFooter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bePage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seZoom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ztbPaper.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ztbPaper)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer pnSplitContainer;
        private DevExpress.XtraLayout.LayoutControl lciOption;
        private DevExpress.XtraLayout.LayoutControlGroup lcgOption;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraLayout.LayoutControlItem lciTitle;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraLayout.LayoutControlItem lciPrint;
        private DevExpress.XtraEditors.SpinEdit seCopy;
        private DevExpress.XtraLayout.LayoutControlItem lciCopy;
        private DevExpress.XtraLayout.EmptySpaceItem esiOne;
        private DevExpress.XtraEditors.LabelControl lblPrinter;
        private DevExpress.XtraLayout.LayoutControlItem lciPrinterLabel;
        private DevExpress.XtraEditors.LabelControl lblSettings;
        private DevExpress.XtraEditors.DropDownButton ddbPrinter;
        private DevExpress.XtraLayout.LayoutControlItem lciPrinter;
        private DevExpress.XtraLayout.LayoutControlItem lciSettings;
        private DevExpress.XtraEditors.DropDownButton ddbDuplex;
        private DevExpress.XtraLayout.LayoutControlItem lciDuplex;
        private DevExpress.XtraEditors.DropDownButton ddbPrintStyle;
        private DevExpress.XtraEditors.DropDownButton ddbPaperSize;
        private DevExpress.XtraEditors.DropDownButton ddbMargins;
        private DevExpress.XtraEditors.DropDownButton ddbOrientation;
        private DevExpress.XtraEditors.DropDownButton ddbCollate;
        private DevExpress.XtraLayout.LayoutControlItem lciCollated;
        private DevExpress.XtraLayout.LayoutControlItem lciOrientation;
        private DevExpress.XtraLayout.LayoutControlItem lciMargins;
        private DevExpress.XtraLayout.LayoutControlItem lciPaperSize;
        private DevExpress.XtraLayout.LayoutControlItem lciPrintStyle;
        private System.Windows.Forms.Panel pnFooter;
        private DevExpress.XtraEditors.ZoomTrackBarControl ztbPaper;
        private DevExpress.XtraEditors.SpinEdit seZoom;
        private DevExpress.XtraEditors.ButtonEdit bePage;
        private System.Windows.Forms.Panel pnPage;
        private DevExpress.XtraPrinting.Control.PrintControl pcPrint;
    }
}
