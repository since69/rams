﻿using System;
using System.Windows.Forms;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class HelpPage : BackstageViewBasePage
    {
        #region VARIABLES
        private bool isLoaded = false;
        private Form frmAbout = null;
        #endregion

        #region CONSTRUCTORS
        public HelpPage()
        {
            InitializeComponent();
            this.Load += new EventHandler(HelpPage_Load);
        }
        #endregion

        #region METHODS
        private void ResizeAboutPanel()
        {
            Panel pnSupport = frmAbout.Parent as Panel;
            frmAbout.Location = new System.Drawing.Point(
                (pnSupport.Width - frmAbout.Width) / 2,
                (pnSupport.Height - frmAbout.Height) / 2);
        }
        #endregion

        #region CONTROL EVENTS
        private void galSupportItems_Gallery_ItemClick(object sender, 
            DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs args)
        {
            string link = string.Format($"{ args.Item.Tag }");
            switch (link)
            {
                case "LinkHelp": link = "www.google.co.kr"; break;
                case "LinkGetStarted": link = "www.google.co.kr"; break;
                case "LinkGetSupport": link = "mailto:silentsky@hotmail.com"; break;
            }
            if (!string.IsNullOrEmpty(link))
                Helpers.ObjectHelper.StartProcess(link);
        }

        private void Panel2_Resize(object sender, EventArgs args)
        {
            ResizeAboutPanel();
        }
        #endregion

        #region FORM EVENTS
        internal void HelpPage_Load(object sender, EventArgs args)
        {
            if (isLoaded) return;
            frmAbout = new Thinkbox.Rams.ReadAlyzer.Forms.AboutForm();
            frmAbout.TopLevel = false;
            frmAbout.Parent = pnSplitContainer.Panel2;
            ResizeAboutPanel();
            frmAbout.Show();
            pnSplitContainer.Panel2.Resize += new EventHandler(Panel2_Resize);
            ResizeAboutPanel();
            isLoaded = true;
        }
        #endregion
    }
}
