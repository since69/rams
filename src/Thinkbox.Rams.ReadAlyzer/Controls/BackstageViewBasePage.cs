﻿using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Drawing;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    /// <summary>
    /// Base control for BackstageViewControl page.
    /// </summary>
    public partial class BackstageViewBasePage : UserControl
    {
        #region PROPERTIES
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }

        public BackstageViewControl BackstageView
        {
            get
            {
                if (Parent == null) return null;
                return Parent.Parent as BackstageViewControl;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public BackstageViewBasePage()
        {
            InitializeComponent();
        }
        #endregion

        #region METHODS
        private Color GetBackgroundColor()
        {
            BackstageViewClientControl parent = Parent as BackstageViewClientControl;
            if (parent == null) return Color.Transparent;
            return parent.GetBackgroundColor();
        }
        #endregion

        #region EVENTS
        protected override void OnPaint(PaintEventArgs args)
        {
            base.OnPaint(args);
            if (BackstageView != null)
            {
                BackstageViewPainter.DrawBackstageViewImage(args, 
                    this, BackstageView);
            }
        }
        #endregion
    }
}
