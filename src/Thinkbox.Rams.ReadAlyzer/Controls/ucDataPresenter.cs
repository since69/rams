﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class ucDataPresenter : UserControl
    {
        #region PROPERTIES
        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }
        [Description("Color of tag."), Category("Appearance")]
        public Color TagColor
        {
            get { return lblTag.BackColor; }
            set { lblTag.BackColor = value; }
        }
        [Description("Color of title."), Category("Appearance")]
        public Color TitleColor
        {
            get { return lblTitle.ForeColor; }
            set { lblTitle.ForeColor = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public ucDataPresenter()
        {
            InitializeComponent();
            txtMeasureData.HideSelection = false;
        }
        #endregion

        #region METHODS
        public void Clear()
        {
            txtMeasureData.Clear();
        }

        public void AppendData(string data)
        {
            txtMeasureData.AppendText(data);
            txtMeasureData.SelectionStart = txtMeasureData.Text.Length;
            txtMeasureData.ScrollToCaret();
        }
        #endregion
    }
}
