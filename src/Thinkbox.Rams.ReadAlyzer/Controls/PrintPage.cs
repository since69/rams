﻿using System;
using System.Drawing.Printing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Gallery;
using DevExpress.XtraPrinting;
using DevExpress.XtraEditors.Controls;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class PrintPage : BackstageViewBasePage
    {
        #region VARIABLES
        private GalleryItem memoStyle;
        private GalleryItem tableStyle;
        private bool updatedZoom = false;
        #endregion

        #region CONSTRUCTORS
        public PrintPage()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region METHODS
        public void InitializePrintingSystem()
        {
            MainForm form = BackstageView.Ribbon.FindForm() as MainForm;
            BarManager manager = (form == null || form.Ribbon == null ? 
                null : form.Ribbon.Manager);
            ((GalleryDropDown)ddbOrientation.DropDownControl).Manager = manager;
            ((GalleryDropDown)ddbMargins.DropDownControl).Manager = manager;
            ((GalleryDropDown)ddbPaperSize.DropDownControl).Manager = manager;
            ((GalleryDropDown)ddbCollate.DropDownControl).Manager = manager;
            ((GalleryDropDown)ddbPrinter.DropDownControl).Manager = manager;
            ((GalleryDropDown)ddbDuplex.DropDownControl).Manager = manager;
            ((GalleryDropDown)ddbPrintStyle.DropDownControl).Manager = manager;
            lciPrintStyle.Visibility = (form.CurrentRichEdit == null ? 
                DevExpress.XtraLayout.Utils.LayoutVisibility.Never :
                DevExpress.XtraLayout.Utils.LayoutVisibility.Always);
            CreateDocument();
        }

        private void UpdatePageButtonsEnabledState(int pageIndex)
        {
            if (pcPrint.PrintingSystem == null) return;
            bePage.Properties.Buttons[0].Enabled = pageIndex != 1;
            bePage.Properties.Buttons[1].Enabled = pageIndex != 
                pcPrint.PrintingSystem.Pages.Count;
            UpdatePagesInfo();
        }

        private void UpdatePageButtonsEnabledState()
        {
            UpdatePageButtonsEnabledState(pcPrint.SelectedPageIndex + 1);
        }

        private void UpdatePagesInfo()
        {
            if (pcPrint.PrintingSystem != null)
            {
                bePage.Properties.DisplayFormat.FormatString =
                    Properties.Resources.PageInfo + 
                    pcPrint.PrintingSystem.Pages.Count;
                btnPrint.Enabled = pcPrint.PrintingSystem.Pages.Count > 0;
                bePage.Enabled = pcPrint.PrintingSystem.Pages.Count > 0;
            }
        }

        private int ZoomValueToValue(int zoomValue)
        {
            if (zoomValue < 100)
                return Math.Min(80, Math.Max(0, (zoomValue - 10) * 40 / 90));
            return Math.Min(80, Math.Max(0, (zoomValue - 100) * 40 / 400 + 40));
        }

        private int GetZoomValue()
        {
            if (ztbPaper.Value < 40)
                return 10 + 90 * (ztbPaper.Value - 0) / 40;
            else
                return 100 + 400 * (ztbPaper.Value - 40) / 40;
        }

        private PaperKind GetPaperKind()
        {
            return (PaperKind)ddbPaperSize.Tag;
        }

        private Margins GetMargins()
        {
            Padding padding = (Padding)ddbMargins.Tag;
            return new Margins((int)(padding.Left * 3.9),
                (int)(padding.Right * 3.9),
                (int)(padding.Top * 3.9),
                (int)(padding.Bottom * 3.9));
        }

        private bool GetLandscape()
        {
            if (ddbOrientation.DropDownControl != null)
            {
                return ((GalleryDropDown)ddbOrientation.DropDownControl)
                    .Gallery.Groups[0].Items[1].Checked;
            }
            return false;    
        }

        private void CreateLink(PrintingSystem ps)
        {
            MainForm form = BackstageView.Ribbon.FindForm() as MainForm;
            bool showMemo = memoStyle.Checked && form.CurrentRichEdit != null;
            if (showMemo)
            {
                Link lnk = new Link(ps);
                lnk.RtfReportHeader = form.CurrentRichEdit.RtfText;
                lnk.PaperKind = GetPaperKind();
                lnk.Landscape = GetLandscape();
                lnk.Margins = GetMargins();
                lnk.CreateDocument();
            }
            else
            {
                PrintableComponentLink lnk = new PrintableComponentLink(ps);
                lnk.Component = form.CurrentPrintableComponent;
                lnk.PaperKind = GetPaperKind();
                lnk.Landscape = GetLandscape();
                lnk.Margins = GetMargins();
                lnk.CreateDocument();
            }
        }

        private void CreateDocument()
        {
            PrintingSystem ps = new PrintingSystem();
            if (true.Equals(pcPrint.Tag))
                ps.StartPrint -= new PrintDocumentEventHandler(OnStartPrint);
            pcPrint.PrintingSystem = ps;
            ps.StartPrint += new PrintDocumentEventHandler(OnStartPrint);
            pcPrint.Tag = true;
            CreateLink(ps);
            bePage.EditValue = 1;
            UpdatePagesInfo();
        }

        private GalleryDropDown CreateListBoxGallery()
        {
            GalleryDropDown gdd = new GalleryDropDown();
            InDropDownGallery gal = gdd.Gallery;

            gal.FixedImageSize = false;
            gal.ShowItemText = true;
            gal.ColumnCount = 1;
            gal.CheckDrawMode = CheckDrawMode.OnlyImage;
            gal.ShowGroupCaption = false;
            gal.AutoSize = GallerySizeMode.Vertical;
            gal.SizeMode = GallerySizeMode.None;
            gal.ShowScrollBar = ShowScrollBar.Hide;
            gal.ItemCheckMode = ItemCheckMode.SingleRadio;

            StateAppearances caption = gal.Appearance.ItemCaptionAppearance;
            caption.Normal.Options.UseTextOptions = true;
            caption.Normal.TextOptions.HAlignment = HorzAlignment.Near;
            caption.Normal.TextOptions.VAlignment = VertAlignment.Center;
            caption.Hovered.Options.UseTextOptions = true;
            caption.Hovered.TextOptions.HAlignment = HorzAlignment.Near;
            caption.Hovered.TextOptions.VAlignment = VertAlignment.Center;
            caption.Pressed.Options.UseTextOptions = true;
            caption.Pressed.TextOptions.HAlignment = HorzAlignment.Near;
            caption.Pressed.TextOptions.VAlignment = VertAlignment.Center;

            gal.ItemImageLocation = Locations.Left;
            StateAppearances description = gal.Appearance.ItemDescriptionAppearance;
            description.Normal.TextOptions.HAlignment = HorzAlignment.Near;
            description.Normal.Options.UseTextOptions = true;
            description.Hovered.TextOptions.HAlignment = HorzAlignment.Near;
            description.Hovered.Options.UseTextOptions = true;
            description.Pressed.TextOptions.HAlignment = HorzAlignment.Near;
            description.Pressed.Options.UseTextOptions = true;

            gal.Groups.Add(new GalleryItemGroup());
            gal.StretchItems = true;

            return gdd;
        }

        private GalleryDropDown CreateOrientationGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            GalleryItem portrait = new GalleryItem();
            portrait.Image = Properties.Resources.PageOrientationPortrait_32x32;
            portrait.Caption = Properties.Resources.PortraitOrientation;
            GalleryItem landscape = new GalleryItem();
            landscape.Image = Properties.Resources.PageOrientationLandscape_32x32;
            landscape.Caption = Properties.Resources.LandscapeOrientation;

            gdd.Gallery.Groups[0].Items.Add(portrait);
            gdd.Gallery.Groups[0].Items.Add(landscape);
            gdd.Gallery.ItemCheckedChanged += new GalleryItemEventHandler(OnOrientationGalleryItemCheckedChanged);
            portrait.Checked = true;

            return gdd;
        }

        private GalleryDropDown CreatePrintStyleGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            gdd.Gallery.ItemCheckMode = ItemCheckMode.SingleRadio;
            memoStyle = new GalleryItem();
            memoStyle.Image = Properties.Resources.MemoStyle_32x32;
            memoStyle.Caption = Properties.Resources.MemoStyleString;
            tableStyle = new GalleryItem();
            tableStyle.Image = Properties.Resources.TableStyle_32x32;
            tableStyle.Caption = Properties.Resources.TableStyleString;
            gdd.Gallery.Groups[0].Items.Add(memoStyle);
            gdd.Gallery.Groups[0].Items.Add(tableStyle);
            gdd.Gallery.ItemCheckedChanged += new GalleryItemEventHandler(
                OnPrintStyleGalleryItemCheckedChanged);
            memoStyle.Checked = true;

            return gdd;
        }

        private GalleryDropDown CreatePrinterGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            PrinterSettings settings = new PrinterSettings();
            GalleryItem defaultPrinter = null;

            try
            {
                foreach (string s in PrinterSettings.InstalledPrinters)
                {
                    GalleryItem item = new GalleryItem();
                    item.Image = Properties.Resources.Printer_32x32;
                    item.Caption = s;
                    gdd.Gallery.Groups[0].Items.Add(item);
                    settings.PrinterName = s;
                    if (settings.IsDefaultPrinter) defaultPrinter = item;
                }
            }
            catch { }

            gdd.Gallery.ItemCheckedChanged += 
                new GalleryItemEventHandler(OnPrinterGalleryItemCheckedChanged);
            if (defaultPrinter != null) defaultPrinter.Checked = true;

            return gdd;
        }

        private GalleryDropDown CreatePageSizeGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            GalleryItem letter = new GalleryItem();
            letter.Image  = Properties.Resources.PaperKind_Letter_32x32;
            letter.Caption  = Properties.Resources.PaperKindLetter;
            letter.Description  = Properties.Resources.PaperKindLetterDescription;
            letter.Tag  = PaperKind.Letter;
            GalleryItem tabloid = new GalleryItem();
            tabloid.Image = Properties.Resources.PaperKind_Tabloid_32x32;
            tabloid.Caption = Properties.Resources.PaperKindTabloid;
            tabloid.Description = Properties.Resources.PaperKindTabloidDescription;
            tabloid.Tag = PaperKind.Tabloid;
            GalleryItem legal = new GalleryItem();
            legal.Image = Properties.Resources.PaperKind_Legal_32x32;
            letter.Caption = Properties.Resources.PaperKindLetter;
            letter.Description = Properties.Resources.PaperKindLetterDescription;
            letter.Tag = PaperKind.Letter;
            GalleryItem executive = new GalleryItem();
            executive.Image = Properties.Resources.PaperKind_Executive_32x32;
            executive.Caption = Properties.Resources.PaperKindExecutive;
            executive.Description = Properties.Resources.PaperKindExecutiveDescription;
            executive.Tag = PaperKind.Executive;
            GalleryItem a3 = new GalleryItem();
            a3.Image = Properties.Resources.PaperKind_A3_32x32;
            a3.Caption = Properties.Resources.PaperKindA3;
            a3.Description = Properties.Resources.PaperKindA3Description;
            a3.Tag = PaperKind.A3;
            GalleryItem a4 = new GalleryItem();
            a4.Image = Properties.Resources.PaperKind_A4_32x32;
            a4.Caption = Properties.Resources.PaperKindA4;
            a4.Description = Properties.Resources.PaperKindA4Description;
            a4.Tag = PaperKind.A4;
            GalleryItem a5 = new GalleryItem();
            a5.Image = Properties.Resources.PaperKind_A5_32x32;
            a5.Caption = Properties.Resources.PaperKindA5;
            a5.Description = Properties.Resources.PaperKindA5Description;
            a5.Tag = PaperKind.A5;
            GalleryItem a6 = new GalleryItem();
            a6.Image = Properties.Resources.PaperKind_A6_32x32;
            a6.Caption = Properties.Resources.PaperKindA6;
            a6.Description = Properties.Resources.PaperKindA6Description;
            a6.Tag = PaperKind.A6;
            gdd.Gallery.Groups[0].Items.Add(letter);
            gdd.Gallery.Groups[0].Items.Add(tabloid);
            gdd.Gallery.Groups[0].Items.Add(legal);
            gdd.Gallery.Groups[0].Items.Add(executive);
            gdd.Gallery.Groups[0].Items.Add(a3);
            gdd.Gallery.Groups[0].Items.Add(a4);
            gdd.Gallery.Groups[0].Items.Add(a5);
            gdd.Gallery.Groups[0].Items.Add(a6);
            gdd.Gallery.ItemCheckedChanged += 
                new GalleryItemEventHandler(OnPaperSizeGalleryItemCheckedChanged);
            a4.Checked = true;

            return gdd;
        }

        private GalleryDropDown CreateMarginsGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            GalleryItem normal = new GalleryItem();
            normal.Image = Properties.Resources.PageMarginsNormal_48x48;
            normal.Caption = Properties.Resources.MarginsNormal;
            normal.Description = Properties.Resources.MarginsNormalDescription;
            normal.Tag = new Padding(25, 25, 25, 25);
            GalleryItem narrow = new GalleryItem();
            narrow.Image = Properties.Resources.PageMarginsNarrow_48x48;
            narrow.Caption = Properties.Resources.MarginsNarrow;
            narrow.Description = Properties.Resources.MarginsNarrowDescription;
            narrow.Tag = new Padding(12, 12, 12, 12);
            GalleryItem moderate = new GalleryItem();
            moderate.Image = Properties.Resources.PageMarginsModerate_48x48;
            moderate.Caption = Properties.Resources.MarginsModerate;
            moderate.Description = Properties.Resources.MarginsModerateDescription;
            moderate.Tag = new Padding(19, 25, 19, 25);
            GalleryItem wide = new GalleryItem();
            wide.Image = Properties.Resources.PageMarginsWide_48x48;
            wide.Caption = Properties.Resources.MarginsWide;
            wide.Description = Properties.Resources.MarginsWideDescription;
            wide.Tag = new Padding(50, 25, 50, 25);
            gdd.Gallery.Groups[0].Items.Add(normal);
            gdd.Gallery.Groups[0].Items.Add(narrow);
            gdd.Gallery.Groups[0].Items.Add(moderate);
            gdd.Gallery.Groups[0].Items.Add(wide);
            gdd.Gallery.ItemCheckedChanged += new GalleryItemEventHandler(
                OnMarginsGalleryItemCheckedChanged);
            normal.Checked = true;

            return gdd;
        }

        private GalleryDropDown CreateDuplexGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            GalleryItem oneSided = new GalleryItem();
            oneSided.Image = Properties.Resources.MultiplePagesLarge_32x32;
            oneSided.Caption = Properties.Resources.OneSide;
            oneSided.Description = Properties.Resources.OneSideDescription;
            oneSided.Tag = false;
            GalleryItem twoSided = new GalleryItem();
            twoSided.Image = Properties.Resources.MultiplePagesLarge_32x32;
            twoSided.Caption = Properties.Resources.TwoSide;
            twoSided.Description = Properties.Resources.TwoSideDescription;
            twoSided.Tag = false;
            gdd.Gallery.Groups[0].Items.Add(oneSided);
            gdd.Gallery.Groups[0].Items.Add(twoSided);
            gdd.Gallery.ItemCheckedChanged += new GalleryItemEventHandler(
                OnDuplexGalleryItemCheckedChanged);
            oneSided.Checked = true;

            return gdd;
        }

        private GalleryDropDown CreateCollateGallery()
        {
            GalleryDropDown gdd = CreateListBoxGallery();
            GalleryItem collated = new GalleryItem();
            collated.Image = Properties.Resources.MultiplePagesLarge_32x32;
            collated.Caption = Properties.Resources.Collated;
            collated.Description = Properties.Resources.CollatedDescription;
            collated.Tag = true;
            GalleryItem uncollated = new GalleryItem();
            uncollated.Image = Properties.Resources.MultiplePagesLarge_32x32;
            uncollated.Caption = Properties.Resources.Uncollated;
            uncollated.Description = Properties.Resources.UncollatedDescription;
            uncollated.Tag = false;
            gdd.Gallery.Groups[0].Items.Add(collated);
            gdd.Gallery.Groups[0].Items.Add(uncollated);
            gdd.Gallery.ItemCheckedChanged += new GalleryItemEventHandler(
                OnCollateGalleryItemCheckedChanged);
            collated.Checked = true;

            return gdd;
        }

        private void InitializeControls()
        {
            pnSplitContainer.Panel1MinSize = lcgOption.MinSize.Width + 6;

            ddbOrientation.DropDownControl = CreateOrientationGallery();
            ddbMargins.DropDownControl = CreateMarginsGallery();
            ddbPaperSize.DropDownControl = CreatePageSizeGallery();
            ddbCollate.DropDownControl = CreateCollateGallery();
            ddbPrinter.DropDownControl = CreatePrinterGallery();
            ddbDuplex.DropDownControl = CreateDuplexGallery();
            ddbPrintStyle.DropDownControl = CreatePrintStyleGallery();

            updatedZoom = true;
            seZoom.EditValue = 70;
            updatedZoom = false;
        }
        #endregion

        #region CONTROL EVENTS
        private void OnPrintStyleGalleryItemCheckedChanged(object sender,
            GalleryItemEventArgs args)
        {
            ddbPrintStyle.Text = args.Item.Caption;
            ddbPrintStyle.Image = args.Item.Image;
            if (pcPrint.PrintingSystem != null) CreateDocument();
        }

        private void OnPrinterGalleryItemCheckedChanged(object sender, 
            GalleryItemEventArgs args)
        {
            ddbPrinter.Text = args.Item.Caption;
            ddbPrinter.Image = args.Item.Image;
        }

        private void OnPaperSizeGalleryItemCheckedChanged(object sender, 
            GalleryItemEventArgs args)
        {
            ddbPaperSize.Image = args.Item.Image;
            ddbPaperSize.Text = args.Item.Caption;
            ddbPaperSize.Tag = args.Item.Tag;
            if (pcPrint.PrintingSystem != null)
                pcPrint.PrintingSystem.PageSettings.PaperKind = GetPaperKind();
            UpdatePageButtonsEnabledState();
        }

        private void OnOrientationGalleryItemCheckedChanged(object sender,
            GalleryItemEventArgs args)
        {
            ddbOrientation.Text = args.Item.Caption;
            ddbOrientation.Image = args.Item.Image;
            if (pcPrint.PrintingSystem != null)
                pcPrint.PrintingSystem.PageSettings.Landscape = GetLandscape();
            UpdatePageButtonsEnabledState();
        }

        private void OnMarginsGalleryItemCheckedChanged(object sender,
            GalleryItemEventArgs args)
        {
            ddbMargins.Image = args.Item.Image;
            ddbMargins.Text = args.Item.Caption;
            ddbMargins.Tag = args.Item.Tag;
            if (pcPrint.PrintingSystem != null)
            {
                Margins margins = GetMargins();
                pcPrint.PrintingSystem.PageSettings.LeftMargin = margins.Left;
                pcPrint.PrintingSystem.PageSettings.RightMargin = margins.Right;
                pcPrint.PrintingSystem.PageSettings.TopMargin = margins.Top;
                pcPrint.PrintingSystem.PageSettings.BottomMargin = margins.Bottom;
            }
            UpdatePageButtonsEnabledState();
        }

        private void OnDuplexGalleryItemCheckedChanged(object sender, 
            GalleryItemEventArgs args)
        {
            ddbDuplex.Image = args.Item.Image;
            ddbDuplex.Text = args.Item.Caption;
            ddbDuplex.Tag = args.Item.Tag;
        }

        private void OnCollateGalleryItemCheckedChanged(object sender, GalleryItemEventArgs args)
        {
            ddbCollate.Image = args.Item.Image;
            ddbCollate.Text = args.Item.Caption;
            ddbCollate.Tag = args.Item.Tag;
        }

        private void OnStartPrint(object sender, PrintDocumentEventArgs args)
        {
            args.PrintDocument.PrinterSettings.Copies = (short)seCopy.Value;
            GetMargins();
            args.PrintDocument.PrinterSettings.Collate = (bool)ddbCollate.Tag;
            args.PrintDocument.PrinterSettings.Duplex = ((bool)ddbDuplex.Tag) ? 
                Duplex.Horizontal :Duplex.Simplex;
        }

        private void ztbPaper_EditValueChanged(object sender, System.EventArgs args)
        {
            if (!updatedZoom)
            {
                updatedZoom = true;
                try { seZoom.EditValue = GetZoomValue(); }
                finally { updatedZoom = false; }
            }
        }

        private void seZoom_EditValueChanged(object sender, System.EventArgs args)
        {
            try
            {
                int zoomValue = Int32.Parse((string)seZoom.EditValue.ToString());
                ztbPaper.Value = ZoomValueToValue(zoomValue);
                pcPrint.Zoom = 0.01f * (int)zoomValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Couldn't zoom. {0}.\n", ex.Message);
            }
        }

        private void bePage_EditValueChanged(object sender, EventArgs args)
        {
            int pageIndex = Convert.ToInt32(bePage.EditValue);
            pcPrint.SelectedPageIndex = pageIndex - 1;
            UpdatePageButtonsEnabledState(pageIndex);
        }

        private void bePage_EditValueChanging(object sender, ChangingEventArgs args)
        {
            try
            {
                int pageIndex = Int32.Parse(args.NewValue.ToString());
                if (pageIndex < 1) pageIndex = 1;
                else if (pageIndex > pcPrint.PrintingSystem.Pages.Count)
                    pageIndex = pcPrint.PrintingSystem.Pages.Count;
                args.NewValue = pageIndex;
            }
            catch (Exception)
            {
                args.NewValue = 1;
            }
        }

        private void bePage_ButtonClick(object sender, ButtonPressedEventArgs args)
        {
            int pageIndex = (int)bePage.EditValue;
            if (args.Button.Kind == ButtonPredefines.Left)
            {
                if (pageIndex > 1) pageIndex--;
            }
            else if (args.Button.Kind == ButtonPredefines.Right)
            {
                if (pageIndex < pcPrint.PrintingSystem.Pages.Count)
                    pageIndex++;
            }
            bePage.EditValue = pageIndex;
        }
        

        private void btnPrint_Click(object sender, System.EventArgs args)
        {
            ((PrintingSystem)pcPrint.PrintingSystem).Print(ddbPrinter.Text);
        }

        private void pcPrint_SelectedPageChanged(object sender, System.EventArgs args)
        {
            bePage.EditValue = pcPrint.SelectedPageIndex + 1;
        }
        #endregion

        #region FORM EVENTS
        #endregion
    }
}
