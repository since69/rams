﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class ExportPage : BackstageViewBasePage
    {
        #region VARIABLES
        List<ImageFormat> imageFormats = new List<ImageFormat>
        {
            ImageFormat.Bmp, ImageFormat.Gif, ImageFormat.Jpeg,
            ImageFormat.Png, ImageFormat.Tiff, ImageFormat.Emf,
            ImageFormat.Wmf, ImageFormat.Png
        };
        #endregion

        #region CONSTRUCTORS
        public ExportPage()
        {
            InitializeComponent();
        }
        #endregion

        #region METHODS
        internal static void ShowExportErrorMessage()
        {
            XtraMessageBox.Show(
                Properties.Resources.ExportErrorText, Properties.Resources.Export,
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ExportTo(string index, string fileName, PrintingSystem ps)
        {
            if (string.IsNullOrEmpty(fileName)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (index.Contains("PDF")) ps.ExportToPdf(fileName);
                else if (index.Contains("HTML")) ps.ExportToHtml(fileName);
                else if (index.Contains("MHT")) ps.ExportToMht(fileName);
                else if (index.Contains("RTF")) ps.ExportToRtf(fileName);
                else if (index.Contains("XLS")) ps.ExportToXls(fileName);
                else if (index.Contains("XlSX")) ps.ExportToXlsx(fileName);
                else if (index.Contains("CSV")) ps.ExportToCsv(fileName);
                else if (index.Contains("Text")) ps.ExportToText(fileName);
                else if (index.Contains("Image")) ps.ExportToImage(fileName, 
                    imageFormats[dlgExport.FilterIndex]);
                Cursor.Current = Cursors.Default;
                if (XtraMessageBox.Show(Properties.Resources.OpentFileQuestion, 
                    Properties.Resources.Export, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Process process = new Process();
                    try
                    {
                        process.StartInfo.FileName = dlgExport.FileName;
                        process.Start();
                    }
                    catch { }
                }
            }
            catch
            {
                ShowExportErrorMessage();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region CONTROL EVENTS
        private void galExport_Gallery_ItemClick(object sender, 
            DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs args)
        {
            string index = string.Format("{0}", args.Item.Tag);
            MainForm form = BackstageView.Ribbon.FindForm() as MainForm;
            if (form == null) return;

            if (index.Contains("PDF"))
                dlgExport.Filter = Properties.Resources.PDFFilter;
            else if (index.Contains("HTML"))
                dlgExport.Filter = Properties.Resources.HTMLFilter;
            else if (index.Contains("MHT"))
                dlgExport.Filter = Properties.Resources.MHTFilter;
            else if (index.Contains("RTF"))
                dlgExport.Filter = Properties.Resources.RTFFilter;
            else if (index.Contains("XLS"))
                dlgExport.Filter = Properties.Resources.XLSFilter;
            else if (index.Contains("XLSX"))
                dlgExport.Filter = Properties.Resources.XLSXFilter;
            else if (index.Contains("CSV"))
                dlgExport.Filter = Properties.Resources.CSVFilter;
            else if (index.Contains("Text"))
                dlgExport.Filter = Properties.Resources.TextFilter;
            else if (index.Contains("Image"))
                dlgExport.Filter = Properties.Resources.ImageFilter;
            
            dlgExport.Filter += "|" + Properties.Resources.AllFilesFilter;
            dlgExport.FilterIndex = 0;
            dlgExport.FileName = form.CurrentModuleName;
            if (dlgExport.ShowDialog() != DialogResult.OK) return;
            Cursor.Current = Cursors.WaitCursor;
            PrintingSystem ps = new PrintingSystem();
            PrintableComponentLink lnk = new PrintableComponentLink(ps);
            lnk.Component = form.CurrentExportComponent;
            lnk.CreateDocument();
            ExportTo(index, dlgExport.FileName, ps);
        }
        #endregion
    }
}
