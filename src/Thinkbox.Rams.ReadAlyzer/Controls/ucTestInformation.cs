﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Thinkbox.Rams.ReadAlyzer.Helpers;
using Thinkbox.Rams.Device;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class ucTestInformation : UserControl
    {
        #region PROPERTIES
        private Data.Test test = null;
        public Data.Test Test
        {
            set
            {
                test = value;
                UpdateInformation();
            }
        }
        #endregion

        #region CONSTRUCTORS
        public ucTestInformation()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region METHODS
        public void Clear()
        {
            ucGazeData.Clear();
        }

        public void UpdateGazeData(GazeData gazeData)
        {
            string data = string.Format("{0:f2}\t {1,-6}, {2,-6}, {3,-6}, {4,-6}\n",
                 gazeData.Elapsed, gazeData.LeftX, gazeData.LeftY, gazeData.RightX,
                 gazeData.RightX);
            ucGazeData.AppendData(data);
        }

        private void UpdateInformation()
        {
            if (test.TestText.Id == 0)
            {
                Clean();
                return;
            }
            
            ucTrainee.TagColor = Color.DodgerBlue;
            ucTestText.TagColor = Color.DodgerBlue;
            ucGazeData.TagColor = Color.DodgerBlue;
            ucTrainee.MajorValue = test.Trainee.Person.FullName;
            ucTrainee.MinorValue = test.Trainee.BirthDate.ToShortDateString();
            UpdateTextInformation();
        }

        private void Clean()
        {
            ucTrainee.TagColor = Color.FromArgb(60, 63, 68);
            ucTrainee.MajorValue = string.Empty;
            ucTrainee.MinorValue = string.Empty;
            ucTestText.TagColor = Color.FromArgb(60, 63, 68);
            ucTestText.Title = "Text";
            ucTestText.FirstValue = string.Empty;
            ucTestText.SecondValue = string.Empty;
            ucTestText.ThirdValue = string.Empty;
            ucGazeData.TagColor = Color.FromArgb(60, 63, 68);
            ucGazeData.Clear();
        }

        private void InitializeControls()
        {
            ucTrainee.Title = "Trainee";
            ucTestText.FirstCaption = "Level";
            ucTestText.SecondCaption = "Line";
            ucTestText.ThirdCaption = "Word";
            ucGazeData.Title = "Measured Data";
            Clean();   
        }

        private void UpdateTextInformation()
        {
            if (test.TestText.Text == null) return;

            int numOfLines = DataAnalyzer.GetNumOfLines(test.TestText.Text);
            int numOfWords = DataAnalyzer.GetNumOfWords(test.TestText.Text);

            ucTestText.Title = test.TestText.Title;
            ucTestText.FirstValue = $"{ test.TestText.Level }";
            ucTestText.SecondValue = $"{ numOfLines }";
            ucTestText.ThirdValue = $"{ numOfWords }";
        }
        #endregion

        #region EVENTS
        #endregion
    }
}
