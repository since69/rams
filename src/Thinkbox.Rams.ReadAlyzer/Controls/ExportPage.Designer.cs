﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ExportPage
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem4 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem5 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem6 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem7 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem8 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem9 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            this.pnSplitContainer = new System.Windows.Forms.SplitContainer();
            this.galExport = new DevExpress.XtraBars.Ribbon.GalleryControl();
            this.galleryControlClient1 = new DevExpress.XtraBars.Ribbon.GalleryControlClient();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.dlgExport = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pnSplitContainer)).BeginInit();
            this.pnSplitContainer.Panel1.SuspendLayout();
            this.pnSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.galExport)).BeginInit();
            this.galExport.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnSplitContainer
            // 
            this.pnSplitContainer.BackColor = System.Drawing.Color.Transparent;
            this.pnSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.pnSplitContainer.ForeColor = System.Drawing.Color.Transparent;
            this.pnSplitContainer.IsSplitterFixed = true;
            this.pnSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.pnSplitContainer.Name = "pnSplitContainer";
            // 
            // pnSplitContainer.Panel1
            // 
            this.pnSplitContainer.Panel1.Controls.Add(this.galExport);
            this.pnSplitContainer.Panel1.Controls.Add(this.lblTitle);
            this.pnSplitContainer.Panel1.Padding = new System.Windows.Forms.Padding(20);
            this.pnSplitContainer.Size = new System.Drawing.Size(1030, 551);
            this.pnSplitContainer.SplitterDistance = 425;
            this.pnSplitContainer.TabIndex = 0;
            // 
            // galExport
            // 
            this.galExport.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.galExport.Controls.Add(this.galleryControlClient1);
            this.galExport.Cursor = System.Windows.Forms.Cursors.Default;
            this.galExport.DesignGalleryGroupIndex = 0;
            this.galExport.DesignGalleryItemIndex = 0;
            this.galExport.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galExport.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.galExport.Gallery.Appearance.ItemDescriptionAppearance.Hovered.Options.UseTextOptions = true;
            this.galExport.Gallery.Appearance.ItemDescriptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galExport.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseTextOptions = true;
            this.galExport.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galExport.Gallery.Appearance.ItemDescriptionAppearance.Pressed.Options.UseTextOptions = true;
            this.galExport.Gallery.Appearance.ItemDescriptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galExport.Gallery.AutoFitColumns = false;
            this.galExport.Gallery.AutoSize = DevExpress.XtraBars.Ribbon.GallerySizeMode.None;
            this.galExport.Gallery.BackColor = System.Drawing.Color.Transparent;
            this.galExport.Gallery.CheckDrawMode = DevExpress.XtraBars.Ribbon.Gallery.CheckDrawMode.OnlyImage;
            this.galExport.Gallery.ColumnCount = 1;
            this.galExport.Gallery.FixedImageSize = false;
            galleryItemGroup1.Caption = "Main Group";
            galleryItem1.Caption = "PDF File";
            galleryItem1.Description = "Adobe Portable Document Format";
            galleryItem1.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToPDF_32x32;
            galleryItem1.Tag = "PDF";
            galleryItem2.Caption = "HTML File";
            galleryItem2.Description = "Web Page";
            galleryItem2.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToHTML_32x32;
            galleryItem2.Tag = "HTML";
            galleryItem3.Caption = "MHT File";
            galleryItem3.Description = "Single File Web Page";
            galleryItem3.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToMHT_32x32;
            galleryItem3.Tag = "MHT";
            galleryItem4.Caption = "RTF File";
            galleryItem4.Description = "Rich Text Format";
            galleryItem4.Hint = "RTF";
            galleryItem4.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToRTF_32x32;
            galleryItem5.Caption = "XLS File";
            galleryItem5.Description = "Microsoft Excel 2000-2003 Workbook";
            galleryItem5.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToXLS_32x32;
            galleryItem5.Tag = "XLS";
            galleryItem6.Caption = "XLSX File";
            galleryItem6.Description = "Microsoft Excel 2007 Workbook";
            galleryItem6.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToXLSX_32x32;
            galleryItem6.Tag = "XLSX";
            galleryItem7.Caption = "CSV File";
            galleryItem7.Description = "Comma-Separated Values Text";
            galleryItem7.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToCSV_32x32;
            galleryItem7.Tag = "CSV";
            galleryItem8.Caption = "TXT File";
            galleryItem8.Description = "Plain Text";
            galleryItem8.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToImage_32x32;
            galleryItem8.Tag = "Text";
            galleryItem9.Caption = "Image FIle";
            galleryItem9.Description = "BMP, GIF, JPEG, PNG, TIFF, EMF, WMF";
            galleryItem9.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.ExportToImage_32x32;
            galleryItem9.Tag = "Image";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3,
            galleryItem4,
            galleryItem5,
            galleryItem6,
            galleryItem7,
            galleryItem8,
            galleryItem9});
            this.galExport.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.galExport.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            this.galExport.Gallery.ShowGroupCaption = false;
            this.galExport.Gallery.ShowItemText = true;
            this.galExport.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Auto;
            this.galExport.Gallery.StretchItems = true;
            this.galExport.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.galExport_Gallery_ItemClick);
            this.galExport.Location = new System.Drawing.Point(20, 56);
            this.galExport.Name = "galExport";
            this.galExport.Size = new System.Drawing.Size(385, 475);
            this.galExport.TabIndex = 2;
            this.galExport.Tag = "";
            this.galExport.Text = "galleryControl1";
            // 
            // galleryControlClient1
            // 
            this.galleryControlClient1.GalleryControl = this.galExport;
            this.galleryControlClient1.Location = new System.Drawing.Point(1, 1);
            this.galleryControlClient1.Size = new System.Drawing.Size(383, 473);
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.Appearance.Options.UseFont = true;
            this.lblTitle.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTitle.LineVisible = true;
            this.lblTitle.Location = new System.Drawing.Point(20, 20);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.ShowLineShadow = false;
            this.lblTitle.Size = new System.Drawing.Size(385, 36);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Export";
            // 
            // dlgExport
            // 
            this.dlgExport.Title = "Export...";
            // 
            // ExportPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnSplitContainer);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.Name = "ExportPage";
            this.Size = new System.Drawing.Size(1030, 551);
            this.pnSplitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnSplitContainer)).EndInit();
            this.pnSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.galExport)).EndInit();
            this.galExport.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer pnSplitContainer;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraBars.Ribbon.GalleryControl galExport;
        private DevExpress.XtraBars.Ribbon.GalleryControlClient galleryControlClient1;
        private System.Windows.Forms.SaveFileDialog dlgExport;
    }
}
