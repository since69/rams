﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class ucPhotoPresenter : UserControl
    {
        #region PROPERTIES
        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }
        
        public Image Photo
        {
            get { return pePhoto.Image; }
            set { pePhoto.Image = value; }
        }
        public string MajorValue
        {
            get { return lblMajorValue.Text; }
            set { lblMajorValue.Text = value; }
        }
        public string MinorValue
        {
            get { return lblMinorValue.Text; }
            set { lblMinorValue.Text = value; }
        }
        [Description("Color of tag."), Category("Appearance")]
        public Color TagColor
        {
            get { return lblTag.BackColor; }
            set { lblTag.BackColor = value; }
        }
        [Description("Color of title."), Category("Appearance")]
        public Color TitleColor
        {
            get { return lblTitle.ForeColor; }
            set { lblTitle.ForeColor = value; }
        }
        [Description("Color of major value."), Category("Appearance")]
        public Color MajorValueColor
        {
            get { return lblMajorValue.ForeColor; }
            set { lblMajorValue.ForeColor = value; }
        }
        [Description("Color of minor value."), Category("Appearance")]
        public Color MinorValueColor
        {
            get { return lblMinorValue.ForeColor; }
            set { lblMinorValue.ForeColor = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public ucPhotoPresenter()
        {
            InitializeComponent();
            InitControls();
        }
        #endregion

        #region METHODS
        private void InitControls()
        {
            pePhoto.Properties.OptionsMask.MaskType = PictureEditMaskType.Circle;
            pePhoto.Image = Properties.Resources.UnknownUser_256x256;
        }
        #endregion
    }
}
