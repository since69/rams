﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Thinkbox.Rams.Device;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    /// <summary>
    /// Display measured data with graph, text.
    /// </summary>
    public partial class ucTestData : UserControl
    {
        #region PROPERTIES
        public string CaptionLeft
        {
            get { return lblCaptionLeft.Text; }
            set { lblCaptionLeft.Text = value; }
        }
        public string CaptionRight
        {
            get { return lblCaptionRight.Text; }
            set { lblCaptionRight.Text = value; }
        }
        public Color tagColor = Color.DodgerBlue;
        [Description("Color of tag."), Category("Appearance")]
        public Color TagColor
        {
            get { return tagColor; }
            set
            {
                tagColor = value;
                lblTagLeft.BackColor = tagColor;
                lblTagRight.BackColor = tagColor;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public ucTestData()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region METHODS
        /// <summary>
        /// Reset chart.
        /// </summary>
        public void Clear()
        {
            chartLeftEye.Series[0].Points.Clear();
            InitializeControls();
        }

        public void UpdateGazeData(GazeData gazeData)
        {
            if (lblTagLeft.BackColor == Color.FromArgb(60, 63, 68))
            {
                lblTagLeft.BackColor = Color.Orange;
                lblTagRight.BackColor = Color.DodgerBlue;
            }
            
            chartLeftEye.Series[0].Points.Add(
                new DevExpress.XtraCharts.SeriesPoint(
                    gazeData.Elapsed, gazeData.LeftX));
            chartRightEye.Series[0].Points.Add(
                new DevExpress.XtraCharts.SeriesPoint(
                    gazeData.Elapsed, gazeData.RightX));
        }

        private void InitializeControls()
        {
            lblTagLeft.BackColor = Color.FromArgb(60, 63, 68);
            lblTagRight.BackColor = Color.FromArgb(60, 63, 68);
        }
        #endregion
    }
}
