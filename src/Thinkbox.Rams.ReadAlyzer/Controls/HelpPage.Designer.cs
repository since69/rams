﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class HelpPage
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpPage));
            DevExpress.XtraBars.Ribbon.GalleryItemGroup galleryItemGroup1 = new DevExpress.XtraBars.Ribbon.GalleryItemGroup();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem1 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem2 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            DevExpress.XtraBars.Ribbon.GalleryItem galleryItem3 = new DevExpress.XtraBars.Ribbon.GalleryItem();
            this.pnSplitContainer = new System.Windows.Forms.SplitContainer();
            this.galSupportItems = new DevExpress.XtraBars.Ribbon.GalleryControl();
            this.galleryControlClient1 = new DevExpress.XtraBars.Ribbon.GalleryControlClient();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblSeprator = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pnSplitContainer)).BeginInit();
            this.pnSplitContainer.Panel1.SuspendLayout();
            this.pnSplitContainer.Panel2.SuspendLayout();
            this.pnSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.galSupportItems)).BeginInit();
            this.galSupportItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnSplitContainer
            // 
            resources.ApplyResources(this.pnSplitContainer, "pnSplitContainer");
            this.pnSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.pnSplitContainer.Name = "pnSplitContainer";
            // 
            // pnSplitContainer.Panel1
            // 
            this.pnSplitContainer.Panel1.Controls.Add(this.galSupportItems);
            this.pnSplitContainer.Panel1.Controls.Add(this.lblTitle);
            resources.ApplyResources(this.pnSplitContainer.Panel1, "pnSplitContainer.Panel1");
            // 
            // pnSplitContainer.Panel2
            // 
            this.pnSplitContainer.Panel2.Controls.Add(this.lblSeprator);
            // 
            // galSupportItems
            // 
            this.galSupportItems.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.galSupportItems.Controls.Add(this.galleryControlClient1);
            this.galSupportItems.Cursor = System.Windows.Forms.Cursors.Default;
            this.galSupportItems.DesignGalleryGroupIndex = 0;
            this.galSupportItems.DesignGalleryItemIndex = 0;
            resources.ApplyResources(this.galSupportItems, "galSupportItems");
            // 
            // 
            // 
            this.galSupportItems.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.galSupportItems.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galSupportItems.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.galSupportItems.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galSupportItems.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.galSupportItems.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galSupportItems.Gallery.Appearance.ItemDescriptionAppearance.Hovered.Options.UseTextOptions = true;
            this.galSupportItems.Gallery.Appearance.ItemDescriptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galSupportItems.Gallery.Appearance.ItemDescriptionAppearance.Normal.Options.UseTextOptions = true;
            this.galSupportItems.Gallery.Appearance.ItemDescriptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galSupportItems.Gallery.Appearance.ItemDescriptionAppearance.Pressed.Options.UseTextOptions = true;
            this.galSupportItems.Gallery.Appearance.ItemDescriptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.galSupportItems.Gallery.AutoFitColumns = false;
            this.galSupportItems.Gallery.AutoSize = DevExpress.XtraBars.Ribbon.GallerySizeMode.Vertical;
            this.galSupportItems.Gallery.BackColor = System.Drawing.Color.Transparent;
            this.galSupportItems.Gallery.ColumnCount = 1;
            this.galSupportItems.Gallery.DistanceItemImageToText = 20;
            this.galSupportItems.Gallery.DrawImageBackground = false;
            this.galSupportItems.Gallery.FixedImageSize = false;
            resources.ApplyResources(galleryItemGroup1, "galleryItemGroup1");
            resources.ApplyResources(galleryItem1, "galleryItem1");
            galleryItem1.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Help_32x32;
            galleryItem1.Tag = "LinkHelp";
            resources.ApplyResources(galleryItem2, "galleryItem2");
            galleryItem2.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.BookHelp_32x32;
            galleryItem2.Tag = "LinkGetStarted";
            resources.ApplyResources(galleryItem3, "galleryItem3");
            galleryItem3.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Mail_32x32;
            galleryItem3.Tag = "LinkGetSupport";
            galleryItemGroup1.Items.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItem[] {
            galleryItem1,
            galleryItem2,
            galleryItem3});
            this.galSupportItems.Gallery.Groups.AddRange(new DevExpress.XtraBars.Ribbon.GalleryItemGroup[] {
            galleryItemGroup1});
            this.galSupportItems.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Left;
            this.galSupportItems.Gallery.ShowGroupCaption = false;
            this.galSupportItems.Gallery.ShowItemText = true;
            this.galSupportItems.Gallery.ShowScrollBar = DevExpress.XtraBars.Ribbon.Gallery.ShowScrollBar.Hide;
            this.galSupportItems.Gallery.ItemClick += new DevExpress.XtraBars.Ribbon.GalleryItemClickEventHandler(this.galSupportItems_Gallery_ItemClick);
            this.galSupportItems.Name = "galSupportItems";
            // 
            // galleryControlClient1
            // 
            this.galleryControlClient1.GalleryControl = this.galSupportItems;
            resources.ApplyResources(this.galleryControlClient1, "galleryControlClient1");
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTitle.Appearance.Font")));
            this.lblTitle.Appearance.Options.UseFont = true;
            resources.ApplyResources(this.lblTitle, "lblTitle");
            this.lblTitle.LineLocation = DevExpress.XtraEditors.LineLocation.Bottom;
            this.lblTitle.LineVisible = true;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.ShowLineShadow = false;
            // 
            // lblSeprator
            // 
            resources.ApplyResources(this.lblSeprator, "lblSeprator");
            this.lblSeprator.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Vertical;
            this.lblSeprator.LineVisible = true;
            this.lblSeprator.Name = "lblSeprator";
            // 
            // HelpPage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnSplitContainer);
            this.ForeColor = System.Drawing.Color.Transparent;
            this.Name = "HelpPage";
            this.pnSplitContainer.Panel1.ResumeLayout(false);
            this.pnSplitContainer.Panel1.PerformLayout();
            this.pnSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnSplitContainer)).EndInit();
            this.pnSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.galSupportItems)).EndInit();
            this.galSupportItems.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer pnSplitContainer;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private DevExpress.XtraBars.Ribbon.GalleryControl galSupportItems;
        private DevExpress.XtraBars.Ribbon.GalleryControlClient galleryControlClient1;
        private DevExpress.XtraEditors.LabelControl lblSeprator;
    }
}
