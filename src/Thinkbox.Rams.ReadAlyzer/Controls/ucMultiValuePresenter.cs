﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    public partial class ucMultiValuePresenter : UserControl
    {
        #region PROPERTIES
        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }
        public string FirstValue
        {
            get { return lblFirstValue.Text; }
            set { lblFirstValue.Text = value; }
        }
        public string FirstCaption
        {
            get { return lblFirstCaption.Text; }
            set { lblFirstCaption.Text = value; }
        }
        public string SecondValue
        {
            get { return lblSecondValue.Text; }
            set { lblSecondValue.Text = value; }
        }
        public string SecondCaption
        {
            get { return lblSecondCaption.Text; }
            set { lblSecondCaption.Text = value; }
        }
        public string ThirdValue
        {
            get { return lblThirdValue.Text; }
            set { lblThirdValue.Text = value; }
        }
        public string ThirdCaption
        {
            get { return lblThirdCaption.Text; }
            set { lblThirdCaption.Text = value; }
        }
        [Description("Color of tag."), Category("Appearance")]
        public Color TagColor
        {
            get { return lblTag.BackColor; }
            set { lblTag.BackColor = value; }
        }
        [Description("Color of title."), Category("Appearance")]
        public Color TitleColor
        {
            get { return lblTitle.ForeColor; }
            set { lblTitle.ForeColor = value; }
        }
        private Color valueColor = Color.FromArgb(226, 226, 226);
        [Description("Color of values."), Category("Appearance")]
        public Color ValueColor
        {
            get { return valueColor; }
            set
            {
                valueColor = value;
                lblFirstValue.ForeColor = valueColor;
                lblSecondValue.ForeColor = valueColor;
                lblThirdValue.ForeColor = valueColor;
            }
        }
        private Color captionColor = Color.DarkGray;
        [Description("Color of captions."), Category("Appearance")]
        public Color CaptionColor
        {
            get { return captionColor; }
            set
            {
                captionColor = value;
                lblFirstCaption.ForeColor = captionColor;
                lblSecondCaption.ForeColor = captionColor;
                lblThirdCaption.ForeColor = captionColor;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public ucMultiValuePresenter()
        {
            InitializeComponent();
        }
        #endregion

        #region METHODS
        #endregion
    }
}
