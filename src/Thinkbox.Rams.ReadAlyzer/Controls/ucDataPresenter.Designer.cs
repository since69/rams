﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ucDataPresenter
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTag = new System.Windows.Forms.Label();
            this.txtMeasureData = new System.Windows.Forms.RichTextBox();
            this.pnWrapper.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.BackColor = System.Drawing.Color.Transparent;
            this.pnWrapper.ColumnCount = 3;
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Controls.Add(this.lblTitle, 2, 0);
            this.pnWrapper.Controls.Add(this.lblTag, 0, 0);
            this.pnWrapper.Controls.Add(this.txtMeasureData, 2, 1);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.RowCount = 2;
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Size = new System.Drawing.Size(310, 300);
            this.pnWrapper.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.lblTitle.Location = new System.Drawing.Point(12, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(295, 30);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "title";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTag
            // 
            this.lblTag.AutoSize = true;
            this.lblTag.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTag.Location = new System.Drawing.Point(0, 0);
            this.lblTag.Margin = new System.Windows.Forms.Padding(0);
            this.lblTag.Name = "lblTag";
            this.pnWrapper.SetRowSpan(this.lblTag, 2);
            this.lblTag.Size = new System.Drawing.Size(3, 300);
            this.lblTag.TabIndex = 0;
            // 
            // txtMeasureData
            // 
            this.txtMeasureData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.txtMeasureData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMeasureData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMeasureData.Font = new System.Drawing.Font("나눔바른고딕", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMeasureData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txtMeasureData.Location = new System.Drawing.Point(12, 33);
            this.txtMeasureData.Name = "txtMeasureData";
            this.txtMeasureData.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.txtMeasureData.Size = new System.Drawing.Size(295, 264);
            this.txtMeasureData.TabIndex = 3;
            this.txtMeasureData.Text = "";
            // 
            // ucDataPresenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.Controls.Add(this.pnWrapper);
            this.Name = "ucDataPresenter";
            this.Size = new System.Drawing.Size(310, 300);
            this.pnWrapper.ResumeLayout(false);
            this.pnWrapper.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private System.Windows.Forms.Label lblTag;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.RichTextBox txtMeasureData;
    }
}
