﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ucTestData
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            this.lblTagLeft = new System.Windows.Forms.Label();
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.chartRightEye = new DevExpress.XtraCharts.ChartControl();
            this.lblTagRight = new System.Windows.Forms.Label();
            this.lblCaptionRight = new System.Windows.Forms.Label();
            this.lblCaptionLeft = new System.Windows.Forms.Label();
            this.chartLeftEye = new DevExpress.XtraCharts.ChartControl();
            this.pnWrapper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRightEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartLeftEye)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTagLeft
            // 
            this.lblTagLeft.AutoSize = true;
            this.lblTagLeft.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblTagLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTagLeft.Location = new System.Drawing.Point(3, 597);
            this.lblTagLeft.Name = "lblTagLeft";
            this.lblTagLeft.Size = new System.Drawing.Size(240, 3);
            this.lblTagLeft.TabIndex = 0;
            // 
            // pnWrapper
            // 
            this.pnWrapper.ColumnCount = 3;
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnWrapper.Controls.Add(this.chartRightEye, 2, 0);
            this.pnWrapper.Controls.Add(this.lblTagRight, 2, 3);
            this.pnWrapper.Controls.Add(this.lblCaptionRight, 2, 2);
            this.pnWrapper.Controls.Add(this.lblTagLeft, 0, 3);
            this.pnWrapper.Controls.Add(this.lblCaptionLeft, 0, 2);
            this.pnWrapper.Controls.Add(this.chartLeftEye, 0, 0);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.RowCount = 4;
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.Size = new System.Drawing.Size(495, 600);
            this.pnWrapper.TabIndex = 0;
            // 
            // chartRightEye
            // 
            this.chartRightEye.BackColor = System.Drawing.Color.Transparent;
            this.chartRightEye.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartRightEye.DataBindings = null;
            xyDiagram1.AxisX.Tickmarks.MinorVisible = false;
            xyDiagram1.AxisX.Visibility = DevExpress.Utils.DefaultBoolean.False;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.GridLines.Visible = false;
            xyDiagram1.AxisY.Tickmarks.MinorVisible = false;
            xyDiagram1.AxisY.Visibility = DevExpress.Utils.DefaultBoolean.True;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.DefaultPane.BackColor = System.Drawing.Color.Transparent;
            xyDiagram1.DefaultPane.BorderVisible = false;
            xyDiagram1.Rotated = true;
            this.chartRightEye.Diagram = xyDiagram1;
            this.chartRightEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartRightEye.Legend.Name = "Default Legend";
            this.chartRightEye.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartRightEye.Location = new System.Drawing.Point(252, 3);
            this.chartRightEye.Name = "chartRightEye";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            series1.LegendName = "Default Legend";
            series1.Name = "Left Eye";
            series1.ShowInLegend = false;
            lineSeriesView1.Color = System.Drawing.Color.DodgerBlue;
            lineSeriesView1.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
            series1.View = lineSeriesView1;
            this.chartRightEye.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.chartRightEye.Size = new System.Drawing.Size(240, 550);
            this.chartRightEye.TabIndex = 5;
            // 
            // lblTagRight
            // 
            this.lblTagRight.AutoSize = true;
            this.lblTagRight.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblTagRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTagRight.Location = new System.Drawing.Point(252, 597);
            this.lblTagRight.Name = "lblTagRight";
            this.lblTagRight.Size = new System.Drawing.Size(240, 3);
            this.lblTagRight.TabIndex = 3;
            // 
            // lblCaptionRight
            // 
            this.lblCaptionRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.lblCaptionRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaptionRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblCaptionRight.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCaptionRight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.lblCaptionRight.Location = new System.Drawing.Point(252, 559);
            this.lblCaptionRight.Name = "lblCaptionRight";
            this.lblCaptionRight.Size = new System.Drawing.Size(240, 38);
            this.lblCaptionRight.TabIndex = 2;
            this.lblCaptionRight.Text = "Right";
            this.lblCaptionRight.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCaptionLeft
            // 
            this.lblCaptionLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.lblCaptionLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaptionLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblCaptionLeft.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblCaptionLeft.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.lblCaptionLeft.Location = new System.Drawing.Point(3, 559);
            this.lblCaptionLeft.Name = "lblCaptionLeft";
            this.lblCaptionLeft.Size = new System.Drawing.Size(240, 38);
            this.lblCaptionLeft.TabIndex = 1;
            this.lblCaptionLeft.Text = "Left";
            this.lblCaptionLeft.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chartLeftEye
            // 
            this.chartLeftEye.BackColor = System.Drawing.Color.Transparent;
            this.chartLeftEye.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartLeftEye.DataBindings = null;
            xyDiagram2.AxisX.Tickmarks.MinorVisible = false;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisY.GridLines.Visible = false;
            xyDiagram2.AxisY.Tickmarks.MinorVisible = false;
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram2.DefaultPane.BackColor = System.Drawing.Color.Transparent;
            xyDiagram2.DefaultPane.BorderVisible = false;
            xyDiagram2.Rotated = true;
            this.chartLeftEye.Diagram = xyDiagram2;
            this.chartLeftEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartLeftEye.Legend.Name = "Default Legend";
            this.chartLeftEye.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartLeftEye.Location = new System.Drawing.Point(3, 3);
            this.chartLeftEye.Name = "chartLeftEye";
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            series2.LegendName = "Default Legend";
            series2.Name = "Left Eye";
            series2.ShowInLegend = false;
            lineSeriesView2.Color = System.Drawing.Color.Orange;
            lineSeriesView2.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
            series2.View = lineSeriesView2;
            this.chartLeftEye.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            this.chartLeftEye.Size = new System.Drawing.Size(240, 550);
            this.chartLeftEye.TabIndex = 4;
            // 
            // ucTestData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnWrapper);
            this.Name = "ucTestData";
            this.Size = new System.Drawing.Size(495, 600);
            this.pnWrapper.ResumeLayout(false);
            this.pnWrapper.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartRightEye)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartLeftEye)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTagLeft;
        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private System.Windows.Forms.Label lblCaptionLeft;
        private System.Windows.Forms.Label lblCaptionRight;
        private System.Windows.Forms.Label lblTagRight;
        private DevExpress.XtraCharts.ChartControl chartLeftEye;
        private DevExpress.XtraCharts.ChartControl chartRightEye;
    }
}
