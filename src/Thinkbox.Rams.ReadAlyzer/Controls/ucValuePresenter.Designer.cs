﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ucValuePresenter
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.lblTag = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnValue = new System.Windows.Forms.TableLayoutPanel();
            this.lblMajorValue = new DevExpress.XtraEditors.LabelControl();
            this.lblMinorValue = new System.Windows.Forms.Label();
            this.pnWrapper.SuspendLayout();
            this.pnValue.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.ColumnCount = 3;
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Controls.Add(this.lblTag, 0, 0);
            this.pnWrapper.Controls.Add(this.lblTitle, 2, 0);
            this.pnWrapper.Controls.Add(this.pnValue, 2, 1);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.RowCount = 2;
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Size = new System.Drawing.Size(310, 110);
            this.pnWrapper.TabIndex = 0;
            // 
            // lblTag
            // 
            this.lblTag.AutoSize = true;
            this.lblTag.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTag.Location = new System.Drawing.Point(0, 0);
            this.lblTag.Margin = new System.Windows.Forms.Padding(0);
            this.lblTag.Name = "lblTag";
            this.pnWrapper.SetRowSpan(this.lblTag, 2);
            this.lblTag.Size = new System.Drawing.Size(3, 110);
            this.lblTag.TabIndex = 0;
            this.lblTag.Text = "label1";
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.lblTitle.Location = new System.Drawing.Point(12, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(295, 30);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "title";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnValue
            // 
            this.pnValue.ColumnCount = 1;
            this.pnValue.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValue.Controls.Add(this.lblMajorValue, 0, 0);
            this.pnValue.Controls.Add(this.lblMinorValue, 0, 1);
            this.pnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnValue.Font = new System.Drawing.Font("나눔스퀘어", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.pnValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.pnValue.Location = new System.Drawing.Point(12, 33);
            this.pnValue.Name = "pnValue";
            this.pnValue.RowCount = 2;
            this.pnValue.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValue.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnValue.Size = new System.Drawing.Size(295, 74);
            this.pnValue.TabIndex = 2;
            // 
            // lblMajorValue
            // 
            this.lblMajorValue.Appearance.Font = new System.Drawing.Font("나눔스퀘어", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMajorValue.Appearance.Options.UseFont = true;
            this.lblMajorValue.Appearance.Options.UseTextOptions = true;
            this.lblMajorValue.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.lblMajorValue.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblMajorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMajorValue.Location = new System.Drawing.Point(3, 3);
            this.lblMajorValue.Name = "lblMajorValue";
            this.lblMajorValue.Size = new System.Drawing.Size(289, 31);
            this.lblMajorValue.TabIndex = 0;
            this.lblMajorValue.Text = "major value";
            // 
            // lblMinorValue
            // 
            this.lblMinorValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMinorValue.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblMinorValue.ForeColor = System.Drawing.Color.DarkGray;
            this.lblMinorValue.Location = new System.Drawing.Point(3, 40);
            this.lblMinorValue.Margin = new System.Windows.Forms.Padding(3);
            this.lblMinorValue.Name = "lblMinorValue";
            this.lblMinorValue.Size = new System.Drawing.Size(289, 31);
            this.lblMinorValue.TabIndex = 1;
            this.lblMinorValue.Text = "minor vlaue";
            this.lblMinorValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ucValuePresenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.Controls.Add(this.pnWrapper);
            this.Name = "ucValuePresenter";
            this.Size = new System.Drawing.Size(310, 110);
            this.pnWrapper.ResumeLayout(false);
            this.pnWrapper.PerformLayout();
            this.pnValue.ResumeLayout(false);
            this.pnValue.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private System.Windows.Forms.Label lblTag;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TableLayoutPanel pnValue;
        private DevExpress.XtraEditors.LabelControl lblMajorValue;
        private System.Windows.Forms.Label lblMinorValue;
    }
}
