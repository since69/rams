﻿namespace Thinkbox.Rams.ReadAlyzer.Controls
{
    partial class ucMultiValuePresenter
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.lblTag = new System.Windows.Forms.Label();
            this.lblTitle = new DevExpress.XtraEditors.LabelControl();
            this.pnValue = new System.Windows.Forms.TableLayoutPanel();
            this.lblFirstCaption = new System.Windows.Forms.Label();
            this.lblThirdCaption = new System.Windows.Forms.Label();
            this.lblThirdValue = new DevExpress.XtraEditors.LabelControl();
            this.lblSecondValue = new DevExpress.XtraEditors.LabelControl();
            this.lblSecondCaption = new System.Windows.Forms.Label();
            this.lblFirstValue = new DevExpress.XtraEditors.LabelControl();
            this.pnWrapper.SuspendLayout();
            this.pnValue.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.ColumnCount = 3;
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.pnWrapper.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Controls.Add(this.lblTag, 0, 0);
            this.pnWrapper.Controls.Add(this.lblTitle, 2, 0);
            this.pnWrapper.Controls.Add(this.pnValue, 2, 1);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.RowCount = 2;
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.pnWrapper.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnWrapper.Size = new System.Drawing.Size(310, 110);
            this.pnWrapper.TabIndex = 0;
            // 
            // lblTag
            // 
            this.lblTag.AutoSize = true;
            this.lblTag.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTag.Location = new System.Drawing.Point(0, 0);
            this.lblTag.Margin = new System.Windows.Forms.Padding(0);
            this.lblTag.Name = "lblTag";
            this.pnWrapper.SetRowSpan(this.lblTag, 2);
            this.lblTag.Size = new System.Drawing.Size(3, 110);
            this.lblTag.TabIndex = 0;
            this.lblTag.Text = "label1";
            // 
            // lblTitle
            // 
            this.lblTitle.Appearance.Font = new System.Drawing.Font("나눔스퀘어", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblTitle.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(118)))), ((int)(((byte)(119)))));
            this.lblTitle.Appearance.Options.UseFont = true;
            this.lblTitle.Appearance.Options.UseForeColor = true;
            this.lblTitle.Appearance.Options.UseTextOptions = true;
            this.lblTitle.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.Location = new System.Drawing.Point(12, 3);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(295, 24);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "title";
            // 
            // pnValue
            // 
            this.pnValue.ColumnCount = 3;
            this.pnValue.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.pnValue.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.pnValue.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.pnValue.Controls.Add(this.lblFirstCaption, 0, 1);
            this.pnValue.Controls.Add(this.lblThirdCaption, 0, 1);
            this.pnValue.Controls.Add(this.lblThirdValue, 2, 0);
            this.pnValue.Controls.Add(this.lblSecondValue, 1, 0);
            this.pnValue.Controls.Add(this.lblSecondCaption, 0, 1);
            this.pnValue.Controls.Add(this.lblFirstValue, 0, 0);
            this.pnValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnValue.Location = new System.Drawing.Point(12, 33);
            this.pnValue.Name = "pnValue";
            this.pnValue.RowCount = 2;
            this.pnValue.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.pnValue.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.pnValue.Size = new System.Drawing.Size(295, 74);
            this.pnValue.TabIndex = 2;
            // 
            // lblFirstCaption
            // 
            this.lblFirstCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFirstCaption.Font = new System.Drawing.Font("나눔고딕", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFirstCaption.ForeColor = System.Drawing.Color.DarkGray;
            this.lblFirstCaption.Location = new System.Drawing.Point(3, 54);
            this.lblFirstCaption.Margin = new System.Windows.Forms.Padding(3);
            this.lblFirstCaption.Name = "lblFirstCaption";
            this.lblFirstCaption.Size = new System.Drawing.Size(92, 17);
            this.lblFirstCaption.TabIndex = 6;
            this.lblFirstCaption.Text = "caption";
            this.lblFirstCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThirdCaption
            // 
            this.lblThirdCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThirdCaption.Font = new System.Drawing.Font("나눔고딕", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblThirdCaption.ForeColor = System.Drawing.Color.DarkGray;
            this.lblThirdCaption.Location = new System.Drawing.Point(199, 54);
            this.lblThirdCaption.Margin = new System.Windows.Forms.Padding(3);
            this.lblThirdCaption.Name = "lblThirdCaption";
            this.lblThirdCaption.Size = new System.Drawing.Size(93, 17);
            this.lblThirdCaption.TabIndex = 5;
            this.lblThirdCaption.Text = "caption";
            this.lblThirdCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThirdValue
            // 
            this.lblThirdValue.Appearance.Font = new System.Drawing.Font("나눔스퀘어", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblThirdValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.lblThirdValue.Appearance.Options.UseFont = true;
            this.lblThirdValue.Appearance.Options.UseForeColor = true;
            this.lblThirdValue.Appearance.Options.UseTextOptions = true;
            this.lblThirdValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblThirdValue.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.lblThirdValue.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblThirdValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblThirdValue.Location = new System.Drawing.Point(199, 3);
            this.lblThirdValue.Name = "lblThirdValue";
            this.lblThirdValue.Size = new System.Drawing.Size(93, 45);
            this.lblThirdValue.TabIndex = 4;
            this.lblThirdValue.Text = "3";
            // 
            // lblSecondValue
            // 
            this.lblSecondValue.Appearance.Font = new System.Drawing.Font("나눔스퀘어", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSecondValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.lblSecondValue.Appearance.Options.UseFont = true;
            this.lblSecondValue.Appearance.Options.UseForeColor = true;
            this.lblSecondValue.Appearance.Options.UseTextOptions = true;
            this.lblSecondValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblSecondValue.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.lblSecondValue.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblSecondValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSecondValue.Location = new System.Drawing.Point(101, 3);
            this.lblSecondValue.Name = "lblSecondValue";
            this.lblSecondValue.Size = new System.Drawing.Size(92, 45);
            this.lblSecondValue.TabIndex = 3;
            this.lblSecondValue.Text = "2";
            // 
            // lblSecondCaption
            // 
            this.lblSecondCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSecondCaption.Font = new System.Drawing.Font("나눔고딕", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblSecondCaption.ForeColor = System.Drawing.Color.DarkGray;
            this.lblSecondCaption.Location = new System.Drawing.Point(101, 54);
            this.lblSecondCaption.Margin = new System.Windows.Forms.Padding(3);
            this.lblSecondCaption.Name = "lblSecondCaption";
            this.lblSecondCaption.Size = new System.Drawing.Size(92, 17);
            this.lblSecondCaption.TabIndex = 2;
            this.lblSecondCaption.Text = "caption";
            this.lblSecondCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFirstValue
            // 
            this.lblFirstValue.Appearance.Font = new System.Drawing.Font("나눔스퀘어", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblFirstValue.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.lblFirstValue.Appearance.Options.UseFont = true;
            this.lblFirstValue.Appearance.Options.UseForeColor = true;
            this.lblFirstValue.Appearance.Options.UseTextOptions = true;
            this.lblFirstValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblFirstValue.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.lblFirstValue.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lblFirstValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFirstValue.Location = new System.Drawing.Point(3, 3);
            this.lblFirstValue.Name = "lblFirstValue";
            this.lblFirstValue.Size = new System.Drawing.Size(92, 45);
            this.lblFirstValue.TabIndex = 1;
            this.lblFirstValue.Text = "1";
            // 
            // ucMultiValuePresenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(63)))), ((int)(((byte)(68)))));
            this.Controls.Add(this.pnWrapper);
            this.Name = "ucMultiValuePresenter";
            this.Size = new System.Drawing.Size(310, 110);
            this.pnWrapper.ResumeLayout(false);
            this.pnWrapper.PerformLayout();
            this.pnValue.ResumeLayout(false);
            this.pnValue.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private System.Windows.Forms.Label lblTag;
        private DevExpress.XtraEditors.LabelControl lblTitle;
        private System.Windows.Forms.TableLayoutPanel pnValue;
        private DevExpress.XtraEditors.LabelControl lblFirstValue;
        private System.Windows.Forms.Label lblSecondCaption;
        private DevExpress.XtraEditors.LabelControl lblSecondValue;
        private System.Windows.Forms.Label lblThirdCaption;
        private DevExpress.XtraEditors.LabelControl lblThirdValue;
        private System.Windows.Forms.Label lblFirstCaption;
    }
}
