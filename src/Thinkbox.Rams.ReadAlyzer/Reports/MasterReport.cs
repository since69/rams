﻿//
// Thinkbox.Rams.ReadAlyzer.Reports.MasterReport
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Thinkbox.Rams.ReadAlyzer.Reports
{
    public partial class MasterReport : DevExpress.XtraReports.UI.XtraReport
    {
        #region CONSTRUCTORS    
        public MasterReport()
        {
            InitializeComponent();
            InitializeReport();
        }
        #endregion

        #region METHODS
        private void InitializeReport()
        {
        }
        #endregion

    }
}
