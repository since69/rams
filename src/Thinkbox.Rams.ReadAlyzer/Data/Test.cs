﻿using System;
using System.Collections.Generic;

namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class Test
    {
        #region PROPERTIE
        public int Id { get; set; }
        public byte Grade { get; set; }
        public string RecordFile { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Data.Trainee Trainee { get; set; } = new Data.Trainee();
        public Data.Trainer Trainer { get; set; } = new Data.Trainer();
        public Data.TestText TestText { get; set; } = new Data.TestText();
        public DateTime ModifiedDate { get; set; } = DateTime.UtcNow;
        public List<Data.TestAnswer> Answers = new List<Data.TestAnswer>();
        #endregion

        #region CONSTRUCTORS
        public Test()
        {
            Id = int.MinValue;
        }

        public Test(Test test)
        {
            this.Assign(test);
        }

        public Test(Rams.Data.Model.Test test)
        {
            Id = test.Id;
            Grade = test.Grade;
            RecordFile = test.RecordFile;
            StartTime = test.StartTime;
            EndTime = test.EndTime;
            Trainee = new Data.Trainee(test.Trainee);
            Trainer = new Data.Trainer(test.Trainer);
            TestText = new Data.TestText(test.TestText);
            ModifiedDate = test.ModifiedDate;

            foreach (Rams.Data.Model.TestAnswer a in test.TestAnswers)
                Answers.Add(new Data.TestAnswer(a));
        }
        #endregion

        #region METHODS
        public void Assign(Test test)
        {
            Id = test.Id;
            Grade = test.Grade;
            RecordFile = test.RecordFile;
            StartTime = test.StartTime;
            EndTime = test.EndTime;
            Trainee = test.Trainee;
            Trainer = test.Trainer;
            TestText = test.TestText;
            ModifiedDate = test.ModifiedDate;
            Answers = test.Answers;
        }

        public Test Clone()
        {
            return new Data.Test(this);
        }
        #endregion
    }
}
