﻿namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class TestRecord
    {
        #region PROPERTIES
        public int Id { get; set; }
        public decimal TestId { get; set; }
        public decimal LeftX { get; set; }
        public decimal LeftY { get; set; }
        public decimal RightX { get; set; }
        public decimal RightY { get; set; }
        public int Elapsed { get; set; }
        #endregion

        #region CONSTRUCTORS
        public TestRecord() { }
        #endregion
    }
}
