﻿using System;
using System.Collections.Generic;

namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class TestAnswer
    {
        #region PROPERTIE
        public int Id { get; set; }
        //public Data.Test Test { get; set; }
        public int TestId { get; set; }
        public Data.TestQuestion Question { get; set; }
        public bool Answer { get; set; }
        public DateTime ModifiedDate { get; set; } = DateTime.UtcNow;
        
        #endregion

        #region CONSTRUCTORS
        public TestAnswer() { }

        public TestAnswer(TestAnswer answer)
        {
            this.Assign(answer);
        }

        public TestAnswer(Rams.Data.Model.TestAnswer answer)
        {
            Id = answer.Id;
            TestId = answer.Test.Id;
            Question = new Data.TestQuestion(answer.TestQuestion);
            Answer = answer.Answer;
            ModifiedDate = answer.ModifiedDate;
        }
        #endregion

        #region METHODS
        public void Assign(TestAnswer answer)
        {
            Id = answer.Id;
            TestId = answer.TestId;
            Question = answer.Question;
            Answer = answer.Answer;
            ModifiedDate = answer.ModifiedDate;
        }

        public TestAnswer Clone()
        {
            return new Data.TestAnswer(this);
        }
        #endregion
    }
}
