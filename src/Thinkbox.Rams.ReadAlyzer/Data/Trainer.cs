﻿using System;
using System.Collections.Generic;
using Thinkbox.Rams.Data;

namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class Trainer : IComparable
    {
        #region PROPERTIE
        public int Id { get; set; }
        public Data.Person Person { get; set; } = new Person() { PersonType = "TN" };
        public string Gender { get; set; } = "M";
        public System.DateTime ModifiedDate { get; set; } = DateTime.UtcNow;
        #endregion

        #region CONSTRUCTORS
        public Trainer()
        {
        }

        public Trainer(Trainer trainer)
        {
            this.Assign(trainer);
        }

        public Trainer(Thinkbox.Rams.Data.Model.Trainer trainer)
        {
            Id = trainer.Id;
            Person = new Data.Person(trainer.Person);
            Gender = trainer.Gender;
            ModifiedDate = trainer.ModifiedDate;
        }
        #endregion

        #region METHODS
        public void Assign(Trainer trainer)
        {
            this.Id = trainer.Id;
            this.Person = trainer.Person;
            this.Gender = trainer.Gender;
            this.ModifiedDate = trainer.ModifiedDate;
        }

        public Trainer Clone()
        {
            return new Data.Trainer(this);
        }

        public int CompareTo(object o)
        {
            return Comparer<string>.Default.Compare(Person.FullName, o.ToString());
        }
        #endregion
    }
}
