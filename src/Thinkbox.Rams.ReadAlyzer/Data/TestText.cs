﻿using System;
using System.Collections.Generic;

namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class TestText
    {
        #region PROPERTIES
        public int Id { get; set; }
        public Data.Culture Culture { get; set; } = new Culture() { Id = "ko" };
        public int Level { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string FontName { get; set; }
        public int FontSize { get; set; }
        public decimal CharacterWidth { get; set; }
        public decimal CharacterHeight { get; set; }
        public decimal LineSpacing { get; set; }
        public DateTime ModifiedDate { get; set; } = DateTime.UtcNow;
        #endregion

        #region CONSTRUCTORS
        public TestText() { }

        public TestText(TestText text)
        {
            this.Assign(text);
        }

        public TestText(Rams.Data.Model.TestText text)
        {
            Id = text.Id;
            Culture = new Data.Culture(text.Culture);
            Level = text.Level;
            Title = text.Title;
            Text = text.Text;
            FontName = text.FontName;
            FontSize = text.FontSize;
            CharacterWidth = text.CharacterWidth;
            CharacterHeight = text.CharacterHeight;
            LineSpacing = text.LineSpacing;
            ModifiedDate = text.ModifiedDate;
        }
        #endregion

        #region METHODS
        public void Assign(TestText text)
        {
            this.Id = text.Id;
            this.Culture = text.Culture;
            this.Level = text.Level;
            this.Title = text.Title;
            this.Text = text.Text;
            this.FontName = text.FontName;
            this.FontSize = text.FontSize;
            this.CharacterWidth = text.CharacterWidth;
            this.CharacterHeight = text.CharacterHeight;
            this.LineSpacing = text.LineSpacing;
            this.ModifiedDate = text.ModifiedDate;
        }

        public TestText Clone()
        {
            return new Data.TestText(this);
        }
        #endregion
    }
}
