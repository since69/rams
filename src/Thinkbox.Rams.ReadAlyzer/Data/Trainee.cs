﻿using System;
using System.Collections.Generic;

namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class Trainee : IComparable
    {
        #region PROPERTIE
        public int Id { get; set; }
        public Data.Person Person { get; set; } = new Data.Person() { PersonType = "TE" };
        public string Gender { get; set; } = "M";
        public DateTime BirthDate { get; set; } = DateTime.UtcNow;
        public DateTime ModifiedDate { get; set; } = DateTime.UtcNow;
        #endregion

        #region CONSTRUCTORS
        public Trainee() { }

        public Trainee(Trainee trainee)
        {
            this.Assign(trainee);
        }

        public Trainee(Thinkbox.Rams.Data.Model.Trainee trainee)
        {
            Id = trainee.Id;
            Person = new Data.Person(trainee.Person);
            Gender = trainee.Gender;
            BirthDate = trainee.BirthDate;
            ModifiedDate = trainee.ModifiedDate;
        }
        #endregion

        #region METHODS
        public void Assign(Trainee trainee)
        {
            this.Id = trainee.Id;
            this.Person = trainee.Person;
            this.Gender = trainee.Gender;
            this.BirthDate = trainee.BirthDate;
            this.ModifiedDate = trainee.ModifiedDate;
        }

        public Trainee Clone()
        {
            return new Data.Trainee(this);
        }

        public int CompareTo(object o)
        {
            return Comparer<string>.Default.Compare(Person.FullName, o.ToString());
        }
        #endregion
    }
}
