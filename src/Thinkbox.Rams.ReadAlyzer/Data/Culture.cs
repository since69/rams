﻿namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class Culture
    {
        #region PROPERTIES
        public string Id { get; set; }
        public string Name { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Culture() { }

        public Culture(Rams.Data.Model.Culture culture)
        {
            Id = culture.Id;
            Name = culture.Name;
            ModifiedDate = culture.ModifiedDate;
        }
        #endregion
    }
}
