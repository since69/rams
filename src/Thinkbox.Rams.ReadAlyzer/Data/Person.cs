﻿namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class Person
    {
        #region PROPERTIES
        public int? Id { get; set; }
        public string PersonType { get; set; }
        public bool NameStyle { get; set; } = false;
        public string Title { get; set; }
        public string FirstName { get; set; } = Properties.Resources.NewFirstName;
        public string MiddleName { get; set; }
        public string LastName { get; set; } = Properties.Resources.NewLastName;
        public string FullName
        {
            get
            {
                return NameStyle ? LastName + " " + FirstName
                    : FirstName + " " + LastName;
            }
        }
        public string Suffix { get; set; }

        public int EmailPromotion { get; set; } = 0;
        public System.DateTime ModifiedDate { get; set; } = System.DateTime.UtcNow;
        #endregion

        #region CONSTRUCTORS
        public Person() {}

        public Person(Rams.Data.Model.Person person)
        {
            Id = person.Id;
            PersonType = person.PersonType;
            NameStyle = person.NameStyle;
            Title = person.Title;
            FirstName = person.FirstName;
            MiddleName = person.MiddleName;
            LastName = person.LastName;
            Suffix = person.Suffix;
            EmailPromotion = person.EmailPromotion;
            ModifiedDate = person.ModifiedDate;
        }
        #endregion
    }
}
