﻿using System;

namespace Thinkbox.Rams.ReadAlyzer.Data
{
    public class TestQuestion
    {
        #region PROPERTIES
        public int Id { get; set; }
        public Data.TestText TestText { get; set; } = new Data.TestText();
        public string Question { get; set; }
        public bool Answer { get; set; }
        public DateTime ModifiedDate { get; set; }
        #endregion

        #region CONSTRUCTORS
        public TestQuestion() { }

        public TestQuestion(TestQuestion question)
        {
            this.Assign(question);
        }

        public TestQuestion(Rams.Data.Model.TestQuestion question)
        {
            Id = question.Id;
            TestText = new Data.TestText(question.TestText);
            Question = question.Question;
            Answer = question.Answer;
            ModifiedDate = question.ModifiedDate;
        }
        #endregion

        #region METHODS
        public void Assign(TestQuestion question)
        {
            this.Id = question.Id;
            this.TestText = question.TestText;
            this.Question = question.Question;
            this.Answer = question.Answer;
            this.ModifiedDate = question.ModifiedDate;
        }

        public TestQuestion Clone()
        {
            return new TestQuestion(this);
        }
        #endregion
    }
}
