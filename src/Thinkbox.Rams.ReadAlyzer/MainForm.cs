﻿using System;
using System.Collections.Generic;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Gallery;
using DevExpress.XtraNavBar;
using DevExpress.XtraPrinting;
using DevExpress.XtraRichEdit;
using Thinkbox.Rams.ReadAlyzer.Forms;
using Thinkbox.Rams.ReadAlyzer.Modules;

namespace Thinkbox.Rams.ReadAlyzer
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region VARIABLES
        internal ModulesNavigator modulesNavigator;
        internal List<BarItem> allowCustomizationMenuList = new List<BarItem>();
        #endregion

        #region PROPERTIES
        internal BackstageViewButtonItem SaveAsMenuItem
        {
            get { return bvbiSaveAs; }
        }

        internal ZoomManager zoomManager;
        internal ZoomManager ZoomManager
        {
            get { return zoomManager; }
        }

        public RibbonStatusBar RibbonStatusBar { get { return rsbMain; } }

        internal InRibbonGallery RecordGallery
        {
            get { return rgbiRecordsCurrentView.Gallery; }
        }

        public IPrintable CurrentPrintableComponent
        {
            get { return modulesNavigator.CurrentModule.PrintableComponent; }
        }

        public IPrintable CurrentExportComponent
        {
            get { return modulesNavigator.CurrentModule.ExportComponent; }
        }

        public RichEditControl CurrentRichEdit
        {
            get { return modulesNavigator.CurrentModule.CurrentRichEdit; }
        }

        public string CurrentModuleName
        {
            get { return modulesNavigator.CurrentModule.ModuleName; }
        }
        #endregion

        #region CONSTRUCTORS
        public MainForm()
        {
            InitializeComponent();

            InitializeForm();
            InitializeControls();
            SetPageLayoutStyle();
        }
        #endregion

        #region METHODS
        internal void ShowReminder()
        {
        }

        internal void ShowInfo(int? count)
        {
            if (count == null)
                bsiInfo.Caption = string.Empty;
            else
                bsiInfo.Caption = string.Format(
                    Properties.Resources.InfoText,
                    count.Value);

            //HtmlText = string.Format("{0}{1}", GetModuleName(), GetModulePartName());
            HtmlText = "Read Alyzer";
        }

        public void ShowInfo(bool visible)
        {
            bsiInfo.Visibility = bsiTemp.Visibility = visible ? 
                BarItemVisibility.Always : BarItemVisibility.Never;
        }

        internal void ShowPrintPreview()
        {
            if (CurrentPrintableComponent == null) return;
            PrintableComponentLink link = new PrintableComponentLink(new PrintingSystem());
            link.Component = CurrentPrintableComponent;
            link.CreateDocument();
            link.ShowRibbonPreviewDialog(this.LookAndFeel);
        }

        internal string GetModulePartName()
        {
            if (string.IsNullOrEmpty(modulesNavigator.CurrentModule.PartName))
                return null;
            return string.Format(" - {0}", modulesNavigator.CurrentModule.PartName);
        }

        internal string GetModuleName()
        {
            if (string.IsNullOrEmpty(modulesNavigator.CurrentModule.PartName))
                return CurrentModuleName;
            return string.Format("<b>{0}</b>", CurrentModuleName);
        }

        internal void SetPageLayoutStyle()
        {
            bbiNormal.Down = 
                nbcMain.OptionsNavPane.NavPaneState == NavPaneState.Expanded;
            bbiReading.Down = 
                nbcMain.OptionsNavPane.NavPaneState == NavPaneState.Collapsed;
        }

        internal void EnableZoomControl(bool enabled)
        {
            beiZoom.Visibility = 
                enabled ? BarItemVisibility.Always : BarItemVisibility.Never;
        }
        
        internal void EnableAnalysis(bool enabled)
        {
            bbiAnalysisTest.Enabled = enabled;
        }

        internal void EnableEditTest(TestStatus status)
        {
            switch (status)
            {
                case TestStatus.Ready:
                    bbiNewTest.Enabled = true;
                    bbiStartTest.Enabled = false;
                    bbiStopTest.Enabled = false;
                    bbiCancelTest.Enabled = false;
                    EnableAnalysis(false);
                    break;
                case TestStatus.Prepared:
                    bbiNewTest.Enabled = false;
                    bbiStartTest.Enabled = true;
                    bbiStopTest.Enabled = false;
                    bbiCancelTest.Enabled = false;
                    EnableAnalysis(false);
                    break;
                case TestStatus.Started:
                    bbiNewTest.Enabled = false;
                    bbiStartTest.Enabled = false;
                    bbiStopTest.Enabled = true;
                    bbiCancelTest.Enabled = true;
                    EnableAnalysis(false);
                    break;
                case TestStatus.Completed:
                    bbiNewTest.Enabled = true;
                    bbiStartTest.Enabled = false;
                    bbiStopTest.Enabled = false;
                    bbiCancelTest.Enabled = false;
                    EnableAnalysis(true);
                    break;
                default: break;
            }
        }

        internal void EnalbleEditTrainer(bool enabled)
        {
            bbiDeleteTrainer.Enabled = enabled;
            bbiEditTrainer.Enabled = enabled;
        }

        internal void EnalbleEditTrainee(bool enabled)
        {
            bbiDeleteTrainee.Enabled = enabled;
            bbiEditTrainee.Enabled = enabled;
        }

        internal void EnalbleEditRecord(bool enabled)
        {
            bbiDeleteRecord.Enabled = enabled;
            bbiEditRecord.Enabled = enabled;
        }

        private void InitializeGalleryItem(GalleryItem item, string tag)
        {
            item.Tag = tag;
        }

        private void InitializeBarButtonItem(BarButtonItem item, object tag)
        {
            item.ItemClick += new ItemClickEventHandler(BarButtonItem_ItemClick);
            item.Tag = tag;
        }

        private void InitializeControls()
        {
            //
            // Initialize ribbon control & items
            //

            InitializeBarButtonItem(bbiNewTest, Tags.TestNew);
            InitializeBarButtonItem(bbiStartTest, Tags.TestStart);
            InitializeBarButtonItem(bbiStopTest, Tags.TestStop);
            InitializeBarButtonItem(bbiCancelTest, Tags.TestCancel);
            InitializeBarButtonItem(bbiAnalysisTest, Tags.TestAnalysis);
            InitializeBarButtonItem(bbiEditRecord, Tags.RecordEdit);
            InitializeBarButtonItem(bbiDeleteRecord, Tags.RecordDelete);
            InitializeBarButtonItem(bbiAnalysisRecord, Tags.RecordAnalysis);
            InitializeBarButtonItem(bbiPrintRecord, Tags.RecordPrint);
            InitializeBarButtonItem(bbiNewTrainee, Tags.TraineeNew);
            InitializeBarButtonItem(bbiEditTrainee, Tags.TraineeEdit);
            InitializeBarButtonItem(bbiDeleteTrainee, Tags.TraineeDelete);
            InitializeBarButtonItem(bbiNewTrainer, Tags.TrainerNew);
            InitializeBarButtonItem(bbiEditTrainer, Tags.TrainerEdit);
            InitializeBarButtonItem(bbiDeleteTrainer, Tags.TrainerDelete);

            InitializeGalleryItem(rgbiRecordsCurrentView.Gallery.Groups[0].Items[0],
                Tags.RecordList);
            InitializeGalleryItem(rgbiRecordsCurrentView.Gallery.Groups[0].Items[1],
                Tags.RecordListParticipant);

            allowCustomizationMenuList.Add(bsiNavigation);

            //
            // Initialize BackstageViewControl.
            //
            
            bvbiSaveAs.Tag = Tags.MenuSaveAs;

            //
            // Create specific managers.
            //
            modulesNavigator = new ModulesNavigator(rcMain, pcMain);
            zoomManager = new ZoomManager(rcMain, modulesNavigator, beiZoom);

            //
            // Initialize NavBarControl.
            //

            nbiTest.Tag = new NavBarGroupTagObject("Test", typeof(Modules.Test));
            nbiRecord.Tag = new NavBarGroupTagObject("Records", typeof(Modules.Records));
            nbiTrainee.Tag = new NavBarGroupTagObject("Trainees", typeof(Modules.Trainees));
            nbiTrainer.Tag = new NavBarGroupTagObject("Trainers", typeof(Modules.Trainers));
            nbiText.Tag = new NavBarGroupTagObject("Texts", typeof(Modules.Texts));
            //nbgModules.SelectedLinkIndex = 0;
            nbgModules.SelectedLinkIndex = 3;

            //
            // Initialize Navigation. (On View Page)
            //

            foreach (NavBarItemLink link in nbgModules.ItemLinks)
            {
                BarButtonItem item = new BarButtonItem(rcMain.Manager, 
                    link.Item.Caption);
                item.Tag = link;
                item.Glyph = link.Item.SmallImage;
                item.ItemClick += new ItemClickEventHandler(bsiNavigation_ItemClick);
                bsiNavigation.ItemLinks.Add(item);
            }
        }

        private void InitializeForm()
        {
            this.Text = "Read Alyzer";
        }
        #endregion

        #region CONTROL EVENTS
        //
        // RibbonStatusBar control events.
        //

        private void bbiReminder_ItemClick(object sender, ItemClickEventArgs args)
        {
            //using ()
        }

        private void bbiReading_ItemClick(object sender, ItemClickEventArgs args)
        {
            if (bbiReading.Down)
                nbcMain.OptionsNavPane.NavPaneState = NavPaneState.Collapsed;
            else
                bbiReading.Down = true;
        }

        private void bbiNormal_ItemClick(object sender, ItemClickEventArgs args)
        {
            if (bbiNormal.Down)
                nbcMain.OptionsNavPane.NavPaneState = NavPaneState.Expanded;
            else
                bbiNormal.Down = true;
        }

        private void beiZoom_ItemClick(object sender, ItemClickEventArgs args)
        {
        }

        //
        // NavBarControl events.
        //

        private void nbcMain_SelectedLinkChanged(object sender, 
            DevExpress.XtraNavBar.ViewInfo.NavBarSelectedLinkChangedEventArgs args)
        {
            if (args.Link != null)
                modulesNavigator.ChangeSelectedItem(args.Link, null);
        }

        private void nbcMain_NavPaneStateChanged(object sender, EventArgs args)
        {
            SetPageLayoutStyle();
        }

        internal void navBarItem_ItemClick(object sender, ItemClickEventArgs args)
        {
            NavBarItemLink link = (NavBarItemLink)args.Item.Tag;
            Console.WriteLine("{0} Command selected.\n", link.Caption);
            nbgModules.SelectedLink = link;
        }

        //
        // BackstageViewControl events.
        //

        private void bvtiPrint_SelectedChanged(object sender, 
            BackstageViewItemEventArgs args)
        {
            if (bvcMain.SelectedTab == bvtiPrint)
                pagePrint.InitializePrintingSystem();
        }

        private void bvbiOption_ItemClick(object sender, 
            BackstageViewItemEventArgs e)
        {
        }

        private void bvbiExit_ItemClick(object sender,
            BackstageViewItemEventArgs ars)
        {
            Close();
        }

        private void bvcMain_ItemClick(object sender, 
            BackstageViewItemEventArgs args)
        {
            if (modulesNavigator.CurrentModule == null)
                return;
            modulesNavigator.CurrentModule.ButtonClick(string.Format("{0}", 
                args.Item.Tag));
        }

        //
        // Ribbon control item events.
        //

        private void bbiPrintPreview_ItemClick(object sender, 
            ItemClickEventArgs args)
        {
            ShowPrintPreview();
        }

        private void bsiNavigation_ItemClick(object sender, 
            ItemClickEventArgs args)
        {
            nbgModules.SelectedLink = (NavBarItemLink)args.Item.Tag;
        }

        private void BarButtonItem_ItemClick(object sender, 
            ItemClickEventArgs args)
        {
            string s = string.Format("{0}", args.Item.Tag);
            modulesNavigator.CurrentModule.ButtonClick(s);
            s = "BarButtonItem_ItemClick::MainForm " + s + " Clicked.\n";
            Console.WriteLine(s);
        }

        //
        // Ribbon control events.
        //

        private void rcMain_ShowCustomizationMenu(object sender,
            RibbonCustomizationMenuEventArgs args)
        {
            args.CustomizationMenu.InitializeMenu();
            if (args.Link == null || !allowCustomizationMenuList.Contains(args.Link.Item))
                args.CustomizationMenu.RemoveLink(args.CustomizationMenu.ItemLinks[0]);
        }

        private void rcMain_BeforeApplicationButtonContentControlShow(object sender,
            EventArgs args)
        {
            if (bvcMain.SelectedTab == bvtiPrint)
                bvcMain.SelectedTab = bvtiInfo;
            bvtiPrint.Enabled = (CurrentRichEdit != null || 
                CurrentPrintableComponent != null);
            bvtiExport.Enabled = CurrentExportComponent != null;
        }

        //
        // Misc events.
        //

        internal void OnModuleShown(BaseModule baseModule)
        {
            rpgPrint.Visible = CurrentPrintableComponent != null;
        }
        #endregion

        #region FROM EVENTS
        private void MainForm_KeyDown(object sender, 
            System.Windows.Forms.KeyEventArgs args)
        {
            modulesNavigator.CurrentModule.SendKeyDown(args);
        }
        #endregion
    }
}
