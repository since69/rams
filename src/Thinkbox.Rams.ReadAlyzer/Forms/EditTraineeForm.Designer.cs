﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class EditTraineeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTraineeForm));
            this.lcEditTrainee = new DevExpress.XtraLayout.LayoutControl();
            this.cbNameStyle = new System.Windows.Forms.ComboBox();
            this.deBirthDate = new DevExpress.XtraEditors.DateEdit();
            this.cbGender = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.teEmail = new DevExpress.XtraEditors.TextEdit();
            this.teLastName = new DevExpress.XtraEditors.TextEdit();
            this.teFirstName = new DevExpress.XtraEditors.TextEdit();
            this.peTrainee = new DevExpress.XtraEditors.PictureEdit();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lcgEditTrainee = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciTraineePhoto = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGender = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciBirthDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciNameStyle = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditTrainee)).BeginInit();
            this.lcEditTrainee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deBirthDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBirthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peTrainee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEditTrainee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTraineePhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBirthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNameStyle)).BeginInit();
            this.SuspendLayout();
            // 
            // lcEditTrainee
            // 
            this.lcEditTrainee.Controls.Add(this.cbNameStyle);
            this.lcEditTrainee.Controls.Add(this.deBirthDate);
            this.lcEditTrainee.Controls.Add(this.cbGender);
            this.lcEditTrainee.Controls.Add(this.teEmail);
            this.lcEditTrainee.Controls.Add(this.teLastName);
            this.lcEditTrainee.Controls.Add(this.teFirstName);
            this.lcEditTrainee.Controls.Add(this.peTrainee);
            this.lcEditTrainee.Controls.Add(this.btnCancel);
            this.lcEditTrainee.Controls.Add(this.btnOk);
            this.lcEditTrainee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcEditTrainee.Location = new System.Drawing.Point(0, 0);
            this.lcEditTrainee.Name = "lcEditTrainee";
            this.lcEditTrainee.Root = this.lcgEditTrainee;
            this.lcEditTrainee.Size = new System.Drawing.Size(564, 291);
            this.lcEditTrainee.TabIndex = 0;
            this.lcEditTrainee.Text = "lcEditTrainee";
            // 
            // cbNameStyle
            // 
            this.cbNameStyle.FormattingEnabled = true;
            this.cbNameStyle.Location = new System.Drawing.Point(251, 12);
            this.cbNameStyle.Name = "cbNameStyle";
            this.cbNameStyle.Size = new System.Drawing.Size(301, 22);
            this.cbNameStyle.TabIndex = 13;
            // 
            // deBirthDate
            // 
            this.deBirthDate.EditValue = null;
            this.deBirthDate.Location = new System.Drawing.Point(263, 164);
            this.deBirthDate.Name = "deBirthDate";
            this.deBirthDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deBirthDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deBirthDate.Size = new System.Drawing.Size(277, 20);
            this.deBirthDate.StyleController = this.lcEditTrainee;
            this.deBirthDate.TabIndex = 12;
            // 
            // cbGender
            // 
            this.cbGender.Location = new System.Drawing.Point(263, 140);
            this.cbGender.Name = "cbGender";
            this.cbGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGender.Size = new System.Drawing.Size(277, 20);
            this.cbGender.StyleController = this.lcEditTrainee;
            this.cbGender.TabIndex = 11;
            // 
            // teEmail
            // 
            this.teEmail.Location = new System.Drawing.Point(263, 116);
            this.teEmail.Name = "teEmail";
            this.teEmail.Size = new System.Drawing.Size(277, 20);
            this.teEmail.StyleController = this.lcEditTrainee;
            this.teEmail.TabIndex = 10;
            // 
            // teLastName
            // 
            this.teLastName.Location = new System.Drawing.Point(251, 61);
            this.teLastName.Name = "teLastName";
            this.teLastName.Size = new System.Drawing.Size(301, 20);
            this.teLastName.StyleController = this.lcEditTrainee;
            this.teLastName.TabIndex = 8;
            // 
            // teFirstName
            // 
            this.teFirstName.Location = new System.Drawing.Point(251, 37);
            this.teFirstName.Name = "teFirstName";
            this.teFirstName.Size = new System.Drawing.Size(301, 20);
            this.teFirstName.StyleController = this.lcEditTrainee;
            this.teFirstName.TabIndex = 7;
            // 
            // peTrainee
            // 
            this.peTrainee.Cursor = System.Windows.Forms.Cursors.Default;
            this.peTrainee.Location = new System.Drawing.Point(12, 12);
            this.peTrainee.Name = "peTrainee";
            this.peTrainee.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.peTrainee.Properties.ZoomAccelerationFactor = 1D;
            this.peTrainee.Size = new System.Drawing.Size(166, 227);
            this.peTrainee.StyleController = this.lcEditTrainee;
            this.peTrainee.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(459, 253);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 26);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(362, 253);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(93, 26);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lcgEditTrainee
            // 
            this.lcgEditTrainee.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgEditTrainee.GroupBordersVisible = false;
            this.lcgEditTrainee.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciOk,
            this.lciCancel,
            this.emptySpaceItem2,
            this.lciTraineePhoto,
            this.lciFirstname,
            this.lciLastName,
            this.lcgInfo,
            this.emptySpaceItem1,
            this.lciNameStyle});
            this.lcgEditTrainee.Location = new System.Drawing.Point(0, 0);
            this.lcgEditTrainee.Name = "lcgEditTrainee";
            this.lcgEditTrainee.Size = new System.Drawing.Size(564, 291);
            this.lcgEditTrainee.TextVisible = false;
            // 
            // lciOk
            // 
            this.lciOk.Control = this.btnOk;
            this.lciOk.Location = new System.Drawing.Point(350, 241);
            this.lciOk.MaxSize = new System.Drawing.Size(97, 30);
            this.lciOk.MinSize = new System.Drawing.Size(97, 30);
            this.lciOk.Name = "lciOk";
            this.lciOk.Size = new System.Drawing.Size(97, 30);
            this.lciOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOk.TextSize = new System.Drawing.Size(0, 0);
            this.lciOk.TextVisible = false;
            // 
            // lciCancel
            // 
            this.lciCancel.Control = this.btnCancel;
            this.lciCancel.Location = new System.Drawing.Point(447, 241);
            this.lciCancel.MaxSize = new System.Drawing.Size(97, 30);
            this.lciCancel.MinSize = new System.Drawing.Size(97, 30);
            this.lciCancel.Name = "lciCancel";
            this.lciCancel.Size = new System.Drawing.Size(97, 30);
            this.lciCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCancel.TextSize = new System.Drawing.Size(0, 0);
            this.lciCancel.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 241);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(350, 30);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciTraineePhoto
            // 
            this.lciTraineePhoto.Control = this.peTrainee;
            this.lciTraineePhoto.Location = new System.Drawing.Point(0, 0);
            this.lciTraineePhoto.Name = "lciTraineePhoto";
            this.lciTraineePhoto.Size = new System.Drawing.Size(170, 231);
            this.lciTraineePhoto.TextSize = new System.Drawing.Size(0, 0);
            this.lciTraineePhoto.TextVisible = false;
            // 
            // lciFirstname
            // 
            this.lciFirstname.Control = this.teFirstName;
            this.lciFirstname.Location = new System.Drawing.Point(170, 25);
            this.lciFirstname.Name = "lciFirstname";
            this.lciFirstname.Size = new System.Drawing.Size(374, 24);
            this.lciFirstname.Text = "First Name: ";
            this.lciFirstname.TextSize = new System.Drawing.Size(66, 14);
            // 
            // lciLastName
            // 
            this.lciLastName.Control = this.teLastName;
            this.lciLastName.Location = new System.Drawing.Point(170, 49);
            this.lciLastName.Name = "lciLastName";
            this.lciLastName.Size = new System.Drawing.Size(374, 24);
            this.lciLastName.Text = "Last Name: ";
            this.lciLastName.TextSize = new System.Drawing.Size(66, 14);
            // 
            // lcgInfo
            // 
            this.lcgInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciEmail,
            this.lciGender,
            this.lciBirthDate,
            this.emptySpaceItem3});
            this.lcgInfo.Location = new System.Drawing.Point(170, 73);
            this.lcgInfo.Name = "lcgInfo";
            this.lcgInfo.Size = new System.Drawing.Size(374, 158);
            this.lcgInfo.Text = "Information";
            // 
            // lciEmail
            // 
            this.lciEmail.Control = this.teEmail;
            this.lciEmail.Location = new System.Drawing.Point(0, 0);
            this.lciEmail.Name = "lciEmail";
            this.lciEmail.Size = new System.Drawing.Size(350, 24);
            this.lciEmail.Text = "Email: ";
            this.lciEmail.TextSize = new System.Drawing.Size(66, 14);
            // 
            // lciGender
            // 
            this.lciGender.Control = this.cbGender;
            this.lciGender.Location = new System.Drawing.Point(0, 24);
            this.lciGender.Name = "lciGender";
            this.lciGender.Size = new System.Drawing.Size(350, 24);
            this.lciGender.Text = "Gender: ";
            this.lciGender.TextSize = new System.Drawing.Size(66, 14);
            // 
            // lciBirthDate
            // 
            this.lciBirthDate.Control = this.deBirthDate;
            this.lciBirthDate.Location = new System.Drawing.Point(0, 48);
            this.lciBirthDate.Name = "lciBirthDate";
            this.lciBirthDate.Size = new System.Drawing.Size(350, 24);
            this.lciBirthDate.Text = "Birth Date:";
            this.lciBirthDate.TextSize = new System.Drawing.Size(66, 14);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(350, 43);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 231);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(544, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciNameStyle
            // 
            this.lciNameStyle.Control = this.cbNameStyle;
            this.lciNameStyle.Location = new System.Drawing.Point(170, 0);
            this.lciNameStyle.Name = "lciNameStyle";
            this.lciNameStyle.Size = new System.Drawing.Size(374, 25);
            this.lciNameStyle.Text = "Name Style:";
            this.lciNameStyle.TextSize = new System.Drawing.Size(66, 14);
            // 
            // EditTraineeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 291);
            this.Controls.Add(this.lcEditTrainee);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditTraineeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Trainee";
            ((System.ComponentModel.ISupportInitialize)(this.lcEditTrainee)).EndInit();
            this.lcEditTrainee.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.deBirthDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deBirthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peTrainee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEditTrainee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTraineePhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBirthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNameStyle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcEditTrainee;
        private DevExpress.XtraLayout.LayoutControlGroup lcgEditTrainee;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private DevExpress.XtraLayout.LayoutControlItem lciOk;
        private DevExpress.XtraLayout.LayoutControlItem lciCancel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PictureEdit peTrainee;
        private DevExpress.XtraLayout.LayoutControlItem lciTraineePhoto;
        private DevExpress.XtraEditors.TextEdit teLastName;
        private DevExpress.XtraEditors.TextEdit teFirstName;
        private DevExpress.XtraLayout.LayoutControlItem lciFirstname;
        private DevExpress.XtraLayout.LayoutControlItem lciLastName;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbGender;
        private DevExpress.XtraEditors.TextEdit teEmail;
        private DevExpress.XtraLayout.LayoutControlGroup lcgInfo;
        private DevExpress.XtraLayout.LayoutControlItem lciEmail;
        private DevExpress.XtraLayout.LayoutControlItem lciGender;
        private DevExpress.XtraEditors.DateEdit deBirthDate;
        private DevExpress.XtraLayout.LayoutControlItem lciBirthDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.ComboBox cbNameStyle;
        private DevExpress.XtraLayout.LayoutControlItem lciNameStyle;
    }
}