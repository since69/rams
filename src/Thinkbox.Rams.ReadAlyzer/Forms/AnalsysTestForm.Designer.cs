﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class AnalsysTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnalsysTestForm));
            DevExpress.XtraCharts.RadarDiagram radarDiagram4 = new DevExpress.XtraCharts.RadarDiagram();
            DevExpress.XtraCharts.Series series10 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RadarLineSeriesView radarLineSeriesView10 = new DevExpress.XtraCharts.RadarLineSeriesView();
            DevExpress.XtraCharts.Series series11 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RadarLineSeriesView radarLineSeriesView11 = new DevExpress.XtraCharts.RadarLineSeriesView();
            DevExpress.XtraCharts.Series series12 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RadarLineSeriesView radarLineSeriesView12 = new DevExpress.XtraCharts.RadarLineSeriesView();
            this.lcMain = new DevExpress.XtraLayout.LayoutControl();
            this.teNumOfReadLines = new DevExpress.XtraEditors.TextEdit();
            this.teNumOfLines = new DevExpress.XtraEditors.TextEdit();
            this.dtEndTime = new System.Windows.Forms.DateTimePicker();
            this.dtStartTime = new System.Windows.Forms.DateTimePicker();
            this.teCorrectAnswer = new DevExpress.XtraEditors.TextEdit();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.teTitle = new DevExpress.XtraEditors.TextEdit();
            this.cbeGender = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.deTraineeBirthDate = new DevExpress.XtraEditors.DateEdit();
            this.teTraineeName = new DevExpress.XtraEditors.TextEdit();
            this.teTrainerName = new DevExpress.XtraEditors.TextEdit();
            this.chartCompare = new DevExpress.XtraCharts.ChartControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciClose = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciCompare = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgTrainee = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTraineeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTraineeBirthDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGender = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.lcgTrainer = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTrainerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgText = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCorrectAnswer = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNumOfLines = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciNumOfReadLines = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgTest = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciEndTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).BeginInit();
            this.lcMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teNumOfReadLines.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNumOfLines.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCorrectAnswer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTraineeBirthDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTraineeBirthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTraineeName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTrainerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCompare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(radarDiagram4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(radarLineSeriesView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(radarLineSeriesView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(radarLineSeriesView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCompare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTrainee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTraineeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTraineeBirthDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTrainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCorrectAnswer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNumOfLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNumOfReadLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStartTime)).BeginInit();
            this.SuspendLayout();
            // 
            // lcMain
            // 
            this.lcMain.Controls.Add(this.teNumOfReadLines);
            this.lcMain.Controls.Add(this.teNumOfLines);
            this.lcMain.Controls.Add(this.dtEndTime);
            this.lcMain.Controls.Add(this.dtStartTime);
            this.lcMain.Controls.Add(this.teCorrectAnswer);
            this.lcMain.Controls.Add(this.cbLevel);
            this.lcMain.Controls.Add(this.teTitle);
            this.lcMain.Controls.Add(this.cbeGender);
            this.lcMain.Controls.Add(this.deTraineeBirthDate);
            this.lcMain.Controls.Add(this.teTraineeName);
            this.lcMain.Controls.Add(this.teTrainerName);
            this.lcMain.Controls.Add(this.chartCompare);
            this.lcMain.Controls.Add(this.btnClose);
            resources.ApplyResources(this.lcMain, "lcMain");
            this.lcMain.Name = "lcMain";
            this.lcMain.Root = this.lcgRoot;
            // 
            // teNumOfReadLines
            // 
            resources.ApplyResources(this.teNumOfReadLines, "teNumOfReadLines");
            this.teNumOfReadLines.Name = "teNumOfReadLines";
            this.teNumOfReadLines.StyleController = this.lcMain;
            // 
            // teNumOfLines
            // 
            resources.ApplyResources(this.teNumOfLines, "teNumOfLines");
            this.teNumOfLines.Name = "teNumOfLines";
            this.teNumOfLines.StyleController = this.lcMain;
            // 
            // dtEndTime
            // 
            resources.ApplyResources(this.dtEndTime, "dtEndTime");
            this.dtEndTime.Name = "dtEndTime";
            // 
            // dtStartTime
            // 
            resources.ApplyResources(this.dtStartTime, "dtStartTime");
            this.dtStartTime.Name = "dtStartTime";
            // 
            // teCorrectAnswer
            // 
            resources.ApplyResources(this.teCorrectAnswer, "teCorrectAnswer");
            this.teCorrectAnswer.Name = "teCorrectAnswer";
            this.teCorrectAnswer.StyleController = this.lcMain;
            // 
            // cbLevel
            // 
            this.cbLevel.FormattingEnabled = true;
            resources.ApplyResources(this.cbLevel, "cbLevel");
            this.cbLevel.Name = "cbLevel";
            // 
            // teTitle
            // 
            resources.ApplyResources(this.teTitle, "teTitle");
            this.teTitle.Name = "teTitle";
            this.teTitle.StyleController = this.lcMain;
            // 
            // cbeGender
            // 
            resources.ApplyResources(this.cbeGender, "cbeGender");
            this.cbeGender.Name = "cbeGender";
            this.cbeGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cbeGender.Properties.Buttons"))))});
            this.cbeGender.StyleController = this.lcMain;
            // 
            // deTraineeBirthDate
            // 
            resources.ApplyResources(this.deTraineeBirthDate, "deTraineeBirthDate");
            this.deTraineeBirthDate.Name = "deTraineeBirthDate";
            this.deTraineeBirthDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("deTraineeBirthDate.Properties.Buttons"))))});
            this.deTraineeBirthDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("deTraineeBirthDate.Properties.CalendarTimeProperties.Buttons"))))});
            this.deTraineeBirthDate.StyleController = this.lcMain;
            // 
            // teTraineeName
            // 
            resources.ApplyResources(this.teTraineeName, "teTraineeName");
            this.teTraineeName.Name = "teTraineeName";
            this.teTraineeName.StyleController = this.lcMain;
            // 
            // teTrainerName
            // 
            resources.ApplyResources(this.teTrainerName, "teTrainerName");
            this.teTrainerName.Name = "teTrainerName";
            this.teTrainerName.StyleController = this.lcMain;
            // 
            // chartCompare
            // 
            this.chartCompare.DataBindings = null;
            radarDiagram4.AxisX.MinorCount = 1;
            radarDiagram4.AxisX.WholeRange.Auto = false;
            radarDiagram4.AxisX.WholeRange.MaxValueSerializable = "5";
            radarDiagram4.AxisX.WholeRange.MinValueSerializable = "0";
            this.chartCompare.Diagram = radarDiagram4;
            this.chartCompare.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartCompare.Legend.Name = "Default Legend";
            resources.ApplyResources(this.chartCompare, "chartCompare");
            this.chartCompare.Name = "chartCompare";
            resources.ApplyResources(series10, "series10");
            series10.View = radarLineSeriesView10;
            resources.ApplyResources(series11, "series11");
            series11.View = radarLineSeriesView11;
            resources.ApplyResources(series12, "series12");
            series12.View = radarLineSeriesView12;
            this.chartCompare.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series10,
        series11,
        series12};
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.StyleController = this.lcMain;
            // 
            // lcgRoot
            // 
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.lciClose,
            this.emptySpaceItem3,
            this.lciCompare,
            this.lcgTrainee,
            this.splitterItem1,
            this.lcgTrainer,
            this.lcgText,
            this.lcgTest});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Size = new System.Drawing.Size(884, 561);
            this.lcgRoot.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(443, 437);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(421, 68);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 515);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(767, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciClose
            // 
            this.lciClose.Control = this.btnClose;
            this.lciClose.Location = new System.Drawing.Point(767, 515);
            this.lciClose.MaxSize = new System.Drawing.Size(97, 26);
            this.lciClose.MinSize = new System.Drawing.Size(97, 26);
            this.lciClose.Name = "lciClose";
            this.lciClose.Size = new System.Drawing.Size(97, 26);
            this.lciClose.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciClose.TextSize = new System.Drawing.Size(0, 0);
            this.lciClose.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 505);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(864, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciCompare
            // 
            this.lciCompare.Control = this.chartCompare;
            resources.ApplyResources(this.lciCompare, "lciCompare");
            this.lciCompare.Location = new System.Drawing.Point(0, 0);
            this.lciCompare.Name = "lciCompare";
            this.lciCompare.Size = new System.Drawing.Size(438, 505);
            this.lciCompare.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciCompare.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lcgTrainee
            // 
            this.lcgTrainee.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTraineeName,
            this.lciTraineeBirthDate,
            this.lciGender});
            this.lcgTrainee.Location = new System.Drawing.Point(443, 67);
            this.lcgTrainee.Name = "lcgTrainee";
            this.lcgTrainee.Size = new System.Drawing.Size(421, 115);
            resources.ApplyResources(this.lcgTrainee, "lcgTrainee");
            // 
            // lciTraineeName
            // 
            this.lciTraineeName.Control = this.teTraineeName;
            this.lciTraineeName.Location = new System.Drawing.Point(0, 0);
            this.lciTraineeName.Name = "lciTraineeName";
            this.lciTraineeName.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciTraineeName, "lciTraineeName");
            this.lciTraineeName.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciTraineeBirthDate
            // 
            this.lciTraineeBirthDate.Control = this.deTraineeBirthDate;
            this.lciTraineeBirthDate.Location = new System.Drawing.Point(0, 24);
            this.lciTraineeBirthDate.Name = "lciTraineeBirthDate";
            this.lciTraineeBirthDate.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciTraineeBirthDate, "lciTraineeBirthDate");
            this.lciTraineeBirthDate.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciGender
            // 
            this.lciGender.Control = this.cbeGender;
            this.lciGender.Location = new System.Drawing.Point(0, 48);
            this.lciGender.Name = "lciGender";
            this.lciGender.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciGender, "lciGender");
            this.lciGender.TextSize = new System.Drawing.Size(167, 14);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(438, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 505);
            // 
            // lcgTrainer
            // 
            this.lcgTrainer.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTrainerName});
            this.lcgTrainer.Location = new System.Drawing.Point(443, 0);
            this.lcgTrainer.Name = "lcgTrainer";
            this.lcgTrainer.Size = new System.Drawing.Size(421, 67);
            resources.ApplyResources(this.lcgTrainer, "lcgTrainer");
            // 
            // lciTrainerName
            // 
            this.lciTrainerName.Control = this.teTrainerName;
            this.lciTrainerName.Location = new System.Drawing.Point(0, 0);
            this.lciTrainerName.Name = "lciTrainerName";
            this.lciTrainerName.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciTrainerName, "lciTrainerName");
            this.lciTrainerName.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lcgText
            // 
            resources.ApplyResources(this.lcgText, "lcgText");
            this.lcgText.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTitle,
            this.lciLevel,
            this.lciCorrectAnswer,
            this.lciNumOfLines,
            this.lciNumOfReadLines});
            this.lcgText.Location = new System.Drawing.Point(443, 182);
            this.lcgText.Name = "lcgText";
            this.lcgText.Size = new System.Drawing.Size(421, 164);
            // 
            // lciTitle
            // 
            this.lciTitle.Control = this.teTitle;
            this.lciTitle.Location = new System.Drawing.Point(0, 0);
            this.lciTitle.Name = "lciTitle";
            this.lciTitle.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciTitle, "lciTitle");
            this.lciTitle.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciLevel
            // 
            this.lciLevel.Control = this.cbLevel;
            this.lciLevel.Location = new System.Drawing.Point(0, 24);
            this.lciLevel.Name = "lciLevel";
            this.lciLevel.Size = new System.Drawing.Size(397, 25);
            resources.ApplyResources(this.lciLevel, "lciLevel");
            this.lciLevel.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciCorrectAnswer
            // 
            this.lciCorrectAnswer.Control = this.teCorrectAnswer;
            this.lciCorrectAnswer.Location = new System.Drawing.Point(0, 97);
            this.lciCorrectAnswer.Name = "lciCorrectAnswer";
            this.lciCorrectAnswer.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciCorrectAnswer, "lciCorrectAnswer");
            this.lciCorrectAnswer.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciNumOfLines
            // 
            this.lciNumOfLines.Control = this.teNumOfLines;
            this.lciNumOfLines.Location = new System.Drawing.Point(0, 49);
            this.lciNumOfLines.Name = "lciNumOfLines";
            this.lciNumOfLines.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciNumOfLines, "lciNumOfLines");
            this.lciNumOfLines.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciNumOfReadLines
            // 
            this.lciNumOfReadLines.Control = this.teNumOfReadLines;
            this.lciNumOfReadLines.Location = new System.Drawing.Point(0, 73);
            this.lciNumOfReadLines.Name = "lciNumOfReadLines";
            this.lciNumOfReadLines.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciNumOfReadLines, "lciNumOfReadLines");
            this.lciNumOfReadLines.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lcgTest
            // 
            this.lcgTest.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciEndTime,
            this.lciStartTime});
            this.lcgTest.Location = new System.Drawing.Point(443, 346);
            this.lcgTest.Name = "lcgTest";
            this.lcgTest.Size = new System.Drawing.Size(421, 91);
            resources.ApplyResources(this.lcgTest, "lcgTest");
            // 
            // lciEndTime
            // 
            this.lciEndTime.Control = this.dtEndTime;
            this.lciEndTime.Location = new System.Drawing.Point(0, 24);
            this.lciEndTime.Name = "lciEndTime";
            this.lciEndTime.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciEndTime, "lciEndTime");
            this.lciEndTime.TextSize = new System.Drawing.Size(167, 14);
            // 
            // lciStartTime
            // 
            this.lciStartTime.Control = this.dtStartTime;
            this.lciStartTime.Location = new System.Drawing.Point(0, 0);
            this.lciStartTime.Name = "lciStartTime";
            this.lciStartTime.Size = new System.Drawing.Size(397, 24);
            resources.ApplyResources(this.lciStartTime, "lciStartTime");
            this.lciStartTime.TextSize = new System.Drawing.Size(167, 14);
            // 
            // AnalsysTestForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcMain);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.Name = "AnalsysTestForm";
            ((System.ComponentModel.ISupportInitialize)(this.lcMain)).EndInit();
            this.lcMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teNumOfReadLines.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teNumOfLines.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teCorrectAnswer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTraineeBirthDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deTraineeBirthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTraineeName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTrainerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(radarDiagram4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(radarLineSeriesView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(radarLineSeriesView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(radarLineSeriesView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCompare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCompare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTrainee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTraineeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTraineeBirthDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTrainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCorrectAnswer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNumOfLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciNumOfReadLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStartTime)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcMain;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lciClose;
        private DevExpress.XtraCharts.ChartControl chartCompare;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem lciCompare;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbeGender;
        private DevExpress.XtraEditors.DateEdit deTraineeBirthDate;
        private DevExpress.XtraEditors.TextEdit teTraineeName;
        private DevExpress.XtraEditors.TextEdit teTrainerName;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainerName;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTrainee;
        private DevExpress.XtraLayout.LayoutControlItem lciTraineeName;
        private DevExpress.XtraLayout.LayoutControlItem lciTraineeBirthDate;
        private DevExpress.XtraLayout.LayoutControlItem lciGender;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTrainer;
        private DevExpress.XtraEditors.TextEdit teTitle;
        private DevExpress.XtraLayout.LayoutControlItem lciTitle;
        private DevExpress.XtraEditors.TextEdit teCorrectAnswer;
        private System.Windows.Forms.ComboBox cbLevel;
        private DevExpress.XtraLayout.LayoutControlGroup lcgText;
        private DevExpress.XtraLayout.LayoutControlItem lciLevel;
        private DevExpress.XtraLayout.LayoutControlItem lciCorrectAnswer;
        private DevExpress.XtraEditors.TextEdit teNumOfReadLines;
        private DevExpress.XtraEditors.TextEdit teNumOfLines;
        private System.Windows.Forms.DateTimePicker dtEndTime;
        private System.Windows.Forms.DateTimePicker dtStartTime;
        private DevExpress.XtraLayout.LayoutControlItem lciNumOfLines;
        private DevExpress.XtraLayout.LayoutControlItem lciNumOfReadLines;
        private DevExpress.XtraLayout.LayoutControlGroup lcgTest;
        private DevExpress.XtraLayout.LayoutControlItem lciEndTime;
        private DevExpress.XtraLayout.LayoutControlItem lciStartTime;
    }
}