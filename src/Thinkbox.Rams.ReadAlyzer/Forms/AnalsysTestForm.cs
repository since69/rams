﻿using System;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using DevExpress.XtraEditors;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class AnalsysTestForm : DevExpress.XtraEditors.XtraForm
    {
        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public AnalsysTestForm()
        {
            InitializeComponent();
            InitializeControls();
        }
        #endregion

        #region METHODS
        private void InitializeControls()
        {
            SeriesPoint[] goals = new SeriesPoint[]
            {
                new SeriesPoint(0, 120),
                new SeriesPoint(1, 25),
                new SeriesPoint(2, 0.27),
                new SeriesPoint(3, 185),
                new SeriesPoint(4, 4),
                new SeriesPoint(5, 21)
            };
            SeriesPoint[] avereges = new SeriesPoint[]
            {
                new SeriesPoint(0, 139),
                new SeriesPoint(1, 31),
                new SeriesPoint(2, 0.27),
                new SeriesPoint(3, 158),
                new SeriesPoint(4, 4),
                new SeriesPoint(5, 22)
            };
            SeriesPoint[] values = new SeriesPoint[]
            {
                new SeriesPoint(0, 100),
                new SeriesPoint(1, 9),
                new SeriesPoint(2, 0.33),
                new SeriesPoint(3, 178),
                new SeriesPoint(4, 7.5),
                new SeriesPoint(5, 11)
            };

            chartCompare.Series["Goal"].Points.AddRange(goals);
            chartCompare.Series["Grade Averege"].Points.AddRange(avereges);
            chartCompare.Series["Measured Values"].Points.AddRange(values);
        }
        #endregion
    }
}