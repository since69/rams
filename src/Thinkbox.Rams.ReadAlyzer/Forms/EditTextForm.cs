﻿using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class EditTextForm : XtraForm
    {
        #region VARIABLES
        private TestText text;
        private TestText bindingText;
        #endregion

        #region CONSTRUCTORS
        public EditTextForm(TestText text, IDXMenuManager menuManager)
        {
            InitializeComponent();

            this.text = text;
            this.bindingText = text.Clone();

            InitializeControls(bindingText, menuManager);
        }
        #endregion

        #region METHODS
        private void InitializeControls(TestText text, IDXMenuManager menuManager)
        {
            cbCulture.DataSource = DataHelper.Cultures;
            cbCulture.DisplayMember = "Name";
            cbCulture.ValueMember = "Id";

            reText.Appearance.Text.Font = new System.Drawing.Font(
                text.FontName, text.FontSize);
            EditHelper.InitLevelComboBox(cbLevel);

            // Intialize menu manger
            foreach (Control control in lcEditText.Controls)
            {
                BaseEdit edit = control as BaseEdit;
                if (edit != null) edit.MenuManager = menuManager;
            }

            cbCulture.SelectedValue = text.Culture.Id;
            cbLevel.SelectedValue = text.Level;
            teTitle.Text = text.Title;
            reText.Text = text.Text;
        }
        #endregion

        #region CONTROL EVENTS
        private void btnOk_Click(object sender, System.EventArgs e)
        {
            bindingText.Culture.Id = cbCulture.SelectedValue.ToString();
            bindingText.Level = (int)cbLevel.SelectedValue;
            bindingText.Title = teTitle.Text;
            bindingText.Text = reText.Text;
            text.Assign(bindingText);
        }
        #endregion
    }
}
