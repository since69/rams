﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class EditTextForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTextForm));
            this.lcEditText = new DevExpress.XtraLayout.LayoutControl();
            this.cbCulture = new System.Windows.Forms.ComboBox();
            this.teTitle = new DevExpress.XtraEditors.TextEdit();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.reText = new DevExpress.XtraRichEdit.RichEditControl();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lcgEditText = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciText = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCulture = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditText)).BeginInit();
            this.lcEditText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTitle.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEditText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCulture)).BeginInit();
            this.SuspendLayout();
            // 
            // lcEditText
            // 
            this.lcEditText.Controls.Add(this.cbCulture);
            this.lcEditText.Controls.Add(this.teTitle);
            this.lcEditText.Controls.Add(this.cbLevel);
            this.lcEditText.Controls.Add(this.reText);
            this.lcEditText.Controls.Add(this.btnCancel);
            this.lcEditText.Controls.Add(this.btnOk);
            this.lcEditText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcEditText.Location = new System.Drawing.Point(0, 0);
            this.lcEditText.Name = "lcEditText";
            this.lcEditText.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(3319, 213, 719, 400);
            this.lcEditText.Root = this.lcgEditText;
            this.lcEditText.Size = new System.Drawing.Size(704, 461);
            this.lcEditText.TabIndex = 0;
            this.lcEditText.Text = "layoutControl1";
            // 
            // cbCulture
            // 
            this.cbCulture.Enabled = false;
            this.cbCulture.FormattingEnabled = true;
            this.cbCulture.Location = new System.Drawing.Point(72, 12);
            this.cbCulture.Name = "cbCulture";
            this.cbCulture.Size = new System.Drawing.Size(620, 22);
            this.cbCulture.TabIndex = 10;
            // 
            // teTitle
            // 
            this.teTitle.Enabled = false;
            this.teTitle.Location = new System.Drawing.Point(72, 62);
            this.teTitle.Name = "teTitle";
            this.teTitle.Size = new System.Drawing.Size(620, 20);
            this.teTitle.StyleController = this.lcEditText;
            this.teTitle.TabIndex = 9;
            // 
            // cbLevel
            // 
            this.cbLevel.Enabled = false;
            this.cbLevel.FormattingEnabled = true;
            this.cbLevel.Location = new System.Drawing.Point(72, 37);
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.Size = new System.Drawing.Size(620, 22);
            this.cbLevel.TabIndex = 8;
            // 
            // reText
            // 
            this.reText.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.reText.Location = new System.Drawing.Point(12, 103);
            this.reText.Name = "reText";
            this.reText.Size = new System.Drawing.Size(680, 314);
            this.reText.TabIndex = 6;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(599, 427);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(93, 22);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(502, 427);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(93, 22);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lcgEditText
            // 
            this.lcgEditText.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgEditText.GroupBordersVisible = false;
            this.lcgEditText.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.lciOk,
            this.lciCancel,
            this.emptySpaceItem2,
            this.lciText,
            this.lciTitle,
            this.lciCulture,
            this.lciLevel});
            this.lcgEditText.Location = new System.Drawing.Point(0, 0);
            this.lcgEditText.Name = "Root";
            this.lcgEditText.Size = new System.Drawing.Size(704, 461);
            this.lcgEditText.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 409);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 6);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 6);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(684, 6);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciOk
            // 
            this.lciOk.Control = this.btnOk;
            this.lciOk.Location = new System.Drawing.Point(490, 415);
            this.lciOk.MaxSize = new System.Drawing.Size(0, 26);
            this.lciOk.MinSize = new System.Drawing.Size(37, 26);
            this.lciOk.Name = "lciOk";
            this.lciOk.Size = new System.Drawing.Size(97, 26);
            this.lciOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOk.TextSize = new System.Drawing.Size(0, 0);
            this.lciOk.TextVisible = false;
            // 
            // lciCancel
            // 
            this.lciCancel.Control = this.btnCancel;
            this.lciCancel.Location = new System.Drawing.Point(587, 415);
            this.lciCancel.MaxSize = new System.Drawing.Size(0, 26);
            this.lciCancel.MinSize = new System.Drawing.Size(37, 26);
            this.lciCancel.Name = "lciCancel";
            this.lciCancel.Size = new System.Drawing.Size(97, 26);
            this.lciCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCancel.TextSize = new System.Drawing.Size(0, 0);
            this.lciCancel.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 415);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(490, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciText
            // 
            this.lciText.Control = this.reText;
            this.lciText.Location = new System.Drawing.Point(0, 74);
            this.lciText.Name = "lciText";
            this.lciText.Size = new System.Drawing.Size(684, 335);
            this.lciText.Text = "Text:";
            this.lciText.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciText.TextSize = new System.Drawing.Size(57, 14);
            // 
            // lciLevel
            // 
            this.lciLevel.Control = this.cbLevel;
            this.lciLevel.Location = new System.Drawing.Point(0, 25);
            this.lciLevel.Name = "lciLevel";
            this.lciLevel.Size = new System.Drawing.Size(684, 25);
            this.lciLevel.Text = "Level: ";
            this.lciLevel.TextSize = new System.Drawing.Size(57, 14);
            // 
            // lciTitle
            // 
            this.lciTitle.Control = this.teTitle;
            this.lciTitle.Location = new System.Drawing.Point(0, 50);
            this.lciTitle.Name = "lciTitle";
            this.lciTitle.Size = new System.Drawing.Size(684, 24);
            this.lciTitle.Text = "Title: ";
            this.lciTitle.TextSize = new System.Drawing.Size(57, 14);
            // 
            // lciCulture
            // 
            this.lciCulture.Control = this.cbCulture;
            this.lciCulture.Location = new System.Drawing.Point(0, 0);
            this.lciCulture.Name = "lciCulture";
            this.lciCulture.Size = new System.Drawing.Size(684, 25);
            this.lciCulture.Text = "Language:";
            this.lciCulture.TextSize = new System.Drawing.Size(57, 14);
            // 
            // EditTextForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 461);
            this.Controls.Add(this.lcEditText);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditTextForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Create New Text";
            ((System.ComponentModel.ISupportInitialize)(this.lcEditText)).EndInit();
            this.lcEditText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teTitle.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEditText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCulture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcEditText;
        private DevExpress.XtraLayout.LayoutControlGroup lcgEditText;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private DevExpress.XtraLayout.LayoutControlItem lciOk;
        private DevExpress.XtraLayout.LayoutControlItem lciCancel;
        private DevExpress.XtraRichEdit.RichEditControl reText;
        private DevExpress.XtraLayout.LayoutControlItem lciText;
        private System.Windows.Forms.ComboBox cbCulture;
        private DevExpress.XtraLayout.LayoutControlItem lciCulture;
        private DevExpress.XtraEditors.TextEdit teTitle;
        private System.Windows.Forms.ComboBox cbLevel;
        private DevExpress.XtraLayout.LayoutControlItem lciLevel;
        private DevExpress.XtraLayout.LayoutControlItem lciTitle;
    }
}