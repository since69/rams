﻿//
// Thinkbox.Rams.ReadAlyzer.Forms.TestResultForm
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using log4net;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class TestResultForm : XtraForm
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        private Data.Test test = null;
        #endregion

        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public TestResultForm(Data.Test test, IDXMenuManager menuManager)
        {
            InitializeComponent();

            this.test = test;
            this.test.Answers.Clear();
            this.test.Answers = DataHelper.GetAnswers(this.test.Id);
            InitializeControls();
        }
        #endregion

        #region METHODS
        private string GetCorrectAnswers()
        {
            int totalAnswers = 0;
            int correctAnswers = 0;

            //DataHelper.GetCorrectAnswers(test.Id, 
            //    out totalAnswers, out correctAnswers);
            DataHelper.GetCorrectAnswers(test.Answers, 
                out totalAnswers, out correctAnswers);
            
            return string.Format("{0} / {1}", correctAnswers, totalAnswers);
        }

        private void InitializeChart()
        {
            int markerSize = 6;

            // create line series.
            DevExpress.XtraCharts.Series left = new DevExpress.XtraCharts.Series(
                "Left", DevExpress.XtraCharts.ViewType.Line);
            DevExpress.XtraCharts.Series right = new DevExpress.XtraCharts.Series(
                "Left", DevExpress.XtraCharts.ViewType.Line);

            // set the numerical argument scale types for the series,
            // as it is qualitative, by default.
            left.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            left.ArgumentDataMember = "RawData.Elapsed";
            left.ValueScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            left.ValueDataMembers.AddRange(new string[] { "RawData.LeftX" });
            right.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            right.ArgumentDataMember = "RawData.Elapsed";
            right.ValueScaleType = DevExpress.XtraCharts.ScaleType.Numerical;
            right.ValueDataMembers.AddRange(new string[] { "RawData.RightX" });

            // add line series to the chart.
            chartRecords.Series.Add(left);
            chartRecords.Series.Add(right);

            chartRecords.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;

            DevExpress.XtraCharts.LineSeriesView leftView = (DevExpress.XtraCharts.LineSeriesView)left.View;
            DevExpress.XtraCharts.LineSeriesView rightView = (DevExpress.XtraCharts.LineSeriesView)right.View;

            // access the view-type-specific options of the series.
            leftView.LineMarkerOptions.Kind = DevExpress.XtraCharts.MarkerKind.Circle;
            leftView.LineMarkerOptions.Size = markerSize;
            leftView.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dash;
            leftView.Color = System.Drawing.Color.Red;
            leftView.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
            rightView.LineMarkerOptions.Kind = DevExpress.XtraCharts.MarkerKind.Circle;
            rightView.LineMarkerOptions.Size = markerSize;
            rightView.LineStyle.DashStyle = DevExpress.XtraCharts.DashStyle.Dash;
            rightView.Color = System.Drawing.Color.Blue;
            rightView.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;

            // cast the chart's diagram to the XYDiagram type, to access its axes.
            DevExpress.XtraCharts.XYDiagram diagram =
                (DevExpress.XtraCharts.XYDiagram)chartRecords.Diagram;

            diagram.Rotated = true;
            diagram.PaneLayoutDirection = DevExpress.XtraCharts.PaneLayoutDirection.Horizontal;

            // add a new additional pane to the diagram.
            diagram.Panes.Add(new DevExpress.XtraCharts.XYDiagramPane("Right Eye Pane"));

            // assign the additional pane  to the second series. 
            // Note that the created pane has the zero index in the collection,
            // because the existing Default pane is a separate entity.
            rightView.Pane = diagram.Panes[0];

            // Define the whole range for the axes.
            //diagram.AxisX.WholeRange.Auto = false;
            //diagram.AxisX.WholeRange.SetMinMaxValues("A", "D");
            //diagram.AxisY.WholeRange.Auto = false;
            //diagram.AxisY.WholeRange.SetMinMaxValues(0, 70);

            // Define the visible range for the axes.  
            //diagram.AxisX.VisualRange.Auto = false;
            //diagram.AxisX.VisualRange.SetMinMaxValues("B", "C");
            //diagram.AxisY.VisualRange.Auto = false;
            //diagram.AxisY.VisualRange.SetMinMaxValues(7, 50);

            // Specify the axes scrolling at the diagram's level.
            diagram.EnableAxisXScrolling = false;
            diagram.EnableAxisYScrolling = true;

            // Individually specify the axes scrolling for the panes.
            diagram.DefaultPane.EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.Default;
            diagram.Panes[0].EnableAxisXScrolling = DevExpress.Utils.DefaultBoolean.False;
            diagram.Panes[0].EnableAxisYScrolling = DevExpress.Utils.DefaultBoolean.True;

            // Adjust how the scrolling can be performed.
            diagram.ScrollingOptions.UseKeyboard = false;
            diagram.ScrollingOptions.UseMouse = false;
            diagram.ScrollingOptions.UseScrollBars = true;
        }

        private void InitializeControls()
        {
            if (test == null) return;

            string singleDecFmt = "{0:0.00000}";
            string doubleDecFmt = "{0:0.00000} / {1:0.00000}";
            string doubleIntFmt = "{0} / {1}";
            Device.TestResult result = Device.DataAnalyzer.Analyze(
                test.TestText.CharacterWidth, test.TestText.CharacterHeight, 
                test.TestText.LineSpacing, test.TestText.Text, 
                FileHelper.ImportRecord(Device.Tracker.RecordingFolder,
                    test.RecordFile));

            InitializeChart();
            //peTrainee.Image = 
            lblTraineeName.Text = test.Trainee.Person.FullName;
            lblGender.Text = test.Trainee.Gender.Equals("M") ? 
                Properties.Resources.Male : Properties.Resources.Female;
            lblBirthDate.Text = test.Trainee.BirthDate.ToShortDateString();
            lblTrainerVal.Text = test.Trainer.Person.FullName;
            lblStartTimeVal.Text = StringHelper.ConvertToString(test.StartTime);
            lblEndTimeVal.Text = StringHelper.ConvertToString(test.EndTime);
            lblGradeVal.Text = $"{ test.Grade }";
            lblTextTitleVal.Text = $"{ test.TestText.Title }";
            lblTextLevelVal.Text = $"{ test.TestText.Level }";
            lblNumOfLinesVal.Text = $"{ result.NumOfLines }";
            lblNumOfWordsVal.Text = $"{ result.NumOfWords }";
            lblQuestionResultVal.Text = GetCorrectAnswers();
            lblFixationWidthVal.Text = string.Format(singleDecFmt, 
                result.FixationWidth);
            lblReadingRateVal.Text = string.Format(singleDecFmt, 
                result.ReadingRate);
            lblNumOfFixationsVal.Text = string.Format(doubleIntFmt, 
                result.NumOfFixations[0], result.NumOfFixations[1]);
            lblNumOfRegressionsVal.Text = string.Format(doubleIntFmt, 
                result.NumOfRegression.Left, result.NumOfRegression.Right);
            lblFixationDurationVal.Text = string.Format(doubleDecFmt, 
                result.FixationDuration.Left / 1000, 
                result.FixationDuration.Right / 1000);
            lblCrossCorrelationVal.Text = string.Format(singleDecFmt, 
                result.CrossCorrelation);
            lblNumOfAnalyzedLinesVal.Text = $"{ result.NumOfUsedLines }";
            gcRecords.DataSource = result.Records;
            chartRecords.DataSource = result.Records;
        }
        #endregion

        #region CONTROL EVENTS
        private void btnPrint_Click(object sender, System.EventArgs e)
        {
            Reports.DetailReport report = new Reports.DetailReport();

            using (ReportPrintTool printTool = new ReportPrintTool(report))
            {
                // Invoke the Print dialog.
                printTool.PrintDialog();

                // Send the report to the default printer.
                printTool.ShowPreview();
                //printTool.Print();

                // Send the report to the specified printer.
                //printTool.Print("myPrinter");
            }
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            Close();
        }
        #endregion
    }
}
