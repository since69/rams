﻿using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class EditTrainerForm : XtraForm
    {
        #region VARIABLES
        private Trainer trainer;
        private Trainer bindingTrainer;
        #endregion

        #region CONSTRUCTORS
        public EditTrainerForm()
        {
            InitializeComponent();
        }

        public EditTrainerForm(Trainer trainer, IDXMenuManager menuManager)
        {
            InitializeComponent();

            this.trainer = trainer;
            this.bindingTrainer = trainer.Clone();

            InitializeControls(bindingTrainer, menuManager);
        }
        #endregion

        #region METHODS
        private void InitializeControls(Trainer trainer, IDXMenuManager menuManager)
        {
            EditHelper.InitGenderComboBox(cbGender.Properties);
            EditHelper.InitNameStyleComboBox(cbNameStyle);

            // Intialize menu manger
            foreach (Control control in lcEditTrainer.Controls)
            {
                BaseEdit edit = control as BaseEdit;
                if (edit != null) edit.MenuManager = menuManager;
            }

            peTrainer.Image = Properties.Resources.UnknownUser_256x256;
            cbNameStyle.SelectedValue = trainer.Person.NameStyle;
            teFirstName.Text = trainer.Person.FirstName;
            teLastName.Text = trainer.Person.LastName;
            cbGender.EditValue = trainer.Gender;
        }
        #endregion

        #region CONTROL EVENTS
        private void btnOk_Click(object sender, System.EventArgs e)
        {
            bindingTrainer.Person.NameStyle = (bool)cbNameStyle.SelectedValue;
            bindingTrainer.Person.FirstName = teFirstName.Text;
            bindingTrainer.Person.LastName = teLastName.Text;
            bindingTrainer.Gender = cbGender.EditValue.ToString();
            trainer.Assign(bindingTrainer);
        }
        #endregion
    }
}
