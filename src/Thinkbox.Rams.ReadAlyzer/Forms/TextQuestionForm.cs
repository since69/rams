﻿using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class TextQuestionForm : XtraForm
    {
        #region VARIABLES
        private List<Data.TestAnswer> answers;
        private int currentQuestion = 0;
        private bool isFinish = false;
        #endregion

        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public TextQuestionForm(List<Data.TestAnswer> answers)
        {
            InitializeComponent();

            this.answers = answers;
        }
        #endregion

        #region METHODS
        private void UpdateQuestion(int index)
        {
            if (index > 0)
                answers[index-1].Answer = (rgAnswer.SelectedIndex == 0 ? true : false);
                
            if (index < answers.Count)
            {
                pgQuestion.Text = $"QUESTION { index + 1 } of { answers.Count }";
                lblQuestion.Text = answers[index].Question.Question;
            }
        }
        #endregion

        #region FORM EVENTS
        private void TextQuestionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isFinish) return;

            DialogResult rc = XtraMessageBox.Show(this, 
                "Asking is not complete. Are you sure exit?", 
                "Test Question", 
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (rc == DialogResult.No) e.Cancel = true;
        }
        #endregion

        #region CONTROL EVENTS
        private void wizTextQuestion_CancelClick(object sender, 
            System.ComponentModel.CancelEventArgs e)
        {
        }

        private void wizTextQuestion_FinishClick(object sender, 
            System.ComponentModel.CancelEventArgs e)
        {
            isFinish = true;
            Close();
        }

        private void wizTextQuestion_NextClick(object sender, 
            DevExpress.XtraWizard.WizardCommandButtonClickEventArgs e)
        {
        }

        private void wizTextQuestion_PrevClick(object sender, 
            DevExpress.XtraWizard.WizardCommandButtonClickEventArgs e)
        {

        }

        private void wizTextQuestion_SelectedPageChanged(object sender, 
            DevExpress.XtraWizard.WizardPageChangedEventArgs e)
        {

        }

        private void wizTextQuestion_SelectedPageChanging(object sender, 
            DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            if (e.PrevPage == pgWelcome || e.PrevPage == pgQuestion)
            {
                if (currentQuestion < answers.Count)
                {
                    UpdateQuestion(currentQuestion);
                    e.Page = pgQuestion;
                    ++currentQuestion;
                }
                if (currentQuestion == answers.Count)
                    UpdateQuestion(currentQuestion);
            }

            if (e.Page == pgComplete)
            {
                int totalAnswers, correctAnswers;
                DataHelper.GetCorrectAnswers(answers, 
                    out totalAnswers, out correctAnswers);
                StringBuilder sb = new StringBuilder("Question is over.\n\n");
                sb.Append($"{ correctAnswers } correct answers ");
                sb.Append($"out of { totalAnswers } total answers.");
                pgComplete.FinishText = sb.ToString();
            }
        }
        #endregion
    }
}
