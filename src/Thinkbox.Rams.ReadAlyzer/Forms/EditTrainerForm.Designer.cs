﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class EditTrainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTrainerForm));
            this.lcEditTrainer = new DevExpress.XtraLayout.LayoutControl();
            this.cbNameStyle = new System.Windows.Forms.ComboBox();
            this.teEmail = new DevExpress.XtraEditors.TextEdit();
            this.cbGender = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.teLastName = new DevExpress.XtraEditors.TextEdit();
            this.teFirstName = new DevExpress.XtraEditors.TextEdit();
            this.peTrainer = new DevExpress.XtraEditors.PictureEdit();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lcgEditTrainer = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciTrainerPhoto = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciFirstName = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.lciLastName = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgInfo = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciGender = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcNameStyle = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcEditTrainer)).BeginInit();
            this.lcEditTrainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peTrainer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEditTrainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainerPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcNameStyle)).BeginInit();
            this.SuspendLayout();
            // 
            // lcEditTrainer
            // 
            this.lcEditTrainer.Controls.Add(this.cbNameStyle);
            this.lcEditTrainer.Controls.Add(this.teEmail);
            this.lcEditTrainer.Controls.Add(this.cbGender);
            this.lcEditTrainer.Controls.Add(this.teLastName);
            this.lcEditTrainer.Controls.Add(this.teFirstName);
            this.lcEditTrainer.Controls.Add(this.peTrainer);
            this.lcEditTrainer.Controls.Add(this.btnOk);
            this.lcEditTrainer.Controls.Add(this.btnCancel);
            resources.ApplyResources(this.lcEditTrainer, "lcEditTrainer");
            this.lcEditTrainer.Name = "lcEditTrainer";
            this.lcEditTrainer.Root = this.lcgEditTrainer;
            // 
            // cbNameStyle
            // 
            this.cbNameStyle.FormattingEnabled = true;
            resources.ApplyResources(this.cbNameStyle, "cbNameStyle");
            this.cbNameStyle.Name = "cbNameStyle";
            // 
            // teEmail
            // 
            resources.ApplyResources(this.teEmail, "teEmail");
            this.teEmail.Name = "teEmail";
            this.teEmail.StyleController = this.lcEditTrainer;
            // 
            // cbGender
            // 
            resources.ApplyResources(this.cbGender, "cbGender");
            this.cbGender.Name = "cbGender";
            this.cbGender.Properties.DropDownRows = 2;
            this.cbGender.StyleController = this.lcEditTrainer;
            // 
            // teLastName
            // 
            resources.ApplyResources(this.teLastName, "teLastName");
            this.teLastName.Name = "teLastName";
            this.teLastName.StyleController = this.lcEditTrainer;
            // 
            // teFirstName
            // 
            resources.ApplyResources(this.teFirstName, "teFirstName");
            this.teFirstName.Name = "teFirstName";
            this.teFirstName.StyleController = this.lcEditTrainer;
            // 
            // peTrainer
            // 
            this.peTrainer.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.peTrainer, "peTrainer");
            this.peTrainer.Name = "peTrainer";
            this.peTrainer.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.peTrainer.StyleController = this.lcEditTrainer;
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lcgEditTrainer
            // 
            this.lcgEditTrainer.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgEditTrainer.GroupBordersVisible = false;
            this.lcgEditTrainer.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciOk,
            this.lciCancel,
            this.emptySpaceItem2,
            this.lciTrainerPhoto,
            this.emptySpaceItem3,
            this.lciFirstName,
            this.splitterItem1,
            this.lciLastName,
            this.lcgInfo,
            this.lcNameStyle});
            this.lcgEditTrainer.Location = new System.Drawing.Point(0, 0);
            this.lcgEditTrainer.Name = "Root";
            this.lcgEditTrainer.Size = new System.Drawing.Size(564, 233);
            this.lcgEditTrainer.TextVisible = false;
            // 
            // lciOk
            // 
            this.lciOk.Control = this.btnOk;
            this.lciOk.Location = new System.Drawing.Point(350, 183);
            this.lciOk.MaxSize = new System.Drawing.Size(97, 30);
            this.lciOk.MinSize = new System.Drawing.Size(97, 30);
            this.lciOk.Name = "lciOk";
            this.lciOk.Size = new System.Drawing.Size(97, 30);
            this.lciOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOk.TextSize = new System.Drawing.Size(0, 0);
            this.lciOk.TextVisible = false;
            // 
            // lciCancel
            // 
            this.lciCancel.Control = this.btnCancel;
            this.lciCancel.Location = new System.Drawing.Point(447, 183);
            this.lciCancel.MaxSize = new System.Drawing.Size(97, 30);
            this.lciCancel.MinSize = new System.Drawing.Size(97, 30);
            this.lciCancel.Name = "lciCancel";
            this.lciCancel.Size = new System.Drawing.Size(97, 30);
            this.lciCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCancel.TextSize = new System.Drawing.Size(0, 0);
            this.lciCancel.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 183);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(350, 30);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciTrainerPhoto
            // 
            this.lciTrainerPhoto.Control = this.peTrainer;
            this.lciTrainerPhoto.Location = new System.Drawing.Point(0, 0);
            this.lciTrainerPhoto.Name = "lciTrainerPhoto";
            this.lciTrainerPhoto.Size = new System.Drawing.Size(170, 177);
            this.lciTrainerPhoto.TextSize = new System.Drawing.Size(0, 0);
            this.lciTrainerPhoto.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 177);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 6);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(544, 6);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciFirstName
            // 
            this.lciFirstName.Control = this.teFirstName;
            this.lciFirstName.Location = new System.Drawing.Point(175, 25);
            this.lciFirstName.Name = "lciFirstName";
            this.lciFirstName.Size = new System.Drawing.Size(369, 24);
            resources.ApplyResources(this.lciFirstName, "lciFirstName");
            this.lciFirstName.TextSize = new System.Drawing.Size(66, 14);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            resources.ApplyResources(this.splitterItem1, "splitterItem1");
            this.splitterItem1.Location = new System.Drawing.Point(170, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(5, 177);
            // 
            // lciLastName
            // 
            this.lciLastName.Control = this.teLastName;
            this.lciLastName.Location = new System.Drawing.Point(175, 49);
            this.lciLastName.Name = "lciLastName";
            this.lciLastName.Size = new System.Drawing.Size(369, 24);
            resources.ApplyResources(this.lciLastName, "lciLastName");
            this.lciLastName.TextSize = new System.Drawing.Size(66, 14);
            // 
            // lcgInfo
            // 
            this.lcgInfo.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciEmail,
            this.lciGender,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.lcgInfo.Location = new System.Drawing.Point(175, 73);
            this.lcgInfo.Name = "lcgInfo";
            this.lcgInfo.Size = new System.Drawing.Size(369, 104);
            resources.ApplyResources(this.lcgInfo, "lcgInfo");
            // 
            // lciEmail
            // 
            this.lciEmail.Control = this.teEmail;
            this.lciEmail.Location = new System.Drawing.Point(0, 0);
            this.lciEmail.Name = "lciEmail";
            this.lciEmail.Size = new System.Drawing.Size(345, 24);
            resources.ApplyResources(this.lciEmail, "lciEmail");
            this.lciEmail.TextSize = new System.Drawing.Size(66, 14);
            // 
            // lciGender
            // 
            this.lciGender.Control = this.cbGender;
            this.lciGender.Location = new System.Drawing.Point(0, 24);
            this.lciGender.Name = "lciGender";
            this.lciGender.Size = new System.Drawing.Size(251, 24);
            resources.ApplyResources(this.lciGender, "lciGender");
            this.lciGender.TextSize = new System.Drawing.Size(66, 14);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(251, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(94, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(345, 13);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcNameStyle
            // 
            this.lcNameStyle.Control = this.cbNameStyle;
            this.lcNameStyle.Location = new System.Drawing.Point(175, 0);
            this.lcNameStyle.Name = "lcNameStyle";
            this.lcNameStyle.Size = new System.Drawing.Size(369, 25);
            resources.ApplyResources(this.lcNameStyle, "lcNameStyle");
            this.lcNameStyle.TextSize = new System.Drawing.Size(66, 14);
            // 
            // EditTrainerForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcEditTrainer);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditTrainerForm";
            ((System.ComponentModel.ISupportInitialize)(this.lcEditTrainer)).EndInit();
            this.lcEditTrainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peTrainer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEditTrainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainerPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcNameStyle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcEditTrainer;
        private DevExpress.XtraLayout.LayoutControlGroup lcgEditTrainer;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraLayout.LayoutControlItem lciOk;
        private DevExpress.XtraLayout.LayoutControlItem lciCancel;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PictureEdit peTrainer;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainerPhoto;
        private DevExpress.XtraEditors.TextEdit teEmail;
        private DevExpress.XtraEditors.ImageComboBoxEdit cbGender;
        private DevExpress.XtraEditors.TextEdit teLastName;
        private DevExpress.XtraEditors.TextEdit teFirstName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem lciFirstName;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.LayoutControlItem lciLastName;
        private DevExpress.XtraLayout.LayoutControlGroup lcgInfo;
        private DevExpress.XtraLayout.LayoutControlItem lciEmail;
        private DevExpress.XtraLayout.LayoutControlItem lciGender;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.ComboBox cbNameStyle;
        private DevExpress.XtraLayout.LayoutControlItem lcNameStyle;
    }
}