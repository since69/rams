﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class TestResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestResultForm));
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.lcRoot = new DevExpress.XtraLayout.LayoutControl();
            this.tabResult = new DevExpress.XtraTab.XtraTabControl();
            this.pgSummary = new DevExpress.XtraTab.XtraTabPage();
            this.lblNumOfAnalyzedLinesVal = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfAnalyzedLines = new DevExpress.XtraEditors.LabelControl();
            this.lblCrossCorrelationVal = new DevExpress.XtraEditors.LabelControl();
            this.lblCrossCorrelation = new DevExpress.XtraEditors.LabelControl();
            this.lblFixationDurationVal = new DevExpress.XtraEditors.LabelControl();
            this.lblFixationDuration = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfRegressionsVal = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfRegressions = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfFixationsVal = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfFixations = new DevExpress.XtraEditors.LabelControl();
            this.lblReadingRateVal = new DevExpress.XtraEditors.LabelControl();
            this.lblReadingRate = new DevExpress.XtraEditors.LabelControl();
            this.lblFixationWidthVal = new DevExpress.XtraEditors.LabelControl();
            this.lblFixationWidth = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfWordsVal = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfLinesVal = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfWords = new DevExpress.XtraEditors.LabelControl();
            this.lblNumOfLines = new DevExpress.XtraEditors.LabelControl();
            this.lblQuestionResultVal = new DevExpress.XtraEditors.LabelControl();
            this.lblQuestionResult = new DevExpress.XtraEditors.LabelControl();
            this.lblTextLevelVal = new DevExpress.XtraEditors.LabelControl();
            this.lblTextLevel = new DevExpress.XtraEditors.LabelControl();
            this.lblTextTitleVal = new DevExpress.XtraEditors.LabelControl();
            this.lblTextTitle = new DevExpress.XtraEditors.LabelControl();
            this.lblEndTimeVal = new DevExpress.XtraEditors.LabelControl();
            this.lblEndTime = new DevExpress.XtraEditors.LabelControl();
            this.lblGradeVal = new DevExpress.XtraEditors.LabelControl();
            this.lblStartTimeVal = new DevExpress.XtraEditors.LabelControl();
            this.lblTrainerVal = new DevExpress.XtraEditors.LabelControl();
            this.lblTrainer = new DevExpress.XtraEditors.LabelControl();
            this.lblTestGrade = new DevExpress.XtraEditors.LabelControl();
            this.lblStartTime = new DevExpress.XtraEditors.LabelControl();
            this.lblBirthDate = new DevExpress.XtraEditors.LabelControl();
            this.lblGender = new DevExpress.XtraEditors.LabelControl();
            this.peTrainee = new DevExpress.XtraEditors.PictureEdit();
            this.lblTraineeName = new DevExpress.XtraEditors.LabelControl();
            this.pgRecords = new DevExpress.XtraTab.XtraTabPage();
            this.gcRecords = new DevExpress.XtraGrid.GridControl();
            this.gvRecords = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colElapsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalculatedLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeftX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeftY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRightY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.pgChart = new DevExpress.XtraTab.XtraTabPage();
            this.chartRecords = new DevExpress.XtraCharts.ChartControl();
            this.btnOk = new DevExpress.XtraEditors.SimpleButton();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciTab = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciPrint = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcRoot)).BeginInit();
            this.lcRoot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).BeginInit();
            this.tabResult.SuspendLayout();
            this.pgSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peTrainee.Properties)).BeginInit();
            this.pgRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.pgChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // btnPrint
            // 
            resources.ApplyResources(this.btnPrint, "btnPrint");
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.StyleController = this.lcRoot;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lcRoot
            // 
            this.lcRoot.Controls.Add(this.tabResult);
            this.lcRoot.Controls.Add(this.btnOk);
            this.lcRoot.Controls.Add(this.btnPrint);
            resources.ApplyResources(this.lcRoot, "lcRoot");
            this.lcRoot.Name = "lcRoot";
            this.lcRoot.Root = this.lcgRoot;
            // 
            // tabResult
            // 
            resources.ApplyResources(this.tabResult, "tabResult");
            this.tabResult.Name = "tabResult";
            this.tabResult.SelectedTabPage = this.pgSummary;
            this.tabResult.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.pgSummary,
            this.pgRecords,
            this.pgChart});
            // 
            // pgSummary
            // 
            this.pgSummary.Controls.Add(this.lblNumOfAnalyzedLinesVal);
            this.pgSummary.Controls.Add(this.lblNumOfAnalyzedLines);
            this.pgSummary.Controls.Add(this.lblCrossCorrelationVal);
            this.pgSummary.Controls.Add(this.lblCrossCorrelation);
            this.pgSummary.Controls.Add(this.lblFixationDurationVal);
            this.pgSummary.Controls.Add(this.lblFixationDuration);
            this.pgSummary.Controls.Add(this.lblNumOfRegressionsVal);
            this.pgSummary.Controls.Add(this.lblNumOfRegressions);
            this.pgSummary.Controls.Add(this.lblNumOfFixationsVal);
            this.pgSummary.Controls.Add(this.lblNumOfFixations);
            this.pgSummary.Controls.Add(this.lblReadingRateVal);
            this.pgSummary.Controls.Add(this.lblReadingRate);
            this.pgSummary.Controls.Add(this.lblFixationWidthVal);
            this.pgSummary.Controls.Add(this.lblFixationWidth);
            this.pgSummary.Controls.Add(this.lblNumOfWordsVal);
            this.pgSummary.Controls.Add(this.lblNumOfLinesVal);
            this.pgSummary.Controls.Add(this.lblNumOfWords);
            this.pgSummary.Controls.Add(this.lblNumOfLines);
            this.pgSummary.Controls.Add(this.lblQuestionResultVal);
            this.pgSummary.Controls.Add(this.lblQuestionResult);
            this.pgSummary.Controls.Add(this.lblTextLevelVal);
            this.pgSummary.Controls.Add(this.lblTextLevel);
            this.pgSummary.Controls.Add(this.lblTextTitleVal);
            this.pgSummary.Controls.Add(this.lblTextTitle);
            this.pgSummary.Controls.Add(this.lblEndTimeVal);
            this.pgSummary.Controls.Add(this.lblEndTime);
            this.pgSummary.Controls.Add(this.lblGradeVal);
            this.pgSummary.Controls.Add(this.lblStartTimeVal);
            this.pgSummary.Controls.Add(this.lblTrainerVal);
            this.pgSummary.Controls.Add(this.lblTrainer);
            this.pgSummary.Controls.Add(this.lblTestGrade);
            this.pgSummary.Controls.Add(this.lblStartTime);
            this.pgSummary.Controls.Add(this.lblBirthDate);
            this.pgSummary.Controls.Add(this.lblGender);
            this.pgSummary.Controls.Add(this.peTrainee);
            this.pgSummary.Controls.Add(this.lblTraineeName);
            this.pgSummary.Name = "pgSummary";
            resources.ApplyResources(this.pgSummary, "pgSummary");
            // 
            // lblNumOfAnalyzedLinesVal
            // 
            this.lblNumOfAnalyzedLinesVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfAnalyzedLinesVal.Appearance.Font")));
            this.lblNumOfAnalyzedLinesVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfAnalyzedLinesVal.Appearance.ForeColor")));
            this.lblNumOfAnalyzedLinesVal.Appearance.Options.UseFont = true;
            this.lblNumOfAnalyzedLinesVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfAnalyzedLinesVal, "lblNumOfAnalyzedLinesVal");
            this.lblNumOfAnalyzedLinesVal.Name = "lblNumOfAnalyzedLinesVal";
            // 
            // lblNumOfAnalyzedLines
            // 
            this.lblNumOfAnalyzedLines.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfAnalyzedLines.Appearance.Font")));
            this.lblNumOfAnalyzedLines.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfAnalyzedLines.Appearance.ForeColor")));
            this.lblNumOfAnalyzedLines.Appearance.Options.UseFont = true;
            this.lblNumOfAnalyzedLines.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfAnalyzedLines, "lblNumOfAnalyzedLines");
            this.lblNumOfAnalyzedLines.Name = "lblNumOfAnalyzedLines";
            // 
            // lblCrossCorrelationVal
            // 
            this.lblCrossCorrelationVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblCrossCorrelationVal.Appearance.Font")));
            this.lblCrossCorrelationVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblCrossCorrelationVal.Appearance.ForeColor")));
            this.lblCrossCorrelationVal.Appearance.Options.UseFont = true;
            this.lblCrossCorrelationVal.Appearance.Options.UseForeColor = true;
            this.lblCrossCorrelationVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblCrossCorrelationVal, "lblCrossCorrelationVal");
            this.lblCrossCorrelationVal.Name = "lblCrossCorrelationVal";
            // 
            // lblCrossCorrelation
            // 
            this.lblCrossCorrelation.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblCrossCorrelation.Appearance.Font")));
            this.lblCrossCorrelation.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblCrossCorrelation.Appearance.ForeColor")));
            this.lblCrossCorrelation.Appearance.Options.UseFont = true;
            this.lblCrossCorrelation.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblCrossCorrelation, "lblCrossCorrelation");
            this.lblCrossCorrelation.Name = "lblCrossCorrelation";
            // 
            // lblFixationDurationVal
            // 
            this.lblFixationDurationVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblFixationDurationVal.Appearance.Font")));
            this.lblFixationDurationVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblFixationDurationVal.Appearance.ForeColor")));
            this.lblFixationDurationVal.Appearance.Options.UseFont = true;
            this.lblFixationDurationVal.Appearance.Options.UseForeColor = true;
            this.lblFixationDurationVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblFixationDurationVal, "lblFixationDurationVal");
            this.lblFixationDurationVal.Name = "lblFixationDurationVal";
            // 
            // lblFixationDuration
            // 
            this.lblFixationDuration.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblFixationDuration.Appearance.Font")));
            this.lblFixationDuration.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblFixationDuration.Appearance.ForeColor")));
            this.lblFixationDuration.Appearance.Options.UseFont = true;
            this.lblFixationDuration.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblFixationDuration, "lblFixationDuration");
            this.lblFixationDuration.Name = "lblFixationDuration";
            // 
            // lblNumOfRegressionsVal
            // 
            this.lblNumOfRegressionsVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfRegressionsVal.Appearance.Font")));
            this.lblNumOfRegressionsVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfRegressionsVal.Appearance.ForeColor")));
            this.lblNumOfRegressionsVal.Appearance.Options.UseFont = true;
            this.lblNumOfRegressionsVal.Appearance.Options.UseForeColor = true;
            this.lblNumOfRegressionsVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblNumOfRegressionsVal, "lblNumOfRegressionsVal");
            this.lblNumOfRegressionsVal.Name = "lblNumOfRegressionsVal";
            // 
            // lblNumOfRegressions
            // 
            this.lblNumOfRegressions.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfRegressions.Appearance.Font")));
            this.lblNumOfRegressions.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfRegressions.Appearance.ForeColor")));
            this.lblNumOfRegressions.Appearance.Options.UseFont = true;
            this.lblNumOfRegressions.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfRegressions, "lblNumOfRegressions");
            this.lblNumOfRegressions.Name = "lblNumOfRegressions";
            // 
            // lblNumOfFixationsVal
            // 
            this.lblNumOfFixationsVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfFixationsVal.Appearance.Font")));
            this.lblNumOfFixationsVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfFixationsVal.Appearance.ForeColor")));
            this.lblNumOfFixationsVal.Appearance.Options.UseFont = true;
            this.lblNumOfFixationsVal.Appearance.Options.UseForeColor = true;
            this.lblNumOfFixationsVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblNumOfFixationsVal, "lblNumOfFixationsVal");
            this.lblNumOfFixationsVal.Name = "lblNumOfFixationsVal";
            // 
            // lblNumOfFixations
            // 
            this.lblNumOfFixations.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfFixations.Appearance.Font")));
            this.lblNumOfFixations.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfFixations.Appearance.ForeColor")));
            this.lblNumOfFixations.Appearance.Options.UseFont = true;
            this.lblNumOfFixations.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfFixations, "lblNumOfFixations");
            this.lblNumOfFixations.Name = "lblNumOfFixations";
            // 
            // lblReadingRateVal
            // 
            this.lblReadingRateVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblReadingRateVal.Appearance.Font")));
            this.lblReadingRateVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblReadingRateVal.Appearance.ForeColor")));
            this.lblReadingRateVal.Appearance.Options.UseFont = true;
            this.lblReadingRateVal.Appearance.Options.UseForeColor = true;
            this.lblReadingRateVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblReadingRateVal, "lblReadingRateVal");
            this.lblReadingRateVal.Name = "lblReadingRateVal";
            // 
            // lblReadingRate
            // 
            this.lblReadingRate.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblReadingRate.Appearance.Font")));
            this.lblReadingRate.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblReadingRate.Appearance.ForeColor")));
            this.lblReadingRate.Appearance.Options.UseFont = true;
            this.lblReadingRate.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblReadingRate, "lblReadingRate");
            this.lblReadingRate.Name = "lblReadingRate";
            // 
            // lblFixationWidthVal
            // 
            this.lblFixationWidthVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblFixationWidthVal.Appearance.Font")));
            this.lblFixationWidthVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblFixationWidthVal.Appearance.ForeColor")));
            this.lblFixationWidthVal.Appearance.Options.UseFont = true;
            this.lblFixationWidthVal.Appearance.Options.UseForeColor = true;
            this.lblFixationWidthVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblFixationWidthVal, "lblFixationWidthVal");
            this.lblFixationWidthVal.Name = "lblFixationWidthVal";
            // 
            // lblFixationWidth
            // 
            this.lblFixationWidth.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblFixationWidth.Appearance.Font")));
            this.lblFixationWidth.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblFixationWidth.Appearance.ForeColor")));
            this.lblFixationWidth.Appearance.Options.UseFont = true;
            this.lblFixationWidth.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblFixationWidth, "lblFixationWidth");
            this.lblFixationWidth.Name = "lblFixationWidth";
            // 
            // lblNumOfWordsVal
            // 
            this.lblNumOfWordsVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfWordsVal.Appearance.Font")));
            this.lblNumOfWordsVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfWordsVal.Appearance.ForeColor")));
            this.lblNumOfWordsVal.Appearance.Options.UseFont = true;
            this.lblNumOfWordsVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfWordsVal, "lblNumOfWordsVal");
            this.lblNumOfWordsVal.Name = "lblNumOfWordsVal";
            // 
            // lblNumOfLinesVal
            // 
            this.lblNumOfLinesVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfLinesVal.Appearance.Font")));
            this.lblNumOfLinesVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfLinesVal.Appearance.ForeColor")));
            this.lblNumOfLinesVal.Appearance.Options.UseFont = true;
            this.lblNumOfLinesVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfLinesVal, "lblNumOfLinesVal");
            this.lblNumOfLinesVal.Name = "lblNumOfLinesVal";
            // 
            // lblNumOfWords
            // 
            this.lblNumOfWords.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfWords.Appearance.Font")));
            this.lblNumOfWords.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfWords.Appearance.ForeColor")));
            this.lblNumOfWords.Appearance.Options.UseFont = true;
            this.lblNumOfWords.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfWords, "lblNumOfWords");
            this.lblNumOfWords.Name = "lblNumOfWords";
            // 
            // lblNumOfLines
            // 
            this.lblNumOfLines.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblNumOfLines.Appearance.Font")));
            this.lblNumOfLines.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblNumOfLines.Appearance.ForeColor")));
            this.lblNumOfLines.Appearance.Options.UseFont = true;
            this.lblNumOfLines.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblNumOfLines, "lblNumOfLines");
            this.lblNumOfLines.Name = "lblNumOfLines";
            // 
            // lblQuestionResultVal
            // 
            this.lblQuestionResultVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblQuestionResultVal.Appearance.Font")));
            this.lblQuestionResultVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblQuestionResultVal.Appearance.ForeColor")));
            this.lblQuestionResultVal.Appearance.Options.UseFont = true;
            this.lblQuestionResultVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblQuestionResultVal, "lblQuestionResultVal");
            this.lblQuestionResultVal.Name = "lblQuestionResultVal";
            // 
            // lblQuestionResult
            // 
            this.lblQuestionResult.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblQuestionResult.Appearance.Font")));
            this.lblQuestionResult.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblQuestionResult.Appearance.ForeColor")));
            this.lblQuestionResult.Appearance.Options.UseFont = true;
            this.lblQuestionResult.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblQuestionResult, "lblQuestionResult");
            this.lblQuestionResult.Name = "lblQuestionResult";
            // 
            // lblTextLevelVal
            // 
            this.lblTextLevelVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTextLevelVal.Appearance.Font")));
            this.lblTextLevelVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTextLevelVal.Appearance.ForeColor")));
            this.lblTextLevelVal.Appearance.Options.UseFont = true;
            this.lblTextLevelVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblTextLevelVal, "lblTextLevelVal");
            this.lblTextLevelVal.Name = "lblTextLevelVal";
            // 
            // lblTextLevel
            // 
            this.lblTextLevel.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTextLevel.Appearance.Font")));
            this.lblTextLevel.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTextLevel.Appearance.ForeColor")));
            this.lblTextLevel.Appearance.Options.UseFont = true;
            this.lblTextLevel.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblTextLevel, "lblTextLevel");
            this.lblTextLevel.Name = "lblTextLevel";
            // 
            // lblTextTitleVal
            // 
            this.lblTextTitleVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTextTitleVal.Appearance.Font")));
            this.lblTextTitleVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTextTitleVal.Appearance.ForeColor")));
            this.lblTextTitleVal.Appearance.Options.UseFont = true;
            this.lblTextTitleVal.Appearance.Options.UseForeColor = true;
            this.lblTextTitleVal.AutoEllipsis = true;
            resources.ApplyResources(this.lblTextTitleVal, "lblTextTitleVal");
            this.lblTextTitleVal.Name = "lblTextTitleVal";
            // 
            // lblTextTitle
            // 
            this.lblTextTitle.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTextTitle.Appearance.Font")));
            this.lblTextTitle.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTextTitle.Appearance.ForeColor")));
            this.lblTextTitle.Appearance.Options.UseFont = true;
            this.lblTextTitle.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblTextTitle, "lblTextTitle");
            this.lblTextTitle.Name = "lblTextTitle";
            // 
            // lblEndTimeVal
            // 
            this.lblEndTimeVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblEndTimeVal.Appearance.Font")));
            this.lblEndTimeVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblEndTimeVal.Appearance.ForeColor")));
            this.lblEndTimeVal.Appearance.Options.UseFont = true;
            this.lblEndTimeVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblEndTimeVal, "lblEndTimeVal");
            this.lblEndTimeVal.Name = "lblEndTimeVal";
            // 
            // lblEndTime
            // 
            this.lblEndTime.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblEndTime.Appearance.Font")));
            this.lblEndTime.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblEndTime.Appearance.ForeColor")));
            this.lblEndTime.Appearance.Options.UseFont = true;
            this.lblEndTime.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblEndTime, "lblEndTime");
            this.lblEndTime.Name = "lblEndTime";
            // 
            // lblGradeVal
            // 
            this.lblGradeVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblGradeVal.Appearance.Font")));
            this.lblGradeVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblGradeVal.Appearance.ForeColor")));
            this.lblGradeVal.Appearance.Options.UseFont = true;
            this.lblGradeVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblGradeVal, "lblGradeVal");
            this.lblGradeVal.Name = "lblGradeVal";
            // 
            // lblStartTimeVal
            // 
            this.lblStartTimeVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblStartTimeVal.Appearance.Font")));
            this.lblStartTimeVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblStartTimeVal.Appearance.ForeColor")));
            this.lblStartTimeVal.Appearance.Options.UseFont = true;
            this.lblStartTimeVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblStartTimeVal, "lblStartTimeVal");
            this.lblStartTimeVal.Name = "lblStartTimeVal";
            // 
            // lblTrainerVal
            // 
            this.lblTrainerVal.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTrainerVal.Appearance.Font")));
            this.lblTrainerVal.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTrainerVal.Appearance.ForeColor")));
            this.lblTrainerVal.Appearance.Options.UseFont = true;
            this.lblTrainerVal.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblTrainerVal, "lblTrainerVal");
            this.lblTrainerVal.Name = "lblTrainerVal";
            // 
            // lblTrainer
            // 
            this.lblTrainer.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTrainer.Appearance.Font")));
            this.lblTrainer.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTrainer.Appearance.ForeColor")));
            this.lblTrainer.Appearance.Options.UseFont = true;
            this.lblTrainer.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblTrainer, "lblTrainer");
            this.lblTrainer.Name = "lblTrainer";
            // 
            // lblTestGrade
            // 
            this.lblTestGrade.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTestGrade.Appearance.Font")));
            this.lblTestGrade.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblTestGrade.Appearance.ForeColor")));
            this.lblTestGrade.Appearance.Options.UseFont = true;
            this.lblTestGrade.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblTestGrade, "lblTestGrade");
            this.lblTestGrade.Name = "lblTestGrade";
            // 
            // lblStartTime
            // 
            this.lblStartTime.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblStartTime.Appearance.Font")));
            this.lblStartTime.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblStartTime.Appearance.ForeColor")));
            this.lblStartTime.Appearance.Options.UseFont = true;
            this.lblStartTime.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblStartTime, "lblStartTime");
            this.lblStartTime.Name = "lblStartTime";
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblBirthDate.Appearance.Font")));
            this.lblBirthDate.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblBirthDate.Appearance.ForeColor")));
            this.lblBirthDate.Appearance.Options.UseFont = true;
            this.lblBirthDate.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblBirthDate, "lblBirthDate");
            this.lblBirthDate.Name = "lblBirthDate";
            // 
            // lblGender
            // 
            this.lblGender.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblGender.Appearance.Font")));
            this.lblGender.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblGender.Appearance.ForeColor")));
            this.lblGender.Appearance.Options.UseFont = true;
            this.lblGender.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblGender, "lblGender");
            this.lblGender.Name = "lblGender";
            // 
            // peTrainee
            // 
            this.peTrainee.Cursor = System.Windows.Forms.Cursors.Default;
            this.peTrainee.EditValue = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.UnknownUser_256x256;
            resources.ApplyResources(this.peTrainee, "peTrainee");
            this.peTrainee.Name = "peTrainee";
            this.peTrainee.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.peTrainee.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.peTrainee.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            // 
            // lblTraineeName
            // 
            this.lblTraineeName.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblTraineeName.Appearance.Font")));
            this.lblTraineeName.Appearance.Options.UseFont = true;
            resources.ApplyResources(this.lblTraineeName, "lblTraineeName");
            this.lblTraineeName.Name = "lblTraineeName";
            // 
            // pgRecords
            // 
            this.pgRecords.Controls.Add(this.gcRecords);
            this.pgRecords.Name = "pgRecords";
            resources.ApplyResources(this.pgRecords, "pgRecords");
            // 
            // gcRecords
            // 
            resources.ApplyResources(this.gcRecords, "gcRecords");
            this.gcRecords.MainView = this.gvRecords;
            this.gcRecords.Name = "gcRecords";
            this.gcRecords.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRecords,
            this.gridView1});
            // 
            // gvRecords
            // 
            this.gvRecords.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colElapsed,
            this.colCalculatedLine,
            this.colLineNumber,
            this.colLeftX,
            this.colLeftY,
            this.colRightX,
            this.colRightY});
            this.gvRecords.GridControl = this.gcRecords;
            this.gvRecords.Name = "gvRecords";
            this.gvRecords.OptionsBehavior.Editable = false;
            this.gvRecords.OptionsView.ShowGroupPanel = false;
            this.gvRecords.OptionsView.ShowIndicator = false;
            this.gvRecords.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colElapsed
            // 
            resources.ApplyResources(this.colElapsed, "colElapsed");
            this.colElapsed.FieldName = "RawData.Elapsed";
            this.colElapsed.Name = "colElapsed";
            this.colElapsed.OptionsColumn.AllowEdit = false;
            this.colElapsed.OptionsColumn.FixedWidth = true;
            // 
            // colCalculatedLine
            // 
            resources.ApplyResources(this.colCalculatedLine, "colCalculatedLine");
            this.colCalculatedLine.FieldName = "CalculatedLine";
            this.colCalculatedLine.Name = "colCalculatedLine";
            this.colCalculatedLine.OptionsColumn.AllowEdit = false;
            this.colCalculatedLine.OptionsColumn.FixedWidth = true;
            // 
            // colLineNumber
            // 
            resources.ApplyResources(this.colLineNumber, "colLineNumber");
            this.colLineNumber.FieldName = "LineNumber";
            this.colLineNumber.Name = "colLineNumber";
            this.colLineNumber.OptionsColumn.AllowEdit = false;
            this.colLineNumber.OptionsColumn.FixedWidth = true;
            // 
            // colLeftX
            // 
            resources.ApplyResources(this.colLeftX, "colLeftX");
            this.colLeftX.FieldName = "RawData.LeftX";
            this.colLeftX.Name = "colLeftX";
            // 
            // colLeftY
            // 
            resources.ApplyResources(this.colLeftY, "colLeftY");
            this.colLeftY.FieldName = "RawData.LeftY";
            this.colLeftY.Name = "colLeftY";
            // 
            // colRightX
            // 
            resources.ApplyResources(this.colRightX, "colRightX");
            this.colRightX.FieldName = "RawData.RightX";
            this.colRightX.Name = "colRightX";
            // 
            // colRightY
            // 
            resources.ApplyResources(this.colRightY, "colRightY");
            this.colRightY.FieldName = "RawData.RightY";
            this.colRightY.Name = "colRightY";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcRecords;
            this.gridView1.Name = "gridView1";
            // 
            // pgChart
            // 
            this.pgChart.Controls.Add(this.chartRecords);
            this.pgChart.Name = "pgChart";
            resources.ApplyResources(this.pgChart, "pgChart");
            // 
            // chartRecords
            // 
            this.chartRecords.DataBindings = null;
            resources.ApplyResources(this.chartRecords, "chartRecords");
            this.chartRecords.Legend.Name = "Default Legend";
            this.chartRecords.Name = "chartRecords";
            this.chartRecords.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.StyleController = this.lcRoot;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lcgRoot
            // 
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciOk,
            this.emptySpaceItem2,
            this.lciTab,
            this.lciPrint});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Size = new System.Drawing.Size(884, 561);
            this.lcgRoot.TextVisible = false;
            // 
            // lciOk
            // 
            this.lciOk.Control = this.btnOk;
            this.lciOk.Location = new System.Drawing.Point(760, 515);
            this.lciOk.MaxSize = new System.Drawing.Size(0, 26);
            this.lciOk.MinSize = new System.Drawing.Size(37, 26);
            this.lciOk.Name = "lciOk";
            this.lciOk.Size = new System.Drawing.Size(104, 26);
            this.lciOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOk.TextSize = new System.Drawing.Size(0, 0);
            this.lciOk.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 515);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(656, 26);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciTab
            // 
            this.lciTab.Control = this.tabResult;
            this.lciTab.Location = new System.Drawing.Point(0, 0);
            this.lciTab.Name = "lciTab";
            this.lciTab.Size = new System.Drawing.Size(864, 515);
            this.lciTab.TextSize = new System.Drawing.Size(0, 0);
            this.lciTab.TextVisible = false;
            // 
            // lciPrint
            // 
            this.lciPrint.Control = this.btnPrint;
            this.lciPrint.Location = new System.Drawing.Point(656, 515);
            this.lciPrint.MaxSize = new System.Drawing.Size(0, 26);
            this.lciPrint.MinSize = new System.Drawing.Size(46, 26);
            this.lciPrint.Name = "lciPrint";
            this.lciPrint.Size = new System.Drawing.Size(104, 26);
            this.lciPrint.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciPrint.TextSize = new System.Drawing.Size(0, 0);
            this.lciPrint.TextVisible = false;
            // 
            // TestResultForm
            // 
            this.Appearance.Options.UseFont = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcRoot);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "TestResultForm";
            ((System.ComponentModel.ISupportInitialize)(this.lcRoot)).EndInit();
            this.lcRoot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabResult)).EndInit();
            this.tabResult.ResumeLayout(false);
            this.pgSummary.ResumeLayout(false);
            this.pgSummary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peTrainee.Properties)).EndInit();
            this.pgRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.pgChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciPrint)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraLayout.LayoutControl lcRoot;
        private DevExpress.XtraTab.XtraTabControl tabResult;
        private DevExpress.XtraTab.XtraTabPage pgSummary;
        private DevExpress.XtraEditors.LabelControl lblNumOfAnalyzedLinesVal;
        private DevExpress.XtraEditors.LabelControl lblNumOfAnalyzedLines;
        private DevExpress.XtraEditors.LabelControl lblCrossCorrelationVal;
        private DevExpress.XtraEditors.LabelControl lblCrossCorrelation;
        private DevExpress.XtraEditors.LabelControl lblFixationDurationVal;
        private DevExpress.XtraEditors.LabelControl lblFixationDuration;
        private DevExpress.XtraEditors.LabelControl lblNumOfRegressionsVal;
        private DevExpress.XtraEditors.LabelControl lblNumOfRegressions;
        private DevExpress.XtraEditors.LabelControl lblNumOfFixationsVal;
        private DevExpress.XtraEditors.LabelControl lblNumOfFixations;
        private DevExpress.XtraEditors.LabelControl lblReadingRateVal;
        private DevExpress.XtraEditors.LabelControl lblReadingRate;
        private DevExpress.XtraEditors.LabelControl lblFixationWidthVal;
        private DevExpress.XtraEditors.LabelControl lblFixationWidth;
        private DevExpress.XtraEditors.LabelControl lblNumOfWordsVal;
        private DevExpress.XtraEditors.LabelControl lblNumOfLinesVal;
        private DevExpress.XtraEditors.LabelControl lblNumOfWords;
        private DevExpress.XtraEditors.LabelControl lblNumOfLines;
        private DevExpress.XtraEditors.LabelControl lblQuestionResultVal;
        private DevExpress.XtraEditors.LabelControl lblQuestionResult;
        private DevExpress.XtraEditors.LabelControl lblTextLevelVal;
        private DevExpress.XtraEditors.LabelControl lblTextLevel;
        private DevExpress.XtraEditors.LabelControl lblTextTitleVal;
        private DevExpress.XtraEditors.LabelControl lblTextTitle;
        private DevExpress.XtraEditors.LabelControl lblEndTimeVal;
        private DevExpress.XtraEditors.LabelControl lblEndTime;
        private DevExpress.XtraEditors.LabelControl lblGradeVal;
        private DevExpress.XtraEditors.LabelControl lblStartTimeVal;
        private DevExpress.XtraEditors.LabelControl lblTrainerVal;
        private DevExpress.XtraEditors.LabelControl lblTrainer;
        private DevExpress.XtraEditors.LabelControl lblTestGrade;
        private DevExpress.XtraEditors.LabelControl lblStartTime;
        private DevExpress.XtraEditors.LabelControl lblBirthDate;
        private DevExpress.XtraEditors.LabelControl lblGender;
        private DevExpress.XtraEditors.PictureEdit peTrainee;
        private DevExpress.XtraEditors.LabelControl lblTraineeName;
        private DevExpress.XtraTab.XtraTabPage pgRecords;
        private DevExpress.XtraGrid.GridControl gcRecords;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRecords;
        private DevExpress.XtraGrid.Columns.GridColumn colElapsed;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculatedLine;
        private DevExpress.XtraGrid.Columns.GridColumn colLineNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLeftX;
        private DevExpress.XtraGrid.Columns.GridColumn colLeftY;
        private DevExpress.XtraGrid.Columns.GridColumn colRightX;
        private DevExpress.XtraGrid.Columns.GridColumn colRightY;
        private DevExpress.XtraTab.XtraTabPage pgChart;
        private DevExpress.XtraCharts.ChartControl chartRecords;
        private DevExpress.XtraEditors.SimpleButton btnOk;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraLayout.LayoutControlItem lciOk;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lciTab;
        private DevExpress.XtraLayout.LayoutControlItem lciPrint;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}