﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnWrapper = new System.Windows.Forms.Panel();
            this.pnCopyright = new System.Windows.Forms.Panel();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.lblReserved = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.pnProduct = new System.Windows.Forms.TableLayoutPanel();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pnWrapper.SuspendLayout();
            this.pnCopyright.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.pnProduct.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnWrapper.Controls.Add(this.pnCopyright);
            this.pnWrapper.Controls.Add(this.pnProduct);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Margin = new System.Windows.Forms.Padding(0);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.Padding = new System.Windows.Forms.Padding(15);
            this.pnWrapper.Size = new System.Drawing.Size(544, 380);
            this.pnWrapper.TabIndex = 0;
            // 
            // pnCopyright
            // 
            this.pnCopyright.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(238)))), ((int)(((byte)(238)))));
            this.pnCopyright.Controls.Add(this.pbLogo);
            this.pnCopyright.Controls.Add(this.lblReserved);
            this.pnCopyright.Controls.Add(this.lblCopyright);
            this.pnCopyright.Location = new System.Drawing.Point(16, 321);
            this.pnCopyright.Name = "pnCopyright";
            this.pnCopyright.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.pnCopyright.Size = new System.Drawing.Size(512, 50);
            this.pnCopyright.TabIndex = 3;
            // 
            // pbLogo
            // 
            this.pbLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbLogo.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.CompanyLogo;
            this.pbLogo.Location = new System.Drawing.Point(406, 10);
            this.pbLogo.Margin = new System.Windows.Forms.Padding(0);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(91, 32);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 2;
            this.pbLogo.TabStop = false;
            // 
            // lblReserved
            // 
            this.lblReserved.AutoSize = true;
            this.lblReserved.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReserved.ForeColor = System.Drawing.Color.Black;
            this.lblReserved.Location = new System.Drawing.Point(15, 25);
            this.lblReserved.Margin = new System.Windows.Forms.Padding(0);
            this.lblReserved.Name = "lblReserved";
            this.lblReserved.Size = new System.Drawing.Size(99, 15);
            this.lblReserved.TabIndex = 4;
            this.lblReserved.Text = "All rights reserved";
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Font = new System.Drawing.Font("Open Sans", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopyright.ForeColor = System.Drawing.Color.Black;
            this.lblCopyright.Location = new System.Drawing.Point(15, 10);
            this.lblCopyright.Margin = new System.Windows.Forms.Padding(0);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(170, 15);
            this.lblCopyright.TabIndex = 3;
            this.lblCopyright.Text = "Copyright © 2017 Hospi Co. Ltd";
            // 
            // pnProduct
            // 
            this.pnProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(148)))), ((int)(((byte)(30)))));
            this.pnProduct.ColumnCount = 2;
            this.pnProduct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnProduct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnProduct.Controls.Add(this.lblDescription, 0, 2);
            this.pnProduct.Controls.Add(this.lblName, 0, 1);
            this.pnProduct.Controls.Add(this.lblVersion, 1, 2);
            this.pnProduct.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnProduct.Location = new System.Drawing.Point(15, 15);
            this.pnProduct.Name = "pnProduct";
            this.pnProduct.Padding = new System.Windows.Forms.Padding(15, 0, 15, 0);
            this.pnProduct.RowCount = 3;
            this.pnProduct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnProduct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.pnProduct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.pnProduct.Size = new System.Drawing.Size(512, 194);
            this.pnProduct.TabIndex = 2;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Open Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.ForeColor = System.Drawing.Color.White;
            this.lblDescription.Location = new System.Drawing.Point(18, 144);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(216, 17);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Reading Ability Measurement System";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.pnProduct.SetColumnSpan(this.lblName, 2);
            this.lblName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblName.Font = new System.Drawing.Font("Open Sans Light", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(18, 94);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(476, 50);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Read Sky";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Open Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.White;
            this.lblVersion.Location = new System.Drawing.Point(469, 144);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(25, 17);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "1.0";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AboutForm
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 380);
            this.Controls.Add(this.pnWrapper);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AboutForm";
            this.Text = "AboutForm";
            this.pnWrapper.ResumeLayout(false);
            this.pnCopyright.ResumeLayout(false);
            this.pnCopyright.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.pnProduct.ResumeLayout(false);
            this.pnProduct.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnWrapper;
        private System.Windows.Forms.Panel pnCopyright;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lblReserved;
        private System.Windows.Forms.Label lblCopyright;
        private System.Windows.Forms.TableLayoutPanel pnProduct;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblVersion;
    }
}