﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class EditTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTestForm));
            this.lcNewTest = new DevExpress.XtraLayout.LayoutControl();
            this.leText = new DevExpress.XtraEditors.LookUpEdit();
            this.leTrainee = new DevExpress.XtraEditors.LookUpEdit();
            this.leTrainer = new DevExpress.XtraEditors.LookUpEdit();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.cbGrade = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lcgNewTest = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciOk = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciCancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciGrade = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTrainer = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTrainee = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciText = new DevExpress.XtraLayout.LayoutControlItem();
            this.vpTest = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.lcNewTest)).BeginInit();
            this.lcNewTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leTrainee.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leTrainer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgNewTest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vpTest)).BeginInit();
            this.SuspendLayout();
            // 
            // lcNewTest
            // 
            this.lcNewTest.AllowCustomization = false;
            this.lcNewTest.Controls.Add(this.leText);
            this.lcNewTest.Controls.Add(this.leTrainee);
            this.lcNewTest.Controls.Add(this.leTrainer);
            this.lcNewTest.Controls.Add(this.cbLevel);
            this.lcNewTest.Controls.Add(this.cbGrade);
            this.lcNewTest.Controls.Add(this.btnOk);
            this.lcNewTest.Controls.Add(this.btnCancel);
            resources.ApplyResources(this.lcNewTest, "lcNewTest");
            this.lcNewTest.Name = "lcNewTest";
            this.lcNewTest.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(55, 152, 450, 400);
            this.lcNewTest.Root = this.lcgNewTest;
            // 
            // leText
            // 
            resources.ApplyResources(this.leText, "leText");
            this.leText.Name = "leText";
            this.leText.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("leText.Properties.Buttons"))))});
            this.leText.Properties.NullText = resources.GetString("leText.Properties.NullText");
            this.leText.StyleController = this.lcNewTest;
            // 
            // leTrainee
            // 
            resources.ApplyResources(this.leTrainee, "leTrainee");
            this.leTrainee.Name = "leTrainee";
            this.leTrainee.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("leTrainee.Properties.Buttons"))))});
            this.leTrainee.Properties.NullText = resources.GetString("leTrainee.Properties.NullText");
            this.leTrainee.StyleController = this.lcNewTest;
            // 
            // leTrainer
            // 
            resources.ApplyResources(this.leTrainer, "leTrainer");
            this.leTrainer.Name = "leTrainer";
            this.leTrainer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("leTrainer.Properties.Buttons"))))});
            this.leTrainer.Properties.NullText = resources.GetString("leTrainer.Properties.NullText");
            this.leTrainer.StyleController = this.lcNewTest;
            // 
            // cbLevel
            // 
            this.cbLevel.FormattingEnabled = true;
            resources.ApplyResources(this.cbLevel, "cbLevel");
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.SelectedValueChanged += new System.EventHandler(this.cbLevel_SelectedValueChanged);
            // 
            // cbGrade
            // 
            this.cbGrade.FormattingEnabled = true;
            resources.ApplyResources(this.cbGrade, "cbGrade");
            this.cbGrade.Name = "cbGrade";
            this.cbGrade.SelectedValueChanged += new System.EventHandler(this.cbGrade_SelectedValueChanged);
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lcgNewTest
            // 
            this.lcgNewTest.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgNewTest.GroupBordersVisible = false;
            this.lcgNewTest.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.lciOk,
            this.lciCancel,
            this.emptySpaceItem2,
            this.lciGrade,
            this.lciLevel,
            this.lciTrainer,
            this.lciTrainee,
            this.lciText});
            this.lcgNewTest.Location = new System.Drawing.Point(0, 0);
            this.lcgNewTest.Name = "Root";
            this.lcgNewTest.Size = new System.Drawing.Size(484, 201);
            this.lcgNewTest.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 122);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(464, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciOk
            // 
            this.lciOk.Control = this.btnOk;
            this.lciOk.Location = new System.Drawing.Point(270, 132);
            this.lciOk.MaxSize = new System.Drawing.Size(97, 30);
            this.lciOk.MinSize = new System.Drawing.Size(97, 30);
            this.lciOk.Name = "lciOk";
            this.lciOk.Size = new System.Drawing.Size(97, 49);
            this.lciOk.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciOk.TextSize = new System.Drawing.Size(0, 0);
            this.lciOk.TextVisible = false;
            // 
            // lciCancel
            // 
            this.lciCancel.Control = this.btnCancel;
            this.lciCancel.Location = new System.Drawing.Point(367, 132);
            this.lciCancel.MaxSize = new System.Drawing.Size(97, 30);
            this.lciCancel.MinSize = new System.Drawing.Size(97, 30);
            this.lciCancel.Name = "lciCancel";
            this.lciCancel.Size = new System.Drawing.Size(97, 49);
            this.lciCancel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciCancel.TextSize = new System.Drawing.Size(0, 0);
            this.lciCancel.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 132);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(270, 49);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciGrade
            // 
            this.lciGrade.Control = this.cbGrade;
            this.lciGrade.Location = new System.Drawing.Point(0, 48);
            this.lciGrade.Name = "lciGrade";
            this.lciGrade.Size = new System.Drawing.Size(464, 25);
            resources.ApplyResources(this.lciGrade, "lciGrade");
            this.lciGrade.TextSize = new System.Drawing.Size(45, 14);
            // 
            // lciLevel
            // 
            this.lciLevel.Control = this.cbLevel;
            this.lciLevel.Location = new System.Drawing.Point(0, 73);
            this.lciLevel.Name = "lciLevel";
            this.lciLevel.Size = new System.Drawing.Size(464, 25);
            resources.ApplyResources(this.lciLevel, "lciLevel");
            this.lciLevel.TextSize = new System.Drawing.Size(45, 14);
            // 
            // lciTrainer
            // 
            this.lciTrainer.Control = this.leTrainer;
            this.lciTrainer.Location = new System.Drawing.Point(0, 0);
            this.lciTrainer.Name = "lciTrainer";
            this.lciTrainer.Size = new System.Drawing.Size(464, 24);
            resources.ApplyResources(this.lciTrainer, "lciTrainer");
            this.lciTrainer.TextSize = new System.Drawing.Size(45, 14);
            // 
            // lciTrainee
            // 
            this.lciTrainee.Control = this.leTrainee;
            this.lciTrainee.Location = new System.Drawing.Point(0, 24);
            this.lciTrainee.Name = "lciTrainee";
            this.lciTrainee.Size = new System.Drawing.Size(464, 24);
            resources.ApplyResources(this.lciTrainee, "lciTrainee");
            this.lciTrainee.TextSize = new System.Drawing.Size(45, 14);
            // 
            // lciText
            // 
            this.lciText.Control = this.leText;
            this.lciText.Location = new System.Drawing.Point(0, 98);
            this.lciText.Name = "lciText";
            this.lciText.Size = new System.Drawing.Size(464, 24);
            resources.ApplyResources(this.lciText, "lciText");
            this.lciText.TextSize = new System.Drawing.Size(45, 14);
            // 
            // vpTest
            // 
            this.vpTest.ValidationMode = DevExpress.XtraEditors.DXErrorProvider.ValidationMode.Manual;
            // 
            // EditTestForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcNewTest);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.Name = "EditTestForm";
            ((System.ComponentModel.ISupportInitialize)(this.lcNewTest)).EndInit();
            this.lcNewTest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leTrainee.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leTrainer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgNewTest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vpTest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcNewTest;
        private DevExpress.XtraLayout.LayoutControlGroup lcgNewTest;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.Button btnCancel;
        private DevExpress.XtraLayout.LayoutControlItem lciCancel;
        private System.Windows.Forms.Button btnOk;
        private DevExpress.XtraLayout.LayoutControlItem lciOk;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.ComboBox cbLevel;
        private System.Windows.Forms.ComboBox cbGrade;
        private DevExpress.XtraLayout.LayoutControlItem lciGrade;
        private DevExpress.XtraLayout.LayoutControlItem lciLevel;
        private DevExpress.XtraEditors.LookUpEdit leTrainer;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainer;
        private DevExpress.XtraEditors.LookUpEdit leTrainee;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainee;
        private DevExpress.XtraEditors.LookUpEdit leText;
        private DevExpress.XtraLayout.LayoutControlItem lciText;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider vpTest;
    }
}