﻿using System.Windows.Forms;
using DevExpress.XtraEditors;
using log4net;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class EditTestForm : XtraForm
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        private Data.Test test;
        #endregion

        #region PROPERTIES
        #endregion

        #region CONSTRUCTORS
        public EditTestForm(Data.Test test)
        {
            InitializeComponent();

            this.test = test;
            InitializeControls();
        }
        #endregion

        #region GENERAL METHODS
        private void InitializeControls()
        {
            EditHelper.InitTrainerLookupEdit(leTrainer.Properties);
            EditHelper.InitTraineeLookupEdit(leTrainee.Properties);
            EditHelper.InitTextLookupEdit(leText.Properties);
            EditHelper.InitGradeComboBox(cbGrade);
            EditHelper.InitLevelComboBox(cbLevel);

            vpTest.SetValidationRule(leTrainer, ValidationRulesHelper.RuleIsNotBlank);
            vpTest.SetValidationRule(leTrainee, ValidationRulesHelper.RuleIsNotBlank);
            vpTest.SetValidationRule(leText, ValidationRulesHelper.RuleIsNotBlank);
        }
        #endregion

        #region OVERRIDE MTHODS
        #endregion

        #region CONTROL EVENTS
        private void cbGrade_SelectedValueChanged(object sender, System.EventArgs e)
        {
            if (cbGrade.SelectedIndex == 0) return;

            int grade = 0;
            if (int.TryParse(cbGrade.SelectedValue.ToString(), out grade))
                cbLevel.SelectedValue = grade;
            else
                log.Error($"Conversion error at cbGrade_SelectedValueChanged@{this.GetType().Name}.");
        }

        private void cbLevel_SelectedValueChanged(object sender, System.EventArgs e)
        {
            if (cbLevel.SelectedIndex == 0) return;

            int level = 0;
            if (int.TryParse(cbLevel.SelectedValue.ToString(), out level))
            {
                leText.Properties.DataSource =
                    EditHelper.GetTextLookupEditDataSource(level);
            }
            else
                log.Error($"Conversion error at cbLevel_SelectedValueChanged@{this.GetType().Name}.");
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {
            byte grade, level;
            int trainerId, traineeId, textId;

            if (!vpTest.Validate())
                this.DialogResult = DialogResult.None;
            else
            {
                byte.TryParse(cbGrade.SelectedValue.ToString(), out grade);
                byte.TryParse(cbLevel.SelectedValue.ToString(), out level);
                int.TryParse(leTrainer.EditValue.ToString(), out trainerId);
                int.TryParse(leTrainee.EditValue.ToString(), out traineeId);
                int.TryParse(leText.EditValue.ToString(), out textId);

                test.Grade = grade;
                test.Trainer = DataHelper.GetTrainer(trainerId);
                test.Trainee = DataHelper.GetTrainee(traineeId);
                test.TestText = DataHelper.GetText(textId);
            }
        }
        #endregion
    }
}
