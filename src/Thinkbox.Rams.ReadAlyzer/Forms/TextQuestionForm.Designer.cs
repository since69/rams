﻿namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    partial class TextQuestionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextQuestionForm));
            this.wizTextQuestion = new DevExpress.XtraWizard.WizardControl();
            this.pgWelcome = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.peIntro = new DevExpress.XtraEditors.PictureEdit();
            this.pgQuestion = new DevExpress.XtraWizard.WizardPage();
            this.rgAnswer = new DevExpress.XtraEditors.RadioGroup();
            this.lblQuestion = new DevExpress.XtraEditors.LabelControl();
            this.pgComplete = new DevExpress.XtraWizard.CompletionWizardPage();
            ((System.ComponentModel.ISupportInitialize)(this.wizTextQuestion)).BeginInit();
            this.wizTextQuestion.SuspendLayout();
            this.pgWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peIntro.Properties)).BeginInit();
            this.pgQuestion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgAnswer.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // wizTextQuestion
            // 
            this.wizTextQuestion.Controls.Add(this.pgWelcome);
            this.wizTextQuestion.Controls.Add(this.pgQuestion);
            this.wizTextQuestion.Controls.Add(this.pgComplete);
            resources.ApplyResources(this.wizTextQuestion, "wizTextQuestion");
            this.wizTextQuestion.Image = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.WizardImage;
            this.wizTextQuestion.ImageWidth = 200;
            this.wizTextQuestion.Name = "wizTextQuestion";
            this.wizTextQuestion.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.pgWelcome,
            this.pgQuestion,
            this.pgComplete});
            this.wizTextQuestion.SelectedPageChanged += new DevExpress.XtraWizard.WizardPageChangedEventHandler(this.wizTextQuestion_SelectedPageChanged);
            this.wizTextQuestion.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.wizTextQuestion_SelectedPageChanging);
            this.wizTextQuestion.CancelClick += new System.ComponentModel.CancelEventHandler(this.wizTextQuestion_CancelClick);
            this.wizTextQuestion.FinishClick += new System.ComponentModel.CancelEventHandler(this.wizTextQuestion_FinishClick);
            this.wizTextQuestion.NextClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.wizTextQuestion_NextClick);
            this.wizTextQuestion.PrevClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.wizTextQuestion_PrevClick);
            // 
            // pgWelcome
            // 
            this.pgWelcome.Controls.Add(this.peIntro);
            resources.ApplyResources(this.pgWelcome, "pgWelcome");
            this.pgWelcome.Name = "pgWelcome";
            // 
            // peIntro
            // 
            this.peIntro.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.peIntro, "peIntro");
            this.peIntro.EditValue = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.WizardIntro;
            this.peIntro.Name = "peIntro";
            this.peIntro.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("peIntro.Properties.Appearance.BackColor")));
            this.peIntro.Properties.Appearance.Options.UseBackColor = true;
            this.peIntro.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peIntro.Properties.PictureAlignment = System.Drawing.ContentAlignment.BottomRight;
            this.peIntro.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            // 
            // pgQuestion
            // 
            this.pgQuestion.Controls.Add(this.rgAnswer);
            this.pgQuestion.Controls.Add(this.lblQuestion);
            resources.ApplyResources(this.pgQuestion, "pgQuestion");
            this.pgQuestion.Name = "pgQuestion";
            // 
            // rgAnswer
            // 
            resources.ApplyResources(this.rgAnswer, "rgAnswer");
            this.rgAnswer.Name = "rgAnswer";
            this.rgAnswer.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("rgAnswer.Properties.Appearance.BackColor")));
            this.rgAnswer.Properties.Appearance.Options.UseBackColor = true;
            this.rgAnswer.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rgAnswer.Properties.Columns = 2;
            this.rgAnswer.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((object)(resources.GetObject("rgAnswer.Properties.Items"))), resources.GetString("rgAnswer.Properties.Items1")),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((object)(resources.GetObject("rgAnswer.Properties.Items2"))), resources.GetString("rgAnswer.Properties.Items3"))});
            // 
            // lblQuestion
            // 
            this.lblQuestion.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblQuestion.Appearance.Font")));
            this.lblQuestion.Appearance.Options.UseFont = true;
            resources.ApplyResources(this.lblQuestion, "lblQuestion");
            this.lblQuestion.Name = "lblQuestion";
            // 
            // pgComplete
            // 
            resources.ApplyResources(this.pgComplete, "pgComplete");
            this.pgComplete.Name = "pgComplete";
            // 
            // TextQuestionForm
            // 
            this.Appearance.Options.UseFont = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.wizTextQuestion);
            this.Name = "TextQuestionForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TextQuestionForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.wizTextQuestion)).EndInit();
            this.wizTextQuestion.ResumeLayout(false);
            this.pgWelcome.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.peIntro.Properties)).EndInit();
            this.pgQuestion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgAnswer.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl wizTextQuestion;
        private DevExpress.XtraWizard.WelcomeWizardPage pgWelcome;
        private DevExpress.XtraWizard.WizardPage pgQuestion;
        private DevExpress.XtraWizard.CompletionWizardPage pgComplete;
        private DevExpress.XtraEditors.RadioGroup rgAnswer;
        private DevExpress.XtraEditors.LabelControl lblQuestion;
        private DevExpress.XtraEditors.PictureEdit peIntro;
    }
}