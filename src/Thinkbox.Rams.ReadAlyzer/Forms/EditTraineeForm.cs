﻿using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraEditors;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class EditTraineeForm : XtraForm
    {
        #region VARIABLES
        private Trainee trainee;
        private Trainee bindingTrainee;
        #endregion

        #region CONSTRUCTORS
        public EditTraineeForm(Trainee trainee, IDXMenuManager menuManager)
        {
            InitializeComponent();

            this.trainee = trainee;
            this.bindingTrainee = trainee.Clone();

            InitializeControls(bindingTrainee, menuManager);
        }
        #endregion

        #region METHODS
        private void InitializeControls(Trainee trainee, IDXMenuManager menuManager)
        {
            EditHelper.InitGenderComboBox(cbGender.Properties);
            EditHelper.InitNameStyleComboBox(cbNameStyle);

            // Intialize menu manger
            foreach (Control control in lcEditTrainee.Controls)
            {
                BaseEdit edit = control as BaseEdit;
                if (edit != null) edit.MenuManager = menuManager;
            }

            peTrainee.Image = Properties.Resources.UnknownUser_256x256;
            cbNameStyle.SelectedValue = trainee.Person.NameStyle;
            teFirstName.Text = trainee.Person.FirstName;
            teLastName.Text = trainee.Person.LastName;
            cbGender.EditValue = trainee.Gender;
            deBirthDate.DateTime = trainee.BirthDate;
        }
        #endregion

        #region CONTROL EVENTS
        private void btnOk_Click(object sender, System.EventArgs e)
        {
            bindingTrainee.Person.NameStyle = (bool)cbNameStyle.SelectedValue;
            bindingTrainee.Person.FirstName = teFirstName.Text;
            bindingTrainee.Person.LastName = teLastName.Text;
            bindingTrainee.Gender = cbGender.EditValue.ToString();
            bindingTrainee.BirthDate = deBirthDate.DateTime;
            trainee.Assign(bindingTrainee);
        }
        #endregion
    }
}
