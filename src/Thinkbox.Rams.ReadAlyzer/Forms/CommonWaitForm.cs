﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraWaitForm;

namespace Thinkbox.Rams.ReadAlyzer.Forms
{
    public partial class CommonWaitForm : WaitForm
    {
        #region CONSTRUCTORS
        public CommonWaitForm()
        {
            InitializeComponent();
            this.progressPanel1.AutoHeight = true;
        }
        #endregion

        #region OVERRIDE METHODS
        public override void SetCaption(string caption)
        {
            base.SetCaption(caption);
            this.progressPanel1.Caption = caption;
        }
        public override void SetDescription(string description)
        {
            base.SetDescription(description);
            this.progressPanel1.Description = description;
        }
        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }
        #endregion

        public enum WaitFormCommand
        {
        }
    }
}