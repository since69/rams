﻿namespace Thinkbox.Rams.ReadAlyzer
{
    /// <summary>
    /// Tag strings for ribbon buttonbar item
    /// </summary>
    public class Tags
    {
        //
        // BackstageViewControls
        //

        public const string MenuSaveAs = "SaveAs";

        //
        // BarButtonItems
        //

        public const string TestNew = "NewTest";
        public const string TestStart = "StartTest";
        public const string TestStop = "StopTest";
        public const string TestAnalysis = "AnalysisTest";
        public const string TestCancel = "CancelTest";

        public const string RecordEdit = "EditRecord";
        public const string RecordDelete = "DeleteRecord";
        public const string RecordAnalysis = "AnalysisRecord";
        public const string RecordSimulate = "SimulateRecord";
        public const string RecordPrint = "PrintRecord";
        public const string RecordList = "ListRecord";
        public const string RecordListParticipant = "ListRecordTrainee";

        public const string TraineeNew = "NewTrainee";
        public const string TraineeEdit = "EditTrainee";
        public const string TraineeDelete = "DeleteTrainee";
        public const string TraineeList = "ListTrainee";
        
        public const string TrainerNew = "NewTrainer";
        public const string TrainerEdit = "EditTrainer";
        public const string TrainerDelete = "DeleteTrainer";
        public const string TrainerList = "ListTrainer";

        public const string TextList = "ListText";
    }
}
