﻿//
// Thinkbox.Rams.ReadAlyzer.Modules.BaseModule
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Thinkbox Co. Ltd
//

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Menu;
using DevExpress.XtraGrid;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraPrinting;
using DevExpress.XtraRichEdit;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public class BaseModule : BaseControl
    {
        #region VARIABLES
        #endregion

        #region PROPERTIES
        protected string partName = string.Empty;
        public string PartName
        {
            get { return partName; }
        }

        public virtual string ModuleName
        {
            get { return this.GetType().Name; }
        }

        public virtual bool AllowRtfTitle { get { return true; } }

        public virtual IPrintable ExportComponent { get { return Grid; } }

        public virtual IPrintable PrintableComponent { get { return Grid; } }

        public virtual float ZoomFactor
        {
            get { return 1; }
            set { }
        }

        protected virtual GridControl Grid { get { return null; } }

        protected internal virtual RichEditControl CurrentRichEdit { get { return null; } }

        protected virtual bool AllowZoomControl { get { return false; } }

        protected virtual bool SaveAsEnable { get { return false; } }

        protected virtual bool SaveAttachmentEnable { get { return false; } }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        protected virtual RibbonStatusBar ChildRibbonStatusBar
        {
            get
            {
                if (ChildRibbon != null)
                    return ChildRibbon.StatusBar;
                return null;
            }
        }
        
        [DefaultValue(false)]
        protected virtual bool AutoMergeRibbon { get; private set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        protected virtual RibbonControl ChildRibbon
        {
            get
            {
                if (!AutoMergeRibbon) return null;
                return FindRibbon(Controls);
            }
        }

        protected RibbonControl MainRibbon
        {
            get { return OwnerForm.Ribbon; }
        }

        internal MainForm OwnerForm
        {
            get { return this.FindForm() as MainForm; }
        }
        #endregion

        #region CONSTRUCTORS
        public BaseModule() { }
        #endregion

        #region METHODS
        //
        // Related to RibbonStatusBar.
        // 

        internal void ShowReminder()
        {
            if (OwnerForm != null)
                OwnerForm.ShowReminder();
        }

        internal void ShowInfo(ColumnView view)
        {
            if (OwnerForm == null) return;
            ShowReminder();
            OwnerForm.ShowInfo(view.DataRowCount);
        }

        internal void ShowInfo()
        {
            if (OwnerForm == null) return;

            if (Grid == null)
            {
                OwnerForm.ShowInfo(null);
                return;
            }

            ICollection list = Grid.DataSource as ICollection;
            if (list == null)
                OwnerForm.ShowInfo(null);
            else
                OwnerForm.ShowInfo(list.Count);
        }

        //
        // Related to RibbonControl.
        //

        internal RibbonControl FindRibbon(ControlCollection controls)
        {
            RibbonControl ribbon = controls.OfType<Control>()
                .FirstOrDefault(x => x is RibbonControl) as RibbonControl;
            if (ribbon != null) return ribbon;

            foreach (Control control in controls)
            {
                if (control.HasChildren)
                {
                    ribbon = FindRibbon(control.Controls);
                    if (ribbon != null) return ribbon;
                }
            }

            return null;
        }

        internal void CapitializeChildRibbonPages()
        {
            if (ChildRibbon == null) return;

            foreach (RibbonPage page in ChildRibbon.Pages)
                page.Text = page.Text.ToUpper();

            foreach (RibbonPageCategory category in ChildRibbon.PageCategories)
            {
                foreach (RibbonPage page in category.Pages)
                    page.Text = page.Text.ToUpper();
            }
        }

        //
        // Common methods
        //

        internal void SetMenuManager(ControlCollection controls, 
            IDXMenuManager manager)
        {
            foreach (Control control in controls)
            {
                GridControl grid = control as GridControl;
                if (grid != null)
                {
                    grid.MenuManager = manager;
                    break;
                }
                PivotGridControl pivot = control as PivotGridControl;
                if (pivot != null)
                {
                    pivot.MenuManager = manager;
                    break;
                }
                BaseEdit edit = control as BaseEdit;
                if (edit != null)
                {
                    edit.MenuManager = manager;
                    break;
                }
                SetMenuManager(control.Controls, manager);
            }
        }

        internal virtual void FocusObject(object o) { }

        internal virtual void HideModule()
        {
            if (AutoMergeRibbon && OwnerForm != null)
            {
                if (OwnerForm.Ribbon.MergedRibbon == ChildRibbon)
                {
                    RibbonPage page = OwnerForm.Ribbon.MergedPages.GetPageByText("VIEW");
                    if (page != null) OwnerForm.Ribbon.Pages.Add(page);
                    OwnerForm.Ribbon.UnMergeRibbon();
                }
                OwnerForm.RibbonStatusBar.UnMergeStatusBar();
                OwnerForm.ShowInfo(true);
            }
        }

        internal virtual void ShowModule(bool firstShow)
        {
            if (OwnerForm == null) return;

            if (AutoMergeRibbon && ChildRibbon != null)
            {
                OwnerForm.Ribbon.MergeRibbon(ChildRibbon);
                RibbonPage page = OwnerForm.Ribbon.Pages.GetPageByText("VIEW");
                if (page != null)
                {
                    OwnerForm.Ribbon.MergedPages.Remove(page);
                    OwnerForm.Ribbon.MergedPages.Insert(
                        OwnerForm.Ribbon.MergedPages.Count, 
                        page);
                }
                if (ChildRibbonStatusBar != null)
                {
                    OwnerForm.RibbonStatusBar.MergeStatusBar(ChildRibbonStatusBar);
                    OwnerForm.ShowInfo(false);
                }
            }

            OwnerForm.SaveAsMenuItem.Enabled = SaveAsEnable;
            ShowReminder();
            ShowInfo();
            OwnerForm.ZoomManager.ZoomFactor = (int)(ZoomFactor * 100);
            SetZoomCaption();
            OwnerForm.EnableZoomControl(AllowZoomControl);
            OwnerForm.OnModuleShown(this);
        }

        internal virtual void InitializeModule(IDXMenuManager manager, object data)
        {
            SetMenuManager(this.Controls, manager);
            if (Grid != null && Grid.MainView is ColumnView)
            {
                ((ColumnView)Grid.MainView).ColumnFilterChanged +=
                    new EventHandler(BaseModule_ColumnFilterChanged);
            }
            CapitializeChildRibbonPages();
        }

        protected virtual void SetZoomCaption() { }
        #endregion

        #region EVENTS
        //
        // GridControl events.
        // 

        internal void BaseModule_ColumnFilterChanged(object sender, EventArgs e)
        {
            ShowInfo(sender as ColumnView);
        }

        //
        // Misc events.
        //

        protected internal virtual void ButtonClick(string tag) { }
        protected internal virtual void SendKeyDown(KeyEventArgs e) { }
        #endregion
    }
}
