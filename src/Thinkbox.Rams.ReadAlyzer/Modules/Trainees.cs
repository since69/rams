﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using log4net;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Thinkbox.Rams.ReadAlyzer.Forms;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public partial class Trainees : BaseModule
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        private Timer alphaChange = null;
        private GetAlphaMethod extractName;
        #endregion

        #region PROPERTIES
        public override string ModuleName
        {
            get { return Properties.Resources.TraineeModuleName; }
        }

        internal Trainee CurrentTrainee
        {
            get
            {
                return gvTrainees.GetRow(gvTrainees.FocusedRowHandle) as Trainee;
            }
        }

        protected override GridControl Grid
        {
            get { return grdTrainees; }
        }
        #endregion

        #region CONSTRUCTORS
        public Trainees()
        {
            InitializeComponent();
            EditHelper.InitGenderComboBox(rcbGender);

            grdTrainees.DataSource = DataHelper.Trainees;
            gvTrainees.ShowFindPanel();
            InitializeIndex(DataHelper.Trainees);
        }
        #endregion

        #region GENERAL METHODS
        internal void UpdateActionButtons()
        {
            //OwnerForm.EnableLayoutButtons(grdTrainees.MainView != lvTrainees);
            //OwnerForm.EnableZoomControl(grdTrainees.MainView != lvTrainees);
        }

        internal void UpdateCurrentTrainee()
        {
            //ucTraineeInfo.Init(CurrentTrainee, null);
            gvTrainees.MakeRowVisible(gvTrainees.FocusedRowHandle);
            OwnerForm.EnalbleEditTrainee(CurrentTrainee != null);
        }

        private void UpdateInfo()
        {
            ShowInfo(grdTrainees.MainView as ColumnView);
        }

        private void UpdateMainView(ColumnView view)
        {
            bool isGrid = view is GridView;
            grdTrainees.MainView = view;
            //splitterItem1.Visibility = isGrid 
            //    ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            //    : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //layoutControlItem2.Visibility = isGrid 
            //    ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always 
            //    : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //GridHelper.SetFindControlImages(grdTrainees);
        }

        private void ClearSortingAndGrouping()
        {
            gvTrainees.ClearGrouping();
            gvTrainees.ClearSorting();
        }

        private DialogResult EditTrainee(Trainee trainee)
        {
            if (trainee == null) return DialogResult.Ignore;
            DialogResult result = DialogResult.Cancel;
            Cursor.Current = Cursors.WaitCursor;
            using (EditTraineeForm form = new EditTraineeForm(trainee, 
                OwnerForm.Ribbon))
            {
                result = form.ShowDialog(OwnerForm);
            }
            UpdateCurrentTrainee();
            Cursor.Current = Cursors.Default;
            return result;
        }


        //
        // Index related methods
        //

        public IList ApplyFilter(List<Trainee> list, AlphaIndex alpha)
        {
            if (alpha == null || alpha == AlphaIndex.All)
                return list;
            var res = from q in list
                      where alpha.Match(extractName(q))
                      select q;
            return res.ToList();
        }

        public void SetupGrid(List<AlphaIndex> list, GridControl grid)
        {
            GridView view = grid.MainView as GridView;
            view.Columns.AddVisible("Index");
            grid.DataSource = list;
            view.FocusedRowChanged += gvIndex_FocusedRowChanged;
        }

        public List<AlphaIndex> Generate(List<Trainee> values,
            GetAlphaMethod extractName)
        {
            var data = from q in values
                       where extractName(q) != null
                       group q by extractName(q) into g
                       orderby g.Key
                       select new AlphaIndex() { Index = g.Key, Count = g.Count() };
            var res = data.ToList();
            res.Insert(0, AlphaIndex.All);
            return res;
        }

        protected void InitializeIndex(List<Trainee> trainees)
        {
            this.extractName = (s) =>
            {
                string name = ((Trainee)s).Person.LastName;
                if (string.IsNullOrEmpty(name)) return null;
                return AlphaIndex.Group(name.Substring(0, 1));
            };
            List<AlphaIndex> dict = Generate(trainees, extractName);
            SetupGrid(dict, grdIndex);
        }
                

        // 
        // Data related methods 
        // 
        
        private void AddTrainee()
        {
            Trainee trainee = new Trainee();
            if (EditTrainee(trainee) == DialogResult.OK)
            {
                grdTrainees.MainView.BeginDataUpdate();
                try
                {
                    DataHelper.AddTrainee(trainee);
                }
                finally
                {
                    grdTrainees.MainView.EndDataUpdate();
                }
                ColumnView view = grdTrainees.MainView as ColumnView;
                if (view != null)
                {
                    GridHelper.GridViewFocusObject(view, trainee);
                    ShowInfo(view);
                }
            }
        }

        private void DeleteTrainee()
        {
            if (CurrentTrainee == null) return;
            int index = gvTrainees.FocusedRowHandle;
            grdTrainees.MainView.BeginDataUpdate();

            try
            {
                DataHelper.DeleteTrainee(CurrentTrainee);
            }
            finally
            {
                grdTrainees.MainView.EndDataUpdate();
            }

            if (index > gvTrainees.DataRowCount - 1) index--;
            gvTrainees.FocusedRowHandle = index;
            ShowInfo(gvTrainees);
        }
        #endregion

        #region OVERRIDE METHODS
        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);
            grdTrainees.Focus();
            //UpdateActionButtons();
            if (firstShow)
            {
                ButtonClick(Tags.TraineeList);
                grdTrainees.ForceInitialize();
                GridHelper.SetFindControlImages(grdTrainees);
                if (DataHelper.Trainees.Count == 0)
                    UpdateCurrentTrainee();
            }
        }
        #endregion

        #region EVENTS
        protected internal override void ButtonClick(string tag)
        {
            switch (tag)
            {
                case Tags.TraineeNew:
                    log.Info("New trainee menu selected.");
                    AddTrainee();
                    break;
                case Tags.TraineeEdit:
                    log.Info("Edit trainee selected.");
                    if (EditTrainee(CurrentTrainee) == DialogResult.OK)
                        DataHelper.UpdateTrainee(CurrentTrainee);
                    break;
                case Tags.TraineeDelete:
                    log.Info("Delete trainee selected.");
                    DeleteTrainee();
                    break;
                case Tags.TraineeList:
                    log.Info("List trainee selected.");
                    UpdateMainView(gvTrainees);
                    ClearSortingAndGrouping();
                    break;
                default:
                    log.Info($"ButtonClick {tag} Clicked @Trainees Module.");
                    break;
            }
            UpdateCurrentTrainee();
            UpdateInfo();
        }

        private void gvTrainees_RowCellClick(object sender, 
            RowCellClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowHandle >= 0 && e.Clicks == 2)
                EditTrainee(CurrentTrainee);
        }

        private void gvTrainees_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && gvTrainees.FocusedRowHandle >= 0)
                EditTrainee(CurrentTrainee);
        }

        private void gvTrainees_ColumnFilterChanged(object sender, 
            System.EventArgs e)
        {
            UpdateCurrentTrainee();
        }

        private void gvTrainees_FocusedRowChanged(object sender, 
            FocusedRowChangedEventArgs e)
        {
        }

        private void gvTrainees_FocusedRowObjectChanged(object sender, 
            FocusedRowObjectChangedEventArgs e)
        {
            if (e.FocusedRowHandle == GridControl.AutoFilterRowHandle)
                gvTrainees.FocusedColumn = colName;
            else if (e.FocusedRowHandle >= 0)
                gvTrainees.FocusedColumn = null;
            UpdateCurrentTrainee();
        }

        private void gvIndex_FocusedRowChanged(object sender,
            FocusedRowChangedEventArgs e)
        {
            if (alphaChange != null) alphaChange.Dispose();
            alphaChange = new Timer();
            alphaChange.Interval = 200;
            alphaChange.Tick += (s, ee) =>
            {
                grdTrainees.DataSource = ApplyFilter(DataHelper.Trainees,
                    ((GridView)sender).GetFocusedRow() as AlphaIndex);
                ((Timer)s).Dispose();
                this.alphaChange = null;
                UpdateInfo();
            };
            alphaChange.Start();
        }
        #endregion
    }
}
