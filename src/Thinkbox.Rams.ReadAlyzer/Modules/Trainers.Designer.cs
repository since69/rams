﻿namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    partial class Trainers
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Trainers));
            this.lcTrainers = new DevExpress.XtraLayout.LayoutControl();
            this.grdIndex = new DevExpress.XtraGrid.GridControl();
            this.gvIndex = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdTrainers = new DevExpress.XtraGrid.GridControl();
            this.gvTrainers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbGender = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPromotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTrainers = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciIndex = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcTrainers)).BeginInit();
            this.lcTrainers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrainers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTrainers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // lcTrainers
            // 
            this.lcTrainers.AllowCustomization = false;
            this.lcTrainers.Controls.Add(this.grdIndex);
            this.lcTrainers.Controls.Add(this.grdTrainers);
            resources.ApplyResources(this.lcTrainers, "lcTrainers");
            this.lcTrainers.Name = "lcTrainers";
            this.lcTrainers.Root = this.lcgRoot;
            // 
            // grdIndex
            // 
            resources.ApplyResources(this.grdIndex, "grdIndex");
            this.grdIndex.MainView = this.gvIndex;
            this.grdIndex.Name = "grdIndex";
            this.grdIndex.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvIndex});
            // 
            // gvIndex
            // 
            this.gvIndex.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gvIndex.GridControl = this.grdIndex;
            this.gvIndex.Name = "gvIndex";
            this.gvIndex.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvIndex.OptionsBehavior.Editable = false;
            this.gvIndex.OptionsDetail.EnableMasterViewMode = false;
            this.gvIndex.OptionsFind.AllowFindPanel = false;
            this.gvIndex.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvIndex.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvIndex.OptionsView.ShowColumnHeaders = false;
            this.gvIndex.OptionsView.ShowGroupPanel = false;
            this.gvIndex.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvIndex.OptionsView.ShowIndicator = false;
            this.gvIndex.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            // 
            // grdTrainers
            // 
            resources.ApplyResources(this.grdTrainers, "grdTrainers");
            this.grdTrainers.MainView = this.gvTrainers;
            this.grdTrainers.Name = "grdTrainers";
            this.grdTrainers.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rcbGender});
            this.grdTrainers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTrainers});
            // 
            // gvTrainers
            // 
            this.gvTrainers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colPersonType,
            this.colNameStyle,
            this.colTitle,
            this.colFirstName,
            this.colMiddleName,
            this.colLastName,
            this.colSuffix,
            this.colName,
            this.colGender,
            this.colEmail,
            this.colPhone,
            this.colEmailPromotion,
            this.colModifiedDate});
            this.gvTrainers.GridControl = this.grdTrainers;
            this.gvTrainers.Name = "gvTrainers";
            this.gvTrainers.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.True;
            this.gvTrainers.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvTrainers.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvTrainers.OptionsBehavior.Editable = false;
            this.gvTrainers.OptionsDetail.EnableMasterViewMode = false;
            this.gvTrainers.OptionsFind.AlwaysVisible = true;
            this.gvTrainers.OptionsPrint.PrintHorzLines = false;
            this.gvTrainers.OptionsPrint.PrintVertLines = false;
            this.gvTrainers.OptionsView.ShowGroupedColumns = true;
            this.gvTrainers.OptionsView.ShowGroupPanel = false;
            this.gvTrainers.OptionsView.ShowIndicator = false;
            this.gvTrainers.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvTrainers.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvTrainers_RowCellClick);
            this.gvTrainers.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvTrainers_FocusedRowChanged);
            this.gvTrainers.FocusedRowObjectChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventHandler(this.gvTrainers_FocusedRowObjectChanged);
            this.gvTrainers.ColumnFilterChanged += new System.EventHandler(this.gvTrainers_ColumnFilterChanged);
            this.gvTrainers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvTrainers_KeyDown);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowFocus = false;
            resources.ApplyResources(this.colId, "colId");
            // 
            // colPersonType
            // 
            resources.ApplyResources(this.colPersonType, "colPersonType");
            this.colPersonType.FieldName = "Person.PersonType";
            this.colPersonType.Name = "colPersonType";
            this.colPersonType.OptionsColumn.AllowFocus = false;
            // 
            // colNameStyle
            // 
            resources.ApplyResources(this.colNameStyle, "colNameStyle");
            this.colNameStyle.FieldName = "Person.NameStyle";
            this.colNameStyle.Name = "colNameStyle";
            this.colNameStyle.OptionsColumn.AllowFocus = false;
            // 
            // colTitle
            // 
            resources.ApplyResources(this.colTitle, "colTitle");
            this.colTitle.FieldName = "Person.Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowFocus = false;
            // 
            // colFirstName
            // 
            resources.ApplyResources(this.colFirstName, "colFirstName");
            this.colFirstName.FieldName = "Person.FirstName";
            this.colFirstName.Name = "colFirstName";
            this.colFirstName.OptionsColumn.AllowFocus = false;
            // 
            // colMiddleName
            // 
            resources.ApplyResources(this.colMiddleName, "colMiddleName");
            this.colMiddleName.FieldName = "Person.MiddleName";
            this.colMiddleName.Name = "colMiddleName";
            this.colMiddleName.OptionsColumn.AllowFocus = false;
            // 
            // colLastName
            // 
            resources.ApplyResources(this.colLastName, "colLastName");
            this.colLastName.FieldName = "Person.LastName";
            this.colLastName.Name = "colLastName";
            this.colLastName.OptionsColumn.AllowFocus = false;
            // 
            // colSuffix
            // 
            resources.ApplyResources(this.colSuffix, "colSuffix");
            this.colSuffix.FieldName = "Person.Suffix";
            this.colSuffix.Name = "colSuffix";
            this.colSuffix.OptionsColumn.AllowFocus = false;
            // 
            // colName
            // 
            resources.ApplyResources(this.colName, "colName");
            this.colName.FieldName = "Person.FullName";
            this.colName.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colGender
            // 
            resources.ApplyResources(this.colGender, "colGender");
            this.colGender.ColumnEdit = this.rcbGender;
            this.colGender.FieldName = "Gender";
            this.colGender.Name = "colGender";
            this.colGender.OptionsColumn.AllowFocus = false;
            this.colGender.OptionsColumn.AllowMove = false;
            this.colGender.OptionsColumn.FixedWidth = true;
            this.colGender.OptionsColumn.ShowCaption = false;
            this.colGender.OptionsFilter.AllowAutoFilter = false;
            // 
            // rcbGender
            // 
            resources.ApplyResources(this.rcbGender, "rcbGender");
            this.rcbGender.Name = "rcbGender";
            // 
            // colEmail
            // 
            resources.ApplyResources(this.colEmail, "colEmail");
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colPhone
            // 
            resources.ApplyResources(this.colPhone, "colPhone");
            this.colPhone.Name = "colPhone";
            this.colPhone.OptionsColumn.AllowFocus = false;
            this.colPhone.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colEmailPromotion
            // 
            resources.ApplyResources(this.colEmailPromotion, "colEmailPromotion");
            this.colEmailPromotion.FieldName = "Person.EmailPromotion";
            this.colEmailPromotion.Name = "colEmailPromotion";
            this.colEmailPromotion.OptionsColumn.AllowFocus = false;
            // 
            // colModifiedDate
            // 
            resources.ApplyResources(this.colModifiedDate, "colModifiedDate");
            this.colModifiedDate.FieldName = "ModifiedDate";
            this.colModifiedDate.Name = "colModifiedDate";
            this.colModifiedDate.OptionsColumn.AllowFocus = false;
            // 
            // lcgRoot
            // 
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTrainers,
            this.lciIndex});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.lcgRoot.Size = new System.Drawing.Size(824, 624);
            resources.ApplyResources(this.lcgRoot, "lcgRoot");
            this.lcgRoot.TextVisible = false;
            // 
            // lciTrainers
            // 
            this.lciTrainers.Control = this.grdTrainers;
            this.lciTrainers.Location = new System.Drawing.Point(0, 0);
            this.lciTrainers.Name = "lciTrainers";
            this.lciTrainers.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciTrainers.Size = new System.Drawing.Size(752, 612);
            this.lciTrainers.TextSize = new System.Drawing.Size(0, 0);
            this.lciTrainers.TextVisible = false;
            // 
            // lciIndex
            // 
            this.lciIndex.Control = this.grdIndex;
            this.lciIndex.Location = new System.Drawing.Point(752, 0);
            this.lciIndex.MaxSize = new System.Drawing.Size(60, 0);
            this.lciIndex.MinSize = new System.Drawing.Size(60, 24);
            this.lciIndex.Name = "lciIndex";
            this.lciIndex.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 0, 0, 0);
            this.lciIndex.Size = new System.Drawing.Size(60, 612);
            this.lciIndex.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciIndex.TextSize = new System.Drawing.Size(0, 0);
            this.lciIndex.TextVisible = false;
            // 
            // Trainers
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcTrainers);
            this.Name = "Trainers";
            ((System.ComponentModel.ISupportInitialize)(this.lcTrainers)).EndInit();
            this.lcTrainers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrainers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTrainers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciIndex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcTrainers;
        private DevExpress.XtraGrid.GridControl grdTrainers;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTrainers;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainers;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.GridControl grdIndex;
        private DevExpress.XtraGrid.Views.Grid.GridView gvIndex;
        private DevExpress.XtraLayout.LayoutControlItem lciIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rcbGender;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colNameStyle;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstName;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleName;
        private DevExpress.XtraGrid.Columns.GridColumn colLastName;
        private DevExpress.XtraGrid.Columns.GridColumn colSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPromotion;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedDate;
    }
}
