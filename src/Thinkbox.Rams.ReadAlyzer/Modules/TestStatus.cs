﻿//
// Thinkbox.Rams.ReadAlyzer.Modules.TestStatus
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public enum TestStatus
    {
        Ready,
        Prepared,
        Started,
        Completed
    }
}
