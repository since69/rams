﻿using System;
using System.Collections.Generic;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public class ZoomManager
    {
        #region VARIABLES
        internal RibbonControl ribbon;
        internal BarEditItem beiZoom;
        internal ModulesNavigator modulesNavigator;
        internal List<int> zoomValues = new List<int>()
        {
            100, 115, 130, 150, 200, 250, 300, 350, 400, 500
        };
        #endregion

        #region PROPERTIES
        internal int zoomFactor = 0;
        public int ZoomFactor
        {
            get { return zoomFactor; }
            set
            {
                zoomFactor = value;
                beiZoom.Caption = string.Format(" {0}%", ZoomFactor);
                int index = zoomValues.IndexOf(ZoomFactor);
                if (index == -1)
                    beiZoom.EditValue = ZoomFactor / 10;
                else
                    beiZoom.EditValue = 10 + index;
                modulesNavigator.CurrentModule.ZoomFactor = (float)ZoomFactor / 100;
            }
        }

        internal ZoomTrackBarControl zoomControl;
        ZoomTrackBarControl ZoomControl { get { return zoomControl; } }
        #endregion

        #region CONSTRUCTORS
        public ZoomManager(RibbonControl ribbon, ModulesNavigator modulesNavigator, BarEditItem beItem)
        {
            this.ribbon = ribbon;
            this.modulesNavigator = modulesNavigator;
            this.beiZoom = beItem;
            this.beiZoom.HiddenEditor += new ItemClickEventHandler(this.beiZoom_HiddenEditor);
            this.beiZoom.ShownEditor += new ItemClickEventHandler(this.beiZoom_ShownEditor);
        }
        #endregion

        #region METHODS
        public void SetZoomCaption(string caption)
        {
            beiZoom.Caption = caption;
        }
        #endregion

        #region EVENTS
        private void beiZoom_ShownEditor(object sender, ItemClickEventArgs e)
        {
            this.zoomControl = ribbon.Manager.ActiveEditor as ZoomTrackBarControl;
            if (ZoomControl != null)
            {
                ZoomControl.ValueChanged += new EventHandler(OnZoomTackValueChanged);
                OnZoomTackValueChanged(ZoomControl, EventArgs.Empty);
            }
        }

        private void beiZoom_HiddenEditor(object sender, ItemClickEventArgs e)
        {
            ZoomControl.ValueChanged -= new EventHandler(OnZoomTackValueChanged);
            this.zoomControl = null;
        }

        private void OnZoomTackValueChanged(object sender, EventArgs e)
        {
            int val = val = ZoomControl.Value * 10;
            if (ZoomControl.Value > 10)
                val = zoomValues[ZoomControl.Value - 10];
            ZoomFactor = val;
        }
        #endregion
    }
}
