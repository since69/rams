﻿namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    partial class Test
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Test));
            this.pnWrapper = new System.Windows.Forms.TableLayoutPanel();
            this.ucInformation = new Thinkbox.Rams.ReadAlyzer.Controls.ucTestInformation();
            this.ucData = new Thinkbox.Rams.ReadAlyzer.Controls.ucTestData();
            this.pnWrapper.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapper
            // 
            this.pnWrapper.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.pnWrapper, "pnWrapper");
            this.pnWrapper.Controls.Add(this.ucInformation, 2, 0);
            this.pnWrapper.Controls.Add(this.ucData, 0, 0);
            this.pnWrapper.Name = "pnWrapper";
            // 
            // ucInformation
            // 
            this.ucInformation.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.ucInformation, "ucInformation");
            this.ucInformation.Name = "ucInformation";
            // 
            // ucData
            // 
            this.ucData.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.ucData, "ucData");
            this.ucData.Name = "ucData";
            // 
            // Test
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnWrapper);
            this.Name = "Test";
            this.pnWrapper.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnWrapper;
        private Controls.ucTestInformation ucInformation;
        private Controls.ucTestData ucData;
    }
}
