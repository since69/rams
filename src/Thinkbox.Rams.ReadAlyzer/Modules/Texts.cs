﻿using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Thinkbox.Rams.ReadAlyzer.Forms;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;
using log4net;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public partial class Texts : BaseModule
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        #endregion

        #region PROPERTIES
        public override string ModuleName
        {
            get { return Properties.Resources.TextModuleName; }
        }

        internal TestText CurrentText
        {
            get { return gvTexts.GetRow(gvTexts.FocusedRowHandle) as TestText; }
        }

        protected override GridControl Grid
        {
            get { return grdTexts; }
        }
        #endregion

        #region CONSTRUCTORS
        public Texts()
        {
            InitializeComponent();

            EditHelper.InitCultureLookupEdit(leCulture);
            EditHelper.InitLevelLookupEdit(leLevel);
            grdTexts.DataSource = DataHelper.Texts;
            gvTexts.ShowFindPanel();
        }
        #endregion

        #region GENERAL METHODS
        internal void UpdateActionButtons()
        {
            //OwnerForm.EnableLayoutButtons(grdTrainees.MainView != lvTrainees);
            //OwnerForm.EnableZoomControl(grdTrainees.MainView != lvTrainees);
        }

        internal void UpdateCurrentText()
        {
            //ucTextInfo.Init(CurrentText, null);
            gvTexts.MakeRowVisible(gvTexts.FocusedRowHandle);
            OwnerForm.EnalbleEditTrainee(CurrentText != null);
        }

        private void UpdateInfo()
        {
            ShowInfo(grdTexts.MainView as ColumnView);
        }

        private void UpdateMainView(ColumnView view)
        {
            //bool isGrid = view is GridView;
            grdTexts.MainView = view;
            //splitterItem1.Visibility = isGrid 
            //    ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            //    : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //layoutControlItem2.Visibility = isGrid 
            //    ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always 
            //    : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            GridHelper.SetFindControlImages(grdTexts);
        }

        private void ClearSortingAndGrouping()
        {
            gvTexts.ClearGrouping();
            gvTexts.ClearSorting();
        }

        private DialogResult EditText(TestText text)
        {
            if (text == null) return DialogResult.Ignore;
            DialogResult result = DialogResult.Cancel;
            Cursor.Current = Cursors.WaitCursor;
            using (EditTextForm form = new EditTextForm(text,
                OwnerForm.Ribbon))
            {
                result = form.ShowDialog(OwnerForm);
            }
            UpdateCurrentText();
            Cursor.Current = Cursors.Default;
            return result;
        }
        #endregion

        #region OVERRIDE METHODS
        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);
            grdTexts.Focus();
            //UpdateActionButtons();
            if (firstShow)
            {
                ButtonClick(Tags.TextList);
                grdTexts.ForceInitialize();
                GridHelper.SetFindControlImages(grdTexts);
                if (DataHelper.Texts.Count == 0)
                    UpdateCurrentText();
            }
        }
        #endregion

        #region EVENTS
        protected internal override void ButtonClick(string tag)
        {
            switch (tag)
            {
                case Tags.TextList:
                    log.Info("List text selected.");
                    UpdateMainView(gvTexts);
                    ClearSortingAndGrouping();
                    break;
                default:
                    log.Info($"ButtonClick {tag} Clicked @Text Module.");
                    break;
            }
            UpdateCurrentText();
            UpdateInfo();
        }

        private void gvTexts_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowHandle >= 0 && e.Clicks == 2)
                EditText(CurrentText);
        }

        private void gvTexts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && gvTexts.FocusedRowHandle >= 0)
                EditText(CurrentText);
        }

        private void gvTexts_ColumnFilterChanged(object sender, 
            System.EventArgs e)
        {
            UpdateCurrentText();
        }

        private void gvTexts_FocusedRowChanged(object sender, 
            FocusedRowChangedEventArgs e)
        {
        }

        private void gvTexts_FocusedRowObjectChanged(object sender, 
            FocusedRowObjectChangedEventArgs e)
        {
            if (e.FocusedRowHandle == GridControl.AutoFilterRowHandle)
                gvTexts.FocusedColumn = colTitle;
            else if (e.FocusedRowHandle >= 0)
                gvTexts.FocusedColumn = null;
            UpdateCurrentText();
        }
        #endregion
    }
}
