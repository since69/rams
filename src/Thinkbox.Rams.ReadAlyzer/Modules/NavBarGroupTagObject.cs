﻿using System;
using DevExpress.XtraBars.Ribbon;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public class NavBarGroupTagObject
    {
        #region PROPERTIES
        internal string name;
        public string Name { get { return name; } }

        internal Type moduleType;
        public Type ModuleType { get { return moduleType; } }

        internal BaseModule module;
        public BaseModule Module
        {
            get { return module; }
            set { module = value; }
        }

        internal RibbonControlColorScheme ribbonScheme = 
            RibbonControlColorScheme.Default;
        public RibbonControlColorScheme RibbonScheme
        {
            get { return ribbonScheme; }
        }
        #endregion

        #region CONSTRUCTORS
        public NavBarGroupTagObject(string name, Type moduletype)
            : this(name, moduletype, RibbonControlColorScheme.Default)
        {
        }

        public NavBarGroupTagObject(string name, Type moduleType, 
            RibbonControlColorScheme ribbonScheme)
        {
            this.name = name;
            this.moduleType = moduleType;
            this.ribbonScheme = ribbonScheme;
            module = null;
        }
        #endregion
    }
}
