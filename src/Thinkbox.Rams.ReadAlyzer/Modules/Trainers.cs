﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using log4net;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using Thinkbox.Rams.ReadAlyzer.Forms;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public partial class Trainers : BaseModule
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        private Timer alphaChange = null;
        private GetAlphaMethod extractName;
        #endregion

        #region PROPERTIES
        public override string ModuleName
        {
            get { return Properties.Resources.TrainerModuleName; }
        }

        internal Trainer CurrentTrainer
        {
            get
            {
                return gvTrainers.GetRow(gvTrainers.FocusedRowHandle) as Trainer;
            }
        }

        protected override GridControl Grid
        {
            get { return grdTrainers; }
        }


        //public override float ZoomFactor
        //{
        //    get {  return ucXXXInfo.ZoomFactor;  }
        //    set
        //    {
        //        ucXXXInfo.ZoomFactor = value;
        //        SetZoomCaption();
        //    }
        //}
        //protected override bool AllowZoomControl { get { return true; } }
        //protected override void SetZoomCaption()
        //{
        //    base.SetZoomCaption();
        //    if (ZoomFactor == 1)
        //        OwnerForm.ZoomManager.SetZoomCaption(Properties.Resources.Picture100Zoom);
        //}
        #endregion

        #region CONSTRUCTORS
        public Trainers()
        {
            InitializeComponent();

            EditHelper.InitGenderComboBox(rcbGender);
            grdTrainers.DataSource = DataHelper.Trainers;
            gvTrainers.ShowFindPanel();
            InitializeIndex(DataHelper.Trainers);
        }
        #endregion

        #region GENERLAL METHODS
        internal void UpdateActionButtons()
        {
            //OwnerForm.EnableLayoutButtons(grdTrainers.MainView != lvTrainers);
            //OwnerForm.EnableZoomControl(grdTrainers.MainView != lvTrainers);
        }

        internal void UpdateCurrentTrainer()
        {
            //ucTrainerInfo.Init(CurrentTrainer, null);
            gvTrainers.MakeRowVisible(gvTrainers.FocusedRowHandle);
            OwnerForm.EnalbleEditTrainer(CurrentTrainer != null);
        }

        
        private void UpdateInfo()
        {
            ShowInfo(grdTrainers.MainView as ColumnView);
        }

        private void UpdateMainView(ColumnView view)
        {
            //bool isGrid = view is GridView;
            grdTrainers.MainView = view;
            //splitterItem1.Visibility = isGrid 
            //    ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            //    : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //layoutControlItem2.Visibility = isGrid 
            //    ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always 
            //    : DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            //GridHelper.SetFindControlImages(grdTrainers);
        }

        private void ClearSortingAndGrouping()
        {
            gvTrainers.ClearGrouping();
            gvTrainers.ClearSorting();
        }

        private DialogResult EditTrainer(Trainer trainer)
        {
            if (trainer == null) return DialogResult.Ignore;
            DialogResult result = DialogResult.Cancel;
            Cursor.Current = Cursors.WaitCursor;
            using (EditTrainerForm f = new EditTrainerForm(trainer, 
                OwnerForm.Ribbon))
            {
                result = f.ShowDialog(OwnerForm);
            }
            UpdateCurrentTrainer();
            Cursor.Current = Cursors.Default;
            return result;
        }


        //
        // Index related methods
        //

        public IList ApplyFilter(List<Trainer> list, AlphaIndex alpha)
        {
            if (alpha == null || alpha == AlphaIndex.All)
                return list;
            var res = from q in list
                      where alpha.Match(extractName(q))
                      select q;
            return res.ToList();
        }

        public void SetupGrid(List<AlphaIndex> list, GridControl grid)
        {
            GridView view = grid.MainView as GridView;
            view.Columns.AddVisible("Index");
            grid.DataSource = list;
            view.FocusedRowChanged += gvIndex_FocusedRowChanged;
        }

        public List<AlphaIndex> Generate(List<Trainer> values,
            GetAlphaMethod extractName)
        {
            var data = from q in values
                       where extractName(q) != null
                       group q by extractName(q) into g
                       orderby g.Key
                       select new AlphaIndex() { Index = g.Key, Count = g.Count() };
            var res = data.ToList();
            res.Insert(0, AlphaIndex.All);
            return res;
        }

        protected void InitializeIndex(List<Trainer> trainers)
        {
            this.extractName = (s) =>
            {
                string name = ((Trainer)s).Person.LastName;
                if (string.IsNullOrEmpty(name)) return null;
                return AlphaIndex.Group(name.Substring(0, 1));
            };
            List<AlphaIndex> dict = Generate(trainers, extractName);
            SetupGrid(dict, grdIndex);
        }
        

        // 
        // Data related methods 
        // 

        private void AddTrainer()
        {
            Trainer trainer = new Trainer();
            if (EditTrainer(trainer) == DialogResult.OK)
            {
                grdTrainers.MainView.BeginDataUpdate();
                try
                {
                    DataHelper.AddTrainer(trainer);
                }
                finally
                {
                    grdTrainers.MainView.EndDataUpdate();
                }
                ColumnView view = grdTrainers.MainView as ColumnView;
                if (view != null)
                {
                    GridHelper.GridViewFocusObject(view, trainer);
                    ShowInfo(view);
                }
            }
        }

        private void DeleteTrainer()
        {
            if (CurrentTrainer == null) return;
            int index = gvTrainers.FocusedRowHandle;
            grdTrainers.MainView.BeginDataUpdate();

            try
            {
                DataHelper.DeleteTrainer(CurrentTrainer);
            }
            finally
            {
                grdTrainers.MainView.EndDataUpdate();
            }

            if (index > gvTrainers.DataRowCount - 1) index--;
            gvTrainers.FocusedRowHandle = index;
            ShowInfo(gvTrainers);
        }
        #endregion

        #region OVERRIDE METHODS
        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);
            grdTrainers.Focus();
            //UpdateActionButtons();
            if (firstShow)
            {
                ButtonClick(Tags.TrainerList);
                grdTrainers.ForceInitialize();
                GridHelper.SetFindControlImages(grdTrainers);
                if (DataHelper.Trainers.Count == 0)
                    UpdateCurrentTrainer();
            }
        }
        #endregion

        #region EVENTS
        protected internal override void ButtonClick(string tag)
        {
            switch (tag)
            {
                case Tags.TrainerNew:
                    log.Info("New trainer menu selected.");
                    AddTrainer();
                    break;
                case Tags.TrainerEdit:
                    log.Info("Edit trainer selected.");
                    if (EditTrainer(CurrentTrainer) == DialogResult.OK)
                        DataHelper.UpdateTrainer(CurrentTrainer);
                    break;
                case Tags.TrainerDelete:
                    log.Info("Delete trainer selected.");
                    DeleteTrainer();
                    break;
                case Tags.TrainerList:
                    log.Info("List trainer selected.");
                    UpdateMainView(gvTrainers);
                    ClearSortingAndGrouping();
                    break;
                default:
                    log.Info($"ButtonClick {tag} Clicked @Trainers Module.");
                    break;
            }
            UpdateCurrentTrainer();
            UpdateInfo();
        }

        private void gvTrainers_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowHandle >= 0 && e.Clicks == 2)
                EditTrainer(CurrentTrainer);
        }

        private void gvTrainers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && gvTrainers.FocusedRowHandle >= 0)
                EditTrainer(CurrentTrainer);
        }

        private void gvTrainers_ColumnFilterChanged(object sender, 
            System.EventArgs e)
        {
            UpdateCurrentTrainer();
        }

        private void gvTrainers_FocusedRowChanged(object sender, 
            FocusedRowChangedEventArgs e)
        {
        }

        private void gvTrainers_FocusedRowObjectChanged(object sender, 
            FocusedRowObjectChangedEventArgs e)
        {
            if (e.FocusedRowHandle == GridControl.AutoFilterRowHandle)
                gvTrainers.FocusedColumn = colName;
            else if (e.FocusedRowHandle >= 0)
                gvTrainers.FocusedColumn = null;
            UpdateCurrentTrainer();
        }

        private void gvIndex_FocusedRowChanged(object sender,
            FocusedRowChangedEventArgs e)
        {
            if (alphaChange != null) alphaChange.Dispose();
            alphaChange = new Timer();
            alphaChange.Interval = 200;
            alphaChange.Tick += (s, ee) =>
            {
                grdTrainers.DataSource = ApplyFilter(DataHelper.Trainers,
                    ((GridView)sender).GetFocusedRow() as AlphaIndex);
                ((Timer)s).Dispose();
                this.alphaChange = null;
                UpdateInfo();
            };
            alphaChange.Start();
        }
        #endregion
    }
}
