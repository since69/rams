﻿//
// Thinkbox.Rams.ReadAlyzer.Modules.Records
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Collections.Generic;
using System.Windows.Forms;
using log4net;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraReports.UI;
using Thinkbox.Rams.ReadAlyzer.Forms;
using Thinkbox.Rams.ReadAlyzer.Data;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public partial class Records : BaseModule
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        #endregion

        #region PROPERTIES
        public override string ModuleName
        {
            get { return Properties.Resources.RecordModuleName; }
        }

        internal Data.Test CurrentRecord
        {
            get { return gvRecords.GetRow(gvRecords.FocusedRowHandle) as Data.Test; }
        }

        protected override GridControl Grid
        {
            get { return grdRecords; }
        }
        #endregion

        #region CONSTRUCTORS
        public Records()
        {
            InitializeComponent();

            EditHelper.InitGradeLookupEdit(leGrade);
            grdRecords.DataSource = DataHelper.Tests;
        }
        #endregion

        #region GENERAL METHODS
        internal void UpdateCurrentRecord()
        {
            gvRecords.MakeRowVisible(gvRecords.FocusedRowHandle);
            OwnerForm.EnalbleEditTrainee(CurrentRecord != null);
        }

        private void UpdateInfo()
        {
            ShowInfo(grdRecords.MainView as ColumnView);
        }

        private void ClearSortingAndGrouping()
        {
            gvRecords.ClearGrouping();
            gvRecords.ClearSorting();
        }

        private DialogResult EditRecord(Data.Test test)
        {
            if (test == null) return DialogResult.Ignore;
            DialogResult result = DialogResult.Cancel;
            Cursor.Current = Cursors.WaitCursor;
            using (TestResultForm form = new TestResultForm(test,
                OwnerForm.Ribbon))
            {
                result = form.ShowDialog(OwnerForm);
            }
            UpdateCurrentRecord();
            Cursor.Current = Cursors.Default;
            return result;
        }

        private void PrintReports(List<Device.TestResult> results)
        {
            Reports.MasterReport report = new Reports.MasterReport();
            report.DataSource = results;

            using (ReportPrintTool printTool = new ReportPrintTool(report))
            {
                // Invoke the Print dialog.
                printTool.PrintDialog();

                // Send the report to the default printer.
                printTool.ShowPreview();
                //printTool.Print();
            }
        }

        private void AnalysisRecord()
        {
            //AnalsysTestForm form = new AnalsysTestForm();
            //form.ShowDialog();
        }

        private void DeleteRecord()
        {
            if (CurrentRecord == null) return;
            int index = gvRecords.FocusedRowHandle;
            grdRecords.MainView.BeginDataUpdate();

            try
            {
                DataHelper.DeleteTest(CurrentRecord);
            }
            finally
            {
                grdRecords.MainView.EndDataUpdate();
            }

            if (index > gvRecords.DataRowCount - 1) index--;
            gvRecords.FocusedRowHandle = index;
            ShowInfo(gvRecords);
        }

        private void PrintRecords()
        {
            List<Device.TestResult> results = new List<Device.TestResult>();
            if (gvRecords.SelectedRowsCount == 0 && CurrentRecord != null)
            {
                Device.TestResult result = Device.DataAnalyzer.Analyze(
                    CurrentRecord.TestText.CharacterWidth, 
                    CurrentRecord.TestText.CharacterHeight,
                    CurrentRecord.TestText.LineSpacing, 
                    CurrentRecord.TestText.Text,
                    FileHelper.ImportRecord(Device.Tracker.RecordingFolder,
                        CurrentRecord.RecordFile));

                result.TraineeName = CurrentRecord.Trainee.Person.FullName;
                result.Grade = CurrentRecord.Grade;
                results.Add(result);
            }
            else
            {
                //ist<Task> selectedTasks = new List<Task>();
                //foreach (int index in gridView1.GetSelectedRows())
                //{
                //    Task task = gridView1.GetRow(index) as Task;
                //    if (task != null) selectedTasks.Add(task);
                //}
                //foreach (Task task in selectedTasks) list.Remove(task);

                foreach (int index in gvRecords.GetSelectedRows())
                {
                    Data.Test test = gvRecords.GetRow(index) as Data.Test;
                    if (test != null)
                    {
                        Device.TestResult result = Device.DataAnalyzer.Analyze(
                            test.TestText.CharacterWidth, test.TestText.CharacterHeight,
                            test.TestText.LineSpacing, test.TestText.Text,
                            FileHelper.ImportRecord(Device.Tracker.RecordingFolder,
                                test.RecordFile));

                        result.TraineeName = test.Trainee.Person.FullName;
                        result.Grade = test.Grade;
                        results.Add(result);
                    }
                }
            }

            if (results.Count > 0)
                PrintReports(results);
        }
        #endregion

        #region  OVERRIDE METHODS
        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);
        }
        #endregion

        #region EVENTS
        
        protected internal override void ButtonClick(string tag)
        {
            switch (tag)
            {
                case Tags.RecordEdit:
                    log.Debug("Edit record selected.");
                    EditRecord(CurrentRecord);
                    break;
                case Tags.RecordDelete:
                    log.Debug("Delete record selected.");
                    DeleteRecord();
                    break;
                case Tags.RecordAnalysis:
                    log.Debug("Analysis record selected.");
                    AnalysisRecord();
                    break;
                case Tags.RecordPrint:
                    log.Debug("Print record selected");
                    PrintRecords();
                    break;
                case Tags.RecordList:
                    log.Debug("List record selected.");
                    ClearSortingAndGrouping();
                    break;
                default:
                    log.DebugFormat("ButtonClick {0} Clicked @Records Module.", tag);
                    break;
            }
            UpdateCurrentRecord();
            UpdateInfo();
        }

        private void gvRecords_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowHandle >= 0 && e.Clicks == 2)
                EditRecord(CurrentRecord);
        }

        private void gvRecords_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter && gvRecords.FocusedRowHandle >= 0)
                EditRecord(CurrentRecord);
        }

        private void gvRecords_ColumnFilterChanged(object sender, System.EventArgs e)
        {
            UpdateCurrentRecord();
        }

        private void gvRecords_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
        }

        private void gvRecords_FocusedRowObjectChanged(object sender, 
            FocusedRowObjectChangedEventArgs e)
        {
            if (e.FocusedRowHandle == GridControl.AutoFilterRowHandle)
                gvRecords.FocusedColumn = colTraineeName;
            else if (e.FocusedRowHandle >= 0)
                gvRecords.FocusedColumn = null;
            UpdateCurrentRecord();
        }
        #endregion
    }
}
