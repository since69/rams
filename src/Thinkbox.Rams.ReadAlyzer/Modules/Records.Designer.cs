﻿namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    partial class Records
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Records));
            this.lcRecords = new DevExpress.XtraLayout.LayoutControl();
            this.nbcTrainees = new DevExpress.XtraNavBar.NavBarControl();
            this.nbgTrainees = new DevExpress.XtraNavBar.NavBarGroup();
            this.grdRecords = new DevExpress.XtraGrid.GridControl();
            this.gvRecords = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leGrade = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGrade = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTraineeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTraineeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrainerId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrainerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTestTextId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTextTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTestTime = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciRecords = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciTrainees = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcRecords)).BeginInit();
            this.lcRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbcTrainees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTestTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainees)).BeginInit();
            this.SuspendLayout();
            // 
            // lcRecords
            // 
            this.lcRecords.AllowCustomization = false;
            this.lcRecords.Appearance.DisabledLayoutGroupCaption.ForeColor = ((System.Drawing.Color)(resources.GetObject("lcRecords.Appearance.DisabledLayoutGroupCaption.ForeColor")));
            this.lcRecords.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.lcRecords.Appearance.DisabledLayoutItem.ForeColor = ((System.Drawing.Color)(resources.GetObject("lcRecords.Appearance.DisabledLayoutItem.ForeColor")));
            this.lcRecords.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.lcRecords.Controls.Add(this.nbcTrainees);
            this.lcRecords.Controls.Add(this.grdRecords);
            resources.ApplyResources(this.lcRecords, "lcRecords");
            this.lcRecords.Name = "lcRecords";
            this.lcRecords.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(842, -11, 630, 697);
            this.lcRecords.Root = this.lcgRoot;
            // 
            // nbcTrainees
            // 
            this.nbcTrainees.ActiveGroup = this.nbgTrainees;
            this.nbcTrainees.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nbgTrainees});
            resources.ApplyResources(this.nbcTrainees, "nbcTrainees");
            this.nbcTrainees.Name = "nbcTrainees";
            this.nbcTrainees.OptionsNavPane.ExpandedWidth = ((int)(resources.GetObject("resource.ExpandedWidth")));
            this.nbcTrainees.OptionsNavPane.ShowGroupImageInHeader = true;
            this.nbcTrainees.OptionsNavPane.ShowOverflowPanel = false;
            this.nbcTrainees.OptionsNavPane.ShowSplitter = false;
            this.nbcTrainees.View = new DevExpress.XtraNavBar.ViewInfo.SkinNavigationPaneViewInfoRegistrator();
            // 
            // nbgTrainees
            // 
            resources.ApplyResources(this.nbgTrainees, "nbgTrainees");
            this.nbgTrainees.Expanded = true;
            this.nbgTrainees.LargeImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainee_32x32;
            this.nbgTrainees.Name = "nbgTrainees";
            this.nbgTrainees.NavigationPaneVisible = false;
            this.nbgTrainees.SmallImage = global::Thinkbox.Rams.ReadAlyzer.Properties.Resources.Trainee_16x16;
            // 
            // grdRecords
            // 
            resources.ApplyResources(this.grdRecords, "grdRecords");
            this.grdRecords.MainView = this.gvRecords;
            this.grdRecords.Name = "grdRecords";
            this.grdRecords.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leGrade,
            this.teTestTime});
            this.grdRecords.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRecords});
            // 
            // gvRecords
            // 
            this.gvRecords.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colGrade,
            this.colTraineeId,
            this.colTraineeName,
            this.colTrainerId,
            this.colTrainerName,
            this.colTestTextId,
            this.colTextTitle,
            this.colStartTime,
            this.colEndTime,
            this.colModifiedDate});
            this.gvRecords.GridControl = this.grdRecords;
            this.gvRecords.Name = "gvRecords";
            this.gvRecords.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.True;
            this.gvRecords.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvRecords.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvRecords.OptionsDetail.EnableMasterViewMode = false;
            this.gvRecords.OptionsFilter.ColumnFilterPopupMode = DevExpress.XtraGrid.Columns.ColumnFilterPopupMode.Excel;
            this.gvRecords.OptionsFind.AlwaysVisible = true;
            this.gvRecords.OptionsPrint.PrintHorzLines = false;
            this.gvRecords.OptionsPrint.PrintVertLines = false;
            this.gvRecords.OptionsSelection.CheckBoxSelectorColumnWidth = 35;
            this.gvRecords.OptionsSelection.MultiSelect = true;
            this.gvRecords.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gvRecords.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gvRecords.OptionsView.ShowGroupPanel = false;
            this.gvRecords.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvRecords.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvRecords_RowCellClick);
            this.gvRecords.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvRecords_FocusedRowChanged);
            this.gvRecords.FocusedRowObjectChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventHandler(this.gvRecords_FocusedRowObjectChanged);
            this.gvRecords.ColumnFilterChanged += new System.EventHandler(this.gvRecords_ColumnFilterChanged);
            this.gvRecords.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvRecords_KeyDown);
            // 
            // colId
            // 
            this.colId.ColumnEdit = this.leGrade;
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colId.OptionsColumn.AllowSize = false;
            this.colId.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colId.OptionsColumn.FixedWidth = true;
            this.colId.OptionsColumn.ShowCaption = false;
            this.colId.OptionsFilter.AllowFilter = false;
            resources.ApplyResources(this.colId, "colId");
            // 
            // leGrade
            // 
            resources.ApplyResources(this.leGrade, "leGrade");
            this.leGrade.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("leGrade.Buttons"))))});
            this.leGrade.Name = "leGrade";
            this.leGrade.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colGrade
            // 
            resources.ApplyResources(this.colGrade, "colGrade");
            this.colGrade.ColumnEdit = this.leGrade;
            this.colGrade.FieldName = "Grade";
            this.colGrade.Name = "colGrade";
            this.colGrade.OptionsColumn.AllowEdit = false;
            this.colGrade.OptionsColumn.AllowFocus = false;
            this.colGrade.OptionsColumn.FixedWidth = true;
            // 
            // colTraineeId
            // 
            this.colTraineeId.FieldName = "Trainee.Id";
            this.colTraineeId.Name = "colTraineeId";
            // 
            // colTraineeName
            // 
            resources.ApplyResources(this.colTraineeName, "colTraineeName");
            this.colTraineeName.FieldName = "Trainee.Person.FullName";
            this.colTraineeName.Name = "colTraineeName";
            this.colTraineeName.OptionsColumn.AllowEdit = false;
            this.colTraineeName.OptionsColumn.FixedWidth = true;
            // 
            // colTrainerId
            // 
            this.colTrainerId.FieldName = "Trainer.Id";
            this.colTrainerId.Name = "colTrainerId";
            // 
            // colTrainerName
            // 
            resources.ApplyResources(this.colTrainerName, "colTrainerName");
            this.colTrainerName.FieldName = "Trainer.Person.FullName";
            this.colTrainerName.Name = "colTrainerName";
            this.colTrainerName.OptionsColumn.AllowEdit = false;
            this.colTrainerName.OptionsColumn.FixedWidth = true;
            // 
            // colTestTextId
            // 
            this.colTestTextId.FieldName = "TestText.Id";
            this.colTestTextId.Name = "colTestTextId";
            // 
            // colTextTitle
            // 
            resources.ApplyResources(this.colTextTitle, "colTextTitle");
            this.colTextTitle.FieldName = "TestText.Title";
            this.colTextTitle.Name = "colTextTitle";
            this.colTextTitle.OptionsColumn.AllowEdit = false;
            // 
            // colStartTime
            // 
            resources.ApplyResources(this.colStartTime, "colStartTime");
            this.colStartTime.ColumnEdit = this.teTestTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.FixedWidth = true;
            // 
            // teTestTime
            // 
            resources.ApplyResources(this.teTestTime, "teTestTime");
            this.teTestTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("teTestTime.Buttons"))))});
            this.teTestTime.Name = "teTestTime";
            // 
            // colEndTime
            // 
            resources.ApplyResources(this.colEndTime, "colEndTime");
            this.colEndTime.ColumnEdit = this.teTestTime;
            this.colEndTime.FieldName = "EndTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.OptionsColumn.AllowFocus = false;
            this.colEndTime.OptionsColumn.FixedWidth = true;
            // 
            // colModifiedDate
            // 
            resources.ApplyResources(this.colModifiedDate, "colModifiedDate");
            this.colModifiedDate.FieldName = "ModifiedDate";
            this.colModifiedDate.Name = "colModifiedDate";
            this.colModifiedDate.OptionsColumn.AllowEdit = false;
            this.colModifiedDate.OptionsColumn.AllowFocus = false;
            this.colModifiedDate.OptionsColumn.FixedWidth = true;
            // 
            // lcgRoot
            // 
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciRecords,
            this.lciTrainees});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.lcgRoot.Size = new System.Drawing.Size(824, 624);
            this.lcgRoot.TextVisible = false;
            // 
            // lciRecords
            // 
            this.lciRecords.Control = this.grdRecords;
            resources.ApplyResources(this.lciRecords, "lciRecords");
            this.lciRecords.Location = new System.Drawing.Point(0, 0);
            this.lciRecords.Name = "lciRecords";
            this.lciRecords.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciRecords.Size = new System.Drawing.Size(597, 612);
            this.lciRecords.TextSize = new System.Drawing.Size(0, 0);
            this.lciRecords.TextVisible = false;
            // 
            // lciTrainees
            // 
            this.lciTrainees.Control = this.nbcTrainees;
            this.lciTrainees.Location = new System.Drawing.Point(597, 0);
            this.lciTrainees.Name = "lciTrainees";
            this.lciTrainees.Size = new System.Drawing.Size(215, 612);
            resources.ApplyResources(this.lciTrainees, "lciTrainees");
            this.lciTrainees.TextSize = new System.Drawing.Size(0, 0);
            this.lciTrainees.TextVisible = false;
            // 
            // Records
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcRecords);
            this.Name = "Records";
            ((System.ComponentModel.ISupportInitialize)(this.lcRecords)).EndInit();
            this.lcRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbcTrainees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTestTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainees)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcRecords;
        private DevExpress.XtraGrid.GridControl grdRecords;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRecords;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraLayout.LayoutControlItem lciRecords;
        private DevExpress.XtraNavBar.NavBarControl nbcTrainees;
        private DevExpress.XtraNavBar.NavBarGroup nbgTrainees;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colGrade;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainees;
        private DevExpress.XtraGrid.Columns.GridColumn colTraineeId;
        private DevExpress.XtraGrid.Columns.GridColumn colTraineeName;
        private DevExpress.XtraGrid.Columns.GridColumn colTrainerId;
        private DevExpress.XtraGrid.Columns.GridColumn colTrainerName;
        private DevExpress.XtraGrid.Columns.GridColumn colTestTextId;
        private DevExpress.XtraGrid.Columns.GridColumn colTextTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit leGrade;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit teTestTime;
    }
}
