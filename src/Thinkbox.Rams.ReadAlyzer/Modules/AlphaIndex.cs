﻿namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public class AlphaIndex
    {
        #region PROPERTIES
        public static AlphaIndex all;
        public static AlphaIndex All
        {
            get
            {
                if (all == null)
                {
                    all = new AlphaIndex()
                    {
                        Count = 0,
                        Index = "All"
                    };
                }
                return all;
            }
        }
        public static AlphaIndex alphaNumber;
        public static AlphaIndex AlphaNumber
        {
            get
            {
                if (alphaNumber == null)
                {
                    alphaNumber = new AlphaIndex()
                    {
                        Count = 0,
                        Index = "0-9"
                    };
                }
                return alphaNumber;
            }
        }
        public int Count { get; set; }
        public string Index { get; set; }
        #endregion

        #region METHODS
        public static string Group(string text)
        {
            if (text.Length == 1)
            {
                char c = text[0];
                if (char.IsNumber(c)) return "0-9";
            }
            return text.ToUpper();
        }

        public bool Match(string text)
        {
            if (Group(text) == Index) return true;
            return false;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Index, Count);
        }
        #endregion
    }

    public delegate string GetAlphaMethod(object target);
}
