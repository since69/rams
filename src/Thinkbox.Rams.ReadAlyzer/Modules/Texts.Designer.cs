﻿namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    partial class Texts
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Texts));
            this.lcTexts = new DevExpress.XtraLayout.LayoutControl();
            this.grdTexts = new DevExpress.XtraGrid.GridControl();
            this.gvTexts = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leLevel = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCulture = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leCulture = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFontName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFontSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTexts = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcTexts)).BeginInit();
            this.lcTexts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTexts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTexts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCulture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTexts)).BeginInit();
            this.SuspendLayout();
            // 
            // lcTexts
            // 
            this.lcTexts.AllowCustomization = false;
            this.lcTexts.Controls.Add(this.grdTexts);
            resources.ApplyResources(this.lcTexts, "lcTexts");
            this.lcTexts.Name = "lcTexts";
            this.lcTexts.Root = this.lcgRoot;
            // 
            // grdTexts
            // 
            resources.ApplyResources(this.grdTexts, "grdTexts");
            this.grdTexts.MainView = this.gvTexts;
            this.grdTexts.Name = "grdTexts";
            this.grdTexts.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leLevel,
            this.leCulture});
            this.grdTexts.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTexts});
            // 
            // gvTexts
            // 
            this.gvTexts.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colCulture,
            this.colLevel,
            this.colTitle,
            this.colFontName,
            this.colFontSize,
            this.colModifiedDate});
            this.gvTexts.GridControl = this.grdTexts;
            this.gvTexts.Name = "gvTexts";
            this.gvTexts.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.True;
            this.gvTexts.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvTexts.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvTexts.OptionsBehavior.Editable = false;
            this.gvTexts.OptionsDetail.EnableMasterViewMode = false;
            this.gvTexts.OptionsFind.AlwaysVisible = true;
            this.gvTexts.OptionsPrint.PrintHorzLines = false;
            this.gvTexts.OptionsPrint.PrintVertLines = false;
            this.gvTexts.OptionsView.ShowGroupedColumns = true;
            this.gvTexts.OptionsView.ShowGroupPanel = false;
            this.gvTexts.OptionsView.ShowIndicator = false;
            this.gvTexts.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvTexts.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvTexts_RowCellClick);
            this.gvTexts.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvTexts_FocusedRowChanged);
            this.gvTexts.FocusedRowObjectChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventHandler(this.gvTexts_FocusedRowObjectChanged);
            this.gvTexts.ColumnFilterChanged += new System.EventHandler(this.gvTexts_ColumnFilterChanged);
            this.gvTexts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvTexts_KeyDown);
            // 
            // colId
            // 
            this.colId.ColumnEdit = this.leLevel;
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // leLevel
            // 
            resources.ApplyResources(this.leLevel, "leLevel");
            this.leLevel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("leLevel.Buttons"))))});
            this.leLevel.Name = "leLevel";
            this.leLevel.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colCulture
            // 
            resources.ApplyResources(this.colCulture, "colCulture");
            this.colCulture.ColumnEdit = this.leCulture;
            this.colCulture.FieldName = "Culture.Id";
            this.colCulture.Name = "colCulture";
            // 
            // leCulture
            // 
            resources.ApplyResources(this.leCulture, "leCulture");
            this.leCulture.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("leCulture.Buttons"))))});
            this.leCulture.Name = "leCulture";
            this.leCulture.View = this.gridView1;
            // 
            // gridView1
            // 
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // colLevel
            // 
            resources.ApplyResources(this.colLevel, "colLevel");
            this.colLevel.ColumnEdit = this.leLevel;
            this.colLevel.FieldName = "Level";
            this.colLevel.Name = "colLevel";
            // 
            // colTitle
            // 
            resources.ApplyResources(this.colTitle, "colTitle");
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colFontName
            // 
            resources.ApplyResources(this.colFontName, "colFontName");
            this.colFontName.FieldName = "FontName";
            this.colFontName.Name = "colFontName";
            this.colFontName.OptionsColumn.AllowEdit = false;
            // 
            // colFontSize
            // 
            resources.ApplyResources(this.colFontSize, "colFontSize");
            this.colFontSize.FieldName = "FontSize";
            this.colFontSize.Name = "colFontSize";
            this.colFontSize.OptionsColumn.AllowEdit = false;
            // 
            // colModifiedDate
            // 
            resources.ApplyResources(this.colModifiedDate, "colModifiedDate");
            this.colModifiedDate.FieldName = "ModifiedDate";
            this.colModifiedDate.Name = "colModifiedDate";
            this.colModifiedDate.OptionsColumn.AllowFocus = false;
            this.colModifiedDate.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // lcgRoot
            // 
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTexts});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.lcgRoot.Size = new System.Drawing.Size(824, 624);
            this.lcgRoot.TextVisible = false;
            // 
            // lciTexts
            // 
            this.lciTexts.Control = this.grdTexts;
            this.lciTexts.Location = new System.Drawing.Point(0, 0);
            this.lciTexts.Name = "lciTexts";
            this.lciTexts.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciTexts.Size = new System.Drawing.Size(812, 612);
            this.lciTexts.TextSize = new System.Drawing.Size(0, 0);
            this.lciTexts.TextVisible = false;
            // 
            // Texts
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcTexts);
            this.Name = "Texts";
            ((System.ComponentModel.ISupportInitialize)(this.lcTexts)).EndInit();
            this.lcTexts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTexts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTexts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leCulture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTexts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcTexts;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraGrid.GridControl grdTexts;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTexts;
        private DevExpress.XtraLayout.LayoutControlItem lciTexts;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit leLevel;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colCulture;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit leCulture;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colFontName;
        private DevExpress.XtraGrid.Columns.GridColumn colFontSize;
    }
}
