﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public class ModulesNavigator
    {
        #region VARIABLES
        internal RibbonControl ribbon;
        internal PanelControl panel;
        internal string currentModuleName = string.Empty;
        #endregion

        #region PROPERTIES
        public BaseModule CurrentModule
        {
            get
            {
                if (panel.Controls.Count == 0)
                    return null;
                return panel.Controls[0] as BaseModule;
            }
        }
        #endregion

        #region CONSTRUCTOR
        public ModulesNavigator(RibbonControl ribbon, PanelControl panel)
        {
            this.ribbon = ribbon;
            this.panel = panel;
        }
        #endregion

        #region METHODS
        public void ChangeSelectedItem(NavBarItemLink link, object moduleData)
        {
            bool allowSetVisiblePage = true;
            NavBarGroupTagObject groupObject = link.Item.Tag as NavBarGroupTagObject;
            if (groupObject == null) return;

            List<RibbonPage> deferredPagesToShow = new List<RibbonPage>();
            foreach (RibbonPage page in ribbon.Pages)
            {
                if (!string.IsNullOrEmpty(string.Format("{0}", page.Tag)))
                {
                    bool isPageVisible = groupObject.Name.Equals(page.Tag);
                    if (isPageVisible != page.Visible && isPageVisible)
                        deferredPagesToShow.Add(page);
                    else
                        page.Visible = isPageVisible;
                }
                if (page.Visible && allowSetVisiblePage)
                {
                    //page.Text = "Home";
                    ribbon.SelectedPage = page;
                    allowSetVisiblePage = false;
                }
            }

            bool firstShow = groupObject.Module == null;
            if (firstShow)
            {
                ConstructorInfo ci = groupObject.ModuleType.GetConstructor(Type.EmptyTypes);
                if (ci != null)
                {
                    try
                    {
                        groupObject.Module = ci.Invoke(null) as BaseModule;
                        groupObject.Module.InitializeModule(ribbon, moduleData);
                        currentModuleName = link.Caption;
                    }
                    catch (Exception e)
                    {
                        var entryAsm = Assembly.GetEntryAssembly();
                        string msg = string.Format("Error on Showing Module: {0}\r\nPrevModule: {1}\r\nStartUp: {2}",
                            link.Caption, currentModuleName, (entryAsm != null ? entryAsm.Location : string.Empty));
                        throw new ApplicationException(msg, e);
                    }
                }
            }

            ribbon.ColorScheme = groupObject.RibbonScheme;
            foreach (RibbonPage page in deferredPagesToShow)
            {
                page.Visible = true;
            }
            foreach (RibbonPage page in ribbon.Pages)
            {
                if (page.Visible)
                {
                    ribbon.SelectedPage = page;
                    break;
                }
            }

            if (groupObject.Module != null)
            {
                if (panel.Controls.Count > 0)
                {
                    BaseModule currentModule = panel.Controls[0] as BaseModule;
                    if (currentModule != null)
                        currentModule.HideModule();
                }
                panel.Controls.Clear();
                panel.Controls.Add(groupObject.Module);
                groupObject.Module.Dock = DockStyle.Fill;
                groupObject.Module.ShowModule(firstShow);
            }
        }
        #endregion
    }
}
