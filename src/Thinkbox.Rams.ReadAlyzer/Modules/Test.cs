﻿//
// Thinkbox.Rams.ReadAlyzer.Modules.Test
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using log4net;
using Thinkbox.Rams.Device;
using Thinkbox.Rams.ReadAlyzer.Forms;
using Thinkbox.Rams.ReadAlyzer.Helpers;

namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    public partial class Test : BaseModule
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        
        private List<GazeData> records = new List<GazeData>();
        private string msg = string.Empty;
        #endregion

        #region PROPERTIES
        private TestStatus prevStatus = TestStatus.Ready;
        private TestStatus status = TestStatus.Ready;
        private TestStatus Status
        {
            get { return status; }
            set
            {
                prevStatus = status;
                status = value;
                log.DebugFormat("Test status changed {0} to {1}.", prevStatus, status);
            }
        }
        
        public Data.Test CurrentTest { get; private set; } = new Data.Test();

        public override string ModuleName
        {
            get { return Properties.Resources.TestModuleName; }
        }
        #endregion

        #region CONSTRUCTORS
        public Test()
        {
            InitializeComponent();
            Tracker.GazeUpdate += new GazeUpdateEventHandler(OnGazeUpdate);
        }
        #endregion

        #region GENERAL METHODS
        private void AnalysisData()
        {
            //AnalsysTestForm form = new AnalsysTestForm();
            TestResultForm form = new TestResultForm(CurrentTest, OwnerForm.Ribbon);
            form.ShowDialog();
        }

        private void AskQuestionAboutText()
        {
            TextQuestionForm form = new TextQuestionForm(CurrentTest.Answers);
            form.ShowDialog();
        }

        private void Stop()
        {
            if (Status != TestStatus.Started)
                return;

            Status = TestStatus.Completed;
            Tracker.Provider.Stop();

            Tracker.Provider.CloseEyeTracker();

            string fileName = string.Format("{0}-G{1:D2}-{2}.rec",
                CurrentTest.StartTime.ToString("yyyyMMddHHmmss"),
                CurrentTest.Grade,
                CurrentTest.Trainee.Person.FullName);

            CurrentTest.EndTime = DateTime.UtcNow;
            CurrentTest.RecordFile = fileName;
            AskQuestionAboutText();

            if (FileHelper.ExportRecord(Tracker.RecordingFolder, fileName, records))
            {
                DataHelper.AddTest(CurrentTest);
                if (CurrentTest.Id != int.MinValue)
                    DataHelper.AddAnswer(CurrentTest);
            }
        }

        private void Start()
        {
            if (Status != TestStatus.Prepared)
                return;

            if (Tracker.Status != TrackerStatus.Ready)
            {
                msg = "Tracker is not ready.";
                log.Debug(msg);
                MessageBox.Show(msg, "ReadAlyzer", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }
            else
                Tracker.Provider.Start();

            log.Debug("Tracker started.");
            log.Debug("Open eye tracker.");
            Tracker.Provider.OpenEyeTracker();
            log.Debug("Eye tracker opened");

            DialogResult result = DialogResult.Yes;
            MessageBox.Show("Do you want to start press OK button.", "ReadAlyzer",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            result = DialogResult.Yes;
            while (result == DialogResult.Yes)
            {
                Tracker.Provider.Calibrate();
                result = MessageBox.Show("Do you want to recalibration?",
                    "ReadAlyzer", MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Question);
            }

            result = MessageBox.Show("Do you want to start measuring?",
                "ReadAlyzer", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                System.Media.SoundPlayer player = 
                    new System.Media.SoundPlayer(@"Sounds\Tone.wav");
                player.Play();

                if (CurrentTest.StartTime == DateTime.MinValue)
                    CurrentTest.StartTime = DateTime.UtcNow;
                Status = TestStatus.Started;
            }
        }

        private void Cancel()
        {
            if (Status == TestStatus.Started || Status == TestStatus.Prepared)
            {
                Tracker.Provider.CloseEyeTracker();

                if (Status == TestStatus.Started)
                    Tracker.Provider.Stop();
                Status = TestStatus.Ready;
                InitializeTest();
            }
        }

        private void AddTest()
        {
            if (Status != TestStatus.Completed
                && Status != TestStatus.Ready)
            {
                msg = "Test is not ready or previous test is not completed.";
                log.Debug(msg);
                MessageBox.Show(msg, "ReadAlyzer", MessageBoxButtons.OK, 
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (Tracker.Status != TrackerStatus.Ready)
            {
                msg = "Tracker is not ready.";
                log.Debug(msg);
                MessageBox.Show(msg, "ReadAlyzer", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }

            if (Status == TestStatus.Completed)
                InitializeTest();

            DialogResult result = DialogResult.Cancel;
            Cursor.Current = Cursors.WaitCursor;
            using (EditTestForm f = new EditTestForm(CurrentTest))
                result = f.ShowDialog(OwnerForm);
            if (result == DialogResult.OK)
            {
                List<Data.TestQuestion> questions =
                    DataHelper.GetTestQuestions(CurrentTest.TestText.Id);
                foreach (Data.TestQuestion q in questions)
                    CurrentTest.Answers.Add(new Data.TestAnswer() { Question = q });
                Status = TestStatus.Prepared;
            }
            Cursor.Current = Cursors.Default;
        }

        private void InitializeTest()
        {
            if (records.Count > 0)
            {
                ucData.Clear();
                ucInformation.Clear();
            }
            CurrentTest = new Data.Test();
        }

        private void UpdateCurrentTest()
        {
            ucInformation.Test = CurrentTest;
            OwnerForm.EnableEditTest(status);
        }
        #endregion

        #region OVERRIDE METHODS
        internal override void ShowModule(bool firstShow)
        {
            base.ShowModule(firstShow);

            if (firstShow)
            {
                UpdateCurrentTest();
            }
        }

        protected internal override void ButtonClick(string tag)
        {
            switch (tag)
            {
                case Tags.TestNew: AddTest(); break;
                case Tags.TestStart: Start(); break;
                case Tags.TestStop: Stop(); break;
                case Tags.TestCancel: Cancel(); break;
                case Tags.TestAnalysis: AnalysisData(); break;
                default: base.ButtonClick(tag); break;
            }

            UpdateCurrentTest();
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            if (Status == TestStatus.Started)
            {
                Tracker.Provider.Stop();
            }
        }
        #endregion

        #region EVENTS
        private void OnGazeUpdate(object sender, GazeUpdateEventArgs args)
        {
            if (Status != TestStatus.Started)
                return;

            GazeData newGazeData = args.GazeData;

            records.Add(newGazeData);
            BeginInvoke(new Action(() => ucInformation.UpdateGazeData(newGazeData)), null);
            BeginInvoke(new Action(() => ucData.UpdateGazeData(newGazeData)), null);

            //log.DebugFormat("E:{0,5}, A:{1},{2}, L:{3},{4}, R:{5},{6}",
            //    newGazeData.Elapsed, newGazeData.AverageX, newGazeData.AverageY,
            //    newGazeData.LeftX, newGazeData.LeftY, 
            //    newGazeData.RightX, newGazeData.RightY);
        }
        #endregion
    }
}
