﻿namespace Thinkbox.Rams.ReadAlyzer.Modules
{
    partial class Trainees
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Trainees));
            this.lcTrainees = new DevExpress.XtraLayout.LayoutControl();
            this.grdIndex = new DevExpress.XtraGrid.GridControl();
            this.gvIndex = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grdTrainees = new DevExpress.XtraGrid.GridControl();
            this.gvTrainees = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbGender = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.colEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPromotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModifiedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lcgRoot = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciTrainees = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciIndex = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.lcTrainees)).BeginInit();
            this.lcTrainees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrainees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTrainees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbGender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // lcTrainees
            // 
            this.lcTrainees.AllowCustomization = false;
            this.lcTrainees.Appearance.DisabledLayoutGroupCaption.ForeColor = ((System.Drawing.Color)(resources.GetObject("lcTrainees.Appearance.DisabledLayoutGroupCaption.ForeColor")));
            this.lcTrainees.Appearance.DisabledLayoutGroupCaption.Options.UseForeColor = true;
            this.lcTrainees.Appearance.DisabledLayoutItem.ForeColor = ((System.Drawing.Color)(resources.GetObject("lcTrainees.Appearance.DisabledLayoutItem.ForeColor")));
            this.lcTrainees.Appearance.DisabledLayoutItem.Options.UseForeColor = true;
            this.lcTrainees.Controls.Add(this.grdIndex);
            this.lcTrainees.Controls.Add(this.grdTrainees);
            resources.ApplyResources(this.lcTrainees, "lcTrainees");
            this.lcTrainees.Name = "lcTrainees";
            this.lcTrainees.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(717, 447, 450, 350);
            this.lcTrainees.Root = this.lcgRoot;
            // 
            // grdIndex
            // 
            resources.ApplyResources(this.grdIndex, "grdIndex");
            this.grdIndex.MainView = this.gvIndex;
            this.grdIndex.Name = "grdIndex";
            this.grdIndex.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvIndex});
            // 
            // gvIndex
            // 
            this.gvIndex.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gvIndex.GridControl = this.grdIndex;
            this.gvIndex.Name = "gvIndex";
            this.gvIndex.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvIndex.OptionsBehavior.Editable = false;
            this.gvIndex.OptionsDetail.EnableMasterViewMode = false;
            this.gvIndex.OptionsFind.AllowFindPanel = false;
            this.gvIndex.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvIndex.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvIndex.OptionsView.ShowColumnHeaders = false;
            this.gvIndex.OptionsView.ShowGroupPanel = false;
            this.gvIndex.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvIndex.OptionsView.ShowIndicator = false;
            this.gvIndex.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            // 
            // grdTrainees
            // 
            resources.ApplyResources(this.grdTrainees, "grdTrainees");
            this.grdTrainees.MainView = this.gvTrainees;
            this.grdTrainees.Name = "grdTrainees";
            this.grdTrainees.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rcbGender});
            this.grdTrainees.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTrainees});
            // 
            // gvTrainees
            // 
            this.gvTrainees.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colPersonType,
            this.colNameStyle,
            this.colTitle,
            this.colFirstname,
            this.colMiddleName,
            this.colLastName,
            this.colSuffix,
            this.colName,
            this.colGender,
            this.colEmail,
            this.colPhone,
            this.colEmailPromotion,
            this.colBirthDate,
            this.colModifiedDate});
            this.gvTrainees.GridControl = this.grdTrainees;
            this.gvTrainees.Name = "gvTrainees";
            this.gvTrainees.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.True;
            this.gvTrainees.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.gvTrainees.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvTrainees.OptionsBehavior.Editable = false;
            this.gvTrainees.OptionsDetail.EnableMasterViewMode = false;
            this.gvTrainees.OptionsFind.AlwaysVisible = true;
            this.gvTrainees.OptionsPrint.PrintHorzLines = false;
            this.gvTrainees.OptionsPrint.PrintVertLines = false;
            this.gvTrainees.OptionsView.ShowGroupedColumns = true;
            this.gvTrainees.OptionsView.ShowGroupPanel = false;
            this.gvTrainees.OptionsView.ShowIndicator = false;
            this.gvTrainees.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gvTrainees.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gvTrainees_RowCellClick);
            this.gvTrainees.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvTrainees_FocusedRowChanged);
            this.gvTrainees.FocusedRowObjectChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowObjectChangedEventHandler(this.gvTrainees_FocusedRowObjectChanged);
            this.gvTrainees.ColumnFilterChanged += new System.EventHandler(this.gvTrainees_ColumnFilterChanged);
            this.gvTrainees.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gvTrainees_KeyDown);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowFocus = false;
            resources.ApplyResources(this.colId, "colId");
            // 
            // colPersonType
            // 
            resources.ApplyResources(this.colPersonType, "colPersonType");
            this.colPersonType.FieldName = "PersonType";
            this.colPersonType.Name = "colPersonType";
            this.colPersonType.OptionsColumn.AllowFocus = false;
            // 
            // colNameStyle
            // 
            resources.ApplyResources(this.colNameStyle, "colNameStyle");
            this.colNameStyle.FieldName = "NameStyle";
            this.colNameStyle.Name = "colNameStyle";
            this.colNameStyle.OptionsColumn.AllowFocus = false;
            // 
            // colTitle
            // 
            resources.ApplyResources(this.colTitle, "colTitle");
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowFocus = false;
            // 
            // colFirstname
            // 
            resources.ApplyResources(this.colFirstname, "colFirstname");
            this.colFirstname.FieldName = "FirstName";
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowFocus = false;
            // 
            // colMiddleName
            // 
            resources.ApplyResources(this.colMiddleName, "colMiddleName");
            this.colMiddleName.FieldName = "MiddleName";
            this.colMiddleName.Name = "colMiddleName";
            this.colMiddleName.OptionsColumn.AllowFocus = false;
            // 
            // colLastName
            // 
            resources.ApplyResources(this.colLastName, "colLastName");
            this.colLastName.FieldName = "LastName";
            this.colLastName.Name = "colLastName";
            this.colLastName.OptionsColumn.AllowFocus = false;
            // 
            // colSuffix
            // 
            resources.ApplyResources(this.colSuffix, "colSuffix");
            this.colSuffix.FieldName = "Suffix";
            this.colSuffix.Name = "colSuffix";
            this.colSuffix.OptionsColumn.AllowFocus = false;
            // 
            // colName
            // 
            resources.ApplyResources(this.colName, "colName");
            this.colName.FieldName = "Person.FullName";
            this.colName.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical;
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colGender
            // 
            this.colGender.ColumnEdit = this.rcbGender;
            this.colGender.FieldName = "Gender";
            this.colGender.Name = "colGender";
            this.colGender.OptionsColumn.AllowFocus = false;
            this.colGender.OptionsColumn.AllowMove = false;
            this.colGender.OptionsColumn.FixedWidth = true;
            this.colGender.OptionsColumn.ShowCaption = false;
            this.colGender.OptionsFilter.AllowAutoFilter = false;
            resources.ApplyResources(this.colGender, "colGender");
            // 
            // rcbGender
            // 
            resources.ApplyResources(this.rcbGender, "rcbGender");
            this.rcbGender.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("rcbGender.Buttons"))))});
            this.rcbGender.Name = "rcbGender";
            // 
            // colEmail
            // 
            resources.ApplyResources(this.colEmail, "colEmail");
            this.colEmail.Name = "colEmail";
            this.colEmail.OptionsColumn.AllowFocus = false;
            this.colEmail.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colPhone
            // 
            resources.ApplyResources(this.colPhone, "colPhone");
            this.colPhone.Name = "colPhone";
            this.colPhone.OptionsColumn.AllowFocus = false;
            this.colPhone.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains;
            // 
            // colEmailPromotion
            // 
            resources.ApplyResources(this.colEmailPromotion, "colEmailPromotion");
            this.colEmailPromotion.FieldName = "EmailPromotion";
            this.colEmailPromotion.Name = "colEmailPromotion";
            this.colEmailPromotion.OptionsColumn.AllowFocus = false;
            // 
            // colBirthDate
            // 
            resources.ApplyResources(this.colBirthDate, "colBirthDate");
            this.colBirthDate.FieldName = "BirthDate";
            this.colBirthDate.Name = "colBirthDate";
            this.colBirthDate.OptionsColumn.AllowFocus = false;
            // 
            // colModifiedDate
            // 
            resources.ApplyResources(this.colModifiedDate, "colModifiedDate");
            this.colModifiedDate.FieldName = "ModifiedDate";
            this.colModifiedDate.Name = "colModifiedDate";
            this.colModifiedDate.OptionsColumn.AllowFocus = false;
            // 
            // lcgRoot
            // 
            this.lcgRoot.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgRoot.GroupBordersVisible = false;
            this.lcgRoot.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciTrainees,
            this.lciIndex});
            this.lcgRoot.Location = new System.Drawing.Point(0, 0);
            this.lcgRoot.Name = "lcgRoot";
            this.lcgRoot.Padding = new DevExpress.XtraLayout.Utils.Padding(6, 6, 6, 6);
            this.lcgRoot.Size = new System.Drawing.Size(824, 624);
            this.lcgRoot.TextVisible = false;
            // 
            // lciTrainees
            // 
            this.lciTrainees.Control = this.grdTrainees;
            this.lciTrainees.Location = new System.Drawing.Point(0, 0);
            this.lciTrainees.Name = "lciTrainees";
            this.lciTrainees.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.lciTrainees.Size = new System.Drawing.Size(752, 612);
            this.lciTrainees.TextSize = new System.Drawing.Size(0, 0);
            this.lciTrainees.TextVisible = false;
            // 
            // lciIndex
            // 
            this.lciIndex.Control = this.grdIndex;
            this.lciIndex.Location = new System.Drawing.Point(752, 0);
            this.lciIndex.MaxSize = new System.Drawing.Size(60, 0);
            this.lciIndex.MinSize = new System.Drawing.Size(60, 24);
            this.lciIndex.Name = "lciIndex";
            this.lciIndex.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 0, 0, 0);
            this.lciIndex.Size = new System.Drawing.Size(60, 612);
            this.lciIndex.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lciIndex.TextSize = new System.Drawing.Size(0, 0);
            this.lciIndex.TextVisible = false;
            // 
            // Trainees
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lcTrainees);
            this.Name = "Trainees";
            ((System.ComponentModel.ISupportInitialize)(this.lcTrainees)).EndInit();
            this.lcTrainees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdTrainees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTrainees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbGender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgRoot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciTrainees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciIndex)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl lcTrainees;
        private DevExpress.XtraLayout.LayoutControlGroup lcgRoot;
        private DevExpress.XtraGrid.GridControl grdTrainees;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTrainees;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone;
        private DevExpress.XtraLayout.LayoutControlItem lciTrainees;
        private DevExpress.XtraGrid.GridControl grdIndex;
        private DevExpress.XtraGrid.Views.Grid.GridView gvIndex;
        private DevExpress.XtraLayout.LayoutControlItem lciIndex;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colLastName;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleName;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colNameStyle;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colSuffix;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPromotion;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthDate;
        private DevExpress.XtraGrid.Columns.GridColumn colModifiedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rcbGender;
    }
}
