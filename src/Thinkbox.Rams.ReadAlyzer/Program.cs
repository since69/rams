﻿using System;
using System.Windows.Forms;
using Thinkbox.Rams.Configuration;

namespace Thinkbox.Rams.ReadAlyzer
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] arguments)
        {
            #region WINFORM SETTINGS
            //string cultureName = Properties.Settings.Default.Culture.ToString();
            string cultureName = Configurator.ReadAlyzerSettings.General.Culture;
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(cultureName);
            System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
            #endregion

            #region DEVEXPRESS SETTINGS
            DevExpress.Utils.LocalizationHelper.SetCurrentCulture(arguments);
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Utils.AppearanceObject.DefaultFont = new System.Drawing.Font("Segoe UI", 8);
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("Office 2016 Colorful");
            //DevExpress.LookAndFeel.UserLookAndFeel.Default.SetSkinStyle("DevExpress Dark Style");
            DevExpress.Skins.SkinManager.EnableFormSkins();
            #endregion

            #region VS GENERATED
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
            #endregion
        }
    }
}
