﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;

namespace Thinkbox.Rams.ReadAlyzer.Helpers
{
    public class EditHelper
    {
        public static void InitCultureLookupEdit(RepositoryItemGridLookUpEdit edit)
        {
            edit.DataSource = DataHelper.GetCultures();
            edit.DisplayMember = "Name";
            edit.ValueMember = "Id";
        }

        public static void InitGradeLookupEdit(RepositoryItemGridLookUpEdit edit)
        {
            // Create datatable.
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Value", typeof(int)));

            // Create datarow and add to datatable.
            for (int i = 0; i < 10; i++)
            {
                DataRow row = dt.NewRow();
                row["Name"] = string.Format("Grade {0}", i + 1);
                row["Value"] = i + 1;
                dt.Rows.Add(row);
            }

            // Bind to lookup edit.
            edit.DataSource = dt;
            edit.DisplayMember = "Name";
            edit.ValueMember = "Value";
        }

        public static void InitLevelLookupEdit(RepositoryItemGridLookUpEdit edit)
        {
            // Create datatable.
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Value", typeof(int)));

            // Create datarow and add to datatable.
            for (int i = 0; i < 10; i++)
            {
                DataRow row = dt.NewRow();
                row["Name"] = string.Format("Level {0}", i + 1);
                row["Value"] = i + 1;
                dt.Rows.Add(row);
            }

            // Bind to lookup edit.
            edit.DataSource = dt;
            edit.DisplayMember = "Name";
            edit.ValueMember = "Value";
        }

        public static void InitTrainerLookupEdit(RepositoryItemLookUpEdit edit)
        {
            //edit.ShowHeader = false;
            edit.DataSource = DataHelper.Trainers;
            edit.Columns.Add(new LookUpColumnInfo("Person.FullName", "Name"));
            edit.DisplayMember = "Person.FullName";
            edit.ValueMember = "Id";
        }

        public static void InitTraineeLookupEdit(RepositoryItemLookUpEdit edit)
        {
            edit.DataSource = DataHelper.Trainees;
            edit.Columns.Add(new LookUpColumnInfo("Person.FullName", "Name"));
            edit.Columns.Add(new LookUpColumnInfo("BirthDate", "Birth Date"));
            edit.DisplayMember = "Person.FullName";
            edit.ValueMember = "Id";
        }

        public static List<Data.TestText> GetTextLookupEditDataSource(int level)
        {
            var texts = from text in DataHelper.Texts
                        where text.Level == level
                        select text;
            return texts.ToList();
        }

        public static void InitTextLookupEdit(RepositoryItemLookUpEdit edit)
        {
            edit.DataSource = GetTextLookupEditDataSource(1);
            edit.Columns.Add(new LookUpColumnInfo("Level", 10));
            edit.Columns.Add(new LookUpColumnInfo("Title"));
            edit.DisplayMember = "Title";
            edit.ValueMember = "Id";
        }

        public static void InitGradeComboBox(ComboBox edit)
        {
            // Create datatable.
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Value", typeof(int)));

            // Create datarow and add to datatable.
            for (int i = 0; i < 10; i++)
            {
                DataRow row = dt.NewRow();
                row["Name"] = string.Format("Grade {0}", i + 1);
                row["Value"] = i + 1;
                dt.Rows.Add(row);
            }

            // Bind to combobox.
            edit.DataSource = dt;
            edit.DisplayMember = "Name";
            edit.ValueMember = "Value";
        }

        public static void InitLevelComboBox(ComboBox edit)
        {
            // Create datatable.
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Value", typeof(int)));

            // Create datarow and add to datatable.
            for (int i = 0; i < 10; i++)
            {
                DataRow row = dt.NewRow();
                row["Name"] = string.Format("Level {0}", i + 1);
                row["Value"] = i + 1;
                dt.Rows.Add(row);
            }

            // Bind to combobox.
            edit.DataSource = dt;
            edit.DisplayMember = "Name";
            edit.ValueMember = "Value";
        }

        public static void InitNameStyleComboBox(ComboBox edit)
        {
            // Create datatable.
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Name", typeof(string)));
            dt.Columns.Add(new DataColumn("Value", typeof(bool)));

            // Create datarow and add to datatable.
            System.Data.DataRow row = dt.NewRow();
            row["Name"] = Properties.Resources.NameStyleWestern;
            row["Value"] = false;
            dt.Rows.Add(row);
            row = dt.NewRow();
            row["Name"] = Properties.Resources.NameStyleEastern;
            row["Value"] = true;
            dt.Rows.Add(row);

            // Bind to lookupedit.
            edit.DataSource = dt;
            edit.DisplayMember = "Name";
            edit.ValueMember = "Value";
        }

        public static string GetGenderFlag(Enums.Gender gender)
        {
            if (gender == Enums.Gender.Female) return "F";
            return "M";
        }

        public static void InitGenderComboBox(RepositoryItemImageComboBox edit)
        {
            ImageCollection iCollection = new ImageCollection();
            iCollection.AddImage(Properties.Resources.Mr_16x16);
            iCollection.AddImage(Properties.Resources.Ms_16x16);

            edit.Items.Add(new ImageComboBoxItem(Properties.Resources.Male, 
                GetGenderFlag(Enums.Gender.Male), 0));
            edit.Items.Add(new ImageComboBoxItem(Properties.Resources.Female, 
                GetGenderFlag(Enums.Gender.Female), 1));
            edit.SmallImages = iCollection;
        }
    }
}
