﻿using System;

namespace Thinkbox.Rams.ReadAlyzer.Helpers
{
    public class StringHelper
    {
        public static string ConvertToString(DateTime dt)
        {
            string dateTime = dt.ToString("yyyy-M-dd H:m:ff");
            return dateTime;
        }
    }
}
