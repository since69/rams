﻿using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Controls;
using DevExpress.XtraGrid.Views.Base;

namespace Thinkbox.Rams.ReadAlyzer.Helpers
{
    public class GridHelper
    {
        public static void GridViewFocusObject(ColumnView view, object obj)
        {
            if (obj == null) return;
            int oldFocusedRowHandle = view.FocusedRowHandle;
            for (int i = 0; i < view.DataRowCount; ++i)
            {
                object rowObj = view.GetRow(i) as object;
                if (rowObj == null) continue;
                if (ReferenceEquals(obj, rowObj))
                {
                    if (i == oldFocusedRowHandle)
                        view.FocusedRowHandle = GridControl.InvalidRowHandle;
                    view.FocusedRowHandle = i;
                    break;
                }
            }
        }

        public static void SetFindControlImages(GridControl grid)
        {
            FindControl findCtrl = null;
            foreach (Control ctrl in grid.Controls)
            {
                findCtrl = ctrl as FindControl;
                if (findCtrl != null) break;
            }
            if (findCtrl != null)
            {
                findCtrl.FindButton.Image = Properties.Resources.Find_16x16;
                findCtrl.ClearButton.Image = Properties.Resources.Clear_16x16;
                findCtrl.CalcButtonsBestFit();
            }
        }
    }
}
