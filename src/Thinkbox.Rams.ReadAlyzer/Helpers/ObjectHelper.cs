﻿using System;

namespace Thinkbox.Rams.ReadAlyzer.Helpers
{
    public class ObjectHelper
    {
        public static void StartProcess(string processName)
        {
            try
            {
                System.Diagnostics.Process.Start(processName);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show(
                    ex.Message, Properties.Resources.Error, 
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    }
}
