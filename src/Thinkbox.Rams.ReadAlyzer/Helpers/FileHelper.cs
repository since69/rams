﻿//
// Thinkbox.Rams.ReadAlyzer.FileHelper
//
// Authors:
//  Joohyoung Kim
//  
//  (C) 2017 Kotech Co. Ltd
//

using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net;

namespace Thinkbox.Rams.ReadAlyzer.Helpers
{
    public class FileHelper
    {
        #region VARIABLES
        private static readonly ILog log = LogManager.GetLogger(typeof(FileHelper));
        #endregion

        #region METHODS
        public static bool FileExist(string srcPath)
        {
            return File.Exists(srcPath);
        }
        
        public static bool DirectoryExist(string srcPath)
        {
            return Directory.Exists(srcPath);
        }

        public static DirectoryInfo CreateDirectory(string path)
        {
            DirectoryInfo di = Directory.CreateDirectory(path);
            log.InfoFormat("{0} created at {1}", path, di.CreationTimeUtc);
            return di;
        }

        public static string RemoveExtension(string file, string extension)
        {
            int index = file.IndexOf(extension,
                file.Length - extension.Length);
            if (index > 0)
                file = file.Remove(index);
            return file;
        }

        public static bool ExportRecord(string folderName, string fileName, 
            List<Device.GazeData> records)
        {
            bool exported = false;
            FileStream fs = null;

            try
            {
                if (!DirectoryExist(folderName))
                    CreateDirectory(folderName);

                fs = new FileStream(string.Format(@"{0}\{1}",
                     folderName, fileName),
                     FileMode.Create);

                using (StreamWriter sw = new StreamWriter(fs, Encoding.Default))
                {
                    fs = null;
                    string formatString = "{0},{1:000.00000},{2:000.00000},{3:000.00000},";
                    formatString += "{4:000.00000},{5:000.00000},{6:000.00000}\n";
                    foreach (Device.GazeData record in records)
                    {
                        sw.Write(string.Format(formatString, record.Elapsed, 
                            record.AverageX, record.AverageY,
                            record.LeftX, record.LeftY, 
                            record.RightX, record.RightY));
                    }
                }
            }
            catch (IOException ex)
            {
                log.ErrorFormat("Test record export failed. {0}",
                    ex.Message);
            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
            }

            return exported;
        }

        public static List<Device.GazeData> ImportRecord(string folderName, 
            string fileName)
        {
            string fullName = string.Format(@"{0}\{1}", folderName, fileName);
            if (!FileExist(fullName))
            {
                log.ErrorFormat("{0} is not exists.", fileName);
                return null;
            }

            List<Device.GazeData> records = new List<Device.GazeData>();
            try
            {
                using (StreamReader sr = new StreamReader(fullName, Encoding.Default))
                {
                    while (sr.Peek() >= 0)
                    {
                        string line = sr.ReadLine();
                        string[] criteria = line.Split(',');
                        Device.GazeData record = new Device.GazeData()
                        {
                            //WorkOrderNo = criteria[0],
                            //LegacyPartialNo = decimal.Parse(criteria[1]),
                            Elapsed = int.Parse(criteria[0]),
                            AverageX = decimal.Parse(criteria[1]),
                            AverageY = decimal.Parse(criteria[2]),
                            LeftX = decimal.Parse(criteria[3]),
                            LeftY = decimal.Parse(criteria[4]),
                            RightX = decimal.Parse(criteria[5]),
                            RightY = decimal.Parse(criteria[6])
                        };
                        records.Add(record);
                    }
                }
            }
            catch (IOException ex)
            {
                log.ErrorFormat("{0} import failed. {1}", 
                    fileName, ex.Message);
                return null;
            }

            return records;
        }

        public static List<string[]> GetStringList(string filePath, char token)
        {
            List<string[]> stringContainer = new List<string[]>();
            string line = "";
            FileStream fs = null;

            try
            {
                fs = new FileStream(filePath, FileMode.Open, 
                    FileAccess.Read, FileShare.ReadWrite);

                using (StreamReader sr = new StreamReader(fs, 
                    System.Text.Encoding.Default, true))
                {
                    fs = null;
                    while (sr.Peek() >= 0)
                    {
                        line = sr.ReadLine();
                        string[] tokens = line.Split(token);
                        stringContainer.Add(tokens);
                    }
                }
            }
            catch (IOException ex)
            {
                log.ErrorFormat("{0} read failed. {1}", filePath, ex.Message);
            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
            }

            return stringContainer;
        }
        #endregion
    }
}
