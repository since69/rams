﻿using System;
using System.Collections.Generic;
using log4net;
using Thinkbox.Data.NHibernate;
using Thinkbox.Rams.Data.Model;
using Thinkbox.Rams.Data.Repositories;

namespace Thinkbox.Rams.ReadAlyzer.Helpers
{
    public class DataHelper
    {
        #region VARIABLES
        private static readonly ILog log = 
            LogManager.GetLogger(typeof(Program));

        private static IBusinessEntityRepository entityRepos = 
            new BusinessEntityRepository();
        private static IPersonRepository personRepos = 
            new PersonRepository();
        private static ITraineeRepository traineeRepos =
            new TraineeRepository();
        private static ITrainerRepository trainerRepos = 
            new TrainerRepository();
        private static ICultureRepository cultureRepos =
            new CultureRepository();
        private static ITestQuestionRepository questionRepos =
            new TestQuestionRepository();
        private static ITestTextRepository textRepos = 
            new TestTextRepository();
        private static ITestRepository testRepos = 
            new TestRepository();
        private static ITestAnswerRepository answerRepos =
            new TestAnswerRepository();
        #endregion

        #region PROPERTIES
        private static List<Data.Trainee> trainees = null;
        public static List<Data.Trainee> Trainees
        {
            get
            {
                if (trainees == null)
                    trainees = GetTrainees();
                return trainees;
            }
        }

        private static List<Data.Trainer> trainers = null;
        public static List<Data.Trainer> Trainers
        {
            get
            {
                if (trainers == null)
                    trainers = GetTrainers();
                return trainers;
            }
        }

        private static List<Data.Culture> cultures = null;
        public static List<Data.Culture> Cultures
        {
            get
            {
                if (cultures == null)
                    cultures = GetCultures();
                return cultures;
            }
        }

        private static List<Data.TestText> texts = null;
        public static List<Data.TestText> Texts
        {
            get
            {
                if (texts == null)
                    texts = GetTexts();
                return texts;
            }
        }

        private static List<Data.Test> tests = null;
        public static List<Data.Test> Tests
        {
            get
            {
                if (tests == null)
                    tests = GetTests();
                return tests;
            }
        }
        #endregion

        #region METHODS
        //
        // Trainee.
        //

        internal static void AddTrainee(Data.Trainee trainee)
        {
            Prepare();
            
            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                DateTime dt = DateTime.UtcNow;
                BusinessEntity newEntity = new BusinessEntity
                {
                    ModifiedDate = dt
                };
                Person newPerson = new Person()
                {
                    PersonType = trainee.Person.PersonType,
                    NameStyle = trainee.Person.NameStyle,
                    Title = trainee.Person.Title,
                    FirstName = trainee.Person.FirstName,
                    MiddleName = trainee.Person.MiddleName,
                    LastName = trainee.Person.LastName,
                    Suffix = trainee.Person.Suffix,
                    EmailPromotion = trainee.Person.EmailPromotion,
                    ModifiedDate = dt
                };
                Trainee newTrainee = new Trainee()
                {
                    Gender = trainee.Gender,
                    BirthDate = trainee.BirthDate,
                    ModifiedDate = dt
                };

                newEntity.Person = newPerson;
                newPerson.BusinessEntity = newEntity;
                newPerson.Trainee = newTrainee;
                newTrainee.Person = newPerson;

                UnitOfWork.CurrentSession.Save(newEntity);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                // Add to list.
                trainee.Id = newTrainee.Id;
                trainee.Person.Id = newPerson.Id;
                Trainees.Add(trainee);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static void UpdateTrainee(Data.Trainee trainee)
        {
            Prepare();

            int id = trainee.Id;
            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                Person p = personRepos.GetById(id);
                Trainee t = traineeRepos.GetById(id);

                p.NameStyle = trainee.Person.NameStyle;
                p.FirstName = trainee.Person.FirstName;
                p.LastName = trainee.Person.LastName;
                t.Gender = trainee.Gender;
                t.BirthDate = trainee.BirthDate;

                UnitOfWork.CurrentSession.Save(p);
                UnitOfWork.CurrentSession.Save(t);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static void DeleteTrainee(Data.Trainee trainee)
        {
            Prepare();

            int id = trainee.Id;
            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                BusinessEntity e = entityRepos.GetById(id);
                Person p = personRepos.GetById(id);
                Trainee t = traineeRepos.GetById(id);

                traineeRepos.Remove(t);
                personRepos.Remove(p);
                entityRepos.Remove(e);

                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                Trainees.Remove(trainee);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static Data.Trainee GetTrainee(int traineeId)
        {
            Prepare();

            Data.Trainee trainee = new Data.Trainee(traineeRepos.GetById(traineeId));
            return trainee;
        }

        internal static List<Data.Trainee> GetTrainees()
        {
            Prepare();

            List<Data.Trainee> trainees = new List<Data.Trainee>();
            foreach (Rams.Data.Model.Trainee r in traineeRepos.FindAll())
                trainees.Add(new Data.Trainee(r));

            return trainees;
        }


        //
        // Trainer.
        //

        internal static void AddTrainer(Data.Trainer trainer)
        {
            Prepare();

            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                DateTime dt = DateTime.UtcNow;
                BusinessEntity newEntity = new BusinessEntity
                {
                    ModifiedDate = dt
                };
                Person newPerson = new Person()
                {
                    PersonType = trainer.Person.PersonType,
                    NameStyle = trainer.Person.NameStyle,
                    Title = trainer.Person.Title,
                    FirstName = trainer.Person.FirstName,
                    MiddleName = trainer.Person.MiddleName,
                    LastName = trainer.Person.LastName,
                    Suffix = trainer.Person.Suffix,
                    EmailPromotion = trainer.Person.EmailPromotion,
                    ModifiedDate = dt
                };
                Trainer newTrainer = new Trainer()
                {
                    Gender = trainer.Gender,
                    ModifiedDate = dt
                };

                newEntity.Person = newPerson;
                newPerson.BusinessEntity = newEntity;
                newPerson.Trainer = newTrainer;
                newTrainer.Person = newPerson;

                UnitOfWork.CurrentSession.Save(newEntity);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                // Add to list.
                trainer.Id = newTrainer.Id;
                trainer.Person.Id = newPerson.Id;
                Trainers.Add(trainer);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static void UpdateTrainer(Data.Trainer trainer)
        {
            Prepare();

            int id = trainer.Id;
            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                Person p = personRepos.GetById(id);
                Trainer t = trainerRepos.GetById(id);

                p.NameStyle = trainer.Person.NameStyle;
                p.FirstName = trainer.Person.FirstName;
                p.LastName = trainer.Person.LastName;
                t.Gender = trainer.Gender;

                UnitOfWork.CurrentSession.Save(p);
                UnitOfWork.CurrentSession.Save(t);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static void DeleteTrainer(Data.Trainer trainer)
        {
            Prepare();

            int id = trainer.Id;
            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                BusinessEntity e = entityRepos.GetById(id);
                Person p = personRepos.GetById(id);
                Trainer t = trainerRepos.GetById(id);

                trainerRepos.Remove(t);
                personRepos.Remove(p);
                entityRepos.Remove(e);
                
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                Trainers.Remove(trainer);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static Data.Trainer GetTrainer(int trainerId)
        {
            Prepare();

            Data.Trainer trainer = new Data.Trainer(trainerRepos.GetById(trainerId));
            return trainer;
        }

        internal static List<Data.Trainer> GetTrainers()
        {
            Prepare();

            List<Data.Trainer> trainers = new List<Data.Trainer>();
            foreach (Rams.Data.Model.Trainer r in trainerRepos.FindAll())
                trainers.Add(new Data.Trainer(r));

            return trainers;
        }


        internal static List<Data.Culture> GetCultures()
        {
            Prepare();

            List<Data.Culture> cultures = new List<Data.Culture>();
            foreach (Rams.Data.Model.Culture r in cultureRepos.FindAll())
                cultures.Add(new Data.Culture(r));

            return cultures;
        }


        //
        // TestQuestion.
        //

        internal static List<Data.TestQuestion> GetTestQuestions(int testTextId)
        {
            Prepare();

            List<Data.TestQuestion> questions = new List<Data.TestQuestion>();
            foreach (Rams.Data.Model.TestQuestion r in questionRepos.FindTestQuestion(testTextId))
                questions.Add(new Data.TestQuestion(r));
            return questions;
        }


        //
        // TestText.
        //

        internal static void AddText(Data.TestText text)
        {
            Prepare();

            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                DateTime dt = DateTime.UtcNow;
                TestText newText = new TestText()
                {
                    Culture = new Culture()
                    {
                        Id = text.Culture.Id,
                        Name = text.Culture.Name,
                        ModifiedDate = text.Culture.ModifiedDate
                    },
                    Level = text.Level,
                    Title = text.Title,
                    Text = text.Text,
                    FontName = text.FontName,
                    FontSize = text.FontSize,
                    CharacterWidth = text.CharacterWidth,
                    CharacterHeight = text.CharacterHeight,
                    LineSpacing = text.LineSpacing,
                    ModifiedDate = dt
                };

                UnitOfWork.CurrentSession.Save(newText);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                // Add to list.
                text.Id = newText.Id;
                Texts.Add(text);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static void DeleteText(Data.TestText text)
        {
            Prepare();

            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                TestText t = textRepos.GetById(text.Id);

                textRepos.Remove(t);

                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                Texts.Remove(text);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static Data.TestText GetText(int textId)
        {
            Prepare();

            Data.TestText text = new Data.TestText(textRepos.GetById(textId));
            return text;
        }

        internal static List<Data.TestText> GetTexts()
        {
            Prepare();

            List<Data.TestText> texts = new List<Data.TestText>();
            foreach (Rams.Data.Model.TestText r in textRepos.FindAll())
                texts.Add(new Data.TestText(r));

            return texts;
        }


        //
        // Test
        //

        internal static void AddTest(Data.Test test)
        {
            Prepare();

            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                DateTime dt = DateTime.UtcNow;
                Trainee trainee = new Trainee()
                {
                    Id = test.Trainee.Id,
                    Gender = test.Trainee.Gender,
                    BirthDate = test.Trainee.BirthDate,
                    Person = new Person()
                    {
                        PersonType = test.Trainee.Person.PersonType,
                        NameStyle = test.Trainee.Person.NameStyle,
                        Title = test.Trainee.Person.Title,
                        FirstName = test.Trainee.Person.FirstName,
                        MiddleName = test.Trainee.Person.MiddleName,
                        LastName = test.Trainee.Person.LastName,
                        Suffix = test.Trainee.Person.Suffix,
                        EmailPromotion = test.Trainee.Person.EmailPromotion,
                        ModifiedDate = test.Trainee.Person.ModifiedDate
                    }
                };
                Trainer trainer = new Trainer()
                {
                    Id = test.Trainer.Id,
                    Gender = test.Trainer.Gender,
                    Person = new Person()
                    {
                        PersonType = test.Trainer.Person.PersonType,
                        NameStyle = test.Trainer.Person.NameStyle,
                        Title = test.Trainer.Person.Title,
                        FirstName = test.Trainer.Person.FirstName,
                        MiddleName = test.Trainer.Person.MiddleName,
                        LastName = test.Trainer.Person.LastName,
                        Suffix = test.Trainer.Person.Suffix,
                        EmailPromotion = test.Trainer.Person.EmailPromotion,
                        ModifiedDate = test.Trainer.Person.ModifiedDate
                    }
                };
                TestText testText = new TestText()
                {
                    Id = test.TestText.Id,
                    Culture = new Culture()
                    {
                        Id = test.TestText.Culture.Id,
                        Name = test.TestText.Culture.Name,
                        ModifiedDate = test.TestText.Culture.ModifiedDate
                    },
                    Level = test.TestText.Level,
                    Title = test.TestText.Title,
                    Text = test.TestText.Text,
                    ModifiedDate = test.TestText.ModifiedDate
                };

                Test newTest = new Test()
                {
                    Grade = test.Grade,
                    RecordFile = test.RecordFile,
                    Trainee = trainee,
                    Trainer = trainer,
                    TestText = testText,
                    StartTime = test.StartTime,
                    EndTime = test.EndTime,
                    ModifiedDate = dt,
                };

                UnitOfWork.CurrentSession.Save(newTest);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                // Add to list.
                test.Id = newTest.Id;
                Tests.Add(test);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static void DeleteTest(Data.Test test)
        {
            Prepare();

            int id = test.Id;
            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                Test t = testRepos.GetById(id);

                testRepos.Remove(t);

                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();

                Tests.Remove(test);
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static List<Data.Test> GetTests()
        {
            Prepare();

            List<Data.Test> tests = new List<Data.Test>();
            foreach (Test r in testRepos.FindAll())
                tests.Add(new Data.Test(r));

            return tests;
        }


        //
        // TestAnswer
        //

        public static void GetCorrectAnswers(int testId, 
            out int totalAnswers, out int correctAnswers)
        {
            List<Data.TestAnswer> answers = DataHelper.GetAnswers(testId);
            GetCorrectAnswers(answers, out totalAnswers, out correctAnswers);
        }

        public static void GetCorrectAnswers(List<Data.TestAnswer> answers,
            out int totalAnswers, out int correctAnswers)
        {
            correctAnswers = 0;
            totalAnswers = answers.Count;
            foreach (Data.TestAnswer answer in answers)
            {
                if (answer.Answer == answer.Question.Answer)
                    ++correctAnswers;
            }
        }

        internal static void AddAnswer(Data.Test test)
        {
            foreach (Data.TestAnswer answer in test.Answers)
                AddAnswer(test, answer);
        }

        internal static void AddAnswer(Data.Test test, Data.TestAnswer answer)
        {
            Prepare();

            var tr = UnitOfWork.Current.BeginTransaction();
            try
            {
                DateTime dt = DateTime.UtcNow;
                TestAnswer newAnswer = new TestAnswer()
                {
                    Answer = answer.Answer,
                    TestQuestion = new TestQuestion() { Id = answer.Question.Id },
                    Test = new Test() { Id = test.Id },
                    ModifiedDate = dt
                };

                UnitOfWork.CurrentSession.Save(newAnswer);
                UnitOfWork.CurrentSession.Flush();
                UnitOfWork.CurrentSession.Clear();

                tr.Commit();
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                tr.Rollback();
            }
        }

        internal static List<Data.TestAnswer> GetAnswers(int testId)
        {
            Prepare();

            List<Data.TestAnswer> answers = new List<Data.TestAnswer>();
            foreach (TestAnswer r in answerRepos.FindTestAnswers(testId))
                answers.Add(new Data.TestAnswer(r));
            return answers;
        }


        //
        // General
        //

        private static void Prepare()
        {
            if (!UnitOfWork.IsStarted)
            {
                UnitOfWork.Configuration.AddAssembly(typeof(BusinessEntity).Assembly);
                UnitOfWork.Start();
            }
        }
        #endregion
    }
}
