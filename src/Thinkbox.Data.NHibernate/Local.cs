﻿using System;
using System.Collections;
using System.Web;

namespace Thinkbox.Data.NHibernate
{
    public static class Local
    {
        static readonly ILocalData data = new LocalData();

        public static ILocalData Data
        {
            get { return data; }
        }

        #region INTERNAL CLASS
        private class LocalData : ILocalData
        {
            [ThreadStatic]
            private static Hashtable localData;
            private static readonly object LocalDataHashtableKey = new object();

            #region PROPERTIES
            private static Hashtable LocalHashtable
            {
                get
                {
                    if (!RunningInWeb)
                    {
                        if (localData == null)
                            localData = new Hashtable();
                        return localData;
                    }
                    else
                    {
                        var webHashtable = HttpContext.Current.Items[LocalDataHashtableKey] as Hashtable;
                        if (webHashtable == null)
                        {
                            webHashtable = new Hashtable();
                            HttpContext.Current.Items[LocalDataHashtableKey] = webHashtable;
                        }
                        return webHashtable;
                    }
                }
            }

            public object this[object key]
            {
                get { return LocalHashtable[key]; }
                set { LocalHashtable[key] = value; }
            }

            public int Count
            {
                get { return LocalHashtable.Count; }
            }

            public static bool RunningInWeb
            {
                get { return HttpContext.Current != null; }
            }
            #endregion

            #region METHODS
            public void Clear()
            {
                LocalHashtable.Clear();
            }
            #endregion
        }
        #endregion
    }
}
