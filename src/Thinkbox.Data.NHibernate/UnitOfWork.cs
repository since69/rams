﻿using System;
using NHibernate;
using NHibernate.Cfg;

namespace Thinkbox.Data.NHibernate
{
    public static class UnitOfWork
    {
        #region VARIABLES
        public const string CurrentUnitOfWorkKey = "CurrentUnitOfWork.Key";
        private static readonly IUnitOfWorkFactory unitOfWorkFactory = new UnitOfWorkFactory();
        #endregion

        #region PROPERTIES
        public static Configuration Configuration
        {
            get { return unitOfWorkFactory.Configuration; }
        }

        private static IUnitOfWork CurrentUnitOfWork
        {
            get { return Local.Data[CurrentUnitOfWorkKey] as IUnitOfWork; }
            set { Local.Data[CurrentUnitOfWorkKey] = value; }
        }

        public static IUnitOfWork Current
        {
            get
            {
                var unitOfWork = CurrentUnitOfWork;
                if (unitOfWork == null)
                {
                    throw new InvalidOperationException(
                        "You are not in a unit of work");
                }
                return unitOfWork;
            }
        }

        public static bool IsStarted
        {
            get { return CurrentUnitOfWork != null; }
        }

        public static ISession CurrentSession
        {
            get { return unitOfWorkFactory.CurrentSession; }
            internal set { unitOfWorkFactory.CurrentSession = value; }
        }
        #endregion

        #region CONSTRUCTORS
        public static void DisposeUnitOfWork(IUnitOfWorkImplementor unitOfWork)
        {
            CurrentUnitOfWork = null;
        }
        #endregion

        #region METHODS
        public static IUnitOfWork Start()
        {
            if (CurrentUnitOfWork != null)
            {
                throw new InvalidOperationException(
                    "You cannot start more than one unit of work at the same time.");
            }

            var unitOfWork = unitOfWorkFactory.Create();
            CurrentUnitOfWork = unitOfWork;
            return unitOfWork;
        }
        #endregion
    }
}
