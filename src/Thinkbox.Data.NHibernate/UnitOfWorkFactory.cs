﻿using System;
using System.IO;
using System.Xml;
using NHibernate;
using NHibernate.Cfg;

namespace Thinkbox.Data.NHibernate
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        #region VARIABLES
        private const string Default_HibernateConfig = "hibernate.cfg.xml";
        #endregion

        #region PROPERTIES
        private static ISession currentSession;
        public ISession CurrentSession
        {
            get
            {
                if (currentSession == null)
                {
                    throw new InvalidOperationException(
                        "You are not in a unit of work.");
                }
                    
                return currentSession;
            }
            set { currentSession = value; }
        }

        private ISessionFactory sessionFactory;
        public ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                    sessionFactory = Configuration.BuildSessionFactory();
                return sessionFactory;
            }
        }

        private Configuration configuration;
        public Configuration Configuration
        {
            get
            {
                if (configuration == null)
                {
                    configuration = new Configuration();
                    string hibernateConfig = Default_HibernateConfig;
                    //if not rooted, assume path from base directory
                    if (Path.IsPathRooted(hibernateConfig) == false)
                    {
                        hibernateConfig = Path.Combine(
                            AppDomain.CurrentDomain.BaseDirectory, 
                            hibernateConfig);
                    }
                        
                    if (File.Exists(hibernateConfig))
                    {
                        configuration.Configure(
                            new XmlTextReader(hibernateConfig));
                    }
                        
                }
                return configuration;
            }
        }
        #endregion

        #region CONSTRUCTOR
        internal UnitOfWorkFactory() { }

        public void DisposeUnitOfWork(UnitOfWorkImplementor adapter)
        {
            CurrentSession = null;
            UnitOfWork.DisposeUnitOfWork(adapter);
        }
        #endregion
        #region MEHTODS
        public IUnitOfWork Create()
        {
            ISession session = CreateSession();
            session.FlushMode = FlushMode.Commit;
            currentSession = session;
            return new UnitOfWorkImplementor(this, session);
        }

        private ISession CreateSession()
        {
            return SessionFactory.OpenSession();
        }
        #endregion

    }
}
