﻿namespace Thinkbox.Data.NHibernate
{
    public interface IUnitOfWorkImplementor : IUnitOfWork
    {
        void IncrementUsages();
    }
}
