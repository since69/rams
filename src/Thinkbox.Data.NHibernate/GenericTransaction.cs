﻿using NHibernate;

namespace Thinkbox.Data.NHibernate
{
    public class GenericTransaction : IGenericTransaction
    {
        private readonly ITransaction transaction;

        #region METHODS
        public GenericTransaction(ITransaction transaction)
        {
            this.transaction = transaction;
        }

        public void Commit()
        {
            transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            transaction.Dispose();
        }
        #endregion
    }
}
