﻿namespace Thinkbox.Data.NHibernate
{
    public interface IGenericTransaction : System.IDisposable
    {
        void Commit();
        void Rollback();
    }
}
