﻿using System;
using System.Data;
using NHibernate;

namespace Thinkbox.Data.NHibernate
{
    public class UnitOfWorkImplementor : IUnitOfWorkImplementor
    {
        #region PROPERTIES
        public bool IsInActiveTransaction
        {
            get
            {
                return session.Transaction.IsActive;
            }
        }

        private readonly IUnitOfWorkFactory factory;
        public IUnitOfWorkFactory Factory
        {
            get { return factory; }
        }

        private readonly ISession session;
        public ISession Session
        {
            get { return session; }
        }
        #endregion

        #region CONSTRUCTORS
        public UnitOfWorkImplementor(IUnitOfWorkFactory factory, ISession session)
        {
            this.factory = factory;
            this.session = session;
        }

        public void Dispose()
        {
            factory.DisposeUnitOfWork(this);
            session.Dispose();
        }
        #endregion

        #region METHODS
        public void IncrementUsages()
        {
            throw new NotImplementedException();
        }

        public void Flush()
        {
            session.Flush();
        }

        public IGenericTransaction BeginTransaction()
        {
            return new GenericTransaction(session.BeginTransaction());
        }

        public IGenericTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return new GenericTransaction(session.BeginTransaction(isolationLevel));
        }

        public void TransactionalFlush()
        {
            TransactionalFlush(IsolationLevel.ReadCommitted);
        }

        public void TransactionalFlush(IsolationLevel isolationLevel)
        {
            // $$$$$$$$$$$$$$$$ gns: take this, when making thread safe! $$$$$$$$$$$$$$
            //IUoWTransaction tx = UnitOfWork.Current.BeginTransaction(isolationLevel);   

            IGenericTransaction tx = BeginTransaction(isolationLevel);
            try
            {
                //forces a flush of the current unit of work
                tx.Commit();
            }
            catch
            {
                tx.Rollback();
                throw;
            }
            finally
            {
                tx.Dispose();
            }
        }
        #endregion
    }
}
