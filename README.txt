﻿Reading Ability Measurement System (RAMS)

The Readalyzer is an objective assessment tool used to measure a patient’s 
reading comprehension and eye movements. 
The patient puts on goggles that act as a recording device.  
This device records eye movements, number of fixations and regressions, 
reading rate, and the duration of fixations through infrared signals. 
The patient will be asked to read a short story silently, followed by 10 yes 
or no comprehension questions to ensure the patient is reading for content and meaning.  
After recording all these variables, the Readalyzer will generate a report 
that measures your performance based on grade level equivalents. 
This is a complementary test we provide when you schedule a screener!
