#define BAUDRATE	9600
#define NUMOFLED	9

int ledPins[NUMOFLED] = { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
int curLed = 0;

void ledOn(int index)
{
	digitalWrite(ledPins[index], HIGH);
}

void ledOff(int index)
{
	digitalWrite(ledPins[index], LOW);	
}

void setup()
{
	Serial.begin(BAUDRATE);
	for (int i = 0; i < NUMOFLED; i++)
		pinMode(ledPins[i], OUTPUT);
}

void loop() 
{
	if (!Serial.available()) return;

	char input = Serial.read();
	if (!isDigit(input)) return;

	int index = input - '0';
	if (index > 9 || index < 0) return;

	if (curLed == 0)
	{
		curLed = index;
		ledOn(curLed - 1);
	}
	else if (curLed == index)
	{
		ledOff(curLed - 1);
		curLed = 0;
	}
	else /*if (curLed != index)*/
	{
		ledOff(curLed - 1);
		curLed = index;
		ledOn(curLed - 1);
	}
}